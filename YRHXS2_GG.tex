%% \documentclass[12pt]{article}
%% \usepackage{epsf,amsmath,amssymb,graphicx,axodraw,longtable}
%% \usepackage{lhchiggs,heppennames2,cernunits}
%% \begin{document}
%Note: Still have to transform references to Bibtex

\providecommand{\qTH}{q_{\mathrm{T,H}}}
\providecommand{\YH}{Y_{\mathrm{H}}}
\providecommand{\pTiso}{p_{\mathrm{T},\mathrm{iso}}}

\section{$\PGg\PGg$ decay mode\footnote{%
    S.~Gascon-Shotkin, M.~Kado (eds.); N.~Chanon, L.~Cieri, G.~Davies, 
    D.~D'Enterria, D.~de Florian, S.~Ganjour,
    J.~-Ph.~Guillet, C.~-M.~Kuo, N.~Lorenzo,E.~Pilon and J.~Schaarschmidt.}}
%Can go as high as subsubsubsection

\subsection{Introduction}
Despite its relatively low branching fraction and considerable reducible and irreducible backgrounds from
SM QCD processes, the $\PGg\PGg$ decay mode benefits from a clean signature, provided that a 
sufficiently high-resolution electromagnetic calorimeter is used. The mode $\PH \rightarrow\PGg\PGg$ is generally
considered to be the principal discovery channel at the LHC for a Higgs boson having a mass between 
$100\UGeV$ and $150\UGeV$. 
Furthermore, given the recent combined results from the ATLAS and CMS collaborations from analyses 
corresponding to up to $2.3\Ufb^{-1}$ of integrated luminosity
%~\cite{HCP2011Combination},
excluding with $95\%$ confidence a Standard Model Higgs boson in the range $141\UGeV<\MH<476\UGeV$, the 
$\PGg\PGg$ decay mode is the channel whose sensitive range remains at this writing the least constrained.  
The corresponding individual ATLAS~\cite{Collaboration:2011ww,ATLAS-CONF-2011-149} and CMS~\cite{CMS-PAS-HIG-11-021} results have also been made public.


\subsection{Sets of acceptance criteria used}

Three sets of acceptance criteria, shown in \refT{tab:HggAccCriteria}, have been used for the studies presented in 
this section. Two of them ('ATLAS', 'CMS') are based on acceptance
criteria of the ATLAS and CMS
$\PH\rightarrow \PGg\PGg$ searches; the third ('Loose') corresponds roughly to
typical acceptance criteria used in the measurement of Standard Model prompt photon
processes which constitute backgrounds to these searches.
\begin{table}[h]\small
\renewcommand{\arraystretch}{1.2}
\setlength{\arraycolsep}{1.5ex}
\caption{The three sets of acceptance criteria used for studies in this section. }
\label{tab:HggAccCriteria}
\centerline{
$\begin{array}{cccc}
\hline
            & \text{'CMS'} & \text{'ATLAS'} & \text{'Loose'} \\
\hline
E_{\mathrm{T}\PGg 1} \mathrm{[GeV]} & >40  & >40 & >20 \mbox{ or } 23 \\
E_{\mathrm{T}\PGg 2} \mathrm{[GeV]} & >30  & >25 & >20 \\
|\eta_{\PGg}| & <2.5 & <2.37 & <2.5 \\
\mbox{Excluded }|\eta_{\PGg}| & [1.4442,1.566] & [1.37,1.52] &   \\
M_{\PGg\PGg} \mathrm{[GeV]} & [100,160] & [100,160] & >80 \\
\hline
\end{array}$ }
\end{table}

For the studies concerning background processes in the Standard Model, parton-level
isolation requirements have been imposed, requiring a maximum transverse hadronic 
energy of $5\UGeV$ within a solid cone of radius $\Delta R=\sqrt{{\Delta\eta}^2+ {\Delta\phi}^2}=0.4$ or
$0.3$ as noted.


\subsection{Signal modelling and differential $K$-factors}

%\subsubsection{Differential $K$-factors for gluon-fusion signal}

\subsubsection{Reweighting of the $\pT$ spectrum for the gluon-fusion production process}

%Wait for input from Ming and Serguei
%use for example MingSerguei_comparison_costheta.pdf
%lxplus435
%Ming to redo with reweighting before applying acceptance cuts

The effect of reweighting the Higgs-boson $\pT$ spectrum given by the NLO program \POWHEG{} (with parton-shower
simulation, hadronisation, and underlying event from \PYTHIA{}~6.4) to that given by the 
inclusive NNLL program {\sc HqT}~\cite{deFlorian:2011xf} has been evaluated for the $\PGg\PGg$  
final state. Figure~\ref{HqtKfactors} shows the distribution of relative {\sc HqT}/\POWHEG{} event weights
as a function of Higgs-boson $\pT$ for $\MH=120\UGeV$; the distribution is fit with a 4th-degree 
polynomial function for $\pT<\MH$ and a constant function for $\pT>\MH$.  The fitted functions for   
four Higgs-boson masses relevant to the $\PH\rightarrow \PGg\PGg$ search are also shown; there is a slight
to moderate dependence on the Higgs-boson mass.    
\begin{figure}
\includegraphics[width=0.49\textwidth]{YRHXS2_GG/SergueiMingHqt_Powheg_ratio_enFonctiondePTHm120.eps}
\includegraphics[width=0.49\textwidth]{YRHXS2_GG/SergueiMingHqt_powheg_weight_functionsFourMasses.eps}
\caption{(Left) Distribution of relative {\sc HqT}/\POWHEG{} event weights
as a function of Higgs-boson $\pT$ for $\MH=120\UGeV$ with fit superposed. (Right) Fitted event weight
functions for $\MH=110,120,130,140\UGeV$ as a function of Higgs-boson  $\pT$. }
\label{HqtKfactors}
\end{figure}

It is important to evaluate the impact of the $\pT$-reweighting on other observables susceptible to be 
used in the $\PH\rightarrow \PGg\PGg$ search. Figure~\ref{HggPtReweightedKinematics} shows the distributions,
after application of the 'CMS' acceptance criteria, of the kinematical observables $\Delta\phi^{\PGg\PGg}$,
the difference in azimuthal angle between the two photons, 
$\eta^{\PGg\PGg}$, the pseudo-rapidity of the diphoton system, and $\cos\theta^*$, the cosine of the angle between
one of the photons and the beamline in the centre-of-mass frame of the Higgs boson, before and after the
{\sc HqT} reweighting, for $\MH=120\UGeV$.
\begin{figure}
\includegraphics[width=0.49\textwidth]{YRHXS2_GG/Ming_Serguei_comparison_dPhi_gg.eps}
\includegraphics[width=0.49\textwidth]{YRHXS2_GG/MingSerguei_comparison_eta_gg.eps}\\
\includegraphics[width=0.49\textwidth]{YRHXS2_GG/MingSerguei_comparison_costheta.eps}
\caption{Distributions of kinematical observables potentially important for the $\PH\rightarrow \PGg\PGg$ search, for 
$\MH=120\UGeV$, after application of 'CMS' acceptance criteria, before (red histogram) and after (blue dots)
reweighting of the Higgs-boson $\pT$ spectrum to that of {\sc HqT}. Top left:  $\Delta\phi^{\PGg\PGg}$; top right:
$\eta^{\PGg\PGg}$;  bottom: $\cos\theta^*$. }
\label{HggPtReweightedKinematics}
\end{figure}
The $\pT$-reweighting has a significant effect on both the $\Delta\phi^{\PGg\PGg}$ and
$\eta^{\PGg\PGg}$ distributions; the $\Delta\phi^{\PGg\PGg}$ distribution is shifted to higher values,
corresponding to the two photons being more back to back, and that of $\eta^{\PGg\PGg}$ is shifted away 
from small rapidity values towards the forward--backward zones. However, the  $\cos\theta^*$ distribution is
only slightly affected, with a small enhancement around  $\cos\theta^{*}=0.7$ and a corresponding small deficiency at 
the highest values. This has important implications for the treatment of the signal--background interference, which will
be discussed below. 
  
\subsubsection{Doubly-differential $K$-factors for the gluon-fusion production process}
\label{se:Kfac_ggHgaga}

To propagate higher-order effects to kinematical distributions produced by {\sc POWHEG}~\cite{Frixione:2007nw}, one can also perform a 2D reweighting with a 
$K$-factor $K(\qTH,\YH)$ where $\qTH$ is the transverse momentum of the Higgs boson and $\YH$ its 
rapidity \cite{Davatz:2006ut}. In the following we describe such a 2D reweighting procedure using 
{\sc HNNLO} \cite{Catani:2007vq} and {\sc POWHEG}~\cite{TheseNChanon}. This study should be repeated with {\sc HqT}
in place of {\sc HNNLO}.

\begin{sloppypar}
The $K$-factors $K(\qTH,\YH)$ are computed by applying the 'Loose' kinematical criteria with $E_{\mathrm{T},\PGg 1}>20\UGeV$ 
and $E_{\mathrm{T}, \PGg 2}>20\UGeV$.
An isolation criterion $\sum E_{\mathrm{T}}<5\UGeV$ in a cone
$\Delta R<0.3$ around the photons is applied at parton level in {\sc HNNLO}, while $\sum E_{\mathrm{T}}<7\UGeV$ is used at 
generator level in {\sc POWHEG}. The $K$-factors have been computed by bins of $4\UGeV$ in $\qTH$ and $0.25$ in $\YH$. 
Since the lowest $\qTH$ bins give a divergent cross section at fixed order, they have been merged in 
order to yield a constant $K$-factor in the range $0<\qTH<20\UGeV$ (the reweighted {\sc POWHEG} spectrum profits 
therefore from a leading-log shape resulting from the {\sc PYTHIA} parton shower in this range). Contiguous bins
in the ($\qTH,\YH$) plane are then merged together to smooth out statistical fluctuations 
(they could also be fitted with smooth functions). The $K$-factors thus obtained by this procedure for the 
Higgs-boson 
masses $\MH=110,120,130,140 \UGeV$ are given in \refA{app:2dKfactorsSignalAndBackground} (\refT{tab2dHNNLO}).
%in \refF{KfactorHgg2Dapplied}. 
The differential cross-section distributions for {\sc HNNLO}, {\sc POWHEG}, and  {\sc POWHEG} after the application
of the $K$-factors are shown in \refF{KfactorHgg2Dapplied}. 
The need for such $K$-factors in the low-$\qTH$, central $\YH$ region is noticeable.
\end{sloppypar}

%\begin{figure}
%  \begin{center}
%    \includegraphics[height=8cm]{YRHXS2_GG/Kfactor2D_HNNLO_POWHEG_H120.eps}
%    \caption{$K$-factor $K(\qTH,\YH)$ for $gg\rightarrow\PGg\PGg$ computed at NNLO with {\sc HNNLO} with 
%respect to {\sc POWHEG} for $\MH=120\UGeV$.}
%    \label{KfactorHgg2D}
%  \end{center}
%\end{figure}




As expected, the use of the 2D $K$-factors is found to accurately reproduce the transverse momentum and the rapidity of the 
Higgs boson (see \refF{KfactorHgg2Dapplied}) to within $5\%$. It also accurately reproduces angular variables such as 
$\cos\theta^{*}$ to the same level of precision.

\begin{figure}
    \includegraphics[height=5.3cm]{YRHXS2_GG/KfactorApplied_HNNLO_POWHEG_H120_qt.eps}
    \includegraphics[height=5.3cm]{YRHXS2_GG/KfactorApplied_HNNLO_POWHEG_H120_ygg.eps}\\
    \includegraphics[height=5.3cm]{YRHXS2_GG/KfactorApplied_HNNLO_POWHEG_H120_costhetastar.eps}
    \caption{Differential cross sections of the $\Pg\Pg\rightarrow \PH\rightarrow \PGg\PGg$ process for $\MH=120\UGeV$: 
Higgs-boson transverse momentum (top left), Higgs-boson rapidity (top right), and $\cos\theta^{*}$ (bottom) for HNNLO, 
{\sc POWHEG}, and {\sc POWHEG} reweighted with $K(\qTH,\YH)$.}
\label{KfactorHgg2Dapplied}
\end{figure}


\subsubsection{Gluon-fusion signal and background interference}

The $\PGg\PGg$ decay channel is affected by destructive interference between the 
Higgs-boson gluon-fusion production process and the Standard Model continuum $\Pg\Pg\rightarrow\PGg\PGg$
'box' process, which constitutes an irreducible background.  This interference has been calculated at the two-loop
level by Dixon and Siu~\cite{Dixon:2003yb}, and the interference factor $\delta$ can be obtained as a
function of the angle $\theta^*$, where $\theta^*$ is the angle between
one of the photons and the beamline in the centre-of-mass frame of the Higgs boson, the distribution
of which is subject to experimental acceptance criteria, and $y$ is the photon rapidity:  
\begin{equation}
\theta^* = \arccos\Bigl(\tanh\frac{y(\PGg_1)-y(\PGg_2)}{2}\Bigr).
\label{eq:costhetastar}
\end{equation}

Average values of $\delta$ are shown in \refT{tab:HggInterference}, where the values of 
$\theta^*$ have been obtained subject to the 'ATLAS' acceptance criteria defined at the beginning
of this section. 
\begin{table}\small
\renewcommand{\arraystretch}{1.2}
\setlength{\arraycolsep}{1.2ex}
\caption{Average values of the destructive interference factor $\delta$ as a function
of Higgs-boson mass, for the 'ATLAS' acceptance criteria.}
\label{tab:HggInterference}
$\begin{array}{cccccccccccc}
\hline
 \text{$\MH$[GeV]}  & 100 & 105 & 110 & 115 & 120 &125 & 130 & 135 & 140 & 145 & 150 \\
\hline
\delta [\%] & -3.16  & -2.83 & -2.59 & -2.42 & -2.31 & -2.28 & -2.36 &-2.54 &-2.87 & -3.40 & -4.33 \\
\hline 
\end{array}$
\end{table}
These are at the level of a few per cent, reaching a minimum of $2.28\%$ for 
$\MH=125\UGeV$ and increasing to $3.16\%$ and $4.33\%$, respectively, for $\MH=100\UGeV$ and
$\MH=150\UGeV$.  However, for very low values of $\theta^*$, $\delta$ can reach far higher
values, as much as $15\%$ or more. \refF{HggInterferenceDelta} shows, for the 'ATLAS' acceptance 
criteria and $\MH=120\UGeV$,
the distribution of $\delta$ values and $\delta$ as a function of $\theta^*$.
 \begin{figure}
\centerline{
    \includegraphics[height=6cm]{YRHXS2_GG/Lorenzodist_Rinterf_m120_logx.eps}
    \includegraphics[height=6cm]{YRHXS2_GG/Lorenzodelta_vs_thetaStar.eps}
}
    \caption{Absolute value of the destructive interference factor $\delta$ (left) and its value as
a function of $\theta^*$, for $\MH=120\UGeV$ using the 'ATLAS' acceptance criteria.}
\label{HggInterferenceDelta}
\end{figure}

At the beginning of this section it was shown that the $\cos\theta^*$ distribution was relatively insensitive
to the $\pT$-reweighting. This means that it probably does not matter which (the $\pT$-reweighting or
the interference correction) is performed first.
It has been suggested~\cite{LanceDixon} that the two steps should be 
performed in both orders, and the difference, for example in the two distributions of the cumulative event weights,
should be taken as a theoretical systematic uncertainty.

In addition, the calculation performed in \Bref{Dixon:2003yb} takes only virtual QCD corrections into account,
and the scattering angle used is that of the beam axis.  To check the stability of the result for those cases 
where the  diphoton system sizeably differs from the framework used for the calculation, the interference term 
is recomputed for  signal events with a transverse momentum of the Higgs boson in excess of $20\UGeV$. The overall 
variation of the  term is of the order of $10\%$. This systematic uncertainty on the $O(3\%)$ correction to 
the $\sigma  \times{}$BR can be neglected when considering the QCD scale, PDF, and $\alpha_s$ systematic 
uncertainties on the  overall signal normalisation.

%Include sources of theoretical uncertainty and how to evaluate them.
% Need input from Ming and Sergey

\subsection{Background extraction and modelling}

%\subsubsection{Special considerations in background-determination methods}
%Caveat on how to take into account 'false peak' from same background model used by ATLAS and CMS.
%How to estimate possible biases from the background fit model:  Use our best knowledge of the background
%(parton-level theoretical predictions and Monte Carlo simulation); Estimate the possible bias; account for
%the bias as a spurious signal term in the overall fit model.
%Input from Marumi

\subsubsection{Background modelling biases and systematic uncertainties}

The search for the Higgs boson in the diphoton channel relies, for both 
ATLAS~\cite{Collaboration:2011ww,ATLAS-CONF-2011-149} and 
CMS~\cite{CMS-PAS-HIG-11-021}, on analytic models of the background shape. The scope of these models is to have a reasonable 
fit of the background diphoton invariant-mass distribution in the data to allow an accurate estimate of the 
background in the signal region from the side bands. Various models were investigated, a single or double 
exponential parametrisation or polynomial shapes. These models and in particular the simple exponential 
function are of course not adequate to model the background over any diphoton invariant-mass range, but are a 
good approximation in small ranges. Assessing the ability of the functional form to model correctly the 
background over a given mass range should be determined from large Monte Carlo simulation samples of 
irreducible and reducible backgrounds and checked in the data.

To quantify the potential bias of a given functional form the difference between the fitted function to large 
Monte Carlo samples and its shape in a narrow mass window can be used, or a signal-plus-background model can 
be fitted, and the average number of signal events fitted will give an estimate of the bias. Preliminary 
studies of this type using the {\sc Diphox}~\cite{Binoth:1999qq} and {\sc RESBOS}~\cite{Balazs:1997hv,Balazs:1999yf, Balazs:2006cc,Balazs:2007hr, Nadolsky:2007ba } simulations have been carried 
out~\cite{Collaboration:2011ww,ATLAS-CONF-2011-149} showing that a simple exponential function or a second 
order polynomial when fitted over 
a mass range of $100\UGeV$ to $160\UGeV$ can introduce sizable biases, of the order of $10{-}20\%$ of a Standard Model 
signal. Such bias can be reduced either by using a higher-order polynomial as was done by CMS~\cite{CMS-PAS-HIG-11-021} or 
reducing the fitting mass range. In both cases the cost of reducing a potential bias is a reduction in the 
statistical precision of the determination of the background. The choice of optimal functional form,
the constraints on its parameters, and the fitting range can be studied using the available Monte Carlo programs.
Checks can also be made using the data, but it should be noted that their result can be biased both by 
statistical fluctuations in the search region and the potential presence of a signal.  Such checks were 
carried out in ATLAS~\cite{Collaboration:2011ww,ATLAS-CONF-2011-149} using a double exponential model fitted to the data to generate an ensemble 
of pseudo-experiments which were then fitted using a simple exponential. This check showed a fair agreement 
between the possible bias measured in the Monte Carlo and that measured from the two parametrisations.

In ATLAS~\cite{Collaboration:2011ww,ATLAS-CONF-2011-149}, to account for a potential bias in the statistical treatment of the results of this 
channel, a fraction of the signal fitted is allowed to be assigned to a bias in the background modeling. 
%This 
%spurious signal term $\delta_{Spurious}$ is treated as a nuisance parameter and enters the normalisation of 
%the signal ($n_s$) in the following way:
%
%$$n_s= \mu \varepsilon \sigma Br \mathcal{L} +\delta_{Spurious}$$
%
%The spurious signal term is not a factor of the signal strength and is therefore fully part of the background model.

Detailed Monte Carlo based studies are necessary to accurately estimate the potential bias arising from a given 
choice of parametrisation of the background in the diphoton channel. Although such bias can be accounted for in 
the statistical treatment of the analysis results, it is preferable to keep it small relative to the expected number 
of signal events. Checks on the data are also important to further confirm the choice of background model. Depending 
on the kinematic requirements chosen in ATLAS and CMS, the background modeling systematic uncertainty could be 
correlated between experiments. 


\subsubsection{Status of background calculations}

%Could become 2 subsections
The irreducible background to the $\PH\rightarrow \PGg\PGg$ search is composed of prompt photon
pairs from the quark--antiquark annihilation, gluon-fusion, and gluon--(anti)quark scattering processes.  
One or both photons come either directly from the hard process or from parton fragmentation, in which 
a cascade of successive collinear splittings yields a radiated photon.  The contributions from the 
so-called 'direct' components have been calculated at NLO and implemented in the programs
{\sc Diphox}~\cite{Binoth:1999qq}, {\sc gamma2MC}~\cite{Bern:2002jx}, and {\sc MCFM}~\cite{MCFMweb}. In addition, 
{\sc Diphox} contains a complete implementation
at NLO of single- and double-fragmentation contributions. The calculation implemented in the 
{\sc RESBOS}~\cite{Balazs:1997hv,Balazs:1999yf, Balazs:2006cc,Balazs:2007hr, Nadolsky:2007ba }
program has NNLL resummation accuracy and an effective treatment of LO single fragmentation. 

The direct contribution from the gluon-fusion channel, known as the 'box' contribution, is technically at
NNLO at lowest order, and has been calculated~\cite{Dicus:1987fk}, in addition to many of the higher-order corrections at 
N$^3$LO, and implemented in {\sc gamma2MC} and  {\sc RESBOS}.  These corrections are 
quantitatively of equal importance as the direct contributions, due to the significant gluon luminosity 
at the LHC.

%SHERPA and NLOjet++

Experimental measurements of differential prompt diphoton pair cross sections at both the 
Tevatron~\cite{Aaltonen:2011vk,Abazov:2010ah } and at
the LHC~\cite{Aad:2011mh, Chatrchyan:2011qt} have exhibited largely satisfactory agreement with the ensemble of theoretical
predictions. The exception has been in the so-called 'collinear' regime, corresponding to low values of 
$\Delta\phi^{\PGg\PGg}$ and $M^{\PGg\PGg}$, high values of $\cos\theta^{*}$, and the characteristic 
'shoulder' in the $\pT$ spectrum of the diphoton system. These disagreements were thought to be due to the absence 
of NNLO contributions in the theoretical predictions from either the direct or fragmentation components.
 
Recently, a fully-differential calculation of the direct components at NNLO using the $q_{\mathrm{T}}$ subtraction 
formalism~\cite{Catani:2007vq} has been performed and implemented in the parton-level program 
{\sc 2gammaNNLO}~\cite{Catani:2011qz}. 
Contributions from fragmentation are not included and are formally eliminated by the application of the 
so-called smooth cone isolation criterion due to Frixione et al.~\cite{Frixione:1998jh}:  For a cone of 
radius $r=\sqrt{{\Delta\eta}^2+{\Delta\phi}^2}< R$ around a photon with transverse momentum $\pT^{\PGg}$,
the total amount of partonic transverse energy $E_{\mathrm{T}}$ must be less than  $E^{\mathrm{max}}_{\mathrm{T}}(r)$, where 
\begin{equation}
E^{\mathrm{max}}_{\mathrm{T}}(r)=\epsilon_{\PGg}\pT^{\PGg}\Bigl(\frac{1-\cos r}{1-\cos R}\Bigr)^n.
\label{eq:Frixione}
\end{equation}

In the above definition $\epsilon_{\PGg}$ and $n$ are parameters, with $0<\epsilon_{\PGg}< 1$. 
Figure~\ref{2gammaNNLOdsigmadphi} shows the differential cross section 
$d\sigma/d\Delta\phi_{\PGg\PGg}$ as predicted by {\sc 2gammaNNLO} at both NNLO and NLO (but excluding
the higher-order 'box' diagram corrections), compared
to a recent measurement by the CMS collaboration~\cite{Chatrchyan:2011qt} at $\sqrt{s}=7\UTeV$, 
with acceptance criteria
closely following the 'Loose' selection defined at the beginning of this section. There is
satisfactory agreement between this preliminary theoretical prediction and the CMS data, indicating
that the addition of the direct NNLO contributions alone may correct much of the disagreement, modulo
the fact that the CMS analysis used simple hollow cone isolation requirements and not the Frixione criterion.
\begin{figure}
\centerline{
\includegraphics[width=0.49\textwidth]{YRHXS2_GG/azim.eps}
}
\caption{Differential diphoton cross section as a function of $\Delta\phi_{\PGg\PGg}$ 
at NNLO (blue) and at NLO (dotted black) calculated with a preliminary result from 
the {\sc 2gammaNNLO} program, superimposed
on results from CMS data (points) from 2010~\cite{Chatrchyan:2011qt}.}
\label{2gammaNNLOdsigmadphi}
\end{figure}

Figure~\ref{2gammaNNLOdsigmadphiHiggs} shows the same differential distribution $d\sigma/d\Delta\phi_{\PGg\PGg}$ 
predicted by {\sc 2gammaNNLO} at both NNLO and NLO, this time for the 'ATLAS' and 'CMS' acceptance criteria defined 
at the beginning of this section.
\begin{figure}
\includegraphics[width=0.49\textwidth]{YRHXS2_GG/azimatlas.eps}
\includegraphics[width=0.49\textwidth]{YRHXS2_GG/azimcms.eps}
\caption{Differential diphoton cross section as a function of $\Delta\phi_{\PGg\PGg}$ 
at NNLO (blue) and at NLO (dotted black) calculated with a preliminary result from 
the {\sc 2gammaNNLO} program, for the 'ATLAS' (left) and 'CMS' (right) acceptance criteria.}
%superimposed
%on results from CMS data (points) from $2010$~\cite{Chatrchyan:2011qt}.}
\label{2gammaNNLOdsigmadphiHiggs}
\end{figure}








%\subsection{Review of background-determination methods}

\subsubsection{Doubly-differential $K$-factors}

Although the ATLAS and CMS $\PH\rightarrow \PGg\PGg$ analyses estimate the background directly from data, it 
is nevertheless useful to benefit from the best possible background estimate from Monte Carlo simulations. 
Furthermore, this is 
needed for meaningful data/Monte Carlo comparisons as well as to train 
classifiers in 
multivariate analyses. For these purposes, we propose in this section 
a differential reweighting of 
parton-shower events to 
NLO calculations. This has been achieved by using parton-shower events obtained with $\PGg\PGg$+jets samples generated with
{\sc Madgraph} \cite{Madgraph} (which contains the Born diagram and up to two supplementary hard jets) 
hadronised with {\sc PYTHIA}~\cite{PYTHIA}, and lowest-order box events generated with {\sc PYTHIA}.
Events have been reweighted to NLO with 
{\sc Diphox}~\cite{Binoth:1999qq} 
(NLO Born and single- and double-fragmentation contributions) and {\sc Gamma2MC}~\cite{Bern:2002jx} 
(NLO box contributions). It should
be noted that the {\sc Madgraph} $\PGg\PGg$+jets process includes the fragmentation contribution at the matrix-element level
as a bremsstrahlung contribution, while {\sc Diphox} includes the full treatment of the fragmentation function at NLO. This 
study should be repeated with {\sc 2gammaNNLO}.



In order to reproduce most of the kinematic features of the NLO processes, it has been found that it is sufficient to 
perform a 2D reweighting with a $K$-factor $K(q_{\mathrm{T},\PGg\PGg}$, $M_{\PGg\PGg}$), where 
$q_{\mathrm{T},\PGg\PGg}$ is the 
transverse momentum of the diphoton system and $M_{\PGg\PGg}$ its invariant mass \cite{TheseNChanon}. The $K$-factors 
$K(q_{\mathrm{T},\PGg\PGg},M_{\PGg\PGg})$ are computed by applying the 'Loose' kinematical cuts with 
$E_{\mathrm{T},\PGg 1}>20\UGeV$ and 
$E_{\mathrm{T}, \PGg 2}>20\UGeV$.
An isolation criterion $\sum E_{\mathrm{T}}<5\UGeV$ in
a cone $\Delta R<0.3$ 
around the photons is applied at parton level and  $\sum E_{\mathrm{T}}<7\UGeV$ at generator level. The $K$-factors
have been 
computed for bins of $4\UGeV$ in $q_{\mathrm{T},\PGg\PGg}$ and $5\UGeV$ in  $M_{\PGg\PGg}$. Contiguous bins in the
($q_{\mathrm{T},\PGg\PGg}$, $M_{\PGg\PGg}$) plane are then merged together to smooth out statistical fluctuations 
(they could be alternatively fitted with smooth functions). The $K$-factors obtained by this procedure are shown in
%\refF{KfactorPromptPrompt2D}.
\refA{app:2dKfactorsSignalAndBackground} (\refT{tab2dPrompt}).
The differential cross-section distributions for a combination of {\sc Diphox} and {\sc Gamma2MC}, and a combination
of {\sc Madgraph} and {\sc PYTHIA} after the application of the $K$-factors are shown in \refF{KfactorPromptPrompt2Dapplied}.
It is interesting to note that the supplementary hard jets in the
{\sc Madgraph} $\PGg\PGg$+jets samples allows
the population of the high-$q_{\mathrm{T},\PGg\PGg}$ and high-$M_{\PGg\PGg}$ regions,
which would have been forbidden by the LO kinematics of the {\sc PYTHIA} Born samples
had they been used.



%\begin{figure}
%  \begin{center}
%    \includegraphics[height=8cm]{YRHXS2_GG/Kfactor2D_DiphoxGamma2MC_MadgraphBornPythiaBox_LHCHiggsStyle.eps}
%    \caption{$K$-factor $K(q_{\mathrm{T},\PGg\PGg}, M_{\PGg\PGg})$ for $\PGg\PGg$+X computed at NLO with 
%{\sc Diphox}+{\sc Gamma2MC} with respect to {\sc Madgraph} $\PGg\PGg$+jets plus \PYTHIA Box.}
%    \label{KfactorPromptPrompt2D}
%  \end{center}
%\end{figure}





As expected, the 2D $K$-factors are found to
accurately reproduce the transverse momentum and the invariant-mass spectra of the
diphoton system (see \refF{KfactorPromptPrompt2Dapplied}) in the region where the $\PH\rightarrow \PGg\PGg$ 
searches are performed ($M_{\PGg\PGg}>100\UGeV$).
They also accurately reproduce angular variables such as $\cos\theta^{*}$.
\begin{figure}
    \includegraphics[height=5.3cm]{YRHXS2_GG/KfactorApplied_PromptPromptNLO_qt.eps}
    \includegraphics[height=5.3cm]{YRHXS2_GG/KfactorApplied_PromptPromptNLO_mgg.eps}\\
    \includegraphics[height=5.3cm]{YRHXS2_GG/KfactorApplied_PromptPromptNLO_costhetastar.eps}
 \caption{Differential cross sections for 
diphoton production from the sum of the quark--antiquark annihilation, gluon-fusion, and gluon--(anti)quark 
scattering processes:  Diphoton transverse momentum (top left), diphoton invariant mass (top right), and 
$\cos\theta^{*}$ (bottom) for a combination of {\sc Diphox} and {\sc Gamma2MC}, a combination of {\sc Madgraph} 
and {\sc PYTHIA}, and the latter combination reweighted with $K(M_{\PGg\PGg},\qTH)$.}
\label{KfactorPromptPrompt2Dapplied}
\end{figure}


%\subsubsection{Isolation considerations for background calculations}

%   Joint work with that being done for LH2011. Include relation between
%partonic and particle-level isolations.

%Comparing reconstructed isolation and generator-level or partonic-level isolation (in higher order codes) is a 
%non-trivial task. It requires to select a generator-level isolation criterion as close as possible to the 
%reconstructed level.  At generator level, the parton-shower Monte-Carlo events include the effect of the 
%parton-shower and the underlying-event (as well as the pile-up) whereas partonic level higher-order matrix 
%element codes do not. The choice of the generator level isolation criteria, to match as close as possible the 
%parton-level criterion, should therefore exclude the effect of the underlying event and pile-up.

%\subsubsubsection{Choice of the isolation criteria at generator level}

%Selecting parton-shower Monte-Carlo events without pile-up, and given a certain isolation criterion at generator-level 
%found to match the reconstructed level, one would like to find the corresponding isolation criterion once the 
%underlying-event is turned off. This can be achieved by considering that the isolation efficiency should be the same 
%with and without underlying-event, leading to tighten the requirement on the maximum allowed transverse momentum 
%$\pTiso$ inside the isolation cone, the loss of efficiency in isolation being due to the presence of additional 
%particles inside the cone coming from the underlying event.

%As an example we use \PYTHIA Born and Box samples, and define a cone of $\Delta R<0.3$ around the two photons. The 
%isolation efficiency as a function of $\pTiso$ is shown in \refF{IsoEffGenLevel}. It was found that allowing a 
%maximum transverse energy of $\pTiso < 7\UGeV$ in the isolation cone with underlying-event corresponds to 
%$\pTiso < 5\UGeV$ without underlying-event.\\

%\begin{figure}
%  \begin{center}
%    \includegraphics[height=6cm]{YRHXS2_GG/Pythia_IsoEfficiencyVsPtIso_GenLevelWithoutUEhad_wrtGenAllPart.eps}
%    \caption{Efficiency of isolation criterion for different maximum allowed transverse momentum $\pTiso$ inside 
%a cone $\Delta R<0.3$ (computed summing the transverse momentum of all generator level particles).}
%    \label{IsoEffGenLevel}
%  \end{center}
%\end{figure}

%\subsubsubsection{Choice of the isolation criteria at partonic level}

%The partonic-level criterion in a higher-order code equivalent to the generator-level criterion in a parton-shower 
%code (once the underlying event has been turned off) can not be easily deduced, because the hard jets from higher-order 
%contributions can not be directly compared with the parton-shower softer jets. For this reason, it is sufficient to 
%check that the isolation efficiency is stable against the $\pTiso$ chosen in the isolation criterion. We show in 
%\refF{IsoEffPartonic} the isolation efficiency in {\sc Diphox}+{\sc Gamma2MC} for different criteria. We found that the 
%criterion chosen in ATLAS ($\pTiso<4\UGeV$) and the one chosen in CMS ($\pTiso<5\UGeV$) give rise to negligible 
%differences in efficiency.

%\begin{figure}
%  \begin{center}
%    \includegraphics[height=6cm]{YRHXS2_GG/DIPHOXGamma2MC_IsoEffVsPt_LHCHiggs.eps}
%    \caption{Efficiency of isolation criterion for different $\Delta R$ cones around the photons and different 
%maximum allowed transverse momentum $\pTiso$ inside the cone (computed summing the transverse momentum of 
%all remaining partons).}
%    \label{IsoEffPartonic}
%  \end{center}
%\end{figure}



