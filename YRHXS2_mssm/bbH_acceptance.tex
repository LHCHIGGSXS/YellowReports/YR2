%svn co svn+ssh://svn.cern.ch/reps/lhchiggsxs/cernrep2
%cd cernrep2/trunk
% make
%
\subsubsection{Comparison of b-jet acceptance and kinematic distributions in $bb\rightarrow H$ production 
               between PYTHIA and high order calculations}

M.~Cutajar and A.~Nikitenko

At large values of tan$\beta$ and $m_{A}$ the $b\bar{b} \rightarrow \phi$ process dominates the production of the
MSSM neutral Higgs bosons~\cite{Djouadi:2005gj}. The most recent SUSY $\phi \rightarrow \tau \tau$ analysis in CMS~\cite{CMSPAS:HIG-11-020} uses 
a combination of two analyses: with at least one b-tagged jet and with zero b-tagged jets. For the event generation
of $pp \rightarrow b\bar{b}h$ process the PYTHIA Monte Carlo (process 186) is used. The comparison of PYTHIA with the 4FS calculations is shown 
in the earlier paper~\cite{Kinnunen:2004ji} for the $p_{T}$ distribution of b-jets where good agreement at the level of 10 \% is found.
In this note we compare acceptance efficiency for b-jets, $p_{T}$ and $\eta$ distributions for b-jets and Higgs bosons as given by 
PYTHIA with the high order calculations in the 5FS described in ~\cite{Harlander:2011fx} and Section XXX. The jets have been reconstructed from 
the final state partons using the anki-kT algorithm~\cite{Cacciari:2008gp} with parameter $R$=0.5. 
The jet is defined as a b-jet if at least one b quark is present in the list of the 
jet constituents. The kinematic acceptance cuts for b-jets used were the same as in the CMS analysis~\cite{CMSPAS:HIG-11-020}: 
$p_{T}>$ 20 GeV, $| \eta | < $ 2.4. Tables~\ref{tab:bbH_acceptance_tab140.tex} and~\ref{tab:bbH_acceptance_tab400.tex} show the fraction 
of b-jets within the acceptance obtained from the high order calculations (second column) and from PYTHIA generator (5-th column) 
for $m_{H}$=140 GeV and 400 GeV. The errors of the theoretical calculations due to the scale variation and PDF are shown in the third and fourth 
columns of Tables~\ref{tab:bbH_acceptance_tab140.tex} and~\ref{tab:bbH_acceptance_tab400.tex}. The total theoretical cross-section
for the Standard Model $b\bar{b} \rightarrow H$ process for $m_{H}$=140 GeV is 0.108.85 pb and for $m_{H}$=400 GeV it is 1.29 pb.
\begin{table}[ht!]
\centering
\caption{The fraction of b-jets within the acceptance obtained from the high order calculations in the 5FS (second column) and from PYTHIA 
         generator (5-th column) for $m_{H}$=140 GeV. The errors of the theoretical calculations due to the scale variation and PDF in \% are shown 
         in third and fourth columns}
\begin{tabular}{|c|c|c|c|c|} 
\hline 
final state $i$  &  $\sigma_{i}^{th}$/$\sigma _{tot}^{th}$ & scale error(\%) & PDF error (\%)    &  PYTHIA $pp \rightarrow b\bar{b}h$   \\ 
\hline 
\hline 
 0 b-jet     &            0.6381                           & -14.4~~+8.8      & -4.6~~+3.6       &         0.621 \\
\hline
 one b-jet   &            0.3286                           & -6.9~~+4.4       & -3.2~~+5.0       &         0.322 \\
\hline
 two b-jets  &            0.0417                           & -33.1~~+59.0     & -3.0~~+2.3       &         0.057 \\
\hline
\hline 
\end{tabular}
\label{tab:bbH_acceptance_tab140.tex}
\end{table}

\begin{table}[ht!]
\centering
\caption{The fraction of b-jets within the acceptance obtained from the high order calculations in the 5FS (second column) and from PYTHIA 
         generator (5-th column) for $m_{H}$=400 GeV. The errors of the theoretical calculations due to the scale variation and PDF are shown in 
          thirth and fourth columns}
\begin{tabular}{|c|c|c|c|c|} 
\hline 
final state $i$  & $\sigma_{i}^{th}$/$\sigma _{tot}^{th}$ & scale error (\%) & PDF error (\%)  &   PYTHIA $pp \rightarrow b\bar{b}h$   \\ 
\hline 
\hline 
 0 b-jet     &            0.519                           & -7.2~~+8.9       &   -6.7~~+7.6    &         0.511 \\
\hline
 one b-jet   &            0.426                           & -8.9~~+5.8       &   -6.4~~+6.4    &         0.387 \\
\hline
 two b-jets  &            0.063                           & -28.4~~+45.5     &   -4.5~~+4.4    &         0.102 \\
\hline
\hline 
\end{tabular}
\label{tab:bbH_acceptance_tab400.tex}
\end{table}
The good agreement between PYTHIA and the high order calculations in the 5FS is found for the fractions of zero b-jets and at least
one b-jet in the acceptance. The fraction of two b-jets predicted by the 5FS calculations is lower than given by PYTHIA, but agrees
within the theoretical errors. 

We compare the differential distributions $p_{T}$ and rapidity ($y$) of the leading $p_{T}$ b-jet within the acceptance
between PYTHIA and the NLO predictions. Figure~\ref{fig:bbHfig1} shows the distributions of $y^{b}$ normalised on unity for $m_{H}$=140 GeV (left)
and $m_{H}$=400 GeV (right). One can see the difference between PYTHIA and the NLO curves especially for the heavy Higgs boson. 
\begin{figure}
\begin{center}
\includegraphics[width=0.45\textwidth]{YRHXS2_mssm/yb140.eps} \hfill
\includegraphics[width=0.45\textwidth]{YRHXS2_mssm/yb400.eps}
\caption{The rapidity distributions of the leading $p_{T}$ b-jet normalised on unity for $m_{H}$=140 GeV (left) 
         and $m_{H}$=400 GeV (right) obtained with PYTHIA (solid lines) and with NLO calculations (dashed lines).}
\label{fig:bbHfig1}
\end{center}
\end{figure}
Figure~\ref{fig:bbHfig2} shows the distributions of $p_{T}^{b}$ normalised on unity for $m_{H}$=140 GeV (left)
and $m_{H}$=400 GeV (right). There is good agreement between PYTHIA and the NLO predictions.
\begin{figure}
\begin{center}
\includegraphics[width=0.45\textwidth]{YRHXS2_mssm/pTb140.eps} \hfill
\includegraphics[width=0.45\textwidth]{YRHXS2_mssm/pTb400.eps}
\caption{The $p_{T}$ distributions of the leading $p_{T}$ b-jet normalised on unity for $m_{H}$=140 GeV (left) and $m_{H}$=400 GeV 
         (right) obtained with PYTHIA (solid lines) and with NLO calculations (dashed lines).}
\label{fig:bbHfig2}
\end{center}
\end{figure}

We also compare the $p_{T}$ and rapidity distributions of the Higgs boson with at least one b-jet in the acceptance.
