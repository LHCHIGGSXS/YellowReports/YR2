\section{WW$^{*}$ decay mode\footnote{%
    S. Diglio, B.~Di Micco, R.~Di~Nardo, A.~Farilla, B.~Mellado and F.~Petrucci (eds.).}}
\subsection{Introduction}

For Higgs-boson masses $\MH\gsim 135\GeV$,
the Higgs boson decays  mainly into a $\PW\PW^{*}$ pair.
The W's decay mainly hadronically,
but this decay topology is experimentally difficult to exploit 
due to the high cross section of multi-jet processes in
$\Pp\Pp$ collisions.  If one of the W's decays to $\Pl\PGn$,
where $\Pl$ indicates an electron or muon, and the other to two quarks,
the background from $\Pp\Pp \to \PW+{}$jets with $\PW \to \Pl\PGn$,
becomes dominant. For high values of the Higgs mass, a strong cut on the
neutrino transverse momentum (measured as missing energy, $E_{\mathrm{T}}^{\rm miss}$, in the detector)
can be used to reject the $\PW+{}$jets
background.
For low values of the Higgs mass only the fully leptonic decay channel
of the W's ($\PW\PW \to \Pl\PGn \Pl\PGn$) can be used for Higgs search with high
sensitivity.

The main background in this channel is $\Pp\Pp \to \PQt \PAQt \to
\PW\PQb \PW \PQb \to \Pl\PGn \Pl\PGn \PQb \PQb$. This background is
characterised by the presence of high missing $E_{\mathrm{T}}$ from the two
neutrinos and high jet multiplicity from the $\PQb$ quarks.

In order to maximise the sensitivity, the analysis is performed in jet
categories, that means the whole  sample is divided in $0$, $1$, and $>1$ jets subsamples.
The use of jet categories is common to other channels like $\PH
\to \PGg \PGg$ and $\PH \to \PGt \PGt$. In the first two subsections  we will discuss the
concrete implementation of the jet categorisation in the $\PH \to \PW\PW $
analysis.
The follwoing sections discuss the treatment of the irreducible SM $\PW\PW$ background.


\subsection{Jet bin categorisation and uncertainties}

% The main Higgs  production process at LHC is gluon fusion $\Pg\Pg \to
% \PH$ whose inclusive cross section is known at NNLO with an accuracy of
% $20\%$, the scale uncertainty being $12\%$  and the PDF uncertainty  $8\%$. 
The main Higgs  production process at LHC is gluon fusion $\Pg\Pg \to
\PH$ whose inclusive cross section is known at NNLO QCD $+$ NLO EW $+$
higher-order improvements~\cite{Dittmaier:2011ti},
with uncertainties from residual QCD scale dependences and from PDF errors
of $6{-}10\%$  and $8{-}10\%$ for the LHC at $7\UTeV$ centre-of-mass energy.
The $\Pg\Pg \to \PH$  process is characterised by large contributions from extra gluon
 emission. The amount of extra jets produced in the process has been computed using the {\sc POWHEG} MC program
interfaced to {\sc PYTHIA} for the showering with the following results:  $44\%$ of the events have more than one
jet and $13\%$ have
 more than two jets. The jet algorithm used is the anti-$k_{\mathrm{T}}$ 
with a cone $\Delta R = 0.4$, and a cut on the transverse momentum
of the jet of $25\UGeV$ and a pseudorapidity cut of 4.5 are imposed. 
 The CTEQ6.6 NLO PDF set has been used for the computation.
Scale uncertainties on the exclusive jet cross sections result to be smaller than
the scale uncertainty on the inclusive cross sections. This points to
several studies, also reported in this report, showing that the
conventional scale uncertainty variation underestimate the exclusive
jet bin uncertainty. In order to compute more realistic uncertainties a procedure has been set up that 
furnishes more reasonable values. As described in the following, it consists in evaluating the scale
uncertainty on the inclusive multi-jet cross sections, $\sigma_{\ge 0}$, $\sigma_{\ge 1}$, and
$\sigma_{\ge 2}$,  and propagating them uncorrelated to
the exclusive jet bins. These uncertainties produce large correlations
among several channels. In the $\PW\PW$ case, three different channels are
analysed: $\Pep \Pem$, $\Pepm \PGmpm$, and
$\PGmp \PGmm$. The jet bin uncertainties have to be considered fully
correlated among them.

Analysis selection criteria for the several channels are different,
and the scale uncertainties on the inclusive jet cross section can be
different according to the several selections that are considered. 
In order to decouple the largest contribution to the jet counting from
the specific analysis criteria the signal yield in a given jet bin is
defined as follows,
\begin{equation}
N_{0j} = \sigma_{\ge 0}f_{0}A_{0}, \quad N_{1j} =
\sigma_{\ge 0}f_{1} A_{1}, \quad  
N_{2j} = \sigma_{\ge 0}f_{2}A_{2},
\end{equation}
where $f_{0}$, $f_{1}$ and $f_{2}$ are defined as
\begin{equation}
f_0 = \frac{\sigma_{\ge 0} - \sigma_{\ge 1}}{\sigma_{\ge 0}}, \quad f_1
= \frac{\sigma_{\ge 1}-\sigma_{\ge 2}}{\sigma_{\ge 0}}, \quad 
f_2 = \frac{\sigma_{\ge 2}}{\sigma_{\ge 0}}.
\end{equation}
The central values of $f_0$, $f_1$, and $f_2$ are computed using the
{\sc POWHEG} MC after the reweighting of the Higgs $\pT$  to {\sc HqT}~2.0 and the full reconstruction chain of
each experiment, while for the purpose of error propagation they are
evaluated using {\sc HNNLO}. In order to properly correlate the
uncertainties among several measurements and between the ATLAS and CMS
collaborations,
a common set of cuts has been defined in order to compute the
uncertainties on the $\sigma_{\ge n \mathrm{jets}}$ and the $f_{i}$ values used
in the error propagation. The cuts are shown in 
\refT{tab:cuts_agreed}.

\begin{table}
\caption{Common cuts for the $\PH \to \PW\PW^{*}$ analysis for
  the evaluation of jet bin uncertainties on the signal yield. The
  $p_{\rm Tl1}$,  $p_{\rm Tl2}$ are the cuts on the  lepton $p_{\rm
    T}$, $E_{\mathrm{T}}^{\rm miss}$ is the total $\pT$ of the neutrinos,
$m_{\Pl\Pl}$ is the invariant mass of the two leptons, and $p_{\rm T
  jet cut}$ is the cut imposed on the jet $\pT$ for the jet counting.} \label{tab:cuts_agreed}
\centerline{
\begin{tabular}{c}
\hline
$p_{\rm Tl1}$, $p_{\rm Tl2}$ > 20 \UGeV{} , $|\eta_1|$, $|\eta_2| < 2.5$\\
$E_{\mathrm{T}}^{\rm miss} > 30$ \UGeV{} \\
$m_{\Pl\Pl} > 12$ \UGeV{} \\
$p_{\rm T jet cut} < 30$ \UGeV{} with $|\eta| < 3$ and using anti-$k_{\mathrm{T}}$
algorithm with cone size $0.5$\\
\hline
\end{tabular}
}
\end{table}

The scale uncertainties on $\sigma_{\ge 0}$, $\sigma_{\ge 1}$, and
$\sigma_{\ge 2}$ have been computed using {\sc HNNLO} and are shown in 
\refT{tab:scale uncertainties}, the renormalisation and factorisation scales were varied by a factor two
up and down in order to get the error. The errors shown are the maximum
variation obtained in the Higgs mass range $130 < \MH < 300$ \UGeV{}.

\begin{table}
\caption{Inclusive jet uncertainties used for exclusive  jet bin uncertainties
  estimation.} \label{tab:scale uncertainties}
\centerline{  
\begin{tabular}{ccc}
\hline
$\Delta \sigma_{\ge 0}$ & $\Delta \sigma_{\ge 1}$ & $\Delta \sigma_{\ge 2}$ \\
\hline
$10 \%$ & $20 \%$ & $70 \%$ \\
\hline
\end{tabular}
}
\end{table}

The $A_{i}$ coefficients are the acceptances specific for each analysis, they 
are the acceptances due to  the further cuts applied in addition to the cuts 
shown in \refT{tab:cuts_agreed}. The scale uncertainty on $A_{i}$ has been evaluated
studying the fractional variation of events passing all analysis cuts
over the event passing the selection in \refT{tab:cuts_agreed}.
The effects observed are small and have been evaluated only for the
$\PGm\PGm$ channel and two Higgs mass points. 
The cuts used by the ATLAS collaboration for the 0-jet and 1-jet
analysis are shown in \refT{tab:acceptance_cuts}. The analysis is
divided in a low mass analysis, applied for $\MH < 200$ \UGeV{},  and an
high-mass analysis for $\MH > 200$ \UGeV{}. The two analyses differ in
the treatment of the $\PW\PW$ background; details are described in
\refS{sec:background}.

\begin{table}
\caption{Analysis cuts used in the $\PH \to {\PW\PW}^{*} \to \Pl\PGn
  \Pl\PGn$ analysis in the ATLAS experiment, the $\pT^{\Pl\Pl}$
    variable is the sum of the $\pT$'s of the two letpons, the
    $\PZ_{\PGt \PGt}$ veto is a veto applied to the invariant mass of
    the $\PGt \PGt$ system reconstructed in the collinear
    approximation (the neutrino from $\PGt$ decay is supposed to fly in
  the same direction of the charged lepton) of $|m_{\PGt \PGt} -
  \MZ| > 15$ \UGeV{}.} \label{tab:acceptance_cuts}
\centerline{
\begin{tabular}{c|c}
\hline
$\MH <   200 \UGeV$ & $\MH > 200 \UGeV$ \\
\hline
$E_{\mathrm{T}}^{\rm  miss rel} > $ 40 \UGeV{} & $E_{\mathrm{T}}^{\rm miss rel} >40$
\UGeV{}\\
$m_{\Pl\Pl}$ < 50 \UGeV{} & $m_{\Pl\Pl} < 150$ \UGeV{} \\
$\Delta \phi_{\Pl\Pl} < 1.8$ & no $\Delta \phi_{\Pl\Pl}$ cut \\
$|m_{\Pl\Pl} - \MZ| > 15 \UGeV$ & $|m_{\Pl\Pl} - \MZ| > 15 \UGeV$ \\ 
\hline
%\multicolumn{2}{c}{0-jet} \\
%\hline
\multicolumn{2}{l}{0-jet: \quad $\pT^{\Pl\Pl} > 30$ \UGeV{}} \\
\hline
%\multicolumn{2}{c}{1-jet} \\
%\hline
\multicolumn{2}{l}{1-jet: \quad $\pT^{\rm l1} + \pT^{\rm l2} + E_{\mathrm{T}}^{\rm miss} + \pT^{\rm  jet} < 30$ \UGeV{}} \\
\multicolumn{2}{c}{Z$_{\PGt \PGt}$ veto} \\
\hline
\end{tabular}
}
\end{table}
The fractional variation of the $A_0$ and $A_1$ varying the
normalisation and factorisation scale at $\muR = \muF = 2\MH$
and $\muR = \muF = 1/2\MH$ with respect to the central value of
  $\muR = \muF = \MH$ are shown in 
  \refT{tab:acceptances_scale} both for the low-mass and the high-mass analyses.
\begin{table}
\caption{Acceptance variation for the 0 and 1-jet analysis for two
  Higgs mass points. The acceptance are evaluated for the low-mass
  analysis for $\MH=130\UGeV$ and the high-mass  analysis for $\MH = 200\UGeV$.} 
\label{tab:acceptances_scale}
\centerline{
\begin{tabular}{cccc}
\hline
& & $\MH = 130 \UGeV$ & $\MH = 200 \UGeV$ \\
\hline
$A_{0}$ & $\mu_{\mathrm{R}} = \mu_{\mathrm{F}} = 2\MH$ & $- 1$ per mil  & $+ 3\%$\\
$A_{0}$ & $\mu_{\mathrm{R}} = \mu_{\mathrm{F}} = \MH/2$ & $+ 5$ per mil  &  $-1.3\%$\\
\hline
$A_{1}$ & $\mu_{\mathrm{R}} = \mu_{\mathrm{F}} = 2\MH$ & $- 6$ per mil  & $+ 4\%$\\
$A_{1}$ & $\mu_{\mathrm{R}} = \mu_{\mathrm{F}} = \MH/2$ & $+ 4\%$  &  $+3\%$\\
\hline
\end{tabular}
}
\end{table}
The uncertainties obtained are much smaller than the scale
uncertainties on the inclusive jet cross sections.

\subsection{Discussion about the 2-jet bin}
The error on the 2-jet inclusive bin has been computed using the {\sc HNNLO}
program. It predicts a quite large error on the 2-jet cross section
because the 2-jet bin is computed at LO. The inlcusive 2-jet cross section
$\sigma_{\ge 2}$ is about $30\%$ of the inclusive 1-jet cross section
$\sigma_{\ge 1}$. With further cuts
that are  usually used in the 2-jet channel  this can reach   very small
values. Typically the 2-jet bin is used in vector-boson fusion (VBF)
and VH analyses where the analyses are tuned to maximise the cross sections in the 
VBF and HV channels.
Recently an NLO calculation of the $\Pg\Pg \to \PH + 2$ jets process became
available and implemented in {\sc MCFM}. The scale uncertainties of these
results are smaller than the uncertainty from {\sc HNNLO} that
computes the $\PH+2$~jets cross section at LO.
In order to use the smaller error from {\sc MCFM} we need to compare the
cross section computed with {\sc MCFM} with the respective cross section computed
with the {\sc POWHEG} Monte Carlo after the reweighting to {\sc HqT}~2.0. In fact the signal 
yield is computed using the {\sc POWHEG} MC that 
evaluates the 2-jet cross section only through the
parton shower. It is therefore important to compare the central value
of the 2-jet cross section from {\sc POWHEG} with Higgs $\pT$ reweighting
and with the {\sc MCFM}~v6.0 computation.
The comparison is performed applying the following cuts,
\begin{equation}
\pT^{\rm l1} > 20 \UGeV, \quad \pT^{\rm l2} > 10 \UGeV, \quad |\eta_{\rm l1}| < 2.5,
\quad |\eta_{\rm l2}| < 2.5,
\end{equation}
and at least two anti-$k_{\mathrm{T}}$ jets with $\pT > 25\UGeV$ with a cone
$\Delta R = 0.4$.
The computation has been performed using the CTEQ6.6 PDF set and the
renormalisation and factorisation scales fixed at the value of the
Higgs mass. The total {\sc POWHEG} cross section has been normalised at the
NNLO value. The results are shown in 
\refT{tab:2_jet_MCFM_POWHEG} and plotted in \refF{fig:plot_POWHEG_MCFM_comparison}.
\begin{table}
\caption{Comparison between the {\sc POWHEG} and {\sc MCFM} 2 jet inclusive cross
  section, the error shown are the statistical errors, the ratio of the
  two cross sections doesn't exceed $11\%$ in the whole $\MH$ range.} \label{tab:2_jet_MCFM_POWHEG}
\centerline{
\begin{tabular}{crrc}
\hline
     $\MH$ [GeV]     &        {\sc MCFM}  [fb]       & {\sc POWHEG} [fb] & Ratio: {\sc POWHEG}/{\sc MCFM} \\ 
\hline
$130$& $ 4.09 \pm 0.05 $ & $ 3.89 \pm 0.05 $ &  $  0.95 \pm 0.02 $ \\ 
$135$& $ 5.41 \pm 0.09 $ & $ 5.13 \pm 0.06 $ &  $  0.95 \pm 0.02 $ \\ 
$140$& $ 6.67 \pm 0.07 $ & $ 6.22 \pm 0.08 $ &  $  0.93 \pm 0.01 $ \\ 
$145$& $ 8.20 \pm 0.18 $ & $ 7.55 \pm 0.09 $ &  $  0.92 \pm 0.02 $ \\ 
$150$& $ 9.02 \pm 0.14 $ & $ 8.66 \pm 0.10 $ &  $  0.96 \pm 0.02 $ \\ 
$155$& $ 9.67 \pm 0.09 $ & $ 9.57 \pm 0.11 $ &  $  0.99 \pm 0.01 $ \\ 
$160$& $10.55 \pm 0.07 $ & $ 10.80 \pm 0.12$  & $   1.02 \pm 0.01$  \\ 
$165$& $10.88 \pm 0.20 $ & $ 11.29 \pm 0.16$  & $   1.04 \pm 0.02$  \\ 
$170$& $10.67 \pm 0.13 $ & $ 10.54 \pm 0.11$  & $   0.99 \pm 0.02$  \\ 
$175$& $ 9.86 \pm 0.10 $ & $ 10.09 \pm 0.11$  & $   1.02 \pm 0.01$  \\ 
$180$& $ 9.41 \pm 0.08 $ & $ 9.69 \pm 0.10 $ &  $  1.03 \pm 0.01 $ \\ 
$185$& $ 8.31 \pm 0.09 $ & $ 8.32 \pm 0.09 $ &  $  1.00 \pm 0.01 $ \\ 
$190$& $ 7.43 \pm 0.05 $ & $ 7.40 \pm 0.08 $ &  $  1.00 \pm 0.01 $ \\ 
$195$& $ 6.82 \pm 0.06 $ & $ 6.97 \pm 0.07 $ &  $  1.02 \pm 0.01 $ \\ 
$200$& $ 6.72 \pm 0.07 $ & $ 6.51 \pm 0.07 $ &  $  0.97 \pm 0.01 $ \\ 
$220$& $ 5.26 \pm 0.05 $ & $ 5.70 \pm 0.09 $ &  $  1.08 \pm 0.02 $ \\ 
$240$& $ 4.97 \pm 0.04 $ & $ 5.17 \pm 0.12 $ &  $  1.04 \pm 0.02 $ \\ 
$260$& $ 4.41 \pm 0.02 $ & $ 4.80 \pm 0.07 $ &  $  1.09 \pm 0.01 $ \\ 
$280$& $ 4.07 \pm 0.04 $ & $ 4.29 \pm 0.10 $ &  $  1.05 \pm 0.02 $ \\ 
$300$& $ 3.77 \pm 0.02 $ & $ 4.18 \pm 0.06 $ &  $  1.11 \pm 0.02 $ \\ 
\hline
\end{tabular}
}
\end{table}

\begin{figure}
\begin{center}
\includegraphics[width=0.7\textwidth]{YRHXS2_WW/YRHXS2_WW_fig1.eps}
\end{center}
\caption{The $\Pp\Pp \to \PH+2$ jets cross section evaluated using typical charged leptons
  and jets selection cuts of the $\PW\PW+2$ jets analysis.} \label{fig:plot_POWHEG_MCFM_comparison}
\end{figure}

Renormalisation and factorisation scale variations from $\MH/4$ to
2 $\MH$  have been evaluated  and tabulated in \refT{tab:2jets_scale_uncertainties}.
\begin{table}
\caption{The $\Pg\Pg \to \PH +2$ jets cross section evaluated for different
  scale choices.} \label{tab:2jets_scale_uncertainties}
\centerline{
\begin{tabular}{cccc}
\hline
      $\MH$ [GeV]     &    $\xi_{\rm R} = \muR/\MH$       & $\xi_{\rm F} = \muF/\MH$ &  $\sigma$ [fb] \\ 
\hline
      & $ 0.25 $&$ 0.25 $&$  5.17  \pm 0.64 $ \\ 
      & $ 0.25 $&$ 0.5  $&$  4.60  \pm 0.24 $ \\ 
      & $ 0.5  $&$ 0.25 $&$  4.88  \pm 0.11 $ \\ 
      & $ 0.5  $&$ 0.5  $&$  5.14  \pm 0.14 $ \\ 
      & $ 0.5  $&$ 1    $&$  4.98  \pm 0.08 $ \\ 
$130$ & $ 1    $&$ 0.5  $&$  4.42  \pm 0.05 $ \\ 
      & $ 1    $&$ 1    $&$  4.17  \pm 0.06 $ \\ 
      & $ 1    $&$ 2    $&$  4.13  \pm 0.05 $ \\ 
      & $ 2    $&$ 1    $&$  3.41  \pm 0.03 $ \\ 
      & $ 2    $&$ 2    $&$  3.34  \pm 0.05 $ \\ 
\hline
      & $ 0.25 $&$ 0.25 $&$  6.47  \pm 0.36 $ \\ 
      & $ 0.25 $&$ 0.5  $&$ 12.46  \pm 0.38 $ \\ 
      & $ 0.5  $&$ 0.25 $&$ 13.27  \pm 0.32 $ \\ 
      & $ 0.5  $&$ 0.5  $&$ 13.03  \pm 0.30 $ \\ 
      & $ 0.5  $&$ 1    $&$ 12.24  \pm 0.36 $ \\ 
$160$ & $ 1    $&$ 0.5  $&$ 10.77  \pm 0.12 $ \\ 
      & $ 1    $&$ 1    $&$ 10.52  \pm 0.13 $ \\ 
      & $ 1    $&$ 2    $&$ 10.28  \pm 0.07 $ \\ 
      & $ 2    $&$ 1    $&$  8.43  \pm 0.09 $ \\ 
      & $ 2    $&$ 2    $&$  8.35  \pm 0.07 $ \\ 
\hline
      & $ 0.25 $&$ 0.25 $&$  6.43  \pm 0.15 $ \\ 
      & $ 0.25 $&$ 0.5  $&$  6.96  \pm 0.11 $ \\ 
      & $ 0.5  $&$ 0.25 $&$  6.84  \pm 0.11 $ \\ 
      & $ 0.5  $&$ 0.5  $&$  6.70  \pm 0.13 $ \\ 
      & $ 0.5  $&$ 1    $&$  6.66  \pm 0.14 $ \\ 
$220$ & $ 1    $&$ 0.5  $&$  5.85  \pm 0.06 $ \\ 
      & $ 1    $&$ 1    $&$  5.56  \pm 0.06 $ \\ 
      & $ 1    $&$ 2    $&$  5.33  \pm 0.04 $ \\ 
      & $ 2    $&$ 1    $&$  4.48  \pm 0.03 $ \\ 
      & $ 2    $&$ 2    $&$  4.59  \pm 0.06 $ \\ 
\hline
 \end{tabular}
}
 \end{table}
The scale uncertainty is computed by taking the maximum spread in
the cross section obtained spanning the values $\MH/2 < \muR, \muF
< 2 \MH$ but keeping $ 1/2 < \muR/\muF < 2$. The obtained
uncertainties are summarised in \refT{tab:scale_uncertainites}.
\begin{table}
\caption{Scale uncertainties on the $\Pg\Pg \to \PH + 2$ jets cross section
  evaluated using the {\sc MCFM} program.} \label{tab:scale_uncertainites}
\centerline{
\begin{tabular}{cc}
\hline
$\MH$ [GeV] & scale uncertainties \\
\hline
$130$ & $23 \%$ \\
$160$ & $24 \%$ \\
$220$ & $20 \%$ \\
\hline
\end{tabular}
}
\end{table}


\subsection{Backgrounds}
\label{sec:background}
The main backgrounds to the $\PH \to \PW\PW $ channel are the top background,
the Drell--Yan process $\Pp\Pp \to \PZ/\PGg^{*}+{}$jets with $\PZ/\PGg^{*} \to \Plp\Plm$, 
the $\Pp\Pp \to \PW+{}$jets, with $\PW\to \Pl\PGn$, with a fake lepton from the jets, 
 and the irreducible $\Pp\Pp \to \PW^{+} \PW^{-}$ background. 
The reducible backgrounds are estimated with data-driven 
technique slightly different between the two collaborations.
The treatment of the irreducible $\PW\PW$ background is mainly affected by
theoretical uncertainties and will be discussed in the following.

For a relatively light Higgs, the $\PW$ bosons in the $\PW\PW^*$ pair produced
in the Higgs decay have opposite spin orientations, since the Higgs has spin
zero. In the weak decay of the $\PW$, due to the $V-A$ nature of the
interaction, the positively charged lepton is preferably emitted in the
direction of the $\PW^{+}$ spin and the negative letpon in the opposite direction
of the $W^{-}$ spin. Therefore the two charged leptons are emitted close to each
other, and their di-lepton invariant mass $m_{\Pl\Pl}$ is small.
This feature is used in the low-mass analysis to define a signal-free control region through
the cut $m_{\Pl\Pl} > 80$ \UGeV{}. The event yield of the $\PW\PW^{*}$ background
is computed in the control region and extrapolated to the signal
region.
The $\PW\PW$ yield in the signal region is therefore
\begin{equation}
N^{\rm WW 0j}_{\rm S.R.} = \alpha_{\rm 0j} N^{\rm WW 0j}_{\rm C.R.}, \quad
N^{\rm WW 1j}_{\rm S.R.} = \alpha_{\rm 1j} N^{\rm WW 1j}_{\rm C.R.}. 
\end{equation}

The value of $\alpha$ is affected both by theoretical and experimental
errors. Because $\alpha$ is defined using only leptonic quantities, the
experimental error is negligible and the theoretical error is
carefully evaluated in \refS{sec:alpha}.

For $\MH > 200\UGeV$ it is not possible to define a signal-free
control region, therefore the $\PW\PW$ yield needs to be determined directly
from the theoretical expectation. In \refS{sec:acceptances} we
discuss the evaluation of the jet bin uncertainties on the expected
$\PW\PW^*$ yield.


\subsection{Theoretical uncertainties on the extrapolation parameters
  $\alpha$ for the 0j and 1j analyses}
\label{sec:alpha}

The $\PW\PW$ background is estimated for $\MH < 200\UGeV$ using event
counts in a well defined control region (C.R.\ in the following).
The control region is defined using cuts on $m_{\Pl\Pl}$ and $\Delta
\phi_{\Pl\Pl}$ variables. 
In \refT{tab:WW_cuts} we show the cuts used by the ATLAS collaborations  to define the signal and the $\PW\PW$ C.R.\
for the different channels.
\begin{table}
\caption{Definition of the signal and control regions.} \label{tab:WW_cuts}
\centerline{
\begin{tabular}{cc}
\hline
$\MH$ [GeV] & $110-200$ \\
\hline
 ATLAS    & $\Delta \phi_{\Pl\Pl} < 1.8$ \\
  S.R.    & $m_{\Pl\Pl} < 50 \UGeV $  \\
\hline
  ATLAS   & $m_{\Pl\Pl} > \MZ + 15 \UGeV (\Pe\Pe,\PGm\PGm)$ \\
  C.R.    & $m_{\Pl\Pl} > 80 \UGeV (\Pe\PGm)$ \\ 
\hline
\end{tabular}
}
\end{table}

\begin{sloppypar}
The amount of $\PW\PW$ background in signal region is determined from the control region through the parameter $\alpha$ defined by
\begin{equation}
\alpha_{\PW\PW} = N^{\PW\PW}_{\rm S.R.}/N^{\PW\PW}_{\rm C.R.},
\end{equation}
where $\alpha$ is evaluated independently for the 0-jet and 1-jet bin.
The Standard Model $\PW\PW^{*}$ yield is obtained using the MC@NLO Monte
Carlo program interfaced to {\sc Herwig} for parton showering. MC@NLO  computes the $\Pp\Pp \to \PW\PW ^{*} \to \Pl\PGn \Pl\PGn$ at NLO including off-shell contributions. Spin correlations
for off-shell $\PW$'s are not treated at matrix-element (ME) level
and a correction is provided after the generation step to take into account 
the spin correlation with some approximation. Furthermore MC@NLO does
not implement all electroweak diagrams contributing to the $\Pp\Pp
\to \Pl\PGn \Pl\PGn$ process. In particular, ``singly-resonant'' processes
are missing in the calculation. A singly-resonant diagram is, e.g.\ 
a diagram where at least a lepton neutrino pair is not connected to the same $\PW$
decay vertex. The full ME calculation for the spin correlation and the inclusion of all
singly-resonant diagrams is implemented in the {\sc MCFM}~v6.0 parton
level Monte Carlo generator. In order to take into account
uncertainties in the modelling of the $\PW\PW$ background, the MC@NLO and
{\sc MCFM}~v6.0 output have been compared. The comparison has been performed 
summing up the contribution of all jet bins in order to integrate out effects
from the simulation of the jet multiplicity, which are not well modelled by parton-level Monte Carlo programs.
The CTEQ6.6 PDF error set has
been used in the comparison and it has been used inclusively in
the number of jets. In \refFs{fig:MCFM_MCNLO_comp_etmiss}, \ref{fig:MCFM_MCNLO_comp_1},
 \ref{fig:MCFM_MCNLO_comp_2}, and \ref{fig:MCFM_MCNLO_comp_3}  we show the
comparison between {\sc MCFM}~v6.0  and MC@NLO on several variables. 
The transverse mass $m_{\rm T}$ is defined by 
\begin{equation}
m_{\rm T} = \sqrt{(E_{\mathrm{T}}^{\Pl \Pl} + E_{\mathrm{T}}^{\rm
    miss})^2-(\vec{p}_{T}^{\Pl \Pl} + \vec{p}_{\mathrm{T}}^{\rm miss})^2},
\end{equation}
where $E_{\mathrm{T}}^{\Pl \Pl} = \sqrt{(\vec{p}_{\mathrm{T}}^{\Pl \Pl})^2 +
  m_{\Pl \Pl}^2}$, $|\vec{p}_{\mathrm{T}}^{\rm miss}| = E_{\mathrm{T}}^{\rm
  miss}$, and $\vec{p}_{T}^{\Pl \Pl}$ is the transverse momentum of
the di-lepton system.
\end{sloppypar}

\begin{figure}
\begin{center}
\includegraphics[width=0.7\textwidth]{YRHXS2_WW/YRHXS2_WW_fig2.eps}
\end{center}
\caption{Comparison between {\sc MCFM}~v6.0 and MC@NLO: The shape  of the 
missing-$E_{\mathrm{T}}$ distribution is shown
 for events  passing the
  charged-lepton and missing-$E_{\mathrm{T}}$ cuts.} \label{fig:MCFM_MCNLO_comp_etmiss}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.49\textwidth]{YRHXS2_WW/YRHXS2_WW_fig3.eps}
\includegraphics[width=0.49\textwidth]{YRHXS2_WW/YRHXS2_WW_fig4.eps}
\end{center}
\caption{Comparison between {\sc MCFM}~v6.0 and MC@NLO: the shapes  of the
  $\pT$ of the softer charged lepton  (left)
  and  of the hardest  charged lepton (right) are shown for events  passing the
  charged-lepton and missing-$E_{\mathrm{T}}$ cuts.} \label{fig:MCFM_MCNLO_comp_1}
\end{figure}

\begin{figure}
\includegraphics[width=0.49\textwidth]{YRHXS2_WW/YRHXS2_WW_fig5.eps}
\includegraphics[width=0.49\textwidth]{YRHXS2_WW/YRHXS2_WW_fig6.eps}
\caption{Comparison between {\sc MCFM}~v6.0 and MC@NLO: the shapes  of the $\pT$
  of the charged letpon system (left) and the transverse mass of the
  event (right), are shown, the definition of the transverse mass variable is given in the text.} \label{fig:MCFM_MCNLO_comp_2}
\end{figure}

\begin{figure}
\includegraphics[width=0.49\textwidth]{YRHXS2_WW/YRHXS2_WW_fig7.eps}
\includegraphics[width=0.49\textwidth]{YRHXS2_WW/YRHXS2_WW_fig8.eps}
\caption{Comparison between {\sc MCFM}~v6.0 and MC@NLO shapes  of the angle
  between the two leptons in the transverse plane $\Delta
  \phi_{\Pl\Pl}$ (left) and the invariant mass of the di-lepton system
  $m_{\Pl\Pl}$  (right). The two variables are used for the definition of
  the signal and the $\PW\PW^{*}$ control regions.} \label{fig:MCFM_MCNLO_comp_3}
\end{figure}

Small difference between the two calculations are visible in all
variables, the $m_{~ll}$ variable that is used to define the $\PW\PW$
C.R. is in very good agreement. The effect of these discrepancies  
on the $\alpha$ parameter has been evaluated to be:
\begin{equation}
\frac{\alpha(\rm MC@NLO)}{\alpha(\rm MCFM)} = 0.980 \pm 0.015,
\end{equation}
where the error is due to the MC statistics, a conservative error of
$3.5\%$ is used in the analysis.


Further uncertainties on the $\alpha$ parameters are obtained using
different PDF sets in the simulation of the $\PW\PW^{*}$ background.
In particular, the $\alpha$ values have been computed with the
CTEQ6.6 error set and computing the error according the recommended
procedure, the values obtained using the central PDFs of MSTW2008
and NNPDF2.1 has also been computed. The results obtained are shown
in \refT{tab:pdfs}.

\begin{table}
\caption{The $\alpha$ parameters computed using different PDF sets and spread obtained spanning on the CTEQ6.6 error set.} \label{tab:pdfs} 
\centerline{
\begin{tabular}{cccc}
\hline
& CTEQ 6.6 error set & MSTW2008 & NNPDF2.1 \\
\hline
$\alpha^{\rm 0j}_{\PW\PW}$ & $2.5\%$ & $+0.1$ per mil & $+2.7\%$ \\
$\alpha^{\rm 1j}_{\PW\PW}$ & $2.6\%$ & $- 7$  per mil & $+1.2\%$ \\
\hline
\end{tabular}
}
\end{table}

Higher-order corrections can affect the $\pT$ distribution of the
$\PW\PW^{*}$ pair and the $\alpha$ values. Their effect
has been estimated by changing the renormalisation ($\muR$) and the
factorisation ($\muF$) scales in MC@NLO. 
The renormalisation scales are defined as $\muR = \xi_{\rm R} \mu_{0}$
and $\muF = \xi_{\rm F} \mu_0$ where $\mu_0$ is a dynamic scale defined
as
\begin{equation}
 \mu_{0} = \frac{\sqrt{p_{\rm T W1}^2 +
    M_{\rm W1}^2}+\sqrt{p_{\rm T W2}^2+M_{\rm W2}^2}}{2}.
\end{equation}
The nominal scale is obtained with $\xi_{\rm R} = \xi_{\rm F} = 1$ while the
scale uncertainty is obtained by varying $\xi_{\rm R}$ and $\xi_{\rm F}$ in
the range $1/2{-}2$ while keeping $\xi_{\rm R}/\xi_{\rm F}$ in between $1/2$ 
and 2; the maximum spread is taken as scale uncertainties. The
uncertainties obtained are shown in \refT{tab:scale_and_pdfs}
where we summarise also the PDF and modelling uncertainties.
The correlation between the $\alpha$ parameters of the 0-jet and 1-jet
analysis is also evaluated in the calculation and they are found to be
fully correlated.

\begin{table}
\caption{Scale and PDF uncertainties on $\PW\PW$ extrapolation parameters $\alpha$ for the NLO 
$\PQq\PQq,\PQq\Pg \to \PW\PW $ process, the
errors are found to be fully correlated between the 0-jet and 1-jet bin.} \label{tab:scale_and_pdfs}
\centerline{
\begin{tabular}{cc|c|c}
\hline
& scale & PDFs  & modelling \\
\hline
$\alpha^{\rm 0j}_{\PW\PW}$ & $2.5\%$ & $3.7\%$ & $3.5\%$\\
$\alpha^{\rm 1j}_{\PW\PW}$ & $4\%$ & $2.9\%$  & $3.5$ \%\\
\hline
$\alpha^{\rm 0j}_{\PW\PW}$, $\alpha^{\rm 1j}_{\PW\PW}$ correlation & \multicolumn{3}{c}{1} \\
\hline
\end{tabular}
}
\end{table}

\subsection{Contribution from the $\Pg\Pg \to \PW\PW$ process}

MC@NLO MC computes the process $\Pp\Pp \to \PW\PW ^{*}$ at NLO. Up to NLO
only, processes initiated by two incoming quarks contribute to the amplitude of the process. 
At the LHC the process $\Pg\Pg \to \PW\PW $ can deliver a
non-negligible contribution. It is therefore important to estimate the
$\Pg\Pg$ contribution at the LHC. This contribution could be further enhanced
by the analysis cuts that mainly select the Higgs topology that is
induced by a $\Pg\Pg$ process. At low mass the $\Pg\Pg$ contribution is again
normalised through the control region, so that only the uncertainties on the
$\alpha$ parameters are relevant for the analysis.
The PDF uncertainty has been evaluated by the band spanned by the CTEQ6.6 PDF
error set and comparing the central value with those from the MSTW2008 and NNPDF2.1 sets.
Moreover, because the $\Pg\Pg \to \PW\PW $ lowest-order cross section is proportional to $\alphas^2$
there are ambiguities on the set of PDF's that has to be used to evaluate the cross section.
Therefore we include in the PDF uncertainty the values obtained using LO, NLO, and NNLO PDF sets. The 
MSTW family of PDF's has been used for this estimate. The results are shown in \refT{tab:pdf_ggww}.
\begin{table}
\caption{Pdf uncertainties on $\PW\PW$ extrapolation parameters $\alpha$ for the $\Pg\Pg \to \PW\PW $ process.} 
\centerline{
\begin{tabular}{cccccc}
\hline
& CTEQ 6.6 error set & MSTW 2008 & NNPDF2.1 & MSTW LO & MSTW NNLO\\
\hline
$\alpha^{\rm 0j}_{\PW\PW}$ & $2.6\%$  & $-4 $per mil & $-4$ per mil & $+3.6\%$ & $-0.6$ per mil\\
$\alpha^{\rm 1j}_{\PW\PW}$ & $ 2.8\%$ & $-2$ per mil & < $1$ per mil &
$+3.6\%$ & $+0.4$ per mil\\
\hline
\end{tabular}
}
\label{tab:pdf_ggww}
\end{table}

The scale uncertainites have been evaluated by varying
independently by a factor four the renormalisation and factorisation
scales with respect to the nominal value $\mu_{~0} = \MW$ while keeping the ratio between $1/2$ and $2$.
The results are shown in \refT{tab:gg2WW} together with the overall PDF uncertainty.
\begin{table}
\caption{Scale and PDF uncertainties on $\PW\PW$ extrapolation parameters $\alpha$ for the $\Pg\Pg \to \PW\PW $ process.} \label{tab:gg2WW}
\centerline{
\begin{tabular}{ccc}
\hline
& scale & PDFs  \\
\hline
$\alpha^{\rm 0j}_{\PW\PW}$ & $6\%$ & $4.4\%$ \\
$\alpha^{\rm 1j}_{\PW\PW}$ & $9\%$ & $4.6\%$\\
\hline
\end{tabular}
}
\end{table}

The values shown in \refT{tab:gg2WW} have to be applied only to
the $\Pg\Pg \to \PW\PW $ component of the $\PW\PW$ background, the incidence of the
$\Pg\Pg \to \PW\PW $ contribution in the signal region is anyway small given
the low $\Pg\Pg \to \PW\PW $ cross section. The yield of the $\Pg\Pg \to \PW\PW $ is $5\%$
of the total $\PW\PW$ background in the 0-jet channel and $7\%$ in the 1-jet
channel.



\subsection{WW background estimate for the high-mass selection}
\subsubsection{Jet-bin cross sections}
\label{sec:acceptances}
For high values of $\MH$, namely $\MH > 200\UGeV$, the statistics in the $\PW\PW$ control region becomes quite small,
and the control region gets contaminated by
a significant signal fraction. In this region a direct evaluation of the $\PW\PW$ yield in the signal region is recommended.
The  $\PW\PW$ yield in jet bins  is treated in the same way and can be written as follows,
\begin{equation}
N^{\PW\PW}_{\rm 0j} = \sigma_{\ge 0}f_{0}A_{0}, \quad 
N^{\PW\PW}_{\rm 1j} = \sigma_{\ge 0}f_{1} A_{1} \label{eq:cross_multi_jets},
\end{equation}
with $f_0$ and $f_1$ defined as
\begin{equation}
f_0 = \frac{\sigma_{\ge 0} - \sigma_{\ge 1}}{\sigma_{\ge 0}}, \quad 
f_1 = \frac{\sigma_{\ge 1}-\sigma_{\ge 2}}{\sigma_{\ge 0}}.
\end{equation}

The cross sections are computed for jets with $\pT > 25\UGeV$ and $|\eta| < 4.5$ for the ATLAS experiment.
The inclusive-cross-section uncertainty is evaluated using MC@NLO, applying the scale variation prescription as above.
The values of the inclusive cross section obtained and their uncertainties are shown in 
\refT{tab:scale_unc_multijet}; they have been evaluated for a single lepton combination.
\begin{table}
\caption{Inclusive multi-jet cross sections computed with MC@NLO and their scale variation. The values shown are obtained for a single lepton combination. CTEQ6.6 PDFs are used in the computation.} \label{tab:scale_unc_multijet}
\centerline{
\begin{tabular}{cccccccc}
\hline
$\sigma_{\ge 0}$ [fb] & $\Delta \sigma_{\ge 0}$ [\%] & $\sigma_{\ge 1}$ [fb]  & $\Delta \sigma_{\ge 1}$ [\%]  & $\sigma_{\ge 2}$ [fb] & $\Delta \sigma_{\ge 2}$ [\%] & $\sigma_{\ge 3}$ [fb] & $\Delta \sigma_{\ge 3}$ \\
\hline
$532$ & $3.3$ & $159$ & $6.5$ & $41$ &  $8.7$ & $9$ & $11$ \\
\hline
\end{tabular}
}
\end{table}

MC@NLO simulates the $\Pp\Pp \to \PW\PW  +2$~jets process through the NLO $\Pp\Pp \to \PW\PW ^{*} \to \Pl\PGn \Pl\PGn$ plus parton shower. This means that no ME computation for the $\Pp\Pp \to \PW\PW  + 2$~jets is
implemented in the generator. In order to take into account mismodelling of the parton-shower MC we compare the ratio $\sigma_{\ge 2}/\sigma_{\ge 1}$ and $\sigma_{\ge 3}/\sigma_{\ge 2}$ between MC@NLO and ALPGEN which computes $\PW\PW$ production cross section up to three jets. The two $\PW$'s are simulated
on shell by ALPGEN, and spin correlations are not included. 
Moreover, only tree-level diagrams are computed by ALPGEN; therefore the comparison is performed only for jet multiplicity higher than one where ALPGEN provides a ME computation for the jet yield. The comparison is shown in \refT{tab:ALPGEN_MCatNLO_COMPUTATION}. The discrepancy between the two generators on these ratios is added in quadrature to the scale variation for the 2-jets and the 3-jets inclusive cross section. 

\begin{table}
\caption{Multi-jet cross section ratios compared between MC@NLO and ALPGEN.} \label{tab:ALPGEN_MCatNLO_COMPUTATION}
\centerline{
\begin{tabular}{ccc}
\hline
& MC@NLO & ALPGEN \\
\hline
$\sigma_{\ge 2}/\sigma_{\ge 1}$ & $0.256$ & $0.360$ \\ 
$\sigma_{\ge 3}/\sigma_{\ge 2}$ & $0.057$ & $0.112$ \\
\hline
\end{tabular}
}
\end{table}

The inclusive multi-jet uncertainties due to scale and modelling are summarised in \refT{tab:multijet_uncertainties}.

\begin{table}
\caption{Uncertainties on the inclusive jet cross sections due to scale and modelling for jet $\pT > 25\UGeV$ and $|\eta|<4.5$.} \label{tab:multijet_uncertainties}
\centerline{
\begin{tabular}{cccc}
\hline
$\Delta \sigma_{\ge 0}$ [\%] & $\Delta \sigma_{\ge 1}$ [\%] & $\Delta \sigma_{\ge 2}$ [\%] & $\Delta \sigma_{\ge 3}$ [\%]   \\
\hline
 $3$ & $6$ & $42$ & $100$ \\
\hline
\end{tabular}
}
\end{table}


\subsubsection{Theoretical errors on acceptances}
In addition to the scale uncertainties on the jet fractions, $f_{i}$, we need to evaluate also the scale uncertainties on the acceptances $A_{i}$  as defined in Eq.~\ref{eq:cross_multi_jets}.
These include scale variation effects with all other cuts except for the jet counting. 
In order to decouple the jet-bin scale uncertainty from other uncertainties, 
the acceptance needs to be evaluated in a given jet bin.
This is done in MC@NLO by requiring a jet-exclusive bin at the beginning of the selection and then evaluating the ratio
\begin{equation}
N_{\rm cuts~and~jet~bin} / N_{\rm jet~bin}.
\end{equation}
The maximum relative spread of this ratio is used as the fractional acceptance scale uncertainty.
In ATLAS the following cuts are applied in both 0-jet and 1-jet bins for $\MH > 220\UGeV$ 
and the $\Pe\Pe$ and $\PGm\PGm$ channels: $p_{T1} > 25\UGeV$, $p_{T2} > 15\UGeV$, $|\eta| < 2.5$, 
$E_{\rm T}^{\rm miss} > 40\UGeV$,  $\Delta \phi_{\Pl\Pl} < 2.6$, 
$m_{\Pl\Pl} < 140\UGeV$ \&\& $|m_{\Pl\Pl}-\MZ| > 15\UGeV$, while
 in the 0-jet channel $|p_{\rm Tl1} + p_{\rm Tl2}| > 30 \UGeV$ and in the 1-jet channel 
$|p_{\rm Tl1} + p_{\rm Tl2} + p_{\rm T jet} + E^{\rm miss}_{\mathrm{T}}| < 30 \UGeV$ cuts are added. 
The scale uncertainty is $5\%$ in the 0-jet bin and $2\%$ in the 1-jet bin.

In case LO MC are used to compute the acceptance in the analysis, the LO--MC@NLO discrepancy on the given acceptance value has to be added to the scale variation value.

\subsection{PDF errors}
PDF errors are much smaller than scale uncertainties and are less linked to the jet activity associated to the $\PW\PW$ production. Also correlations between jet bins can be neglected.
Therefore in evaluating PDF uncertainties we directly compute the product $f_{i}A_{i}$, which is the relative variation in the event yield in the signal region, by varying PDFs within the CTEQ6.6 error set and comparing
the central values to the ones obtained with MSTW2008 and NNPDF2.1. The uncertainty obtained is $2\%$ for both 0-jet and 1-jet bins.










  
