\subsection{Systematic studies of NLO MC tools in experimental implementations}\label{NLOEx}
In conjunction with systematic studies of theoretical tools, 
described in the previous sections,
the NLO MC group has estimated the systematic uncertainties of the NLO MC tools in  
their implementation within the event generation framework of the ATLAS and CMS experiments.
This section describes the results of these studies performed both at the parton level, after hard scatterer 
parton showering, and after a fast detector simulation to account for detector effects.  

The initial plans for the systematic uncertainty studies entail as follows:
\begin{itemize}
\item Higgs Production process: gluon fusion, $\Pg\Pg\rightarrow \PH$ 
\item Decay Processes: $\PH\rightarrow \PW\PW \rightarrow \Pl \PGn \Pl \PGn$ and 
$\PH\rightarrow \PZ\PZ \rightarrow 4\Pl$ 
\item MC Tools : \POWHEGBOX, \MCatNLO, \SHERPA, and \HERWIG ++
\item Parton showering: \PYTHIA\ and \HERWIG 
\item Higgs mass: $130\UGeV$ and $170\UGeV$
\item PDF: MSTW, CT10, NNPDF and CTEQ6.6 
\item Underlying events: Initial stage to switch off the UE in \PYTHIA\ and switch off the soft UE in \HERWIG\ to focus on matrix element and parton shower effects. 
\end{itemize}
In the following sections, we describe the current studies with \POWHEG interfaced with two different parton showering (PS) programs, 
\PYTHIA\ or \HERWIG, as implemented in ATLAS and CMS MC event generation.

\subsubsection{NLO Monte Carlo tools and parameters}
The study presented hereafter is based on the ATLAS implementation of \POWHEGPYTHIA and \POWHEGHERWIG. 
Two different Higgs mass values are considered: $\MH=170\UGeV$ and $\MH=130\UGeV$, with $38\UMeV$
 and $4.9\UMeV$ widths, respectively. 
The parton distribution function (PDF) CTEQ6.6 set is used for this study.
All other parameters are kept at the  default values implemented in \POWHEG~\cite{Nason:2010ap}.
We present here the results for the $\PH\rightarrow \PW\PW \rightarrow \Pl \PGn \Pl \PGn$ final state. 
This is one of the most relevant channels for discovery.
A study for the other golden channel $\PH\rightarrow \PZ\PZ \rightarrow 4\Pl$ is in progress.

\subsubsection{Comparisons of \POWHEGPYTHIA or \POWHEGHERWIG}

The statistics of the $\PH\rightarrow \PW\PW \rightarrow \Pl \PGn \Pl \PGn$ event samples consists  
of  22k events for \POWHEGPYTHIA and 50k event for \POWHEGHERWIG, for each Higgs mass values, $\MH=170\UGeV$ and $130\UGeV$.
The event generator is interfaced to ATLAS fast detector simulation to include detector effects.
This, however, along with the specific final state selection scheme slowed down the event generation significantly, costing us about a week per sample.
In order to expedite complicated systematic uncertainties, such as that of PDF, it would be desirable to improve the speed of specific selection schemes.
The results presented in this section, however, are using the particle level information without using the fast simulation to be comparable with studies carried out in other sections.

The comparisons of various Higgs kinematic quantities with \POWHEGPYTHIA and \POWHEGHERWIG showering have been investigated.  
Most the kinematic variables, such as Higgs mass, W transverse mass and the kinematic variables of the leptons from the decay show no appreciable differences between \POWHEGPYTHIA and \POWHEGHERWIG parton showering.
\refF{fig:nlomc:exp:atlas-130}.a for $\MH=130\UGeV$ and \refF{fig:nlomc:exp:atlas-170}.a for $\MH=170\UGeV$ show good agreements between Pythia and Herwig parton showering.
The solid red circles in all plots represent the quantities from \POWHEGPYTHIA while the blue histograms represent those from \POWHEGHERWIG. 
The bin-by-bin ratio of \POWHEGHERWIG\ distributions with respect to \POWHEGPYTHIA  distributions 
is shown in the lower plots, to compare the shapes of the distributions.
However, as can be seen in \refF{fig:nlomc:exp:atlas-130}.b for $\MH=130\UGeV$ and \refF{fig:nlomc:exp:atlas-170}.b for $\MH=170\UGeV$, a systematic difference between the two PS schemes is observed for this quantity.
A systematic trend that the \POWHEGPYTHIA events display harder Higgs $\pT$ distribution than for \POWHEGHERWIG can be seen clearly from the ratio plots, despite the fact that the statistical uncertainties increases as $\pT$ grows.
The linear fit to the ratio for Higgs $\pT$ shows this trend with the value of the slope at $(9.1\pm 2.6)\times10^{-4}$ and $(6.2\pm 1.6)\times10^{-4}$ for $\MH=130\UGeV$ and $\MH=170\UGeV$, respectively, demonstrating statistically significant systematic effect.
The same trends have been observed in $\pT$ of the $\PWp$ and $\PWm$ from the Higgs decay as well.

In order to investigate this effect further, we have looked into the $\pT$ distributions of the jets, 
number of associated jets and jet efficiencies  as a function of jet $\pT$ cut values as shown in \refF{fig:nlomc:exp:atlas-11}  for $\MH$=170~GeV and $\MH=130\UGeV$. 
It is shown that \PYTHIA\ produces harder jet $\pT$ distributions of the jets since their momentum must balance that of the Higgs.

\begin{figure}[t]
  \centering
  \includegraphics[width=1.0\textwidth]{YRHXS2_NLOMC/figures/exp/atlas-130.eps}
  \caption{ (a) Higgs transverse mass without cuts, (b) Higgs transverse momentum without cuts, (c) Higgs transverse mass with cuts and (d) Higgs transverse momentum with cuts for \POWHEGPYTHIA parton showering (red circles) 
and for \POWHEGHERWIG  parton showering (blue histogram) for $\MH=130\UGeV$.  
The plots below each of the histogram are the ratio of \POWHEGHERWIG\ with respect to \POWHEGPYTHIA.}
\label{fig:nlomc:exp:atlas-130}
\end{figure}

\begin{figure}[t]
  \centering
  \includegraphics[width=1.0\textwidth]{YRHXS2_NLOMC/figures/exp/atlas-170.eps}
  \caption{ (a) Higgs transverse mass without cuts, (b) Higgs transverse momentum without cuts, (c) Higgs transverse mass with cuts and (d) Higgs transverse momentum with cuts for \POWHEGPYTHIA parton showering (red circles) 
and for \POWHEGHERWIG  parton showering (blue histogram) for $\MH=170\UGeV$.  
The plots below each of the histogram are the ratio of \POWHEGHERWIG\ with respect to \POWHEGPYTHIA.}
\label{fig:nlomc:exp:atlas-170}
\end{figure}

\begin{figure}[t]
  \centering
  \includegraphics[width=1.0\textwidth]{YRHXS2_NLOMC/figures/exp/atlas-11.eps}
\caption{$\pT$ distributions of the jets from parton showering for (a) $\MH=130\UGeV$ and (b) $\MH=170\UGeV$, Number of associated jets with $\pT>25\UGeV$ for (c) $\MH=130\UGeV$ and (d) $\MH=170\UGeV$ and jet efficiency as a function of jet $\pT$ (e) $\MH=130\UGeV$ and (f) $\MH=130\UGeV$. \POWHEGPYTHIA parton showering (red circles) and for \POWHEGHERWIG\ parton showering (blue histogram).  The plots below each of the histogram are the ratio of \POWHEGHERWIG\ with respect to \POWHEGPYTHIA.}
  \label{fig:nlomc:exp:atlas-11}
\end{figure}

\subsection{Studies with experimental cuts}
In order to ensure the relevance of these results for the experimental searches, 
the cuts applied in this study follow the recommendation from ATLAS $\PH\rightarrow \PW\PW$ search group~\cite{ATLAS-CONF-2011-134}, as follows:
\begin{itemize}
\item Exactly two leptons
\item First leading lepton (l1) $\pT> 25\UGeV$, subleading lepton (l2) $\pT > 15\UGeV$;
\item Two leptons have opposite charge $M_{\Pl\Pl} > 15\UGeV$;
\item If l1,l2 have the same flavor, $M_{\Pl\Pl} > 10\UGeV$;
\item If l1,l2 have the same flavors, apply a $Z$ veto, $|M_{\Pl\Pl}-\MZ| > 15\UGeV$;
\item If l1,l2 has  the same flavors $\pT^{\Pl\Pl} > 30\UGeV$;
\item $M_{\Pl\Pl} < 50\UGeV$ for $\MH < 170\UGeV$;
\item $|\Delta\phi_{\Pl\Pl}|< 1.3$ for $\MH< 170\UGeV$; $|\Delta\phi_{\Pl\Pl}|< 1.8$ for $170\UGeV \leq \MH < 220\UGeV$; 
No $|\Delta\phi_{\Pl\Pl}|$ 
cut for $\MH\geq 220\UGeV$;
\item $|\eta_{\Pl\Pl} < 1.3$ for $\MH < 170\UGeV$.
\end{itemize}
\refF{fig:nlomc:exp:atlas-130}.c and \refF{fig:nlomc:exp:atlas-170}.c show the reconstructed transverse mass of the Higgs from the final state lepton and neutrino systems before and after the experimental 
selection cuts have been applied, for $\MH=130\UGeV$ and $\MH=170\UGeV$, respectively.
The comparison of the reconstructed transverse mass distributions show no significant difference, before and after experimental cuts between the two PS schemes.
\refF{fig:nlomc:exp:atlas-130}.d and \refF{fig:nlomc:exp:atlas-170}.d show the Higgs transverse momentum 
reconstructed from the final state lepton and neutrino systems, before and after the experimental 
selection cuts have been applied, for $\MH=130\UGeV$ and $\MH=170\UGeV$.
The difference in these distributions between the two PS programs reflects the observation at parton level that  \POWHEG + \PYTHIA\ displays harder Higgs $\pT$ than that from \POWHEGHERWIG, while the statistical uncertainties at high $\pT$ bins diminish.

\subsubsection{Conclusions and future work}

We have demonstrated that the systematic uncertainties resulting from interfacing \POWHEG to  \PYTHIA\ and  \HERWIG\  programs are small 
in most of the quantities used for the search, independently of the selection cut values and of the Higgs boson mass. 
On the other hand, the systematic uncertainties on Higgs and jets transverse momenta show sufficiently large 
differences between the two PS algorithms and therefore the associated systematic uncertainties cannot be ignored.
A similar study is in progress for the $\PZ\PZ$ final state. 

Thus, we conclude that the main difference between the two PS programs, interfaced to \POWHEG, is observed in the Higgs transverse momentum, reflecting into the experimentally reconstructed transverse momentum of the leptons plus missing transverse energy system.
However, this difference is found to be independent of the selection cuts and of the Higgs mass value within the two masses we have studied.  Additional studies on higher Higgs masses would be needed to  further confirm this conclusion.

Based on our experience with this study, we emphasize that it is necessary 
to improve the speed of the event generators in order to compute promptly the uncertainties associated to
the PDF set choices, as well as to the description of the underlying event and pile-up effects. As it will be presented in the following, a first
step in this direction has been undertaken by the \aMCatNLO Monte Carlo which provides automatic evaluation of scale and PDF uncertainties by a simple reweighting procedure. Finally, further studies of fast simulation quantities will allow full assessment of the propagation of the theoretical uncertainties to the experimental quantities used for the search.
