\noindent
\subsection{Perturbative uncertainties in exclusive jet bins\footnote{I. W. Stewart and F. J. Tackmann.}}
\label{sec:jetveto}


%===============================================================================
\subsubsection{Overview}
\label{sec:jetveto:overview}
%===============================================================================

In this section we discuss the evaluation of perturbative theory uncertainties
in predictions for exclusive jet cross sections, which have a particular number
of jets in the final state. This is relevant for measurements where the data are
divided into exclusive jet bins, and is usually done when the background
composition strongly depends on the number of jets, or when the overall
sensitivity can be increased by optimizing the analysis for the individual jet
bins. The primary example is the $\PH\to \PW\PW$ analysis, which is performed
separately in exclusive $0$-jet, $1$-jet, and $2$-jet channels. Other examples
are vector-boson fusion analyses, which are typically performed in the exclusive
$2$-jet channel, boosted $\PH\to \Pb\bar \Pb$ analyses that include a veto on
additional jets, as well as $\PH\to \PGt\PGt$ and $\PH\to \PGg\PGg$ which
benefit from improved sensitivity when the Higgs recoils against a jet.  When
the measurements are performed in exclusive jet bins, the perturbative
uncertainties in the theoretical predictions must also be evaluated separately
for each individual jet bin~\cite{Anastasiou:2009bt}.  When combining channels
with different jet multiplicities, the correlations between the theoretical
uncertainties can be significant and must be taken into
account~\cite{Stewart:2011cf}.  We will use the notation $\sigma_N$ for an
\emph{exclusive} $N$-jet cross section (with exactly $N$ jets), and the notation
$\sigma_{\ge N}$ for an \emph{inclusive} $N$-jet cross section (with $N$ or more
jets). Three possible methods for evaluating the
uncertainties in exclusive jet cross sections are:
%%%
\begin{itemize}
\item[A)] ``Direct Exclusive Scale Variation''.
  Here the uncertainties are evaluated by
  directly varying the renormalisation and factorisation scales in the
  fixed-order predictions for each exclusive jet cross section $\sigma_N$. The
  results are taken as $100\%$ correlated, such that when adding the exclusive jet
  cross sections one recovers the scale variation in the total cross
  section.
\item[B)] ``Combined Inclusive Scale Variation'', as proposed in
  \Bref{Stewart:2011cf}.  Here, the perturbative uncertainties in the
  inclusive $N$-jet cross sections, $\sigma_{\geq N}$, are treated as the
  primary uncertainties that can be evaluated by scale variations in fixed-order
  perturbation theory. These uncertainties are treated as uncorrelated for
  different $N$.  The exclusive $N$-jet cross sections are obtained using
  $\sigma_N = \sigma_{\geq N} - \sigma_{\geq N+1}$. The uncertainties and correlations
  follow from standard error propagation, including the appropriate
  anticorrelations between $\sigma_N$ and $\sigma_{N\pm 1}$ related to the
  division into jet bins.
\item[C)] ``Uncertainties from Resummation with Reweighting.''  Resummed
  calculations for exclusive jet cross sections can provide uncertainty
  estimates that allow one to simultaneously include both types of correlated and
  anticorrelated uncertainties as in methods A and B. The magnitude of the uncertainties
  may also be reduced from the resummation of large logarithms.
\end{itemize}
%%%
Method B avoids a potential underestimate of the uncertainties in individual
jet bins due to strong cancellations that can potentially take place in method
A. An explicit demonstration that different treatments of the uncontrolled higher-order $\orde{\alphas^3}$ terms in method A can lead to very different LHC predictions is given in \refS{sec:vetosum}.  Method B produces realistic perturbative uncertainties for exclusive jet cross sections when using fixed-order
predictions for various processes. It is the main topic of this section, which follows
\Bref{Stewart:2011cf}. In method C one can utilise higher-order resummed
predictions for the exclusive jet cross sections, which allow one to obtain
improved central values and further refined uncertainty estimates. This is
discussed in \refS{sec:jetvetosum}. The uncertainties from method B are more consistent
with the information one gains about jet-binning effects from using resummation.

For method B the theoretical motivation from the basic structure of the
perturbative series is outlined in \refS{jetbin_motivation}. An
implementation for the example of the $0$-jet and $1$-jet bins is given in
\refS{jetbin_01jet}.  If theory predictions are required for irreducible
backgrounds in jet bins, then the same method should be used to evaluate the
perturbative uncertainties, for \eg in the case of $\PH\to \PW\PW$, the $\Pq\bar \Pq\to
\PW\PW$ and $\Pg\Pg\to \PW\PW$ direct production channels. 

Note that here we are only discussing the theoretical uncertainties due to
unknown higher-order perturbative corrections, which are commonly estimated
using scale variation. Parametric uncertainties, such as PDF and $\alphas$
uncertainties, must be treated appropriately as common sources for all
investigated channels. 


%===============================================================================
\subsubsection{Theoretical motivation}
\label{jetbin_motivation}
%===============================================================================

To start, we consider the simplest example of dividing the total cross
section, $\sigma_\total$, into an \emph{exclusive} $0$-jet bin,
$\sigma_0(p^\cut)$, and the remaining \emph{inclusive} $(\geq 1)$-jet bin,
$\sigma_{\geq 1}(p^\cut)$,
%%%
\begin{align} \label{eq:pcut}
\sigma_\total = \int_0^{p^\cut}\!d p\, \frac{d\sigma}{d p} + \int_{p^\cut}\!d p\, \frac{d\sigma}{d p}
\equiv  \sigma_0(p^\cut) + \sigma_{\geq 1}(p^\cut)
\,.\end{align}
%%%
Here $p$ denotes the kinematic variable which is used to divide up the cross
section into jet bins.  A typical example is $p \equiv \pT^\jet$, defined by the
largest $\pT$ of any jet in the event, such that $\sigma_0(\pT^\cut)$ only
contains events with jets having $\pT < \pT^\cut$, and $\sigma_{\geq
  1}(\pT^\cut)$ contains events with at least one jet with $\pT > \pT^\cut$.
The definition of $\sigma_0$ may also include dependence on rapidity, such as
only considering jets within the rapidity range $\lvert \eta^\jet\rvert <
\eta^\cut$.

The phase-space restriction defining $\sigma_0$ changes its perturbative
structure compared to that of $\sigma_\total$ and in general gives rise to an
additional perturbative uncertainty which we denote by $\Delta_\cut$. This can
be thought of as an uncertainty related to large logarithms of $p^\cut$, or more
generally as an uncertainty associated to computing a less inclusive cross
section, which is theoretically more challenging. In Eq.~\eqref{eq:pcut} both $\sigma_0$
and $\sigma_{\geq 1}$ depend on the phase-space cut, $p^\cut$, and by
construction this dependence cancels in $\sigma_0+\sigma_{\geq 1}$.  Hence, the
additional uncertainty $\Delta_\cut$ induced by $p^\cut$ must be $100\%$
anticorrelated between $\sigma_0(p^\cut)$ and $\sigma_{\geq 1}(p^\cut)$, such
that it cancels in their sum. For example, using a covariance matrix to model
the uncertainties and correlations, the contribution of $\Delta_\cut$ to the
covariance matrix for $\{\sigma_0, \sigma_{\geq 1}\}$ must be of the form
%%%
\begin{equation} \label{eq:cutmatrix}
C_\cut = \begin{pmatrix}
   \Delta_\cut^2 &  - \Delta_\cut^2 \\
   -\Delta_\cut^2 & \Delta_\cut^2
\end{pmatrix}\,.
\end{equation}
%%%
The questions then are: (1) How can we estimate $\Delta_\cut$ in a simple way,
and (2) how is the perturbative uncertainty $\Delta_\total$ of $\sigma_\total$
related to the uncertainties of $\sigma_0$ and $\sigma_{\geq 1}$?  To answer
these questions, we discuss the perturbative structure of the cross sections in
more detail.

By restricting the cross section to the $0$-jet region, one restricts the
collinear initial-state radiation from the colliding hard partons as well as the
overall soft radiation in the event. This restriction on additional emissions
leads to the appearance of Sudakov double logarithms of the form $L^2 =
\ln^2(p^\cut/Q)$ at each order in perturbation theory, where $Q$ is the hard
scale of the process. For Higgs production from gluon fusion, $Q = \MH$, and the
leading double logarithms appearing at $\orde{\alphas}$ are
%%%
\begin{align} \label{eq:sig0dbleL}
\sigma_0(\pT^\cut) &= \sigma_B \Bigl(1 - \frac{3\alphas}{\pi}\, 2\ln^2 \frac{\pT^\cut}{\MH} + \dotsb \Bigr)
\,,\end{align}
%%%
where $\sigma_B$ is the Born (tree-level) cross section.

The total cross section only depends on the hard scale $Q$,
which means by choosing the scale $\mu \simeq Q$, the
fixed-order expansion does not contain large logarithms and has the
structure%
\footnote{These expressions for the perturbative series are schematic.  The
  convolution with the parton distribution functions (PDFs) and $\mu$-dependent
  logarithms enter in the coefficients of the series, which are not displayed.
  (The single logarithms related to the PDF evolution are not the logarithms we
  are most interested in discussing.)  }
%%%
\begin{equation} \label{eq:sigmatot}
\sigma_\total \simeq \sigma_B\big[ 1 + \alphas + \alphas^2 + \orde{\alphas^3} \big]
\,.\end{equation}
%%%
% For $\Pg\Pg \to \PH$, the coefficients of this series can be large, corresponding to 
% the well-known large K factors.
As usual, varying the scale in $\alphas$ (and the PDFs) one obtains an estimate
of the size of the missing higher-order terms in this series, corresponding to
$\Delta_\total$.

The inclusive $1$-jet cross section has the perturbative structure
%%%
\begin{align} \label{eq:sigma1}
\sigma_{\geq 1}(p^\cut)
\simeq \sigma_B\bigl[ \alphas (L^2 + L + 1)
+ \alphas^2 (L^4 + L^3 + L^2 + L + 1) + \orde{\alphas^3 L^6} \bigr]
\,,\end{align}
%%%
where the logarithms $L = \ln(p^\cut/Q)$. For $p^\cut \muchless Q$ these logarithms
can get large enough to overcome the $\alphas$ suppression. In the limit
$\alphas L^2 \simeq 1$, the fixed-order perturbative expansion breaks down and
the logarithmic terms must be resummed to all orders in $\alphas$ to obtain a
meaningful result. For typical experimental values of $p^\cut$ fixed-order
perturbation theory can still be considered, but the logarithms cause large
corrections at each order and dominate the series. This means varying the scale
in $\alphas$ in Eq.~\eqref{eq:sigma1} tracks the size of the large logarithms and
therefore allows one to get an estimate of the size of missing higher-order
terms caused by $p^\cut$, which corresponds to the uncertainty $\Delta_\cut$.
Therefore, we can approximate $\Delta_\cut = \Delta_{\geq 1}$, where
$\Delta_{\geq 1}$ is obtained from the scale variation for $\sigma_{\geq 1}$.

The exclusive $0$-jet cross section is equal to the
difference between Eqs.~\eqref{eq:sigmatot} and \eqref{eq:sigma1}, and so has the schematic structure
%%%
\begin{align} \label{eq:sigma0}
\sigma_0 & (p^\cut) = \sigma_\total - \sigma_{\geq1}(p^\cut)
\nonumber\\
&\simeq \sigma_B \Bigl\{ \bigl[ 1 + \alphas + \alphas^2 + \orde{\alphas^3} \bigr]
- \bigl[\alphas (L^2 \!+ L + 1) + \alphas^2 (L^4 \!+ L^3 \!+ L^2 \!+ L + 1)
% \nonumber\\ & \qquad\quad
+ \orde{\alphas^3 L^6} \bigr] \Bigr\}
\,.\end{align}
%%%
In this difference, the large positive corrections in $\sigma_\total$ partly
cancel against the large negative logarithmic corrections in $\sigma_{\geq 1}$.
For example, at $\orde{\alphas}$ there is a value of $L$ for which the
$\alphas$ terms in Eq.~\eqref{eq:sigma0} cancel exactly. At this $p^\cut$ the NLO $0$-jet
cross section has vanishing scale dependence and is equal to the LO cross
section, $\sigma_0(p^\cut)=\sigma_B$.  Due to this cancellation, a standard use
of scale variation in $\sigma_0(p^\cut)$ does not actually probe the size of the
large logarithms, and thus is not suitable to estimate $\Delta_\cut$. This issue
impacts the uncertainties in the experimentally relevant region for $p^\cut$.

For example, for $\Pg\Pg \to \PH$ (with $\sqrt{s} =7\UTeV$, $\MH = 165\UGeV$, $\muF = \muR =
\MH/2$), one finds~\cite{Anastasiou:2004xq, Anastasiou:2005qj, Catani:2007vq, Grazzini:2008tf}
%%%
\begin{align}
\sigma_\total &= (3.32 \pb) \bigl[1 + 9.5\,\alphas + 35\,\alphas^2 + \orde{\alphas^3} \bigr]
\,,\nonumber\\
\sigma_{\geq 1}\bigl(\pT^\jet >30\UGeV, \abs{\eta^\jet} < 3.0\bigr)
&= (3.32 \pb) \bigl[4.7\,\alphas + 26\,\alphas^2 + \orde{\alphas^3} \bigr] \,.
\end{align}
%%%
In $\sigma_\total$ one can see the impact of the well-known large $K$ factors.
(Using instead $\muF = \muR = \MH$ the $\alphas$ and $\alphas^2$
coefficients in $\sigma_\total$ increase to $11$ and $65$.)  In $\sigma_{\geq 1}$,
one can see the impact of the large logarithms on the perturbative series.
Taking their difference to get $\sigma_0$, one observes a sizeable numerical
cancellation between the two series at each order in $\alphas$.

Since $\Delta_\cut$ and $\Delta_\total$ are by definition uncorrelated, by
associating $\Delta_\cut = \Delta_{\geq 1}$ we are effectively treating
the perturbative series for $\sigma_\total$ and $\sigma_{\geq 1}$ as independent
with uncorrelated perturbative uncertainties. That is, considering
$\{\sigma_\total, \sigma_{\geq 1}\}$, the covariance matrix is diagonal,
%%%
\begin{equation} \label{eq:diagmatrix}
\begin{pmatrix}
  \Delta_\total^2 & 0 \\ 0 & \Delta_{\geq1}^2
\end{pmatrix}
\,,\end{equation}
%%%
where $\Delta_\total$ and $\Delta_{\geq 1}$ are
evaluated by separate scale variations in the fixed-order predictions for
$\sigma_\total$ and $\sigma_{\geq 1}$.
This is consistent, since for small $p^\cut$ the two series have very different
structures. In particular, there is no reason to believe that the same
cancellations in $\sigma_0$ will persist at every order in perturbation theory
at a given $p^\cut$. It follows that the perturbative uncertainty in
$\sigma_0 = \sigma_\total - \sigma_{\geq 1}$ is given by
$\Delta_\total^2 + \Delta_{\geq 1}^2$, and the resulting
covariance matrix for $\{\sigma_0, \sigma_{\geq 1}\}$ is
%%%
\begin{equation} \label{eq:fullmatrix}
C = \begin{pmatrix}
   \Delta_{\geq 1}^2 + \Delta_\total^2 &  - \Delta_{\geq 1}^2 \\
   -\Delta_{\geq 1}^2 & \Delta_{\geq 1}^2
\end{pmatrix}\,.
\end{equation}
%%%
The $\Delta_{\geq 1}$
contributions here are equivalent to Eq.~\eqref{eq:cutmatrix} with $\Delta_\cut =
\Delta_{\geq 1}$. Note also that all of $\Delta_\total$
occurs in the uncertainty for $\sigma_0$.  This is reasonable from the point of
view that $\sigma_0$ starts at the same order in $\alphas$ as $\sigma_\total$ and
contains the same leading virtual corrections.


\begin{figure}[t!]
\includegraphics[width=0.5\textwidth]{YRHXS2_ggF/YRHXS2_ggHsigpTcut_7_165_eta30_FOA_lxl}%
\hfill%
\includegraphics[width=0.5\textwidth]{YRHXS2_ggF/YRHXS2_ggHsigpTcut_7_165_eta30_FOB_lxl}%
\vspace{-0.5ex}
\caption{\label{fig:0jet} Fixed-order perturbative uncertainties for $\Pg\Pg \to \PH +
  0$ jets at NLO and NNLO. On the left, the uncertainties are obtained from the
  direct exclusive scale variation in $\sigma_0(\pT^\cut)$ between $\mu = \MH/4$
  and $\mu = \MH$ (method A). On the right, the uncertainties are obtained by
  independently evaluating the inclusive scale uncertainties in $\sigma_\total$
  and $\sigma_{\geq 1}(p^\cut)$ and combining them in quadrature (method B). The
  plots are taken from \Ref~\cite{Stewart:2011cf}.}
\end{figure}

\begin{figure}[t!]
\includegraphics[width=0.5\textwidth]{YRHXS2_ggF/YRHXS2_ggHfracpTcut_7_165_eta30_FOA_lxl}%
\hfill%
\includegraphics[width=0.5\textwidth]{YRHXS2_ggF/YRHXS2_ggHfracpTcut_7_165_eta30_FOB_lxl}%
\vspace{-0.5ex}
\caption{\label{fig:0jetfrac} Same as \refF{fig:0jet} but for the $0$-jet fraction $f_0(\pT^\cut) = \sigma_0(\pT^\cut)/\sigma_\total$.}
\end{figure}


The limit $\Delta_\cut = \Delta_{\geq 1}$ that Eq.~\eqref{eq:fullmatrix} is based on is of
course not exact. However, the preceding arguments show that it is a more
reasonable starting point than using a common scale variation for the different
jet bins as in method A, since the latter does not account for the additional $p^\cut$ induced
uncertainties. These two methods of evaluating the perturbative uncertainties
are contrasted in \refF{fig:0jet} for $\Pg\Pg \to \PH+0$ jets at NLO (light gray)
and NNLO (dark gray) as a function of $\pT^\cut$ (using $\mu=\MH/2$ for the
central scale choice). The left panel shows the uncertainties from method A obtained from a
direct scale variation by a factor of two in $\sigma_0(\pT^\cut)$.
For small values of $\pT^\cut$ the cancellations that take place in
$\sigma_0(p^\cut)$ cause the error bands to shrink and eventually vanish at
$\pT^\cut\simeq 25\UGeV$, where there is an almost exact cancellation between the
two series in Eq.~\eqref{eq:sigma0}. In contrast, in the right panel the uncertainties are
obtained using the above method B by combining the independent inclusive
uncertainties to obtain the exclusive uncertainty, $\Delta_0^2 =\Delta_\total^2 +
\Delta_{\ge 1}^2$. For large values of $\pT^\cut$ this reproduces the direct
exclusive scale variation, since $\sigma_{\geq 1}(p^\cut)$ becomes small. On the
other hand, for small values of $\pT^\cut$ the uncertainties estimated in this
way are more realistic, because they explicitly estimate the uncertainties due
to the large logarithmic corrections. The features of this plot are quite
generic. In particular, the same pattern of uncertainties is observed for the
Tevatron, when using $\mu=\MH$ as our central scale (with $\mu=2\MH$ and
$\mu=\MH/2$ for the range of scale variation), whether or not we only look at
jets at central rapidities, or when considering the exclusive $1$-jet cross
section.  We also note that using independent variations for $\muF$ and $\muR$
does not change this picture, in particular the $\muF$ variation for fixed
$\muR$ is quite small.  In \refF{fig:0jetfrac} we again compare the two methods, but now for the event
fraction $f_0(\pT^\cut)=\sigma_0(\pT^\cut)/\sigma_\total$. 
%At large $\pT^\cut$ the curves asymptote to one and to zero uncertainty.  
At large $\pT^\cut$ the curves approach unity, and the uncertainty asymptotically vanishes.
At small $\pT^\cut$ the
uncertainties in $f_0$ are quite small with method A. In method B the
uncertainties are more realistic, and there is now a significant overlap between
the bands at NLO and NNLO.

The generalisation of the above discussion to more jets and several jet bins is
straightforward.  For the $N$-jet bin we replace $\sigma_\total \to \sigma_{\ge
  N}$, $\sigma_0\to \sigma_N$, and $\sigma_{\ge 1} \to \sigma_{\ge N+1}$, and
take the appropriate $\sigma_B$. If the perturbative series for $\sigma_{\ge N}$
exhibits large $\alphas$ corrections due to its logarithmic series or
otherwise, then the presence of a different series of large logarithms in
$\sigma_{\ge N+1}$ will again lead to cancellations when we consider the
difference $\sigma_N = \sigma_{\geq N} - \sigma_{\geq N+1}$. Hence,
$\Delta_{\geq N+1}$ will again give a better estimate for the extra
$\Delta_\cut$ type uncertainty that arises from separating $\sigma_{\geq N}$
into $\sigma_N$ and $\sigma_{\geq N+1}$. Plots for the $1$-jet bin for Higgs
production from gluon fusion can be found in \Bref{Stewart:2011cf}.


%===============================================================================
\subsubsection{Example implementation for $H+0$ Jet and $H+1$ jet channels}
\label{jetbin_01jet}
%===============================================================================

To illustrate the implications for a concrete example we consider the $0$-jet
and $1$-jet bins together with the remaining $(\ge 2)$-jet bin. By construction
only neighboring jet bins are correlated, so the generalisation to more jet bins
is not any more complicated. We denote the total inclusive cross section by
$\sigma_\total$, and the inclusive $1$-jet and $2$-jet cross sections by
$\sigma_{\geq 1}$ and $\sigma_{\geq 2}$. Their respective absolute uncertainties
are $\Delta_\total$, $\Delta_{\geq 1}$, $\Delta_{\geq 2}$, and their relative
uncertainties are given by $\delta_i = \Delta_i/\sigma_i$. The exclusive $0$-jet
cross section, $\sigma_0$, and $1$-jet cross section, $\sigma_1$, satisfy the
relations
%%%
\begin{equation} \label{eq:sigma01}
\sigma_0 = \sigma_\total - \sigma_{\geq 1}
\,,\qquad
\sigma_1 = \sigma_\mathrm{\geq 1} - \sigma_{\geq 2}
\,,\qquad
\sigma_\total = \sigma_0 + \sigma_1 + \sigma_{\geq 2}
\,.\end{equation}
%%%
Experimentally it is convenient to work with the exclusive $0$-jet and $1$-jet
fractions defined as
%%%
\begin{equation} \label{eq:f01}
f_0 = \frac{\sigma_0}{\sigma_\total}
\,,\qquad
f_1 = \frac{\sigma_1}{\sigma_\total}
\,.\end{equation}
%%%

Treating the inclusive uncertainties $\Delta_\total$, $\Delta_{\geq 1}$, $\Delta_{\geq 2}$ as uncorrelated, the covariance matrix for the three quantities $\{\sigma_\total, \sigma_0, \sigma_1 \}$ is given by
%%%
\begin{equation} \label{eq:Cov}
C =
\begin{pmatrix}
\Delta_\total^2 & \Delta_\total^2 & 0 \\
\Delta_\total^2 & \Delta_\total^2 + \Delta_{\geq 1}^2 & -\Delta_{\geq1}^2 \\
0 &-\Delta_{\geq1}^2 & \Delta_{\geq 1}^2 + \Delta_{\geq 2}^2
\end{pmatrix}
\,.\end{equation}
%%%
The relative uncertainties and correlations for $\sigma_0$ and $\sigma_1$ directly follow from Eq.~\eqref{eq:Cov}. Writing them in terms of the relative quantities $f_i$ and $\delta_i$, one gets
%%%
\begin{align} \label{eq:deltasigma01}
\delta(\sigma_0)^2 &= \frac{1}{f_0^2}\, \delta_\total^2 + \Bigl(\frac{1}{f_0} - 1\Bigr)^2 \delta_{\geq 1}^2
\,,\nonumber\\
\delta(\sigma_1)^2  &= \Bigl(\frac{1-f_0}{f_1}\Bigr)^2 \delta_{\geq 1}^2 +
\Bigl(\frac{1-f_0}{f_1} - 1\Bigr)^2 \delta_{\geq 2}^2
\,,\nonumber\\
\rho(\sigma_0, \sigma_\total)
&= \biggl[1 + \frac{\delta_{\geq 1}^2}{\delta_\total^2}(1-f_0)^2 \biggr]^{-1/2}
\,,\nonumber\\
\rho(\sigma_1, \sigma_\total) &= 0
\,,\nonumber\\
\rho(\sigma_0, \sigma_1)
&= - \biggl[1 + \frac{\delta_\total^2}{\delta_{\geq 1}^2}\, \frac{1}{(1-f_0)^2}\biggr]^{-1/2}
% \nonumber\\ & \quad\times
\biggl[1 + \frac{\delta_{\geq2}^2}{\delta_{\geq 1}^2} \Bigl(1 - \frac{f_1}{1-f_0}\Bigr)^2\biggr]^{-1/2}
% \,,\nonumber\\
% \rho(\sigma_0, \sigma_{\geq 2}) &= 0
% \,,\nonumber\\
% \rho(\sigma_1, \sigma_{\geq 2})
% &= -\biggl[1 + \frac{\delta_{\geq 1}^2}{\delta_{\geq2}^2} \Bigl(1 - \frac{f_1}{1-f_0}\Bigr)^{-2} \biggr]^{-1/2}
\,.\end{align}
%%%
Alternatively, we can use $\{\sigma_\total, f_0, f_1\}$ as the three independent
quantities. Their relative uncertainties and correlations following from
Eq.~\eqref{eq:Cov} are then
%%%
\begin{align} \label{eq:deltaf01}
\delta(f_0)^2
&= \Bigl(\frac{1}{f_0} - 1 \Bigr)^2\bigl(\delta_\total^2 + \delta_{\geq 1}^2\bigr)
\,,\nonumber\\
\delta(f_1)^2
&= \delta_\total^2 + \Bigl(\frac{1-f_0}{f_1}\Bigr)^2 \delta_{\geq 1}^2 +
\Bigl(\frac{1-f_0}{f_1} - 1\Bigr)^2 \delta_{\geq 2}^2
\,,\nonumber\\
\rho(f_0, \sigma_\total)
&= \biggl[1 + \frac{\delta_{\geq 1}^2}{\delta_\total^2} \biggr]^{-1/2}
\,,\nonumber\\
\rho(f_1, \sigma_\total)
&= -\frac{\delta_\total}{\delta(f_1)}
\,,\nonumber\\
\rho(f_0, f_1)
&= -\biggl(1 + \frac{1-f_0}{f_1}\frac{\delta_{\geq 1}^2}{\delta_\total^2} \biggr)
\Bigl(\frac{1}{f_0} - 1\Bigr) \frac{\delta_\total^2}{\delta(f_0)\delta(f_1)}
\,.\end{align}
%%%
The basic steps of the analysis are the same irrespective of the statistical model:
\begin{enumerate}
\item Independently evaluate the inclusive perturbative uncertainties
  $\delta_\total$, $\delta_{\geq 1}$, $\delta_{\geq 2}$ by appropriate scale
  variations in the available fixed-order calculations.
\item Consider the $\delta_i$ uncorrelated and use Eqs.~\eqref{eq:sigma01} or \eqref{eq:f01} to propagate them into the uncertainties of $\sigma_{0,1}$ or $f_{0,1}$.
\end{enumerate}
For example, when using log-likelihoods, the independent uncertainties
$\delta_\total$, $\delta_{\geq 1}$, $\delta_{\geq 2}$ are implemented via three
independent nuisance parameters.

In Step 1, $\delta_\total$ can be taken from the NNLO perturbative uncertainty in
the first Yellow Report~\cite{Dittmaier:2011ti}, while $\delta_{\geq 1}$ is evaluated at
relative NLO and $\delta_{\geq 2}$ at relative LO using any of {\sc HNNLO}~\cite{Catani:2007vq,
Grazzini:2008tf}, {\sc FEHiP}~\cite{Anastasiou:2004xq, Anastasiou:2005qj}, or
{\sc MCFM}~\cite{Campbell:2010cz}. For the central scales $\muF = \muR = \MH/2$ should be used to be
consistent with $\sigma_\total$ from \Bref{Dittmaier:2011ti}.  Numerically
$\sigma_{\geq 1}$ also exhibits better convergence with this choice for the
central scale.  Note that $\sigma_{\geq 2}$ is defined by inverting the cut on
the second jet in $\sigma_1$ and is not independent of $\{\sigma_\total$,
$\sigma_0$, $\sigma_1\}$. Its per-cent uncertainty $\delta_{\ge 2}$ naturally
enters because of the theory correlation model and for
this purpose it should be taken at LO. Also note that $\sigma_{\geq 2}$ is
different from the $2$-jet bin, $\sigma_2$.  If $\sigma_2$ is included one
extends the matrix by an extra row/column, and uses as input that the
uncertainties in $\sigma_\total$, $\sigma_{\ge 1}$, $\sigma_{\ge 2}$, $\sigma_{\ge
  3}$ are independent.

When $\sigma_2$ is used in the analysis with additional vector-boson fusion
cuts, one can determine the uncertainties with the appropriate $\sigma_{\geq 2}$
at NLO and $\sigma_{\geq 3}$ at LO from MCFM~\cite{Campbell:2010cz}. To combine the $\sigma_2$ with
additional vector-boson-fusion cuts with the $0$-jet and $1$-jet gluon-fusion
bins, one treats the VBF uncertainty as uncorrelated with those from gluon
fusion, and assigns a $100\%$ correlation between the perturbative uncertainties
of the two different gluon-fusion $\sigma_{\geq 2}$s that appear in these
computations.

Step 2 above also requires values for $f_0$ and $f_1$ as input. In the
experimental analyses, the central values for $f_0$ and $f_1$ effectively come
from a full Monte Carlo simulation. By only using relative quantities, the
different overall normalisations in the fixed-order calculation and the Monte Carlo
drop out. However, the values for $f_0$ and $f_1$ obtained from the fixed-order
calculation can still be quite different from those in the Monte Carlo, because
the fixed-order results incorporate NNLO $\alphas$ corrections, while the Monte
Carlo includes some resummation as well as detector effects.
A priori the values for $f_{0,1}$ obtained either way could be used as input
for the uncertainty calculation in Step 2. One can cross check
that the two choices lead to similar results for the final uncertainties.

It is useful to illustrate this with a numerical example corresponding to
\refF{fig:0jet}, for which we take $\pT^\cut = 30\UGeV$ and
$\eta^\cut = 3.0$.  For $\MH = 165\UGeV$, we have $\sigma_\total = (8.76
\pm 0.80)\Upb$, $\sigma_{\geq1} = (3.10\pm0.61)\Upb$, and
$\sigma_{\geq 2} = (0.73 \pm 0.42)\Upb$, corresponding to the relative
uncertainties $\delta_\total = 9.1\%$, $\delta_{\geq 1} = 19.9\%$, and
$\delta_{\geq 2} = 57\%$. Using these as inputs in Eq.~\eqref{eq:deltasigma01}
we find for the exclusive jet cross sections $\delta(\sigma_0) = 18\%$,
$\delta(\sigma_1) = 31\%$ with correlations coefficients $\rho(\sigma_0,
\sigma_\total) = 0.79$, $\rho(\sigma_1, \sigma_\total) = 0$, and $\rho(\sigma_0,
\sigma_1) = -0.50$. We see that $\sigma_0$ and $\sigma_1$ have a substantial
negative correlation because of the jet bin boundary they share.  For the
exclusive jet fractions using Eq.~\eqref{eq:deltaf01} we obtain $\delta(f_0) =
12\%$ and $\delta(f_1) = 33\%$ with correlations $\rho(f_0, \sigma_\total) =
0.42$, $\rho(f_1, \sigma_\total) = -0.28$, and $\rho(f_0,f_1) = -0.84$.  The
presence of $\sigma_\total$ in the denominator of the $f_i$'s yields a nonzero
anticorrelation for $f_1$ and $\sigma_\total$, and a decreased correlation for
$f_0$ and $\sigma_\total$ compared to $\sigma_0$ and $\sigma_\total$.  In contrast
to these results, in method A one considers the direct scale variation in the exclusive
$\sigma_{0,1}$ as in the left panel of \refF{fig:0jet}. Due
to the cancellations between the perturbative series, this approach leads to
unrealistically small uncertainties (with the above inputs $\delta(\sigma_0) =
3.2\%$ and $\delta(\sigma_1) = 8.3\%$), which is reflected in the pinching of
the bands in the left plot in \refF{fig:0jet}.

As a final remark, we note that strictly speaking, it is somewhat inconsistent
to use the relative uncertainties for $f_{0,1}$ from fixed order and apply them to the Monte
Carlo predictions for $f_{0,1}$. A more sophisticated treatment would be to
first correct the measurements for detector effects to truth-level using Monte
Carlo. In the second step, the corrected measurements of $\sigma_0$ and
$\sigma_1$ (with truth-level cuts) can be directly compared to the available
calculations, taking into account the theoretical uncertainties as described
above. This also automatically makes a clean separation between experimental and
theoretical uncertainties. Given the importance and impact of the jet selection
cuts, this is very desirable.  It would both strengthen the robustness of the
extracted experimental limits and validate the theoretical description of the
jet selection.

