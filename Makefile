LATEX    = latex
BIBTEX   = bibtex
DVIPS    = dvips

BASENAME = YRHXS2

default: testlatex

testlatex:
	latex  ${BASENAME}
	latex  ${BASENAME}
	bibtex ${BASENAME}
	latex  ${BASENAME}
	latex  ${BASENAME}
	dvips -j0 ${BASENAME}.dvi -o ${BASENAME}.ps
	ps2pdf13 -sPAPERSIZE=a4 ${BASENAME}.ps
#	dvips -o ${BASENAME}.ps ${BASENAME}.dvi  
#	dvipdf -sPAPERSIZE=a4 -dPDFSETTINGS=/prepress ${BASENAME}
#	dvipdf -sPAPERSIZE=letter -dPDFSETTINGS=/prepress ${BASENAME}
#
# commit LaTeX outputs to SVN
	svn ci -m "update YRHXS2.pdf" YRHXS2.pdf
#	svn ci -m "update YRHXS2.ps " YRHXS2.ps
#	svn ci -m "update YRHXS2.dvi" YRHXS2.dvi
#	svn ci -m "update YRHXS2.bbl" YRHXS2.bbl
#	svn ci -m "update YRHXS2.blg" YRHXS2.blg
#	svn ci -m "update YRHXS2.log" YRHXS2.log
#	svn ci -m "update YRHXS2.aux" YRHXS2.toc
#	svn ci -m "update YRHXS2.aux" YRHXS2.aux
#	echo "Successfully committed LaTeX outputs to SVN."

testpdflatex:
	pdflatex  ${BASENAME}
	pdflatex  ${BASENAME}
	bibtex    ${BASENAME}
	pdflatex  ${BASENAME}
	pdflatex  ${BASENAME}

#
# standard Latex targets
#

%.dvi:	%.tex 
	$(LATEX) $<

%.bbl:	%.tex *.bib
	$(LATEX) $*
	$(BIBTEX) $*

%.ps:	%.dvi
	$(DVIPS) $< -o $@

%.pdf:	%.tex
	$(PDFLATEX) $<

.PHONY: clean

clean:
	rm -f *.aux *.log *.bbl *.blg *.brf *.cb *.ind *.idx *.ilg *.inx *.dvi *.toc *.out *~ ~* spellTmp 

