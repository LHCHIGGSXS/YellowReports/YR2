%\def\alphas{\alpha_S}
\def\ltap{\raisebox{-.6ex}{\rlap{$\,\sim\,$}}\raisebox{.4ex}{$\,<\,$}}
\def\gtap{\raisebox{-.4ex}{\rlap{$\,\sim\,$}} \raisebox{.4ex}{$\,>\,$}}
\def\Wcal{{\cal W}}


\subsection{The Higgs $\pT$ spectrum and its uncertainties\footnote{D. de Florian, G.Ferrera, M.Grazzini and D.Tommasini.}}
\label{sec:hqt}


\subsubsection{Introduction}
\label{sec:hqt-intro}

In this section we focus on the
transverse-momentum ($\pT$) spectrum of the SM Higgs boson.
This observable is of direct importance in the experimental search.
A good knowledge of the $\pT$ spectrum can help to define strategies
to improve the statistical significance.
When studying the $\pT$ distribution 
of the Higgs boson in QCD perturbation theory it is convenient to distinguish two regions of
transverse momenta.
In the large-$\pT$ region ($\pT\sim \MH$), where the transverse momentum is
of the order of the Higgs-boson mass $\MH$,
perturbative QCD
calculations based on the truncation of the perturbative series at a
fixed order in $\alphas$ 
are theoretically justified.
In this region, the $\pT$ spectrum is known up to leading order (LO) \cite{Baur:1989cm}
with the full dependence of the masses of the
top and bottom quarks, and up to the next-to-leading
order (NLO) \cite{deFlorian:1999zd,Ravindran:2002dc,Glosser:2002gm}
in the large-$\Mt$ limit.

In the small-$\pT$ region ($\pT\muchless\MH$),
where the bulk of the events is produced, 
the convergence of the fixed-order expansion is spoiled by the presence of 
large logarithmic terms, $\alphas^n\ln^m (\MH^2/\pT^2)$.
%In order 
To obtain reliable predictions, 
these logarithmically-enhanced terms
have to be systematically
resummed to all
perturbative orders \cite{Dokshitzer:1978yd,Dokshitzer:1978hw,Parisi:1979se,Curci:1979bg,Collins:1981uk,Collins:1981va,Collins:1984kg,Kodaira:1981nh,Catani:1988vd,Catani:2000vq,Bozzi:2005wk,Catani:2010pd}.
It is then important to consistently match the resummed and fixed-order
calculations 
at intermediate values of $\pT$, in order
to obtain accurate QCD predictions for the entire range of transverse momenta.

The resummation of the logarithmically enhanced terms is effectively (approximately) performed
by standard Monte Carlo event generators.
In particular, MC@NLO \cite{Frixione:2002ik} and POWEG \cite{Nason:2004rx} combine soft-gluon resummation through the parton shower
with the LO result valid at large $\pT$, thus achieving a result with formal NLO accuracy.

The numerical program {\sc HqT} \cite{Bozzi:2005wk}
implements
soft-gluon resummation up to NNLL
accuracy \cite{deFlorian:2000pr} combined with fixed-order perturbation theory up to NLO in the large-$\pT$ region \cite{Glosser:2002gm}.
The program is used by the Tevatron and LHC experimental collaborations
to reweight the $\pT$ spectrum of the Monte Carlo event generators used in the analysis and
is thus of direct relevance in the Higgs-boson search.

The program {\sc HqT} is based on the transverse-momentum resummation formalism described
in \Brefs{Catani:2000vq, Bozzi:2005wk, Catani:2010pd}, 
which is valid for %can be applied to 
a generic process in which
a high-mass system of non strongly-interacting particles is produced 
in hadron--hadron collisions.

The phenomenological analysis presented 
in \Bref{Bozzi:2005wk} has been recently extended in \Bref{deFlorian:2011xf}. In particular,
the exact values of the NNLO hard-collinear coefficients ${\cal H}_N^{\PH(2)}$
\Brefs{Catani:2007vq,Catani:2011kr} and of the 
NNLL coefficient $A^{(3)}$ \cite{Becher:2010tm} have been implemented.
The ensuing calculation of the $\pT$ spectrum 
is implemented in the updated version of the numerical code {\sc HqT}, 
which can be downloaded from \Bref{hqt}.

\subsubsection{Transverse-momentum resummation}
\label{sec:theory}


In this section we briefly recall the 
main points of the transverse-momentum resummation approach proposed in  
\Brefs{Catani:2000vq,Bozzi:2005wk,Catani:2010pd},
in the case of a Higgs boson $\PH$  produced
by gluon fusion. As recently pointed out in \Bref{Catani:2010pd},
the gluon fusion $\pT$-resummation formula has a 
structure different from the resummation formula for $\PQq\PAQq$ 
annihilation. The difference originates from the 
collinear correlations that are a specific feature 
of the perturbative evolution of colliding
hadrons into gluon partonic initial states.
These gluon collinear correlations produce, in the small-$\pT$ region,
coherent spin correlations between the helicity
states of the initial-state gluons and definite azimuthal-angle correlations
 between the final-states particles of the observed high-mass system.
Both these kinds of correlations have no analogue for $\PQq\PAQq$ annihilation
processes in the small-$\pT$ region.
In the case of Higgs-boson production, $\PH$ being a scalar particle,
the azimuthal correlations vanish and only gluon spin correlations are
present \cite{Catani:2010pd}.
 
We consider the inclusive hard-scattering process
\begin{equation}
\Pp(p_1) + \Pp(p_2) \;\to\; \PH (\MH,\pT ) + X ,   
\label{first}
\end{equation}
where the protons with momenta
$p_1$ and $p_2$ collide to produce the Higgs boson $\PH$ of mass
$\MH$ and transverse momentum $\pT$,
and $X$ is an arbitrary and undetected final state. 

According to the QCD factorisation theorem
the corresponding transverse-momentum 
differential cross 
section $d\sigma_{\PH}/d\pT^2$ can be written as
\begin{equation}
\label{dcross}
\frac{d\sigma_{\PH}}{d \pT^2}(\pT,\MH,s)= \sum_{a,b}
\int_0^1 dx_1 dx_2 \,f_{a/h_1}(x_1,\muF^2)
\,f_{b/h_2}(x_2,\muF^2) \;
\frac{d{\hat \sigma}_{H,ab}}{d \pT^2}(\pT, \MH,{\hat s};
\alphas(\muR^2),\muR^2,\muF^2) 
\;\;,
\end{equation}
where $f_{a/h}(x,\muF^2)$ ($a=q,{\bar q}, g$)
are the parton densities of the colliding hadron $h$ 
at the factorisation scale $\muF$, 
$d\hat\sigma_{H,ab}/d{\pT^2}$ are the perturbative QCD 
%computable
partonic cross sections, % computables
%in QCD perturbation theory, 
$s$ ($\hat s = x_1 x_2 s$) 
%${\sqrt s}$ ($\hat s = x_1 x_2 s$)
is the square of the 
hadronic (partonic) centre-of-mass  energy, 
and $\muR$ is the renormalisation 
scale. 

In the region where 
$\pT \sim  \MH$,
the QCD perturbative
series is controlled by a small expansion parameter, 
$\alphas(\MH)$,
and fixed-order calculations are
theoretically justified. In this region, 
the QCD radiative corrections are known up to
NLO \cite{deFlorian:1999zd,Ravindran:2002dc,Glosser:2002gm}. 
In the small-$\pT$ region 
($\pT\muchless\MH$),
the convergence of the fixed-order
perturbative expansion is hampered
by the presence 
of large logarithmic terms, 
%$\alpha_S^n\ln^m \MH^2/\pT^2$.
$\alphas^n\ln^m (\MH^2/\pT^2)$ (with $1\leq m \leq 2n-1)$.
To obtain reliable predictions these terms have to be resummed to all orders.

The resummation is addressed
at the level of the partonic cross section, which
is decomposed~as
\begin{equation}
\label{resplusfin}
\frac{d{\hat \sigma}_{\PH,ab}}{d\pT^2}=
\frac{d{\hat \sigma}_{\PH,ab}^{(\rm res.)}}{d\pT^2}
+\frac{d{\hat \sigma}_{\PH,ab}^{(\rm fin.)}}{d\pT^2}\; .
\end{equation}
The first term on the right-hand side
includes all the logarithmically-enhanced contributions, at small $\pT$,
and has to be evaluated to all orders in $\alphas$.
The second term
is free of such contributions and
can thus be computed at fixed order in the perturbative expansion. 
%
To correctly take into account the kinematic constraint of
transverse-momentum conservation, the resummation program has to be carried out
in the impact parameter space $b$. 
Using the Bessel transformation between the conjugate variables 
$\pT$ and  $b$,
the resummed component $d{\hat \sigma}^{({\rm res.})}_{\PH,ac}$
can be expressed as
\begin{equation}
\label{resum}
\frac{d{\hat \sigma}_{\PH,ac}^{(\rm res.)}}{d\pT^2}(\pT,\MH,{\hat s};
\alphas(\muR^2),\muR^2,\muF^2) 
= %\f{\MH^2}{\hat s} \;
\int_0^\infty db \; \frac{b}{2} \;J_0(b \pT) 
\;\Wcal^{\PH}_{ac}(b,\MH,{\hat s};\alphas(\muR^2),\muR^2,\muF^2) \;,
\end{equation}
where $J_0(x)$ is the $0$th-order Bessel function.
%By taking
The resummation structure of $\Wcal^{\PH}_{ac}$ can %indeed 
be organised in exponential form
considering
the Mellin $N$-moments $\Wcal^{\PH}_N$ of $\Wcal^{\PH}$ with respect to the variable 
$z=\MH^2/{\hat s}$ at fixed 
$\MH$\,\footnote{For the sake of simplicity the resummation
formulae is written only for 
the specific case of 
the diagonal terms in the flavour space. 
In general, the exponential
is replaced by an exponential matrix with respect
to the partonic indices (a 
detailed discussion of the general case can be found in
\Ref~\cite{Bozzi:2005wk}).},
\begin{align}
\label{wtilde}
\Wcal^{\PH}_{N}(b,\MH;\alphas(\muR^2),\muR^2,\muF^2)
&={\cal H}_{N}^{\PH}\left(\MH, 
\alphas(\muR^2);\MH^2/\muR^2,\MH^2/\muF^2,\MH^2/Q^2
\right) \nonumber \\
&\times \exp\{{\cal G}_{N}(\alphas(\muR^2),L;\MH^2/\muR^2,\MH^2/Q^2
)\}
\;\;,
\end{align}
were we have defined the logarithmic expansion parameter 
$L\equiv \ln ({Q^2 b^2}/{b_0^2})$,
and $b_0=2e^{-\gamma_E}$ ($\gamma_E=0.5772...$ 
is the Euler number).

The scale $Q\sim \MH$, appearing in the right-hand side of Eq.~(\ref{wtilde}), 
named resummation scale \cite{Bozzi:2005wk}, 
parameterises the
arbitrariness in the resummation procedure.
The  form factor $\exp\{ {\cal G}_N\}$ is 
{\itshape universal} and contains all
the terms $\alphas^nL^m$ with $1 \leq m \leq 2n$, 
that are logarithmically divergent 
as $b \to \infty$ (or, equivalently, $\pT\to 0$).
The exponent ${\cal G}_N$ 
can  be systematically expanded as
\begin{align}
\label{exponent}
{\cal G}_{N}(\alphas, L;\MH^2/\muR^2,\MH^2/Q^2)&=L 
\;g^{(1)}(\alphas L)+g_N^{(2)}(\alphas L;\MH^2/\muR^2,\MH^2/Q^2)\nonumber\\
&+\frac{\alphas}{\pi} g_N^{(3)}(\alphas L;\MH^2/\muR^2,\MH^2/Q^2)
%+\dots
+{\cal O}(\alphas^n L^{n-2})
\end{align}
where the term $L\, g^{(1)}$ resums the leading logarithmic (LL) 
contributions $\alphas^nL^{n+1}$, the function $g_N^{(2)}$ includes
the NLL contributions $\alphas^nL^{n}$ \cite{Catani:1988vd}, 
$g_N^{(3)}$ controls the NNLL 
terms $\alphas^nL^{n-1}$ \cite{deFlorian:2000pr,Becher:2010tm},
and so forth. The explicit form of the functions
$g^{(1)}$, $g_N^{(2)}$, and $g_N^{(3)}$ can be found in \Bref{Bozzi:2005wk}.

The {\itshape process-dependent} function ${\cal H}_N^{\PH}$ 
does not depend on the impact parameter $b$ and 
includes all the perturbative
terms that behave as constants as $b \to \infty$. 
It can thus be expanded in powers of $\alphas=\alphas(\muR^2)$:
\begin{align}
\label{hexpan}
{\cal H}_N^{\PH}(\MH,\alphas;\MH^2/\muR^2,\MH^2/\muF^2,\MH^2/Q^2)&=
\sigma_{\PH}^{(0)}(\alphas,\MH)
\Bigl[ 1+ \frac{\alphas}{\pi} \,{\cal H}_N^{\PH,(1)}(\MH^2/\muF^2,\MH^2/Q^2) 
\Bigr. \nonumber \\
&\hspace*{-5em}
+ \Bigl.
\left(\frac{\alphas}{\pi}\right)^2 
\,{\cal H}_N^{\PH,(2)}(\MH^2/\muR^2,\MH^2/\muF^2,\MH^2/Q^2)+
%\dots 
{\cal O}(\alphas^3)
\Bigr] \;\;,
\end{align}
where $\sigma_{\PH}^{(0)}(\alphas,\MH)$
is the partonic cross section at the Born level.
The
first order ${\cal H}_{N}^{\PH,(1)}$ \cite{Kauffman:1991cx}
and the second order ${\cal H}_{N}^{\PH,(2)}$ \cite{Catani:2007vq,Catani:2011kr}
coefficients  in Eq.~(\ref{hexpan}),
for the case
of Higgs-boson production in the large-$\Mt$ approximation, are known.

To reduce the impact of unjustified higher-order contributions in 
the large-$\pT$ region,
the logarithmic variable $L$ in Eq.~(\ref{wtilde}), 
which diverges for $b\to 0$, 
is actually replaced  by 
${\widetilde L}\equiv \ln \left({Q^2 b^2}/{b_0^2}+1\right)$ \cite{Bozzi:2005wk, Bozzi:2003jy}.
The variables $L$ and ${\widetilde L}$ are equivalent when $Qb\gg 1$ 
(i.e. at small values of $\pT$), but they 
lead to a different behaviour
of the form factor at small values of $b$. % (i.e.\ large values of $\pT$).
An important
consequence of this replacement 
is that, after inclusion of the finite component,  
we exactly recover the fixed-order perturbative value of the total cross section
upon integration of the $\pT$  distribution over $\pT$.

The finite component of the transverse-momentum cross section $d\sigma_{\PH}^{({\rm fin.})}$
(see Eq.~(\ref{resplusfin}))
 does not contain large logarithmic terms
in the small-$\pT$ region,
it can thus be evaluated by truncation of the perturbative series
at a given fixed order.

In summary,
 to carry out the resummation at NLL+LO accuracy, we need the
inclusion of the functions $g^{(1)}$, $g_N^{(2)}$,
${\cal H}_N^{\PH,(1)}$, in Eqs.~(\ref{exponent},\ref{hexpan}),
together with the evaluation of the finite component at LO;
the addition of the functions $g_N^{(3)}$ and ${\cal H}_N^{\PH,(2)}$, together 
with the finite component at NLO (i.e.\ at relative ${\cal O}(\alphas^2)$)
leads to the NNLL+NLO 
accuracy.
We point out that our best theoretical prediction
(NNLL+NLO) includes the {\em full} NNLO 
perturbative contribution in the small-$\pT$ region plus
the NLO correction at large $\pT$.
In particular,  the NNLO  result for the total cross section  
is exactly recovered upon integration
over $\pT$ of the differential cross section $d \sigma_{\PH}/d\pT$ at NNLL+NLO
accuracy.


Finally we recall 
that the resummed form factor 
$\exp \{{\cal G}_N(\alphas(\muR^2),{\widetilde L})\}$
has a singular behaviour, related to the presence of the Landau 
pole in the QCD running coupling, at 
the values of $b$ where $\alphas(\muR^2) {\widetilde L} > \pi/\beta_0$ 
($\beta_0$ is the first-order coefficient of the QCD $\beta$ function).
To perform 
the  inverse Bessel
transformation with respect to the impact parameter $b$ a prescription
is thus necessary.
We follow the regularisation prescription of
\Refs~\cite{Laenen:2000de,Kulesza:2002rh}: 
the singularity is avoided by deforming the 
integration contour in the complex $b$ space.


\subsubsection{Results}
\label{sec:results}

The results we are going to present are obtained with an updated version of the 
numerical code {\sc HqT} \cite{hqt}. 
The new version of this code was improved with respect
to the one used in \Ref~\cite{Bozzi:2005wk}. 
The main differences regard the implementation  
of the exact value of the second-order coefficients  
${\cal H}_{N}^{\PH,(2)}$
computed in \Bref{Catani:2007vq}
and the use of the recently derived 
value of the coefficient $A^{(3)}$ \cite{Becher:2010tm},
which  contributes to NNLL accuracy
(the results in \Bref{Bozzi:2005wk} were obtained by using the 
$A^{(3)}$  value from threshold resummation \cite{Moch:2004pa}). 
Detailed results for the $\pT$ spectrum at the Tevatron and the LHC are presented in \Bref{deFlorian:2011xf}. Here we focus on the uncertainties of the $\pT$ spectrum at the LHC.

The calculation is performed strictly in the large-$\Mt$ approximation.
Effects beyond this approximation were discussed in \refS{sec:finitemass}.

The hadronic $\pT$ cross section at NNLL+NLO accuracy
is computed
by using NNLO parton distributions functions (PDFs)
with $\alphas(\muR^2)$ evaluated at 3-loop order.
We use the MSTW2008 parton densities
unless otherwise stated.

As discussed in \refS{sec:theory}, the 
resummed calculation depends on the factorisation and 
renormalisation scales and on the resummation scale $Q$. 
Our convention to compute factorisation 
and renormalisation scale uncertainties is to consider
independent variations of $\muF$ and $\muR$ by a factor of two around 
the central values $\muF=\muR=\MH$
(i.e. we consider the range $\MH/2< \{\muF,\muR\}< 2\,\MH$), with the constraint
$0.5 < \muF/\muR < 2$. 
Similarly, we follow \Bref{Bozzi:2005wk} and
choose $Q=\MH/2$ as central value of the resummation scale,
considering scale variations in the 
range $\MH/4 < Q < \MH$.

We focus on the uncertainties on the {\em normalised} $\pT$ spectrum (i.e., $1/\sigma \times d\sigma/d\pT$). As mentioned in \refS{sec:hqt-intro}, the
typical procedure of the experimental collaborations is to use the information
on the total cross section \cite{Dittmaier:2011ti} to rescale the best theoretical predictions of Monte Carlo
event generators, whereas the
NNLL+NLO result for the spectrum, obtained with the public program {\sc HqT},
is used to reweight the transverse-momentum
spectrum of the Higgs boson in the simulation.
Such a procedure implies that the important information provided by the resummed NNLL+NLO spectrum
is not its integral, i.e.\ the total cross section, but its {\em shape}.
The sources of uncertainties on the shape of the spectrum are essentially the same
as for the inclusive cross section: the uncertainty from missing higher-order contributions,
estimated through scale variations, PDF uncertainties,
and the uncertainty from the use of the large-$\Mt$ approximation, which is discussed in \Section~\ref{sec:finitemass}.
One additional uncertainty in the $\pT$ spectrum that needs be considered comes from non-perturbative (NP) effects.

It is known that the transverse-momentum
distribution is affected by NP effects, which become important as $\pT$ becomes small.
A customary way of modelling these effects is to introduce an NP transverse-momentum smearing
of the distribution. In the case of resummed calculations in impact-parameter space,
the NP smearing is implemented by multiplying the $b$-space perturbative
form factor by an NP form factor.
The parameters controlling this NP form factor are typically obtained through a comparison to data.
%Since there is no evidence for the Higgs boson yet, 
Since the Higgs boson has not been discovered yet,
the way to fix the NP form factor is somewhat arbitrary.
Here we follow the procedure adopted in \Brefs{Bozzi:2005wk,deFlorian:2011xf}, and we multiply the resummed form factor in Eq.~(\ref{resum}) by a gaussian smearing $S_{NP}=\exp\{-g b^2\}$, where the parameter $g$ is taken in the range ($g=1.67{-}5.64\UGeV^2$) suggested by the study of 
\Bref{Kulesza:2003wi}.
The above procedure can give us some insight on the quantitative impact of these NP effects on the Higgs-boson spectrum.

In \refF{gghqt_fig} (left panels) we compare the NNLL+NLO shape uncertainty as coming from scale variations (solid lines)
to the NP effects (dashed lines) for $\MH=120\UGeV$ and $\MH=165\UGeV$.
Scale variations are performed as follows:
we independently vary $\muF,\muR$ and $Q$
in the ranges
$\MH/2< \{\muF,\muR\} < 2\MH$ and $\MH/4< Q< \MH$,
with the constraints $0.5 < \muF/\muR < 2$ and 
$0.5 < Q/\muR < 2$.
The bands are obtained in practice by normalizing each spectrum to unity, and computing the
relative difference with respect to the central normalised prediction obtained
with the MSTW2008 NNLO set (with $g=0$).
In other words, studying uncertainties on the normalised distribution allows
us to assess the true uncertainty in the shape of the resummed $\pT$ spectrum.

We see that, both for $\MH=120\UGeV$ and for $\MH=165\UGeV$, the scale uncertainty ranges from about $\pm 4\%$ at $\pT\sim 10\UGeV$ to $\pm 5\%$ at $\pT\sim 70\UGeV$.
As $\pT$ increases, the scale uncertainty rapidly increases.
This should not be considered as particularly worrying, since for large
transverse momenta, the resummed result looses predictivity,
and should be replaced by the standard fixed-order result.
The impact of NP effects ranges from about $10\%$ to $20\%$
in the very small-$\pT$ region ($\pT\ltap 10\UGeV$),
is about $3{-}4\%$ for $\pT\sim 20\UGeV$, and quickly decreases as $\pT$ increases.
We conclude that the uncertainty from unknown NP effects is smaller than the scale uncertainty,
and is comparable to the latter only in the very-small-$\pT$ region.

The impact of PDF uncertainties at $68\%$ CL on the shape of the $\pT$ spectrum
is studied in \refFs{gghqt_fig} (right panels).
By evaluating PDF uncertainties with MSTW2008 NNLO PDFs
we see that the uncertainty is at the $\pm (1{-}2)\%$ level.
The use of different PDF sets affects not only the absolute value of the NNLO cross section (see, \eg \Bref{Watt:2011kp}), but also the shape of the $\pT$ spectrum. The predictions obtained with NNPDF~2.1 PDFs are in good agreement with those obtained with the MSTW2008 set, and
the uncertainty bands overlap over a wide range of transverse momenta, both for $\MH=120\UGeV$ and $\MH=165\UGeV$.
On the contrary, the prediction obtained with the ABKM09 NNLO set is softer,
and the uncertainty band does not overlap with the MSTW2008 band.
This behaviour is not completely unexpected: when the Higgs boson is produced
at large transverse momenta, larger values of Bjorken $x$ are probed, where the ABKM gluon is smaller than MSTW2008 one.
The JR09 band shows a good compatibility with the MSTW2008 result
where the uncertainty is, however, rather large.

%%====================================
\begin{figure}[htb]
\vspace{5pt}
\begin{center}
\begin{tabular}{cc}
\includegraphics[width=.46\linewidth]{YRHXS2_ggF/YRHXS2_gghqt_fig1.ps} &
\includegraphics[width=.46\linewidth]{YRHXS2_ggF/YRHXS2_gghqt_fig2.ps}\\
\includegraphics[width=.46\linewidth]{YRHXS2_ggF/YRHXS2_gghqt_fig3.ps} &
\includegraphics[width=.46\linewidth]{YRHXS2_ggF/YRHXS2_gghqt_fig4.ps}\\
\end{tabular}
\end{center}
\caption{\label{gghqt_fig} Uncertainties in the shape of the $\pT$ spectrum: scale uncertanties compared with NP effects (left panels); PDF uncertainties (right panels).}
\label{fig:comp}
\end{figure}
%%====================================



