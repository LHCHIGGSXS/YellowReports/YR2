
\subsection{Monte Carlo and resummation for exclusive jet bins%
\footnote{I. W. Stewart, F. St\"ockli, F. J. Tackmann and W. J. Waalewijn.}}

\label{sec:jetvetosum}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%===============================================================================
\subsubsection{Overview}
%===============================================================================


In this section we discuss the use of predictions for exclusive jet cross
sections that involve resummation for the jet-veto logarithms,
$\ln(p^\cut/\MH)$, induced by the jet cut parameter $p^\cut$ shown in
Eq.~\eqref{eq:pcut} of \refS{sec:jetveto}.  It is common practice to
account for the $p^\cut$ dependence in exclusive $H+\text{0 jet}$ cross sections
using Monte Carlo programs such as {\sc Pythia}, MC@NLO, or POWHEG, which include a
resummation of at least the leading logarithms (LL). One may also improve the
accuracy of spectrum predictions by reweighting the Monte Carlo to resummed
predictions for the Higgs $q_T$ spectrum (see \refS{sec:hqt}),
which starts to differ from the
jet-veto spectrum at ${\cal O}(\alphas^2)$. The question then remains how to
assess theoretical uncertainties, and three methods (A, B, and C) were outlined
in \refS{sec:jetveto}.

In this section we consider assessing the
perturbative uncertainties when using resummed predictions for variables $p^\cut$
that implement a jet veto, corresponding to method C. An advantage of
using these resummed predictions with method C is that they contain perturbation theory
scale parameters which allow for an evaluation of two components of the
theory error, one which is $100\%$ correlated with the total cross section (as in
method A), and one related to the presence of the jet-bin cut which is
anti-correlated between neighboring jet bins (as in method B). Our discussion of
the correlation matrix obtained from method C follows
\Bref{Stewart:2011cf}.

We consider two choices for the jet-veto variable, the standard $\pT^\jet$
variable with a rapidity cut $|\eta| < \eta^\cut$ (using anti-$k_T$ with $R = 0.5$),
and the beam thrust variable~\cite{Stewart:2009yx}, which is a rapidity-weighted $H_T$, defined as
%%%
\begin{equation} \label{eq:Tau_def}
\Tcm = \sum_k | \vec p_{Tk}| e^{-\abs{\eta_k}} = \sum_k (E_k - |p_k^z|)
\,.\end{equation}
%%%
The sum here is over all objects in the final state except the Higgs decay products, and
can in principle be considered over particles, topo-clusters, or
jets with a small $R$ parameter. We make use of resummed predictions for
$H+0$ jets from gluon fusion at next-to-next-to-leading logarithmic order with
NNLO fixed-order precision (NNLL+NNLO) from Ref.~\cite{Berger:2010xi}. The
resulting cross section $\sigma_0(\Tcmc)$ has the jet veto implemented by a cut
$\Tcm< \Tcmc$.  This cross section contains a resummation of large logarithms
at two orders beyond standard LL parton-shower programs, and also includes the
correct NNLO corrections for $\sigma_0(\Tcmc)$ for any cut.

A similar resummation for the case of $\pT^\jet$ is not available. Instead, we
use MC@NLO and reweight it to the resummed predictions in $\Tcm$ including
uncertainties and then use the reweighted Monte Carlo sample to obtain 
cross-section predictions for the standard jet veto, $\sigma_0(\pT^\cut)$. We will
refer to this as the reweighted NNLL+NNLO result. Since the Monte Carlo here is
only used to provide a transfer matrix between $\Tcm$ and $\pT^\jet$, and both
variables implement a jet veto, one expects that most of the improvements from
the higher-order resummation are preserved by the reweighting. However, we
caution that this is not equivalent to a complete NNLL+NNLO result for the
$\pT^\cut$ spectrum, since the reweighting may not fully capture effects
associated with the choice of jet algorithm and other effects that enter at this
order for $\pT^\cut$. The dependence on the Monte Carlo transfer matrix also
introduces an additional uncertainty, which should be studied and is not
included in our numerical results below. (We have checked that varying the
fixed-order scale in MC@NLO between $\mu = \MH/4$ and $\mu = 2\MH$ has a very
small effect on the reweighted results.) The transfer matrix is obtained at
the parton level, without hadronisation or underlying event, since we are
reweighting a partonic NNLL+NNLO calculation. In all our results
we consistently use MSTW2008 NNLO PDFs.

In \refS{jetbin_resum} we discuss the determination of perturbative
uncertainties in the resummed calculations (method C), and compare them with those at NNLO
obtained from a direct exclusive scale variation (method A) and from combined
inclusive scale variation (method B) for both $\sigma_0(\pT^\cut)$ and
$\sigma_0(\Tcmc)$. In \refS{jetbin_resumandMC} we compare the predictions
for the 0-jet bin event fraction at different levels of resummation, comparing
results from NNLO, MC@NLO, and the (reweighted) NNLL+NNLO analytic results.


%===============================================================================
\subsubsection{Uncertainties and correlations from resummation}
\label{jetbin_resum}
%===============================================================================

The resummed $H+\text{0-jet}$ cross section predictions of
Ref.~\cite{Berger:2010xi} follow from a factorisation theorem for the $0$-jet
cross section~\cite{Stewart:2009yx}, $\sigma_0(\Tcmc) = H\, {\cal I}_{gi}\,{\cal
  I}_{gj}\otimes S f_i f_j$, where $H$ contains hard virtual effects, the ${\cal
  I}$s and $S$ describe the veto-restricted collinear and soft radiation, and
the $f$s are standard parton distributions. Fixed-order perturbation theory is
carried out at three scales, a hard scale $\mu_H^2\sim \MH^2$ in $H$, and beam
and soft scales $\mu_B^2\sim \MH\Tcmc$ and $\mu_S^2\sim (\Tcmc)^2$ for ${\cal
  I}$ and $S$, and are then connected by NNLL renormalisation group evolution
that sums the jet-veto logarithms, which are encoded in ratios of these scales.
The perturbative uncertainties can be assessed by considering two sources: i) an
overall scale variation that simultaneously varies $\{\mu_H,\mu_B,\mu_S\}$ up
and down by a factor of two which we denote by $\Delta_{H0}$, and ii) individual
variations of $\mu_B$ or $\mu_S$ that each hold the other two scales
fixed~\cite{Berger:2010xi}, whose envelope we denote by the uncertainty
$\Delta_{SB}$. Here $\Delta_{H0}$ is dominated by the same sources of
uncertainty as the total cross section $\sigma_\total$, and hence should be
considered $100\%$ correlated with its uncertainty $\Delta_\total$. The uncertainty
$\Delta_{SB}$ is only present due to the jet bin cut, and hence gives the
$\Delta_\cut$ uncertainty discussed in \refS{sec:jetveto} that is
anti-correlated between neighboring jet bins.

If we simultaneously consider the cross sections $\{\sigma_0, \sigma_{\ge 1}\}$ then the full
correlation matrix with method C is
%%%
\begin{align} \label{eq:resummatrix}
C &=
\begin{pmatrix}
 \Delta_{SB}^2 &  - \Delta_{SB}^2 \\
-\Delta_{SB}^2 & \Delta_{SB}^2
\end{pmatrix}
+%,
\begin{pmatrix}
\Delta_{H0}^2 &  \Delta_{H0}\,\Delta_{H\geq 1}  \\
\Delta_{H0}\,\Delta_{H\geq 1} & \Delta_{H\geq 1}^2
\end{pmatrix}
,\end{align}
%%%
where $\Delta_{H\geq 1} =\Delta_\total - \Delta_{H0}$ encodes the $100\%$ correlated
component of the uncertainty for the $(\ge 1)$-jet inclusive cross section.
Computing the uncertainty in $\sigma_\total$ gives back $\Delta_\total$.
Eq.~\eqref{eq:resummatrix} can be compared to the corresponding correlation
matrix from method A, which would correspond to taking $\Delta_{SB}\to 0$ and
obtaining the analog of $\Delta_{H0}$ by up/down scale variation without
resummation ($\mu_H=\mu_B=\mu_S$).  It can also be compared to method B, which
would correspond to taking $\Delta_{SB}\to \Delta_{\ge 1}$ and $\Delta_{H\ge
  1}\to 0$, such that $\Delta_{H0}\to \Delta_\total$. Using method C captures
both of the types of uncertainty that appear in methods A and B. Note that the
numerical dominance of $\Delta_{SB}^2$ over $\Delta_{H0}\Delta_{H\ge 1}$ in the
$0$-jet region is another way to justify the
preference for using method B when given a choice between methods A and B.
For example, for $\pT^\cut = 30\UGeV$ and $|\eta^\jet| < 5.0$ we have
$\Delta_{SB}^2 = 0.17$ and $\Delta_{H0}\Delta_{H\ge 1} = 0.02$.
From \eqr{resummatrix} it is straightforward to derive the uncertainties and
correlations in method C when considering the $0$-jet event fraction, $\{\sigma_\total,f_0\}$,
in place of the jet cross sections. We will discuss results for $f_0(\pT^\cut)$
and $f_0(\Tcmc)$ in \refS{jetbin_resumandMC} below.

In \refF{fig:3scales} we show the uncertainties $\Delta_{SB}$ (light green)
and $\Delta_{H0}$ (medium blue) as a function of the jet-veto variable, as well
as the combined uncertainty adding these components in quadrature (dark orange).
From the figure we see
that the $\mu_{H0}$ dominates at large values where the veto is turned off and
we approach the total cross section, and that the jet-cut uncertainty $\Delta_{SB}$
dominates for the small cut values that are typical of experimental analyses
with Higgs jet bins. The same pattern is observed in the left panel which
directly uses the NNLL+NNLO predictions for $\Tcmc$, and the right panel which
shows the result from reweighting these predictions to $\pT^\cut$ as explained above.

\begin{figure}
\includegraphics[width=0.5\textwidth]{YRHXS2_ggF/YRHXS2_ggHsigTauBcmcut_7_165_DelSBH_rel_lxl}%
\hfill%
\includegraphics[width=0.5\textwidth]{YRHXS2_ggF/YRHXS2_ggHsigpTcut_7_165_eta50_DelSBH_rel_lxl}%
\vspace{-0.5ex}
\caption{\label{fig:3scales} Relative uncertainties for the 0-jet bin cross
  section from resummation at NNLL+NNLO for beam thrust $\Tcm$ on the left and
  $\pT^{\rm jet}$ on the right.}
\end{figure}

\begin{figure}
\includegraphics[width=0.5\textwidth]{YRHXS2_ggF/YRHXS2_ggHsigTauBcmcut_7_165_compABC_rel_lxl}%
\hfill%
\includegraphics[width=0.5\textwidth]{YRHXS2_ggF/YRHXS2_ggHsigpTcut_7_165_eta50_compABC_rel_lxl}%
\vspace{-0.5ex}
\caption{\label{fig:ABC} Comparison of uncertainties for the
  0-jet bin cross section for beam thrust $\Tcm$ on the left and $\pT^{\rm jet}$ on
  the right. Results are shown at NNLO using methods A and B (direct exclusive
  scale variation and combined inclusive scale variation as discussed in
  \refS{sec:jetveto}), and for the NNLL+NNLO resummed result (method C).
  All curves are normalised relative to the NNLL+NNLO central value.}
\end{figure}

A comparison of the uncertainties for the 0-jet bin cross section from methods A
(medium green), B (light green), and C (dark orange) is shown in
\refF{fig:ABC}, where the results are normalised to the highest-order result
to better show the relative differences and uncertainties.  The NNLO
uncertainties in methods A and B are computed in the manner discussed in
\refS{sec:jetveto}. The uncertainties in method C are the combined
uncertainties from resummation given by $\sqrt{\Delta_{H0}^2+\Delta_{SB}^2}$ in
\refF{fig:3scales}.  In the left panel we use $\Tcmc$ as jet-veto
variable and full results for the NNLO and NNLL+NNLO cross sections, while in
the right panel we use $\pT^\cut$ as jet-veto variable with the full NNLO and
the reweighted NNLL+NNLO results. One observes that the resummation of the large
jet-veto logarithms lowers the cross section in both cases. For cut values
$\gtrsim 25\UGeV$ the relative uncertainties in the resummed result and the
reduction in the resummed central value compared to NNLO are similar for both
jet-veto variables. One can also see that the NNLO uncertainties from method B
are more consistent with the higher-order NNLL+NNLO-resummed results than those
in method A.

From \refF{fig:ABC} we observe that the uncertainties in method C including
resummation (dark orange bands) are reduced by about a factor of two compared
to those in method B (light green bands).
Since the $0$-jet bin plays a crucial role in the $\PH\to \PW\PW$ channel for Higgs
searches, and these improvements will also be reflected in uncertainties for the
$1$-jet bin, the improved theoretical precision obtained with method C has the
potential to be quite important.

%%%
\begin{table}[t]
\tabcolsep 10pt
  \centering
  \begin{tabular}{c c c c }
  \hline\hline
  & method A & method B & method C \\
  \hline
  $\delta\sigma_0(\pT^\cut)$         & $3\%$  & $19\%$  & $9\%$ \\
  $\delta\sigma_{\ge 1}(\pT^\cut)$   & $19\%$ & $19\%$  & $14\%$ \\
  $\rho(\sigma_\total,\sigma_0)$       & $1$    & $0.78$  & $0.15$ \\
  $\rho(\sigma_\total,\sigma_{\ge 1})$ & $1$    & $0$     & $0.65$ \\
  $\rho(\sigma_0,\sigma_{\ge 1})$    & $1$    & $-0.63$ & $-0.65$ \\
\hline
  $\delta f_0(\pT^\cut)$             & $6\%$  & $13\%$  & $9\%$ \\
  $\delta f_{\ge 1}(\pT^\cut)$       & $10\%$ & $21\%$  & $11\%$ \\
  $\rho(\sigma_\total,f_0)$            & $-1$   & $0.43$  & $-0.38$ \\
  $\rho(\sigma_\total,f_{\ge 1})$      & $1$    & $-0.43$ & $0.38$ \\
  \hline
  \end{tabular}
  \caption{Example of relative uncertainties $\delta$ and correlations $\rho$ obtained for the
    LHC at $7\,{\rm TeV}$ for $\pT^\cut=30\,{\rm GeV}$ and $\lvert\eta^\jet\rvert < 5.0$.
    (Method A is shown for illustration only and should not be used for the reasons discussed in
    \refS{sec:jetveto}.)
    }
\label{tab:corrcoeffs}
\end{table}
%%%
%
To appreciate the effects of the different methods on the correlation matrix we
consider as an example the results for $\pT^\cut = 30\UGeV$ and
$\lvert\eta^\jet\rvert < 5.0$.  The inclusive cross sections are $\sigma_\total
= (8.76 \pm 0.80)\,\mathrm{pb}$ at NNLO, and $\sigma_{\geq1} =
(3.10\pm0.61)\,\mathrm{pb}$ at NLO. The relative uncertainties and correlations
at these cuts for the three methods are shown in \refT{tab:corrcoeffs}.
The numbers for the cross sections are also translated into the equivalent
results for the event fractions, $f_0(\pT^\cut)=\sigma_0(\pT^\cut)/\sigma_\total$
and $f_{\ge 1}(\pT^\cut) = \sigma_{\ge 1}(\pT^\cut)/\sigma_\total$. Note that 
method A should not be used for the reasons discussed in detail in \refS{sec:jetveto},
which are related to the lack of a contribution analogous to $\Delta_{SB}$ in this method, and
the resulting very small and underestimated $\delta\sigma_0$. In methods B and C we
see, as expected, that $\sigma_0$ and $\sigma_{\geq 1}$ have a substantial
anti-correlation due to the jet-bin boundary they share.

In \refS{sec:jetveto}, method B was discussed for
$\{\sigma_\total,\sigma_0,\sigma_1\}$, where we also account for the jet-bin
boundary between $\sigma_1$ and $\sigma_{\ge 2}$. The method C results discussed
here so far are relevant for the jet-bin boundary between $\sigma_0$ and
$\sigma_{\ge 1}$.  To also separate $\sigma_{\ge 1}$ into a $1$-jet bin
$\sigma_1$ and a $\sigma_{\ge 2}$ one can simply use method B for this boundary
by treating $\Delta_{\ge 2}$ as uncorrelated with the total uncertainty
$\Delta_{\ge 1}$ from method C. Once it becomes available one can also use a
resummed prediction with uncertainties for this boundary with method C.


%===============================================================================
\subsubsection{Comparison of NNLO, MC@NLO, and resummation at NNLL+NNLO}
\label{jetbin_resumandMC}
%===============================================================================

In this section we compare the results for the $0$-jet event fraction $f_0$ from
different theoretical methods including various levels of logarithmic
resummation. We use the event fraction for this comparison since it is the
quantity used in experimental analyses and what is typically provided by the
Monte Carlo. We compare three different results using both $\pT^{\rm jet}$ and
beam thrust as jet-veto variables:
\begin{enumerate}
\item Fixed-order perturbation theory at NNLO without resummation, where the
  uncertainties are evaluated using method B.
\item MC@NLO, which includes the LL (and partial NLL) resummation provided by
  the parton shower. For the uncertainties we use the relative NNLO
  uncertainties evaluated using method B.
\item Resummation at NNLL+NNLO, with the uncertainties provided by the
  resummation (method C). For beam thrust we directly compare to the full result
  at this order. For $\pT^{\rm \jet}$ we use the resummed beam-thrust result
  reweighted to $\pT^{\rm jet}$ using Monte Carlo as explained at the beginning
  of this section.
\end{enumerate}


The comparison for $f_0(\Tcmc)$ is shown in \refF{fig:f0Taucm} and for
$f_0(\pT^\jet)$ in \refF{fig:f0pTjet}.  The left panels in each case show
the $0$-jet event fractions, and the right panels normalise these same results
to the highest-order curve to illustrate the relative differences and
uncertainties. Since the parton shower includes the resummation of the leading
logarithms, we expect the MC@NLO results to show a behavior similar to the
NNLL-resummed result, which is indeed the case. For both variables, the MC@NLO
central value is near the upper edge of the NNLL+NNLO uncertainty band (dark
orange band).  From \refF{fig:f0pTjet} we see that the uncertainties
assigned to the MC@NLO results via method B (light blue band) include the
NNLL+NNLO central value for $f_0(\pT^\cut)$.\footnote{The blue uncertainty bands
  for the MC@NLO curves are cut off at very small cut values only because the
  NNLO cross section diverges there and so its relative uncertainties are no
  longer meaningful.  This happens for cut values well below the region of
  experimental interest.} We also see that the uncertainties for $f_0(\pT^\cut)$
in method C are reduced by roughly a factor of two compared to MC@NLO with
method B. This is the analog of the observation that we made for
$\sigma_0(\pT^\cut)$ in \refF{fig:ABC} when comparing NNLO method B and
method C uncertainties.

From the $\delta f_0$ plots in \refF{fig:f0Taucm} and \refF{fig:f0pTjet} we
observe that the impact of the resummation on the NNLL+NNLO central value
compared to the NNLO central value without resummation is similar for both
jet-veto variables for cut values $\gtrsim 25\UGeV$. In this region the MC@NLO
central value lies closer to the NNLO than the NNLL+NNLO for both variables.
However, it is hard to draw conclusions on the impact of resummation by only
comparing MC@NLO and NNLO since they each contain a different set of
higher-order corrections beyond NLO.  For smaller cut values $\lesssim 25\UGeV$,
the two variables start to behave differently, and the NNLL+NNLO resummation
has a larger impact relative to NNLO when cutting on $\Tcm$ than when cutting on
$\pT^\cut$.

To understand these features in more detail one has to take into account two different
effects arising from the two ways in which these jet-veto variables differ.
First, the objects are weighted differently according to their rapidity in the
two jet-veto variables. For $\Tcm$ the particles are weighted by $e^{-|\eta|}$,
while for $\pT^\jet$ no weighting in $\eta$ takes place. Second, the way in
which the cut restriction is applied to the objects in the final state is
different. By cutting on $\Tcm$, the restriction is applied to the sum over all
objects (either particles or jets) in the final state, while by cutting on the
leading $\pT^\jet$ the restriction is applied to the maximum value of all
objects (after an initial grouping into jets with small radius). To disentangle
these two effects we consider two additional jet-veto variables: $H_T$ which
inclusively sums over all object $|\pT|$s in the same way as $\Tcm$ does, but
without the rapidity weighting, and also ${\cal T}_{\rm jet}$, the largest
individual beam thrust of any jet, which has the same object treatment as
$\pT^\jet$, but with the beam-thrust rapidity weighting. The effect of the
different rapidity weighting already appears in the LL series for the jet veto,
i.e., at ${\cal O}(\alphas)$ the coefficient of the leading double logarithm is
a factor of two larger for $\pT^\cut$ than for $\Tcmc$, $\sigma_0(\pT^\cut)
\propto 1-6\alphas/\pi \ln^2(\pT^\cut/\MH)+\ldots$ versus $\sigma_0(\Tcmc)
\propto 1-3\alphas/\pi \ln^2(\Tcmc/\MH)+\ldots$. In contrast, the LL series is
the same for $H_T$ and $\pT^\jet$, and for $\Tcm$ and ${\cal T}_\jet$. The
larger logarithms for $\pT^\cut$ than $\Tcmc$ are reflected by the fact that
$\sigma_0(\pT^\cut)$ is noticeably smaller than $\sigma_0(\Tcmc)$ at equal cut
values. For the same type of object restriction the effect of the resummation
follows the pattern expected from the leading logarithms: The resummation has a
larger impact for $\pT^\jet$ than ${\cal T}_{\rm jet}$, and also for $H_T$ than
$\Tcm$.  On the other hand, a cut on the (scalar) sum of objects is always a
stronger restriction than the same cut on the maximum value of all objects,
since the former restricts wide-angle radiation more. As a result the
resummation has more impact on $\Tcm$ than on ${\cal T}_{\rm jet}$, and also on
$H_T$ than on $\pT^\jet$.  From what we observe in \refF{fig:f0Taucm}
and \refF{fig:f0pTjet}, these two competing effects appear to approximately
balance each other for $\pT^\jet$ and $\Tcm$ for cut values $\gtrsim 25\UGeV$.


\begin{figure}
\includegraphics[width=0.5\textwidth]{YRHXS2_ggF/YRHXS2_ggHfracTauBcmcut_7_165_compMCatNLOB_lxl}%
\hfill%
\includegraphics[width=0.5\textwidth]{YRHXS2_ggF/YRHXS2_ggHfracTauBcmcut_7_165_compMCatNLOB_rel_lxl}%
\vspace{-0.5ex}
\caption{\label{fig:f0Taucm} Comparison of the $0$-jet fraction for different levels of resummation using beam thrust as the jet-veto variable. Results are shown at NNLO (using method B uncertainties), MC@NLO (using the relative NNLO uncertainty from method B), and for the analytic NNLL+NNLO resummed result.}
\end{figure}


\begin{figure}
\includegraphics[width=0.5\textwidth]{YRHXS2_ggF/YRHXS2_ggHfracpTcut_7_165_eta50_compMCatNLOB_lxl}%
\hfill%
\includegraphics[width=0.5\textwidth]{YRHXS2_ggF/YRHXS2_ggHfracpTcut_7_165_eta50_compMCatNLOB_rel_lxl}%
\vspace{-0.5ex}
\caption{\label{fig:f0pTjet} Comparison of the $0$-jet fraction for different levels of resummation using $\pT^\jet$ with $\abs{\eta^\jet} < 5.0$ as the jet-veto variable. Results are shown at NNLO (using method B uncertainties), MC@NLO (using the relative NNLO uncertainty from method B), and for the reweighted NNLL+NNLO resummed result.}
\end{figure}


To summarise, our results provide an important validation of using method B with
relative uncertainties obtained at NNLO and applying these to the event
fractions obtained from NLO Monte Carlos. While we have only compared to MC@NLO,
we expect the same to be true for POWHEG as well. By reweighting the Monte Carlo
to the NNLL+NNLO result the uncertainties in the predictions can be further
reduced to those obtained from the higher-order resummation using method C, and
this provides an important avenue for improving the analysis beyond method B.
It would also be very useful to investigate experimentally the viability of
using ${\cal T}_\jet$ as the jet-veto variable, by only summing over the jet (or
jets) with the largest individual beam thrust, as this would combine the
advantages of a jet-based variable with the theoretical control provided by the
beam-thrust resummation.
