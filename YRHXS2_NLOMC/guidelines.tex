\subsection{Guidelines for the use of
\HqT{} results to improve NLO+PS programs}\label{NLOPSsec:guidelines}

In several parts of this report regarding specific Higgs signals,
methods to reweight NLO+PS generators have been presented.
In general, reweighting is not a straightforward and free-of-risk procedure, and reaching a
final recommendation would require more studies. Here we summarise a few key 
points that should be kept in mind when setting up a reweighting procedure.

First of all, in general it is always recommended to rescale the total cross section of fully inclusive MC samples obtained via
different techniques, LO+PS, NLO+PS, matching and so on, to the best available prediction,
at NNLO, fixed-order or resummed. For differential distributions a  general strategy is, of course, not available 
and reweighting should be considered case-by-case and observable-by-observable.

An observable that has a key role in the acceptance determination and therefore
in exclusion limits is the transverse momentum distribution of the Higgs. For this 
observable the best predictions are those provided by the \HqT{} code. It is therefore
natural to suggest to reweight all events from  NLO+PS generators to the \HqT{} distributions.
Our recommendation, in this case, is to reweight at
 the level of showered events,
\emph{without} hadronisation and underlying event. This is especially important in the very low $\pT$ region,
where non-perturbative effects are sizeable. One must proceed in this
way, since \HqT{} does not include these non-perturbative effects.
Thus, reweighting the full hadron level Monte Carlo output to \HqT{} may lead to washing out small
transverse momentum effects that are determined by hadronisation and the
underlying events.

In the following discussion, we refer to the Higgs $\pT$ spectrum
obtained with the NLO+PS generator by switching off hadronisation and
underlying event as the ``shower distribution''. The same distribution
obtained with the full NLO+PS generator, including hadronisation and
underlying event, as ``hadron distribution''. These definitions work for any
NLO+PS scheme, \MCatNLO{} or \POWHEG{}. In the \POWHEG{} case, however,
one can also compute the Higgs $\pT$ using what is stored in the LHEF common block, before showering.
We will call this `` \POWHEG{}-level distribution''.

An appropriate reweighting strategy can be the following.
First of all, one determines a reweighting function as the ratio
of the \HqT{} distribution to the shower distribution.
When generating events, one should then apply the reweighting function evaluated
at the 
transverse momentum of the Higgs, as determined at the end of the shower development,
before hadronisation and underlying events are introduced.
The event is then hadronised, and the underlying event is added. The $\pT$ of
the Higgs will be modified at this stage, especially at small $\pT$.
This procedure can only be applied if the shower level output is
available on an event by event basis. This may be the case of the
fortran version of HERWIG, but it is not certainly the case of PYTHIA,
with the underlying event characterised by multiparton interaction
interleaved with the shower development.

An alternative reweighting procedure may be applied in the \POWHEG{}
case, where the Higgs transverse momentum distribution is
available also at the parton level, before the shower.
It is usually observed that the effect of the shower on this
distribution is quite mild. One then should determine
a reweighting function to be applied as a function of the Higgs
transverse momentum determined at the parton level, such that the
transverse momentum distribution of the Higgs after full shower
(but before hadronisation) matches the one computed with \HqT{}.
%{\bf Fabio: is the following iterative explanation really needed?}
It may be possible to achieve this by an iterative procedure:
one takes the ratio of the shower spectrum over the parton-level spectrum
as the initial reweighting function. One then generates events using
this reweighting function, applied however to the Higgs transverse
momentum at the parton level. This will lead to a residual mismatch
of the shower spectrum with respect to the \HqT{} spectrum,
that can be used to multiplicatively correct the reweighting
function. One keeps iterating the procedure, until convergence is
reached. Events are then generated at the full hadron level, and reweighted
on the basis of the transverse momentum at the \POWHEG{} level.

No studies on the implementation of \HqT{} reweighting according to
the above guidelines have been performed for this report, and this topic
requires further studies.

If the reweighting factor is nearly constant, the reweighting procedure
is considerably simplified, since a nearly constant factor can be
applied safely as a function of the Higgs $\pT$ at the hadron level.
We have noticed here that, with non-default value of parameters,
\POWHEG{} and \MCatNLO{} approach better the \HqT{} result. It is thus advisable
to use this settings before attempting to reweight the distributions.

One simple reweighting option is to use the NLO+PS generators with the
best settings discussed above, and reweight by a constant factor, to match
the NNLO cross section.
In this way, \HqT{} is only used to make a preferred choice of parameter
settings in the NLO+PS generator. A conservative
way to estimate the error band, in this case, would be to use the
NLO+PS scale variation band, multiplied by the
ratio of the NNLO cross section over the NLO+PS cross section
evaluated at the central value of the scales.


The preferred settings are summarised as follows.
In the  \MCatNLO{} case, we recommend to use the reference factorisation and
renormalisation scale equal to $\MH$ rather than $m_T$. In \POWHEG{},
we recommend using the $R^{s}/R^{f}$ separation, with the
{\tt powheg.input} variable {\tt hfact} set equal to $\MH/1.2$.
For the scale variation, we recommend to vary $\muR$ and $\muF$
independently by a factor of 2 above and below the reference scale,
with the constraint $0.5<\muR/\muF<2$.


\subsection{Guidelines to estimate non-perturbative uncertainties}

In this section we propose a mechanism to evaluate the impact of 
non-perturbative uncertainties related to hadronisation and underlying 
event modelling.  In the past, the former typically has been treated by
comparing \pythia\ and \herwig\ results, and analyzing, bin-by-bin, the 
effect of the different fragmentation (\ie\ parton showering) and hadronisation
schemes.  In contrast the latter often is dealt with by merely comparing a
handful of different tunes of the same program, typically \pythia.  Especially
the latter seems to be a fairly unsystematic way, in particular when taking
into account that various tunes rely on very different input data, and some
of the traditional tunes still in use did not even include LHC results (and
typically they therefore fail to describe them very well).  We therefore 
propose the following scheme which essentially relies on systematic variations
around a single tune, with a single PDF:
\begin{itemize}
\item In order to quantify hadronisation uncertainties within a model, use 
  two different tune variations around the central one, defined by producing 
  one charged particle more or less at LEP.  The difference between different
  physics assumptions entering the hadronisation model (\ie cluster vs.\ 
  Lund string hadronisation), but tuned to the same data, still needs to be 
  tested, of course, by running these models\footnote{
    In \SHERPA\ this can be achieved even on top of the {\em same}
    parton showering by contrasting a native cluster hadronisation model with
    the Lund string of \pythia, made available through an interface.  Of course,
    both models have been tuned to the same LEP data.}.
\item For the model- and tune-intrinsic uncertainties of the underlying event
  simulation we propose to systematically vary the activity, number of charged
  particles and their summed transverse momentum, in the transverse region.
  There are basically two ways of doing it, one is by increasing or decreasing
  the respective plateaus by $10$\% (this has been done in the study here);
  alternatively, one could obtain tune variations which increase or decrease
  the ``jettiness'' of the underlying event.  Effectively this amounts to
  changing the shape of the various activities in the transverse region.
\end{itemize}
While we appreciate that this way of obtaining systematic uncertainties is
somewhat cumbersome at the moment, we would like to stress that with the
advent of modern tuning tools such as \rivet-\professor \cite{Buckley:2009bj},
this is a perfectly straightforward and controlled procedure.  

At this point it should also be noted that usually different PDFs lead to
different tunes for the underlying event.  Therefore, in order to obtain
a meaningful estimate of uncertainties related to observables which are
susceptible to the underlying event, it is important to also retune its
modelling accordingly.  Just changing the PDF but keeping the underlying event 
model parameters constant will typically lead to overestimated uncertainties.
In a similar fashion, the impact of the strong coupling constant may lead
to the necessity of a retune.  This has recently been intensively discussed
in \Bref{Cooper:2011gk}\footnote{In \SHERPA\ this never was an issue, as the
  strong coupling used throughout the code is consistently given by the PDF.},
a similar discussion for the NLO+PS tools discussed here is still missing.

