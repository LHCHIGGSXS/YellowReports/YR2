
%\include{YRHXS2_mssm/ggH_acceptance}
\subsubsection{Difference in kinematic acceptance between SM and MSSM
production of $\Pg\Pg\to \PH\to\tau\tau$} 

Current LHC MSSM $\PH\to\tau\tau$ analyses~\cite{Chatrchyan:2011nx,Aad:2011rv} 
use the Standard Model generation of $\Pg\Pg\to \PH$ events with the infinite
top mass approximation as implemented in {\tt PYTHIA}~\cite{Sjostrand:2006za}
(used by CMS) or POWHEG ~\cite{Alioli:2010xd} (used by ATLAS). Both ATLAS and
CMS collaborations have presented analyses in the \mhmaxx
scenario~\cite{Carena:1999xa} as given in Eq.~\ref{YRHXS_MSSM_neutral_eq:mhmax}.
At large values of $\tan\beta$ in the \mhmaxx 
scenario $\Pg\Pg\to \PH$
production proceeds predominantly via a bottom quark loop producing a
softer $p_{T}^{\PH}$ spectrum than that predicted in the Standard Model
production dominated by the top
loop~\cite{Langenegger:2006wu,Alwall:2011cy}.
\footnote{For 
$\tan\beta \geq 12$ and $\MA = 140 \UGeV$ in the \mhmaxx scenario 
the cross-section of $\Pg\Pg \to \phi$ ($\phi = \Ph, \PH, \PA$)
production through only $\Pb$-loop~\cite{Baglio:2010ae} is less than 10\%
different from the full MSSM $\Pg\Pg \to \phi$ production
cross-section~\cite{Dittmaier:2011ti}} As a result the kinematic
acceptance of $\Pg\Pg\to \PH\to\tau\tau$ events may be overestimated in the
current LHC analyses. To determine the size of this effect,  
the {\tt PYTHIA} generated $p_{T}^{\PH}$ spectrum is re-weighted to
match the shape predicted when considering only the $\Pb$-loop
contribution to the $\Pg\Pg\to \PH$ process~\cite{Alwall:2011cy}. The
acceptance of $\PH\to\tau\tau$ events in the $e + \tau$-jet
final state in the CMS detector is then estimated before and after
re-weighting the events. 

Samples of $\Pg\Pg\to \PH\to\tau\tau$ events with $\MH = 140 \UGeV$ and
$\MH = 400 \UGeV$ are generated with {\tt PYTHIA} using 
the infinite top mass approximation and the $\tau$ leptons are decayed
with {\tt TAUOLA}. Using the $p_{T}^{\PH}$ spectra obtained with
$\Pb$-loop only ~\cite{Alwall:2011cy}, the generated events are assigned
weights 
$w_{i} = N_{i}^{\Pb-{\rm loop}} / N_{i}^{\tt Pythia}$ in $i$ bins of 
$p_{T}^{\PH}$ where $N_{i}^{\tt Pythia}$ and $N_{i}^{\Pb-{\rm loop}}$ are the
normalised event rates expected from {\tt PYTHIA} and with only $\Pb$-loop
contribution respectively. Events with $p_{T}^{\PH} < 240 \UGeV$ and 
$p_{T}^{\PH} < 300 \UGeV$ are considered for $\MH = 140 \UGeV$ and 
$\MH = 400 \UGeV$, respectively. Figure~\ref{fig:ggHAcceptancePtH}
shows the generated $p_{T}^{\PH}$ distributions for the two mass points
before and after applying the event weights.

%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\begin{center}
\includegraphics[width=0.45\textwidth]{YRHXS2_mssm/ggHAcceptancePtH140.eps} \hfill
\includegraphics[width=0.45\textwidth]{YRHXS2_mssm/ggHAcceptancePtH400.eps}
\caption{Generated $p_{T}^{\PH}$ distributions for $\MH = 140 \UGeV$ (left) and 
$\MH = 400 \UGeV$ (right) for the $\Pg\Pg \to \PH$ process generated with {\tt PYTHIA} 
(dashed) and after re-weighting (solid) to correct for $\Pg\Pg \to \PH$ production
with only $\Pb$-loop.} 
\label{fig:ggHAcceptancePtH}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Figure~\ref{fig:ggHAcceptancePtElecPtTau} shows the generated $p_{T}$ 
distributions of the visible $\tau$ decay products; electrons and $\tau$-jets 
($\tau_{\Ph}$). The electrons and $\tau$-jets are required to pass kinematic 
selections at the generator level corresponding the kinematic selections of the 
CMS $\PH\to\tau\tau$ analysis~\cite{Chatrchyan:2011nx}. The electron is 
required to have $p_{T} > 20 \UGeV$ and $|\eta| < 2.1$ and the $\tau_{\Ph}$ is 
required to have $p_{T} > 20 \UGeV$ and $|\eta| < 2.3$. Events containing a 
$e + \tau_{\Ph}$ pair separated by $\Delta R >$ 0.5 are selected. The 
acceptance, defined as the ratio of selected to generated events, is calculated 
before and after applying the reweighting procedure and shown in 
Table~\ref{tab:ggHAcceptanceResults}. 

%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\begin{center}
\includegraphics[width=0.45\textwidth]{YRHXS2_mssm/ggHAcceptancePtElec140.eps} \hfill
\includegraphics[width=0.45\textwidth]{YRHXS2_mssm/ggHAcceptancePtElec400.eps} \hfill
\includegraphics[width=0.45\textwidth]{YRHXS2_mssm/ggHAcceptancePtTau140.eps} \hfill
\includegraphics[width=0.45\textwidth]{YRHXS2_mssm/ggHAcceptancePtTau400.eps}
\caption{Generated $p_{T}^{\Pe}$ and $p_{T}^{\tau}$ distributions for 
$\MH = 140 \UGeV$ (left) and $\MH = 400 \UGeV$ (right) for $\Pg\Pg \to \PH$ process 
generated with {\tt PYTHIA} (dashed) and after re-weighting (solid) to correct for 
$\Pg\Pg \to \PH$ production with only $\Pb$-loop.}
\label{fig:ggHAcceptancePtElecPtTau}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%% T A B L E %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
$\MH$ [GeV]    & Acceptance, {\tt PYTHIA} $\Pg\Pg\to \PH$ & Acceptance, 
re-weighted for $\Pb$-loop & Correction factor \\ 
\hline
\hline
   140 &  0.072 $\pm$ 0.001 & 0.070 $\pm$ 0.001 & 0.97 $\pm$ 0.01 \\
\hline
   400 &  0.149 $\pm$ 0.001 & 0.152 $\pm$ 0.001 & 1.02 $\pm$ 0.02 \\
\hline
\end{tabular}
\caption{The $e + \tau_{\Ph}$ acceptances before and after
re-weighting to correct for $\Pb$-loop contribution.} 
\label{tab:ggHAcceptanceResults}
\end{center}
\end{table}
%%%%%%%%%%%%%%%%%% T A B L E %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

It is found that in current analyses, the acceptance for the MSSM 
$\Pg\Pg\to \PH\to\tau\tau$ process with only b-loop is smaller than for the SM 
$\Pg\Pg\to \PH\to\tau\tau$ process by approximately 3\% for 
$\MH = 140 \UGeV$, and consistent with the SM process for 
$\MH = 400 \UGeV$. The small size of the effect is due to the relatively
low $p_{T}$ thresholds on the visible $\tau$ decay products used in the current analyses.

% It is found that the efficiency for the MSSM $\Pg\Pg\to \PH\to\tau\tau$
% process with only b-loop is smaller than for the SM 
% $\Pg\Pg\to \PH\to\tau\tau$ process by approximately 16\% for 
% $\MH = 140 \UGeV$ and 10\% for $\MH = 400 \UGeV$.
% We thus conclude the difference is not negligible and should be taken
% into account in the experimental analyses. 
