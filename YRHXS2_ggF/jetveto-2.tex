
\noindent
\subsection{Perturbative uncertainties in jet-veto
  efficiencies\footnote{A.~Banfi, G.~P.~Salam and G.~Zanderighi.}}
\label{sec:vetosum}

%\noindent A.~Banfi, G.~P.~Salam, G.~Zanderighi

%In the context of Higgs-boson searches, it is common to split the Higgs
%production cross section into different bins, corresponding to having a
%Higgs accompanied by 0, 1, 2, or more jets.
%
In this section we continue our discussion of perturbative uncertainties
in predictions for exclusive cross sections,
concentrating on the 0-jet cross section,
corresponding to
having a Higgs and no jets with a transverse momentum above a given
$\pT^{{\rm cut}}$.
%Such a jet-veto condition plays a crucial role in the
%search for a Higgs decaying into a pair of $W$ bosons, in that it
%suppresses the large background from top-antitop
%production. 
%

We examine here the question of the theoretical uncertainty in the
prediction of the jet-veto efficiency.
%
In particular, we consider the ambiguity that arises in its
calculation from a ratio of two cross sections that are both known at
next-to-next-to-leading order (NNLO).

In the following, $\sigma_0(\pT^{{\rm cut}})$ will denote the 0-jet cross
section as a function of the jet-transverse-momentum threshold
$\pT^{{\rm cut}}$, whilst $\sigma_\total$ will denote the Higgs total cross
section, without any jet veto. 
%
It is also useful to consider the ratio of these cross sections,
$f_0(\pT^{{\rm cut}})=\sigma_0(\pT^{{\rm cut}})/\sigma_\total$, which is commonly
referred to as the jet-veto efficiency, or the 0-jet fraction as in \refS{sec:jetveto}.
%
Knowledge of this efficiency, and its uncertainty, is important in
interpreting measured limits on the Higgs cross section in the 0-jet
bin as a limit on the total Higgs production cross section.

Both $\sigma_0(\pT^{{\rm cut}})$ and $\sigma_\total$ have a fixed-order perturbative
expansion of the form
\begin{subequations}
  \begin{align}
    \label{eq:Sigma-expansion}
    \sigma_0(\pT^{{\rm cut}}) &= \sigma_0^{(0)}(\pT^{{\rm cut}}) + \sigma_0^{(1)}(\pT^{{\rm cut}}) + \sigma_0^{(2)}(\pT^{{\rm cut}}) + \ldots \,,\\
    \sigma_\total &= \sigma^{(0)} + \sigma^{(1)} + \sigma^{(2)} + \ldots \,,
  \end{align}
\end{subequations}
where the superscript $i$ denotes the fact that the given contribution to
the cross section is proportional to $\alphas^i$ relative to the Born
cross section (of order $\alphas^2$ in the present case). Since no
jets are present at the Born level we have $\sigma_0^{(0)}(\pT^{{\rm cut}})
\equiv \sigma^{(0)}$.

The state-of-the-art of fixed-order QCD predictions is NNLO, i.e.\ the
calculation of $\sigma_0(\pT^{{\rm cut}})$ and $\sigma_\total$ with tools like
{\sc FEHiP}~\cite{Anastasiou:2005qj} and
{\sc HNNLO}~\cite{Grazzini:2008tf}.
%Mixed QCD and electroweak
%corrections~\cite{Anastasiou:2008tj} are also known, but we will not
%consider them here.

There is little ambiguity in the definition of the fixed-order results
for the total and jet-vetoed cross sections, with the only freedom
being, as usual, in the choice of the renormalisation and the
factorisation scale.
%
However, given the expressions of $\sigma_0$ and $\sigma_\total$ at a given
perturbative order, there is some additional freedom in the way one
computes the jet-veto efficiency. For instance, at NNLO the efficiency
can be defined as
\begin{equation}
  f_0^{(a)}(\pT^{{\rm cut}}) \equiv 
  \frac{\sigma_0^{(0)}(\pT^{{\rm cut}})+\sigma_0^{(1)}(\pT^{{\rm cut}})
+\sigma_0^{(2)}(\pT^{{\rm cut}})}{\sigma^{(0)}+\sigma^{(1)}+\sigma^{(2)}}\,.
\label{eq:NNLOa}
\end{equation}
This option is the most widely used and may appear at first sight to
be the most natural, insofar as one keeps as many terms as possible
both in the numerator and denominator.  It corresponds to method A of evaluating the uncertainty 
in the fraction of events in the $0$-jet bin defined in \refS{sec:jetveto:overview}.

However, other prescriptions are possible. For instance, since the
zeroth-order term of $f_0(\pT^{{\rm cut}})$ is equal to $1$, one
can argue that it is really only $1-f_0(\pT^{{\rm cut}})$ that has
a non-trivial perturbative series, given by the ratio of the inclusive
1-jet cross section above $\pT^{{\rm cut}}$,
$\sigma_{\text{1-jet}}^\text{NLO}(\pT^{{\rm cut}})$, to the total cross
section, where
\begin{equation}
  \label{eq:sigma-jet-nlo}
  \sigma_{\text{1-jet}}^\text{NLO}(\pT^{{\rm cut}}) =  \sigma^{(1)} +
  \sigma^{(2)} - \left( \sigma_0^{(1)}(\pT^{{\rm cut}}) + \sigma_0^{(2)}(\pT^{{\rm cut}})
  \right)\,.
\end{equation}
%
Insofar as the 1-jet cross section is known only to NLO, in taking the
ratio to the total cross section one should also use NLO for the
latter, so that an alternative prescription reads
\begin{equation}
  \label{eq:NNLOb}
  f_0^{(b)}(\pT^{{\rm cut}}) = 1 -
  \frac{\sigma_{\text{1-jet}}^\text{NLO}(\pT^{{\rm cut}})}{\sigma^{(0)}+\sigma^{(1)}}\,.
\end{equation}
%
Finally, another motivated expression for the jet-veto efficiency is
just the fixed-order expansion up to ${\cal O}(\alphas^2)$ of
Eq.~\eqref{eq:NNLOa}, which can be expressed in terms of the LO and
NLO inclusive jet cross sections above $\pT^{{\rm cut}}$ as follows
%
\begin{equation}
  \label{eq:NNLOc}
  f_0^{(c)}(\pT^{{\rm cut}}) = 1 -
  \frac{\sigma_{\text{1-jet}}^\text{NLO}(\pT^{{\rm cut}})}{\sigma^{(0)}}+
  \frac{\sigma^{(1)}}{(\sigma^{(0)})^2}\sigma_{\text{1-jet}}^\text{LO}(\pT^{{\rm cut}})\,.
\end{equation}
%
Prescriptions (a), (b), and (c) differ by terms of relative order
$\alphas^3$ with respect to the Born level, i.e.\ NNNLO.
%
Therefore, the size of the differences between them is a way to
estimate the associated theoretical uncertainty that goes beyond the
usual variation of scales.

Let us see how these three prescriptions fare in practice in the case
of interest, namely Higgs production at the LHC with 7 \UTeV\
centre-of-mass energy. 
%
We use MSTW2008NNLO parton distribution
functions~\cite{Martin:2009iq} (even 
for LO and NLO predictions) with $\alphas(\MZ) =0.11707$ and
three-loop running. Furthermore, we use the large-$\Mt$
approximation. We choose a Higgs mass of $145\UGeV$, and default
renormalisation and factorisation scales of $\MH/2$. 
%
We cluster partons into jets using the anti-$\kT$ jet
algorithm~\cite{Cacciari:2008gp} with $R=0.5$, which is the default
jet definition for CMS.
%
Switching to $R=0.4$, as used by ATLAS, has a negligible impact
relative to the size of the uncertainties.
%
We include jets up to infinite rapidity, but have checked that the
effect of a rapidity cut of $4.5/5$, corresponding to the ATLAS/CMS
acceptances, is also much smaller than other uncertainties discussed
here.

\begin{figure}
  \includegraphics[width=.48\textwidth]{YRHXS2_ggF/YRHXS2_ptmax_H_antikt-log-LHC7-145-mstw8nn-NNLO-hwg}\hfill
  \includegraphics[width=.48\textwidth]{YRHXS2_ggF/YRHXS2_ptmax_DY_antikt-log-LHC7-mstw8nn-NNLO-hwg}
  \caption{Jet-veto efficiency for Higgs (left) and
    Z-boson production (right) using three different prescriptions for
    the NNLO expansion, see
    Eqs.~(\ref{eq:NNLOa}),~(\ref{eq:NNLOb}),~(\ref{eq:NNLOc}). 
    %
    The bands are obtained by varying renormalisation and
    factorisation scales independently around the central value
    $\MH/2$ ($\MZ/2$) by a factor of two up and down (with the
    constraint $\frac12 < \muR/\muF < 2$). 
    %
    NNLO predictions are obtained by suitably combining the total
    cross sections obtained with {\sc HNNLO}~\cite{Grazzini:2008tf}
    ({\sc DYNNLO}~\cite{Catani:2009sm}) with the 1-jet cross
    section $\sigma_\text{1-jet}(\pT^{{\rm cut}})$ computed with
    {\sc MCFM}~\cite{MCFMweb}.}
\label{fig:ptjv3NNLO}
\end{figure} 

The corresponding results for the jet-veto efficiencies over a wide
range of values of $\pT^{{\rm cut}}/\MH$ are shown in
\refF{fig:ptjv3NNLO} (left). Each of the three prescriptions
Eqs.~\eqref{eq:NNLOa},~\eqref{eq:NNLOb},~\eqref{eq:NNLOc} is presented
together with an associated uncertainty band corresponding to
an independent variation of renormalisation and factorisation scales
$\MH/4 < \muR, \muF < \MH$ (with the constraint $\frac12 <
\muR/\muF < 2$).  The solid red vertical line corresponds to a
reference jet veto of $0.2 \MH\sim 29\UGeV$, which is in the ballpark
of the value used by ATLAS and CMS to split the cross section in 0-,
1-, and 2-jet bins ($25\UGeV$ and $30\UGeV$,
respectively).
%
Several features can be observed: firstly, the three schemes lead to
substantially different predictions for the jet-veto efficiency,
spanning a range from about 0.50 to 0.85 at the reference jet-veto
value.
%
Furthermore, the uncertainty bands from the different schemes barely
overlap, indicating that scale uncertainties alone are a poor
indicator of true uncertainties here.
%
Finally the uncertainty bands' widths are themselves quite different
from one scheme to the next.

The above features are all caused by the poor convergence of the
perturbative series.
%
In particular, it seems that two classes of effects are at play here.
%
Firstly, for $\pT^{{\rm cut}} \muchless \MH$, there are large Sudakov logarithms
$\alphas^n\ln^{2n}(\pT^{{\rm cut}}/\MH)$.
%
These are the terms responsible for the drop in veto efficiency at low
$\pT^{{\rm cut}}$ and the lack of a resummation of these terms to all
orders is responsible for the unphysical increase in veto efficiency
seen at very low $\pT^{{\rm cut}}$ (resummations of related observables are
discussed in \Bref{deFlorian:2011xf,Berger:2010xi}).
%
The second class of effects stems from the fact that the total cross
section has a very large NLO/LO $K$-factor, $\sim 2$, with substantial
corrections also at NNLO (see \refT{tab:sigma}).
%
The jet-veto efficiency is closely connected to the 1-jet rate, for
which the NNLO corrections are not currently known.
%
It
is conceivable that they could be as large, in relative terms, as the
NNLO corrections to the total cross section and
%
our different schemes for calculating the perturbative efficiency
effectively take that uncertainty into account.
%

\begin{table}[t]
\begin{center}
\begin{tabular}{cccc}
  \hline
  &LO& NLO& NNLO\\ %& $c_1$ &$c_2$ \\
  \hline
  &&&\\[-11pt]
  H [pb] & $3.94^{+1.01}_{-0.73}$ & $8.53^{+1.86}_{-1.34}$ & $10.5^{+0.8}_{-1.0}$ \\[2pt] 
  \hline
  &&&\\[-11pt]
  Z [nb] & $22.84^{+2.07}_{-2.40}$ & $28.6^{+0.8}_{-1.2}$ &
  $28.6^{+0.4}_{-0.4}$ \\[2pt] 
  \hline
\end{tabular}
\end{center}\vspace{-1em}
\caption{Cross sections for Higgs and Z-boson production in 7 \UTeV\ \Pp\Pp
  collisions, at various
  orders in perturbation theory. The central value corresponds to the
  default scale $\muR = \muF = \MH/2$ ($\MZ/2$), 
  the error denotes the scale variation when
  $\muR$ and $\muF$  are varied independently by a factor two around 
  the central value, with the constraint $\frac12 < \muR/\muF < 2$. 
}
\label{tab:sigma}
\end{table}

The reader may wonder whether it is really possible to attribute the
differences between schemes to the poor convergence of the total cross
section. 
%
One cross-check of this statement is to examine the jet-veto
efficiency for Z-boson production, where, with a central scale choice
$\mu= \MZ/2$, NLO corrections to the total cross section are about
$25\%$, and the NNLO ones are a couple of per cent (see \refT{tab:sigma}).
%
The results for the jet-veto efficiency are shown in
\refF{fig:ptjv3NNLO} (right). While overall uncertainties remain
non-negligible (presumably due to large Sudakov logarithms), the three
expansion schemes do indeed give almost identical results for $\pT^\cut/\MZ=0.2$.
%
This supports our interpretation that the poor total-cross-section
convergence is a culprit in causing the differences between the three
schemes in the Higgs case.

To conclude, in determining the jet-veto efficiency for Higgs
production, one should be aware that fixed-order predictions depend
significantly on the precise form of the perturbative expansion used
to calculate the efficiency, Eqs.~(\ref{eq:NNLOa}),~(\ref{eq:NNLOb}),
or (\ref{eq:NNLOc}).
%
This uncertainty is not reflected in the scale dependence of the
predictions, which is likely due to the accidental cancellations between physically unrelated
large Sudakov effects and large total-cross-section corrections that
are discussed in \refS{sec:jetveto} and \Bref{Stewart:2011cf}.

