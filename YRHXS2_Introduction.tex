% -------------
% Introduction
% -------------

\section{Introduction\footnote{S.~Dittmaier, C.~Mariotti, G.~Passarino and R.~Tanaka.}}

\newcommand{\ggF}{\ensuremath{\Pg\Pg \to \PH}}
\newcommand{\VBF}{\ensuremath{\PQq\PQq' \to \PQq\PQq'\PH}}
\newcommand{\VH}{\ensuremath{\PQq\PAQq \to \PW\PH/\PZ\PH}}
\newcommand{\ttH}{\ensuremath{\PQq\PAQq/\Pg\Pg \to \PQt\PAQt\PH}}

% Introduction & LHC

The quest for the origin of electroweak symmetry breaking is one
of the major physics goals of the Large Hadron Collider (LHC) at
CERN. After the successful start of $\Pp\Pp$ collisions in 2009 and 2010,
the LHC machine has been operated at the centre-of-mass energy of
$7$\UTeV\ in 2011, and data corresponding to a luminosity of $5.7\Ufb^{-1}$ have been
delivered. 
The LHC is expected to operate at $\sqrt{s} = 7$ or $8\UTeV$ in 2012
and a long shutdown (2013--2014) is scheduled to prepare for the run
at the design centre-of-mass energy of $14\UTeV$. 

% Higgs search results 2011

At the LHC, the most important Standard Model (SM) Higgs-boson production processes are:
the gluon-fusion process (\ggF), where a $\Pg\Pg$ pair annihilates into the Higgs boson
through a loop with heavy-quark dominance;
vector-boson fusion (\VBF), where vector bosons are radiated off quarks
and couple to produce a Higgs boson;
vector-boson associated production (\VH), where the Higgs boson is radiated off a gauge boson;
top-quark pair associated production (\ttH), where the Higgs boson is radiated of a top quark.

ATLAS and CMS, with data currently analysed, are able to exclude a substantial region 
of the possible Higgs-boson mass range. The results that were presented in December 2011
show that the region of Higgs masses between approximately $116$ and $127\UGeV$ is not
excluded, and the excess of events observed
for hypothesised Higgs-boson masses at this low end of the explored range makes the
observed limits weaker than expected. To ascertain the origin of this excess, more
data are required.
%The results that were presented in December 2011 are just showing intriguing signs that 
%a Standard Model Higgs boson could be there, between 114 GeV and 128
%GeVaround $125$\UGeV\ in particular in the three 
%most sensitive decay channels ($\PH \rightarrow \PGg\PGg, \PH \rightarrow \PZ\PZ,\PH 
%\rightarrow \PW\PW$). Of course the opposite could also be true.
With much more data accumulated in 2012, one may eventually reach the discovery of the 
Higgs boson. For this, predictions with the highest precision for
Higgs-boson production and decay rates and
associated uncertainty estimates are crucial.  
If there is a discovery then theoretical predictions for Higgs-boson property measurements
will become even more important. This is the reason why the LHC Higgs Cross Section Working 
Group has been created in 2010 as the joint forum of the experimental
collaborations (ATLAS, CMS, and LHCb) and the theory communities. 

In the LHC Higgs Cross Section Working Group, there are $16$ subgroups.
The first four address different Higgs-boson production modes: \ggF, \VBF, \VH, and 
\ttH\ processes. Two more subgroups are focusing on MSSM neutral- and MSSM charged-Higgs production.
In addition, six new subgroups were created in 2010 to study the Higgs-boson decay modes:
$\PH \to \PGg\PGg, \PWp\PWm, \PZ\PZ, \PGtprp\PGtprm, \PQb\PAQb$, and $\PH^{\pm} $.
Four subgroups discuss common issues across the various production modes:
Higgs-boson decay branching ratios (BR) in the SM and in the Minimal Supersymmetric Standard Model
(MSSM),
parton distribution functions (PDFs),
next-to-leading order (NLO) Monte Carlo (MC) generators
%which became the de facto at LHC 
for both Higgs signal and SM backgrounds and, finally, the definition of Higgs
pseudo-observables, in particular, the heavy-Higgs-boson lineshape.

%\subsection{Inclusive Observables}

In the first Report~\cite{Dittmaier:2011ti}, the state-of-the-art inclusive Higgs-boson 
production cross sections and decay branching ratios have been compiled. 
The major part of the Report was devoted to discussing the computation of cross 
sections and branching ratios for the SM and MSSM Higgs bosons.
The related theoretical uncertainties due to QCD scale and PDF were discussed. 
The Higgs-boson production cross sections are calculated with varying
precision in the perturbative expansion.
For total cross sections,
the calculations are performed up to the next-to-next-to-leading-order
(NNLO) QCD correction for the \ggF, \VBF, and \VH\ processes,
while up to NLO for \ttH\ process.
In most cases, the NLO electroweak (EW) corrections have been applied assuming factorisation 
with the QCD corrections.
The Higgs-boson decay branching ratios take into account the recently 
calculated higher-order NLO QCD and EW corrections in each Higgs-boson decay mode.
The resulting SM Higgs-boson production cross sections times branching
ratios are shown in \refF{fig:sm-crossbr}.
\begin{figure}
  \begin{center}
    \includegraphics[width=0.55\textwidth]{YRHXS2_Introduction/YRHXS2_Introduction_XSBR_7TeV_SM.eps}
        \caption{The SM Higgs-boson production cross sections
                      multiplied by decay branching ratios in $\Pp\Pp$ collisions 
                      at \mbox{$\sqrt{s}=7\UTeV$}  as a function of Higgs-boson mass.
                      All production modes are summed in the channels of 
                      $\PH \to \PGtprp\PGtprm$, $\PGg\PGg$, or
                      $\PW\PW/\PZ\PZ(\to 4~\mathrm{fermions})$.
                      In the $\PH\to \PQb\PAQb$ channel, only the vector-boson associated 
                      production is considered.}
            \label{fig:sm-crossbr}
  \end{center}
\end{figure}
For these calculations, the common SM input parameter set has been used as given in
\Bref{LHCHiggsCrossSectionWorkingGroup:SMinput}.
The coherent theory results to the experimental collaborations facilitated the first LHC 
combination of the Higgs-boson search results, as described in
\Bref{LHCHiggsCombination:2011}.

The present Report, in particular, covers updates on inclusive observables. 
%
%% CERN Report 2
%
%%\subsection{Differential Distributions}
%
%% - Differential Distributions
%
The goal of this second Report is to extend the previous study of inclusive cross sections 
to differential distributions. The experimental analysis must impose cuts on the final 
state in order to extract the signal from background: a precise determination of the 
corresponding signal acceptance is therefore necessary. 

Various studies are performed in different Higgs-boson production modes (\ggF, \VBF, \VH, 
and \ttH\ processes); the benchmark cuts for these processes have been defined, and the
differential distributions have been compared at various levels of theoretical accuracy, \ie 
at NLO/NNLO and with MC generators:
\begin{itemize}
\item
In addition, many search modes for the Higgs boson are carried out in the
exclusive mode, \ie by separating the events according to number of jets or the
transverse momentum ($\pT$) of the Higgs boson.
A particularly important channel is $\PH \to \PW\PW \to \Pl\nu\Pl\nu$
in \ggF\ process, where the events are classified in $\PH{+}0,1,2$-jet multiplicity bins to 
improve the signal-to-noise ratio. There are large logarithms associated with the ratio of 
the Higgs-boson mass over the defining $\pT$ of the jet: the theoretical error assignment 
in the exclusive jet bins has been extensively discussed, and a precise prescription is given 
in this Report. 
\item
% - Higgs boson pT
The $\pT$ of the Higgs boson is a particularly interesting quantity, as it can be used as 
the discriminant variable against the SM backgrounds. 
Possible large logarithms that can occur when cuts are imposed should be studied carefully:
for instance, the Higgs-boson transverse-momentum spectrum in \ggF\ process has been
studied at NLO accuracy and supplemented with next-to-next-to-leading logarithmic (NNLL)
resummation of small-$\pT$ logarithms. A systematic study of the uncertainties of the shape 
of the resummed Higgs-boson $\pT$ spectrum has also been carried out.
\looseness-1
\item
% - SM backgrounds
The differential distributions of the SM backgrounds (in particular the irreducible
backgrounds to Higgs-boson searches) have been studied extensively in this Report. 
In the searches at LHC, most of the backgrounds in the signal regions are derived from
measurements in control regions (so-called ``data-driven'' methods);
the extrapolation to the signal region relies on MC simulations, and the related theoretical 
uncertainty is usually estimated by comparing different MC generators and by varying the 
QCD scales. Whenever possible, not only the normalisation, but also the parametrisation
of the background shape should be taken from the control region.
However, there are backgrounds for which one must rely on theoretical predictions;
the main example is represented by di-boson backgrounds $\PQq\PAQq/\Pg\Pg \to \PW\PW/\PZ\PZ$ 
where data control regions cannot be used, due to the limited size of the data sample 
that is available.
%
%The $\Pg\Pg \to \PW\PW/\PZ\PZ$ process starts to contribute
%to the cross section for $\Pp\Pp \to \PW\PW/\PZ\PZ$ at NNLO QCD.
%The contribution from the process $gg \to WW/ZZ$ is enhanced by the
%large gluon luminosity at the LHC and also by experimental Higgs
%boson search selection cuts.
%The corrections can easily be of the order of $10\%$.
\item
In addition, the interference between the Higgs-boson signal in $\Pg\Pg \to \PH \to \PW\PW/\PZ\PZ$
and the gluon-induced continuum $\PW\PW/\PZ\PZ$ production may not be negligible after 
introducing experimental cuts. It will be important to compute the interference between 
signal and background and to try to access this at NLO level. The NLO Monte Carlo's will be 
used to simulate this background and to determine how the $K$-factor is changing with the 
chosen kinematic cuts.
\end{itemize}
% --- Common Issues
%\subsection{Common Issues}

This Report also discusses issues common to different Higgs-boson search channels. 
\begin{itemize}
\item
% - BR
The Higgs-boson BRs are the important ingredients for Higgs physics.
Their most precise estimate with the state-of-the-art calculations for SM
and MSSM is presented and the associated uncertainties are discussed. 
\item
% - PDF
PDFs are crucial for the prediction of Higgs-boson production processes, hence PDFs and 
their uncertainties are of particular significance. At present, these PDFs are obtained from 
fits to data from deep-inelastic scattering, Drell--Yan processes, and jet production from a wide
variety of different experiments. Upon arrival of new LHC data, significant improvements 
are expected for the PDF predictions. Different groups have produced publicly available 
PDFs using different data sets and analysis frameworks, and updates are reported. 
\item
% - NLOMC
NLO MCs are now widely used at LHC; the main progress is represented by a consistent 
inclusion of exact NLO corrections matched to the parton-shower (PS) simulations.
At present, all the main Higgs-boson production channels (\ggF, \VBF, \VH, and \ttH)
are simulated with NLO+PS, together with most important SM backgrounds, like 
$\PQq\PAQq/\Pg\Pg \to \PW\PW/\PZ\PZ, \PW\PQb\PAQb/\PZ\PQb\PAQb, \PQt\PAQt$, etc.  
Tuning of NLO+PS generators is an important issue, and particularly relevant
is the $\pT$ of the Higgs boson. 
Estimates of uncertainties in NLO+PS simulations due to QCD scale uncertainties or
different matching procedures are also reported, and uncertainties due to hadronisation 
and underlying events are discussed.
\item
% - PO, Heavy Higgs lineshape
The current searches for a heavy Higgs boson assume on-shell (stable)
Higgs-boson production. The production cross section is then sampled over
a Breit--Wigner distribution (either fixed-width or running-width scheme), as implemented 
in the MC simulations. Recent studies have shown that the effects due to off-shell 
Higgs-boson production and decay and to interference of the signal with the SM backgrounds 
may become sizable for Higgs-boson masses $\MH > 300\UGeV$; the Higgs-boson lineshape is expected 
to be altered as well. Thus concrete theoretical predictions for the heavy-Higgs-boson lineshape is
discussed in this Report. 
\end{itemize}
%Following the recommendation by the LHC Higgs Cross Section working
%group~\cite{LHCHiggsCrossSectionWorkingGroup:HeavyHiggs},
%an additional theoretical uncertainty of
%$(150\%) \times \left( \frac{m_H}{\mathrm{TeV}/c^2} \right)^3$ on the
%total cross section has been included.
%This amounts to about $\pm 10\ (30)\%$ uncertainty for $m_H = 400\
%(600)\ \mathrm{GeV}/c^2$ and
%should effectively cover the possible uncertainties in both the
%Higgs boson event yield and the lineshape in different theoretical
%schemes.

% ------------------------------------------------------------------------

%\subsection{Beyond Standard Model Higgs boson}

Several models beyond the SM are also discussed in this Report: in the MSSM the Higgs-boson 
sector contains, two scalar doublets, accommodating five physical Higgs bosons
of the light and heavy CP-even $\PSh$ and $\PH$, the CP-odd $\PSA$, and the charged 
Higgs bosons $\PSHpm$; BRs and various kinematical distributions are discussed.
A model which contains a 4th generation of heavy fermions, consisting of an up- and a 
down-type quark $(\PQtpr,\PQbpr)$, a charged lepton $(\Pl')$, and a massive 
neutrino $(\PGnl')$ has been studied: a large effect of the higher-order electroweak
corrections has been found.

% ------------------------------------------------------------------------

\enlargethispage{1em}
This Report is based upon the outcome of a series of workshops
throughout 2010--2011, joint effort for Higgs-boson cross sections between
ATLAS, CMS, LHCb collaborations, and the theory community.
These results are recommended as the common inputs to the experimental
collaborations, and for the Higgs combinations at LHC
\footnote{Any updates will be bade available at the TWiki page: 
https://twiki.cern.ch/twiki/bin/view/LHCPhysics/CrossSections}.


%\vspace{1.5cm}

%\leftline{\it Stefan Dittmaier}
%\leftline{\it Chiara Mariotti}
%\leftline{\it Giampiero Passarino}
%\leftline{\it Reisaburo Tanaka}
