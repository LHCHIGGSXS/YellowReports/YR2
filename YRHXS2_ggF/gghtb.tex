\noindent
\subsection{Finite-quark-mass effects in the SM gluon-fusion process
  in {\sc POWHEG}\footnote{E.A. Bagnaschi, G. Degrassi, P. Slavich and A. Vicini.}}
\label{sec:finitemass}

\subsubsection{Introduction}
\label{sec:gghtb-intro}
%An accurate description of Higgs boson production via gluon fusion, not only of the total cross section but also of
%all the differential distributions, will be paramount to study in
%detail the nature and properties of the Higgs boson -- should the
%latter be detected at the LHC -- or to set stringent exclusion limits
%on its mass.
%
The description of the gluon-fusion process can be
approximated, in many cases, by an effective theory (ET) obtained by
taking the limit of infinite mass for the top quark running in the
lowest-order loop that couples the Higgs to the gluons, and neglecting
all the other quark flavors.  This limit greatly simplifies many
calculations, reducing the number of loops that have to be considered
by one.  On the other hand it is important, when possible, to check
whether the effect of an exact treatment of the quark contributions is
significant compared to the actual size of the theoretical
uncertainty.

The validity of the ET approach has been carefully analyzed for the
total cross section. The latter receives very large 
NLO QCD corrections, which have been computed first in the ET
\cite{Dawson:1990zj,Djouadi:1991tka} and then retaining the exact
dependence on the masses of the quarks that run in the loops
\cite{Spira:1995rr,Anastasiou:2006hc,Harlander:2005rq,
  Aglietti:2006tp,Bonciani:2007ex}.  The NLO results computed in the
ET and then rescaled by the exact leading-order (LO) result with full
dependence on the top- and bottom-quark masses provide a description
accurate at the few-per-cent level for $\MH<2\, \Mt$. The deviation
for $2\Mt < \MH < 1\UTeV$ does not exceed the $10\%$ level.
The NNLO QCD corrections to the total
cross section are still large and have been computed in the ET
\cite{Harlander:2002wh, Anastasiou:2002yz, Ravindran:2003um}.  The
finite-top-mass effects at NNLO QCD have been studied in
\Brefs{Marzani:2008az, Harlander:2009bw, Harlander:2009mq,
  Harlander:2009my } and found to be small.  The resummation to all
orders of soft-gluon radiation up to NNLL+NNLO has been studied in
\Bref{Catani:2003zt}, within the ET, and including the exact dependence
of the masses of the top and bottom quarks up to NLL+NLO in \Brefs{deFlorian:2009hc,Dittmaier:2011ti}.
The leading third-order
(NNNLO) QCD terms have been discussed in the ET
\cite{Moch:2005ky}.  The role of electroweak (EW) corrections has been
discussed in
\Brefs{Aglietti:2004nj,Degrassi:2004mx,Degrassi:2005mc,Actis:2008ug,Actis:2008uh,Actis:2008ts,Bonciani:2010ms}
and found to be, for a light Higgs, of the same order of magnitude as
the QCD theoretical uncertainty.  The impact of mixed QCD--EW
corrections has been discussed in \Bref{Anastasiou:2008tj}.  The
residual uncertainty on the total cross section depends mainly on the
uncomputed higher-order QCD effects and on the uncertainties that
affect the parton distribution functions of the proton
\cite{Demartin:2010er,Alekhin:2011sk,Dittmaier:2011ti}.

The Higgs differential distributions have been studied at NNLO
QCD in the ET,
first in the region in which the Higgs $\pT$
is non-vanishing \cite{deFlorian:1999zd,Glosser:2002gm,Ravindran:2002dc}
\footnote{These computations provide an NLO QCD calculation
of the Higgs $\pT$ spectrum.},
and then with proper treatment of the $\pT=0$ contribution \cite{Anastasiou:2004xq,Anastasiou:2005qj,Catani:2007vq}.  The
NLO QCD results including the exact dependence on the top and bottom-quark
masses were first included in the code {\sc HIGLU} based on
\Bref{Spira:1995rr}.  More recently, the same calculation was
repeated in \Brefs{Keung:2009bs,Brein:2010xj,Anastasiou:2009kn}.  The latter
discussed at NLO QCD the role of the bottom quark, for light Higgs-boson
masses.  A description of the Higgs differential
distributions, in the ET,
including transverse-momentum resummation up to NNLL
and matching to NLO and NNLO QCD results
has been provided in
\Brefs{Balazs:2000wv,Balazs:2000sz} and \cite{Cao:2009md}, respectively, and has been implemented in the code
{\sc Resbos}. The Higgs transverse-momentum distribution, in the ET,
including full NNLO QCD results and matched at NNLL QCD with the
resummation of soft-gluon emissions, has been presented in
\Bref{Bozzi:2005wk} and is implemented in the code {\sc HqT}.
The latter allows for a quite accurate estimate of the perturbative
uncertainty on this distribution, which turns out to be of the order
of $\pm 10$\% for light Higgs and for Higgs transverse momentum
$\pT^{\mathrm{H}}< 100 \UGeV$ \cite{deFlorian:2011xf}.

The shower Monte Carlo (SMC) codes matching NLO QCD results with QCD
Parton Shower (PS) \cite{Frixione:2002ik,Alioli:2008tz} consider the
gluon-fusion process only in the ET.
%Recently, a step toward the
%inclusion of the finite quark mass effects in the PS was taken in
%\Ref~\cite{Alwall:2011cy}, where parton-level events for Higgs
%production accompanied by zero, one or two jets are generated with
%matrix elements obtained in the ET and then, before being passed to
%the PS, they are re-weighted by the ratio of the full one-loop
%amplitudes over the ET ones.
Recently, a step towards the inclusion of the finite-quark-mass effects
in PS was taken in \Bref{Alwall:2011cy}, where parton-level events for Higgs production
accompanied by zero, one, or two partons are generated with matrix elements
obtained in the ET and then, before being passed to the PS, they are re-weighted
by the ratio of the  full one-loop amplitudes over the ET ones.
This procedure is equivalent to generating events directly with
the full one-loop amplitudes, yet it is much faster.

In \Bref{Bagnaschi:2011tu} the implementation
in the {\sc POWHEG} framework
\cite{Nason:2004rx,Frixione:2007vw,Alioli:2010xd} of the NLO QCD
matrix elements for the gluon fusion, including the exact dependence
on the top- and bottom-quark masses, has been presented.  We discuss
here the effect of the exact treatment of the quark masses on the
Higgs transverse-momentum distribution, comparing the results of the
old {\sc POWHEG} release, obtained in the ET, with those of this new
implementation \cite{Bagnaschi:2011tu}. In particular, we consider here the
matching of {\sc POWHEG} with the {\sc PYTHIA} \cite{Sjostrand:2006za}
PS.

\subsubsection{Quark mass effects in the {\sc POWHEG} framework}
\label{sec:gghtb-massffects}

In this section we briefly discuss the implementation of the
gluon-fusion Higgs production process in the {\sc POWHEG
  BOX}\ framework, following closely \Ref~\cite{Alioli:2008tz}. We fix
the notation keeping the discussion at a general level, and refer to
\Bref{Bagnaschi:2011tu} for a detailed description and for the explicit
expressions of the matrix elements.  The generation of the hardest
emission is done in {\sc POWHEG} according to the following formula:
%
\begin{eqnarray}
\label{eq:gghtb-POWHEG}
d\sigma &=& \bar{B}(\bar{\Phi}_1)\, d \bar{\Phi}_1
 \left\{ \Delta\left(\bar{\Phi}_1,\pT^{\mathrm{min}}\right)
+
\Delta\left(\bar{\Phi}_1, \pT \right)\, \frac{
 R\left(\bar{\Phi}_1,\Phi_{\rm rad} \right)}{ B\left(\bar{
    \Phi}_1\right)} \, d\Phi_{\rm rad} \right\} 
\nonumber\\ 
&+& \sum_q R_{q \bar q}\left(\bar{
  \Phi}_1,\Phi_{\rm rad} \right) d \bar{\Phi}_1 d\Phi_{\rm rad} ~.
\end{eqnarray}
%
In the equation above the variables $\bar{\Phi}_1 \equiv (M^2,Y)$
denote the invariant mass squared and the rapidity of the Higgs boson,
which describe the kinematics of the Born (i.e., lowest-order) process
$\Pg\Pg\rightarrow\phi$.  The variables $\Phi_{\rm rad}$ describe the
kinematics of the additional final-state parton in the real emission
processes.
%
The factor $\bar{B}(\bar{\Phi}_1)$ in Eq.~(\ref{eq:gghtb-POWHEG}) is
related to the total cross section computed at NLO in QCD.  It
contains the value of the differential cross section, including real
and virtual radiative corrections, for a given configuration of the
Born final-state variables, integrated over the radiation variables.
The integral of this quantity on $d\bar{\Phi}_1$, without acceptance
cuts, yields the total cross section and is responsible for the
correct NLO QCD normalisation of the result.
%
The terms within curly brackets in Eq.~(\ref{eq:gghtb-POWHEG})
describe the real emission spectrum of an extra parton: the first term
is the probability of not emitting any parton with transverse momentum
larger than a cutoff $\pT^{\mathrm{min}}$, while the second term is the
probability of not emitting any parton with transverse momentum larger
than a given value $\pT$ times the probability of emitting a parton
with transverse momentum equal to $\pT$. The sum of the two terms
fully describes the probability of having either zero or one
additional parton in the final state. The probability of non-emission
of a parton with transverse momentum $\kT$ larger than $\pT$ is
obtained using the {\sc POWHEG} Sudakov form factor
%
\begin{equation}
\Delta(\bar \Phi_1,\pT)=
\exp
\left\{
-\int d\Phi_{\rm rad}
\frac{R(\bar{\Phi}_1,\Phi_{\rm rad})}{B(\bar{\Phi}_1)}
\theta(k_{\mathrm{T}}-\pT)
\right\}~,
\label{eq:gghtb-Sudakov}
\end{equation}
%
where the Born squared matrix element is indicated by
$B(\bar{\Phi}_1)$ and the squared matrix element for the real emission
of an additional parton can be written, considering the subprocesses
$\Pg\Pg\to\phi \Pg$ and $\Pg\PQq\to\phi \PQq$, as
%
\begin{equation}
R(\bar{\Phi}_1,\Phi_{\rm rad}) =
R_{\Pg\PQq}(\bar{\Phi}_1,\Phi_{\rm rad}) + \sum_{\PQq} \left[
R_{\Pg\PQq}(\bar{\Phi}_1,\Phi_{\rm rad}) + R_{\PQq\Pg}(\bar{\Phi}_1,\Phi_{\rm rad}) \right]~.
\label{eq:gghtb-real}
\end{equation}
%
Finally, the last term in Eq.~(\ref{eq:gghtb-POWHEG}) describes the effect
of the $\PQq \PAQq \to \phi \Pg$ channel, which has been kept apart in the
generation of the first hard emission, because it does not factorise
into the Born cross section times an emission factor.

\begin{figure}[t]
\begin{center}
\includegraphics[height=76mm,angle=0]{YRHXS2_ggF/YRHXS2_gghtb_1}~
\includegraphics[height=76mm,angle=0]{YRHXS2_ggF/YRHXS2_gghtb_2}
\end{center}
\vspace{-0.5cm}
\caption{ Ratio, for different values of the SM Higgs-boson mass, of
  the normalised Higgs transverse-momentum distribution computed with
  exact top- and bottom-mass dependence over the one obtained in the
  ET.  Left: ratio of the NLO QCD predictions. Right: ratio of the
  {\sc POWHEG}+{\sc PYTHIA} predictions.}
\label{fig:gghtb-shaperatio}
\end{figure}

The NLO QCD matrix elements used in this implementation have been
computed in \Brefs{Aglietti:2006tp,Bonciani:2007ex}.  We compared
the numerical results for the distributions with those of
the code {\sc FehiPro} \cite{Anastasiou:2006hc}, finding good
agreement.  We also checked that, in the case of a light Higgs and
considering only the top contribution, the ET provides a very good
approximation of the exact result for small values of the Higgs
transverse momentum $\pT^{\mathrm{H}}$, and only when $\pT^{\mathrm{H}}> \Mt$ does a
significant discrepancy appear, due to the fact that the internal
structure of the top-quark loop is resolved.

In order to appreciate the importance of the exact treatment of the
quark masses, we compare in \refF{fig:gghtb-shaperatio} the
normalised distributions computed with the exact top- and bottom-mass
dependence with the corresponding distributions obtained in the
ET. The normalised distributions are defined dividing each
distribution by the corresponding total cross section, and allow a
comparison of the shape of the distributions.  In the left panel of
\refF{fig:gghtb-shaperatio} we plot the ratio of the normalised
$\pT^{\mathrm{H}}$ distributions (exact over ET) computed 
using the real radiation matrix elements that enter in the
NLO QCD calculation,
for different values of the Higgs mass ($\MH=125,\,165,\,500 \UGeV$). The
plot shows that, for a light Higgs, the bottom-quark contribution
induces positive ${\cal O}(10\%)$ corrections in the intermediate
$\pT^{\mathrm{H}}$ range. On the other hand, for a heavy Higgs, the bottom-quark
contribution is negligible, while the exact treatment of the top-quark
contribution tends to enhance the distribution at small $\pT^{\mathrm{H}}$ and
significantly reduce it at large $\pT^{\mathrm{H}}$ (where in any case the cross
section is small).

The matching of the NLO QCD results with a QCD PS is obtained by using
the basic {\sc POWHEG} formula, Eq.~(\ref{eq:gghtb-POWHEG}), for the
first hard emission, and then by vetoing in the PS any emission with a
virtuality larger than the one of the first emission.  The use of the
exact matrix elements in Eq.~(\ref{eq:gghtb-POWHEG}), and in
particular in the {\sc POWHEG} Sudakov form factor,
Eq.\,(\ref{eq:gghtb-Sudakov}), has an important impact on the $\pT^{\mathrm{H}}$
distribution when compared with the distribution obtained in the ET.
Indeed, one should observe that the {\sc POWHEG} Sudakov form factor
is a process-dependent quantity. For a given transverse momentum
$\pT$, in the exponent we find the integral of the ratio of the full
squared matrix elements $R/B$ over all the transverse momenta $k_{\mathrm{T}}$
larger than $\pT$.

The results of the matching of {\sc POWHEG} with the {\sc PYTHIA} PS
are illustrated in the right plot of \refF{fig:gghtb-shaperatio},
where we show the ratio of the normalised $\pT^{\mathrm{H}}$ distribution
computed with exact top- and bottom-mass dependence over the
corresponding distribution computed in the ET, for the same values of
$\MH$ as in the left plot. We shall first discuss the case of a light
Higgs boson, with $\MH=125 \UGeV$. For small $\pT^{\mathrm{H}}$, the Sudakov form
factor with exact top- and bottom-quark mass dependence
$\Delta(\Pt+\Pb,\mathrm{exact})$ is smaller than the corresponding factor
$\Delta(\Pt,\infty)$ with only top in the ET, 
because we have that
$R(\Pt+\Pb,\mathrm{exact})/B(\Pt+\Pb,\mathrm{exact})>R(\Pt,\infty)/B(\Pt,\infty)$.  
The inequality holds for two reasons:
$i)$ the $\pT^{\mathrm{H}}$ distribution is proportional to the squared real matrix element $R$
and, for $\pT^{\mathrm{H}}<200 \UGeV$, $R(\Pt+\Pb,\mathrm{exact})>R(\Pt,\infty)$, 
as  has been discussed in 
\Brefs{Keung:2009bs,Anastasiou:2009kn,Bagnaschi:2011tu};
$ii)$ the bottom quark reduces the LO cross section, with respect 
 to the case with only the top quark in the ET 
\cite{Spira:1995rr}.
Thus, for small
$\pT^{\mathrm{H}}$ the Sudakov form factor suppresses the $\pT^{\mathrm{H}}$ distribution by
almost $10\%$ with respect to the results obtained in the ET.  Since the
emission probability is also proportional to the ratio $R/B$, as can
be read from Eq.~(\ref{eq:gghtb-POWHEG}), starting from $\pT^{\mathrm{H}} \simeq
30 \UGeV$ this factor prevails over the Sudakov factor, and the
distribution with exact dependence on the quark masses becomes larger
than the one in the ET by slightly more than $10\%$ -- as already
observed at NLO QCD in the left plot.  We remark that the effects due
to the exact treatment of the masses of the top and bottom quarks $i)$
are of the same order of magnitude as the QCD perturbative theoretical
uncertainty estimated with {\sc HqT} and $ii)$ have a non-trivial
shape for different values of $\pT^{\mathrm{H}}$. If added to the {\sc HqT}
prediction, these effects would modify in a non-trivial way the
prediction of the central value of the distribution.

A different behaviour is found in the case of a heavy Higgs boson,
with $\MH=500 \UGeV$, because the bottom quark plays a negligible role.
At NLO QCD only the top-quark mass effects are relevant, at large
$\pT^{\mathrm{H}}$, yielding a negative correction.  In turn, the Sudakov form
factor, evaluated for small $\pT^{\mathrm{H}}$, is larger than in the ET (in fact
$R(\Pt+\Pb,\mathrm{exact})/B(\Pt+\Pb,\mathrm{exact})<R(\Pt,\infty)/B(\Pt,\infty)$), yielding what we
would call a Sudakov enhancement.  Also in this case, starting from
$\pT^{\mathrm{H}} \simeq 70 \UGeV$ the effect of the emission probability factor
$R/B$ prevails over the effect of the Sudakov form factor, leading to
negative corrections with respect to the ET case.

\subsubsection{Summary}
\label{sec:gghtb-concl}

An improved release of the code for Higgs-boson production in gluon
fusion in the {\sc POWHEG} framework has been prepared \cite{Bagnaschi:2011tu},
including the complete NLO QCD matrix elements with exact dependence
on the top- and bottom-quark masses.  This code allows a full
simulation of the process, matching the NLO results with the SMC {\sc
  PYTHIA}.

The quark-mass effects on the total cross section and on the Higgs
rapidity distribution are at the level of a few per cent. On the other
hand, the bottom-quark contribution is especially relevant in the
study of the transverse-momentum distribution of a light Higgs
boson. Indeed, the bottom contribution enhances the real emission
amplitude with respect to the result obtained in the ET.  The outcome
is a non-trivial distortion of the shape of the Higgs transverse-momentum 
distribution, at the level of ${\cal O}(10\%)$ of the result
obtained in the ET.  These effects are comparable to the present
estimates of the perturbative QCD theoretical uncertainty (see \refS{sec:hqt}).
