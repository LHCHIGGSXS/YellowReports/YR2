\section{Parton distribution functions\protect\footnote{S.~Forte, J.~Huston and R.~S.~Thorne  (eds); S.~Alekhin, J.~Bl\"umlein, A.M.~Cooper-Sarkar, S.~Glazov, P.~Jimenez-Delgado, S.~Moch, P.~Nadolsky, V.~Radescu, J.~Rojo, A.~Sapronov and W.J.~Stirling.}}
\label{se:PDFs}

\subsection{PDF set updates}

Several of the PDF sets which were discussed in the previous 
Yellow Report~\cite{Dittmaier:2011ti} have been updated since. 
NNPDF2.1~\cite{Ball:2011mu} is an updated 
version of NNPDF2.0~\cite{Ball:2010de} which uses the FONLL general mass 
VFN scheme~\cite{Forte:2010ta}  
instead of a zero-mass scheme. There are also now NNPDF 
NNLO  and LO sets~\cite{Ball:2011uy}, which are based on the same data
and methodology as NNPDF2.1. 
The HERA PDF group now use HERAPDF1.5~\cite{CooperSarkar:2010wm}, 
which contains more data than 
HERAPDF1.0~\cite{:2009wt}, a wider examination of parameter dependence, and an NNLO set 
with uncertainty. This set is available in LHAPDF, however it is partially
based on preliminary HERA-II structure-function data. 
%The first CTEQ PDF set at NNLO is imminent. 
%ABM~\cite{Alekhin:2011cf} have investigated the effect of including 
%Tevatron jet data in their analysis, but there is currently no corresponding 
%publicly available a PDF set. 

%\subsection{PDF4LHC prescription update}

The current PDF4LHC prescription~\cite{Botje:2011sn,PDF4LHCwebpage}
for calculating a central value and uncertainty for a given process should
undergo the simple modification of   
using the most up-to-date relevant set from the relevant 
group, \ie  NNPDF2.0 should be
replaced with NNPDF2.1~\cite{Ball:2011mu} and CTEQ6.6~\cite{Nadolsky:2008zw} 
with CT10~\cite{Lai:2010vv}. At NNLO the existing prescription should be 
used, but with the uncertainty envelope calculated using the up-to-date sets 
noted above.  


\subsection{Correlations}

The main aim of this section is to examine the correlations between 
different Higgs production processes and/or backgrounds to these processes. 
The PDF uncertainty analysis may be extended to define a \emph{correlation}
between the uncertainties of two variables, say $X(\vec{a})$ and
$Y(\vec{a}).$  As for the case of PDFs, the physical concept of PDF
correlations can be determined both from PDF determinations based
on the Hessian approach and on the Monte Carlo approach. 
For convenience and commonality, all physical processes were 
calculated using the {\sc MCFM NLO} program 
(versions 5.8 and 6.0)~\cite{Campbell:2000bg,Campbell:2002tg}
with a common set of input files for all groups. 

We present the results for the PDF correlations at NLO 
for Higgs production via gluon--gluon fusion, vector-boson fusion, 
in association with $\PW$ or with a $\PQt \PAQt$ pair at masses $\MH=120\UGeV$,
$\MH=160\UGeV$, $\MH=200\UGeV$, $\MH=300\UGeV$, $\MH=500\UGeV$. We also include 
a wide variety of background processes and other standard production 
mechanisms, \ie  $\PW$, $\PW\PW$, $\PW\PZ$, $\PW\PGg$, $\PW\PQb\PAQb$, $\PQt \PAQt$, 
$\PQt \PAQb$ and the $t$-channel $\PQt (\to \PAQb) + \PQq$, where $\PW$ denotes the 
average of $\PW^+$ and $\PW^-$. 
   
For MSTW2008~\cite{Martin:2009iq}, CT10~\cite{Lai:2010vv}, 
GJR08~\cite{Gluck:2007ck,Gluck:2008gs}, and ABKM09~\cite{Alekhin:2009ni}
 PDFs the correlations of any two quantities $X$ and $Y$ 
are calculated using the 
standard  formula~\cite{Pumplin:2001ct}
\begin{equation}
\rho\left( X,Y\right) \equiv \cos{\varphi}=
\frac{\sum_i (X_i-X_0) (Y_i-Y_0)}{\sqrt{ \sum_i  (X_i-X_0)^2 \sum_i  (Y_i-Y_0)^2}}.
\end{equation}
The index in the sum runs over the number of free parameters and
$X_0, Y_0$ correspond to the values obtained by the central PDF value. 
When positive and negative direction PDF eigenvectors are used this is equivalent to 
(see \eg \Bref{Nadolsky:2008zw})
\begin{eqnarray}
\cos{\varphi} &=& \frac{\vec{\Delta} X \cdot \vec{\Delta} Y}{\Delta X \Delta Y}= \frac{1}{4\Delta X\Delta Y}\sum_{i=1}^{N} \left( X_i^{(+)}-X_i^{(-)}\right) \left( Y_i^{(+)}-Y_i^{(-)}\right), \cr
\Delta X &=& \left| \vec{\Delta} X \right| = \frac{1}{2} \sqrt{\sum_{i=1}^{N} \left( X_i^{(+)}-X_i^{(-)} \right)^2},  
\end{eqnarray}
where the sum is over the $N$ pairs of positive and negative 
PDF eigenvectors. For MSTW2008 and CT10 the 
eigenvectors by default contain only PDF parameters, and $\alphas$ variation may 
be considered separately. For GJR08  and ABKM09, $\alphas$ is one of the 
parameters used directly in calculating the correlation coefficient,
with the central value and variation determined by the fit. 

Due to the specific error 
calculation prescription for HERAPDF1.5 which includes
parametrisation and model errors, the correlations can not be 
calculated in exactly the same way. Rather, a
formula for uncertainty propagation can be used 
in which correlations can be expressed
via relative errors of compounds and their combination:
\begin{align}
\biggl[\sigma\left(\frac{X}{\sigma(X)}+\frac{Y}{\sigma(Y)}\right)\biggr]^2 = 2+2\cos{\varphi},
\end{align}
where $\sigma(O)$ is the PDF error of observable $O$ calculated 
using the HERAPDF prescription. 

The correlations for the NNPDF prescription 
are calculated as discussed in \Bref{Demartin:2010er}, namely
\begin{equation}
\rho\left( X,Y\right)=\frac{\left\langle XY \right\rangle_{\rm rep}-
\left\langle X \right\rangle_{\rm rep}\left\langle Y \right\rangle_{\rm rep}}{\sigma_{X}
\sigma_{Y}}
\end{equation}
where the averages are performed over the $N_{\rm rep}=100$ replicas of the NNPDF2.1 set.

For all sets the correlations are for the appropriate value of $\alphas$ for 
the relevant PDF set.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}
\begin{center}
\caption[]{The up-to-date PDF4LHC average for the
correlations between all signal processes
with other signal and background processes for
Higgs production
considered here. The processes have
been classified in correlation classes, as discussed in the text. 
\label{tab:signal-correlations}} 
\small
\vspace{0.2cm}
\begin{center}
{
\begin{tabular}{ccccc}
%\hline 
%  $ 120\UGeV$ &&&&  \\ 
\hline 
$\MH=120\UGeV$ & $\Pg\Pg\PH$ & VBF & $\PW\PH$ & $\PQt\PAQt \PH$  \\
\hline 
 $\Pg\Pg\PH$            
&$    1 $
&$  -0.6$
%&$  -0.4$
&$  -0.2$
&$  -0.2$
  \\
 VBF            
&$  -0.6$
&$    1 $
&$   0.6$
&$  -0.4$
  \\
 $\PW\PH$             
&$  -0.2$
&$   0.6$
&$    1 $
&$  -0.2$
  \\
 $\PQt\PAQt H$            
&$  -0.2$
&$  -0.4$
&$  -0.2$
&$    1 $
  \\
 $\PW$              
&$  -0.2$
&$   0.6$
&$   0.8$
&$  -0.6$
  \\
 $\PW\PW$             
&$  -0.4$
&$   0.8$
&$    1 $
&$  -0.2$
  \\
 $\PW\PZ$             
&$  -0.2$
&$   0.4$
&$   0.8$
&$  -0.4$
  \\
  $\PW\PGg$             
&$   0  $
&$   0.6$
&$   0.8$
&$  -0.6$
  \\
 $\PW\PQb\PAQb$            
&$  -0.2$
&$   0.6$
&$    1 $
&$  -0.2$
  \\
  $\PQt\PAQt$             
&$   0.2$
&$  -0.4$
&$  -0.4$
&$    1 $
  \\
 $\PQt\PAQb$             
&$  -0.4$
&$   0.6$
&$    1 $
&$  -0.2$
  \\
 $\PQt(\to \PAQb)\PQq$            
&$   0.4$
&$   0  $
&$   0  $
&$   0  $
  \\
\hline 
\end{tabular}
}
\quad
{
\begin{tabular}{ccccc}
%\hline 
%   $160 \UGeV$ &&&&  \\ 
\hline 
$\MH=160 \UGeV$ & $\Pg\Pg\PH$ & VBF & $\PW\PH$ & $\PQt\PAQt \PH$  \\
\hline 
 $\Pg\Pg\PH$            
&$    1 $
&$  -0.6$
&$  -0.4$
&$   0.2$
  \\
 VBF            
&$  -0.6$
&$    1 $
&$   0.6$
&$  -0.2$
  \\
 $\PW\PH$             
&$  -0.4$
&$   0.6$
&$    1 $
&$   0  $
  \\
 $\PQt\PAQt \PH$            
&$   0.2$
&$  -0.2$
&$   0  $
&$    1 $
  \\
 $\PW$              
&$  -0.4$
&$   0.4$
&$   0.6$
&$  -0.4$
  \\
 $\PW\PW$             
&$  -0.4$
&$   0.6$
&$   0.8$
&$  -0.2$
  \\
 $\PW\PZ$             
&$  -0.4$
&$   0.4$
&$   0.8$
&$  -0.2$
  \\
  $\PW\PGg$             
&$  -0.4$
&$   0.6$
&$   0.6$
&$  -0.6$
  \\
 $\PW\PQb\PAQb$            
&$  -0.2$
&$   0.6$
&$   0.8$
&$  -0.2$
  \\
  $\PQt\PAQt$             
&$   0.4$
&$  -0.4$
&$  -0.2$
&$   0.8$
  \\
 $\PQt\PAQb$             
&$  -0.4$
&$   0.6$
&$    1 $
&$   0  $
  \\
 $\PQt(\to \PAQb)\PQq$            
&$   0.6$
&$   0  $
&$   0  $
&$   0  $
  \\
\hline 
\end{tabular}
}
\end{center}
\begin{center}
{
\begin{tabular}{ccccc}
%\hline 
%   $200 \UGeV$ &&&&  \\ 
\hline 
$\MH=200 \UGeV$ & $\Pg\Pg\PH$ & VBF & $\PW\PH$ & $\PQt\PAQt \PH$  \\
\hline 
 $\Pg\Pg\PH$            
&$    1 $
&$  -0.6$
&$  -0.4$
&$   0.4$
  \\
 VBF            
&$  -0.6$
&$    1 $
%&$   0.4$
&$   0.6$
&$  -0.2$
  \\
 $\PW\PH$             
&$  -0.4$
&$   0.6$
&$    1 $
&$   0  $
  \\
 $\PQt\PAQt \PH$            
&$   0.4$
&$  -0.2$
&$   0  $
&$    1 $
  \\
 $\PW$              
&$  -0.6$
&$   0.4$
&$   0.6$
&$  -0.4$
  \\
 $\PW\PW$             
&$  -0.4$
&$   0.6$
&$   0.8$
&$  -0.2$
  \\
 $\PW\PZ$             
&$  -0.4$
&$   0.4$
&$   0.8$
&$  -0.2$
  \\
  $\PW\PGg$             
&$  -0.4$
&$   0.4$
&$   0.6$
&$  -0.6$
  \\
 $\PW\PQb \PAQb$            
&$  -0.2$
&$   0.6$
&$   0.8$
&$  -0.2$
  \\
  $\PQt\PAQt$             
&$   0.6$
&$  -0.4$
&$  -0.2$
&$   0.8$
  \\
 $\PQt\PAQb$             
&$  -0.4$
&$   0.6$
&$   0.8$
&$   0  $
  \\
 $\PQt(\to \PAQb)\PQq$            
&$   0.6$
&$  -0.2$
&$   0  $
&$   0  $
  \\
\hline 
\end{tabular}
}
 \quad 
{
\begin{tabular}{ccccc}
%\hline 
%  $300 \UGeV$ &&&&  \\ 
\hline 
$\MH=300 \UGeV$ & $\Pg\Pg\PH$ & VBF & $\PW\PH$ & $\PQt\PAQt \PH$  \\
\hline 
 $\Pg\Pg\PH$            
&$    1 $
&$  -0.4$
&$  -0.2$
&$   0.6$
  \\
 VBF            
&$  -0.4$
&$    1 $
&$   0.4$
&$  -0.2$
  \\
 $\PW\PH$             
&$  -0.2$
&$   0.4$
&$    1 $
&$   0.2$
  \\
 $\PQt\PAQt \PH$            
&$   0.6$
&$  -0.2$
&$   0.2$
&$    1 $
  \\
 $\PW$              
&$  -0.6$
&$   0.4$
&$   0.4$
&$  -0.6$
  \\
 $\PW\PW$             
&$  -0.4$
&$   0.6$
&$   0.8$
&$  -0.2$
  \\
 $\PW\PZ$             
&$  -0.6$
&$   0.4$
&$   0.6$
&$  -0.4$
  \\
  $\PW\PGg$             
&$  -0.6$
&$   0.4$
&$   0.4$
&$  -0.6$
  \\
 $\PW\PQb \PAQb$            
&$  -0.2$
&$   0.4$
&$   0.8$
&$  -0.2$
  \\
  $\PQt\PAQt$             
&$    1 $
&$  -0.4$
&$   0  $
&$   0.8$
  \\
 $\PQt\PAQb$             
&$  -0.4$
&$   0.4$
&$   0.8$
&$  -0.2$
  \\
 $\PQt(\to \PAQb)\PQq$            
&$   0.4$
&$  -0.2$
&$   0  $
&$  -0.2$
  \\
\hline 
\end{tabular}
}
\end{center}
\begin{center}
{
\begin{tabular}{ccccc}
%\hline 
%  $500 \UGeV$ &&&&  \\ 
\hline 
$\MH=500 \UGeV$ & $\Pg\Pg\PH$ & VBF & $\PW\PH$ & $\PQt\PAQt \PH$  \\
\hline 
 $\Pg\Pg\PH$            
&$    1 $
&$  -0.4$
&$   0  $
&$   0.8$
  \\
 VBF            
&$  -0.4$
&$    1 $
&$   0.4$
&$  -0.2$
  \\
 $\PW\PH$             
&$   0  $
&$   0.4$
&$    1 $
&$   0  $
  \\
 $\PQt\PAQt \PH$            
&$   0.8$
&$  -0.2$
&$   0  $
&$    1 $
  \\
 $\PW$              
&$  -0.6$
&$   0.4$
&$   0.2$
&$  -0.6$
  \\
 $\PW\PW$             
&$  -0.4$
&$   0.6$
&$   0.6$
&$  -0.4$
  \\
 $\PW\PZ$             
&$  -0.6$
&$   0.4$
&$   0.6$
&$  -0.4$
  \\
  $\PW\PGg$             
&$  -0.6$
&$   0.4$
&$   0.2$
&$  -0.6$
  \\
 $\PW\PQb \PAQb$            
&$  -0.4$
&$   0.4$
&$   0.6$
&$  -0.4$
  \\
  $\PQt\PAQt$             
&$    1 $
&$  -0.4$
&$   0  $
&$   0.8$
  \\
 $\PQt\PAQb$             
&$  -0.4$
&$   0.4$
&$   0.8$
&$  -0.2$
  \\
 $\PQt(\to \PAQb)\PQq$            
&$   0.2$
&$  -0.2$
&$   0  $
&$  -0.2$
  \\
\hline 
\end{tabular}
}
  \\
\end{center}
\end{center}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}
\begin{center}
\caption[]{The same as  \refT{tab:signal-correlations}
for the correlations between background processes.
\label{tab:back-correlations}}
\small
\vspace{0.2cm}
\begin{center}
{
\small
\begin{tabular}{ccccccccc}
\hline 
%& & & & & & & & \\
&  $\PW$                  &  $\PW\PW$                 &  $\PW\PZ$                 &  $\PW\PGg$            &  $\PW\PQb \PAQb$         
&  $\Pt \PAQt$           &  $\PQt\PAQb$            &  $\PQt(\to \PAQb)\PQq$      \\
\hline 
 $\PW$                 
&$    1 $
&$   0.8$
&$   0.8$
&$    1 $
&$   0.6$
&$  -0.6$
&$   0.6$
&$  -0.2$
  \\
 $\PW\PW$                
&$   0.8$
&$    1 $
&$   0.8$
&$   0.8$
&$   0.8$
&$  -0.4$
&$   0.8$
&$   0  $
  \\
 $\PW\PZ$                
&$   0.8$
&$   0.8$
&$    1 $
&$   0.8$
&$   0.8$
&$  -0.4$
&$   0.8$
&$   0  $
  \\
 $\PW\PGg$           
&$    1 $
&$   0.8$
&$   0.8$
&$    1 $
&$   0.6$
%&$  -0.4$
&$  -0.6$
&$   0.8$
&$   0  $
  \\
 $\PW\PQb \PAQb$         
&$   0.6$
&$   0.8$
&$   0.8$
&$   0.6$
&$    1 $
&$  -0.2$
&$   0.6$
&$   0  $
  \\
 $\PQt \PAQt$          
&$  -0.6$
&$  -0.4$
&$  -0.4$
&$  -0.6$
&$  -0.2$
&$    1 $
&$  -0.4$
&$   0.2$
  \\
 $\PQt\PAQb$           
&$   0.6$
&$   0.8$
&$   0.8$
&$   0.8$
&$   0.6$
&$  -0.4$
&$    1 $
&$   0.2$
  \\
 $\PQt(\to \PAQb)\PQq$    
&$  -0.2$
&$   0  $
&$   0  $
&$   0  $
&$   0  $
&$   0.2$
&$   0.2$
&$    1 $
  \\
\hline 
\end{tabular}
}
  \\
\end{center}
\end{center} 
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsubsection{Results for the correlation study}

Our main result is the computation of the correlation
between physical processes relevant to Higgs production,
either as signal or as background.
It is summarised in 
\refTs{tab:signal-correlations} and 
\ref{tab:back-correlations}, where we show the  PDF4LHC average
for each of the correlations between signal and
background processes considered. These tables classify each correlation 
in classes with $\Delta\rho=0.2$, that is, if the correlation
is  $1> \rho > 0.9$ the processes is assigned correlation 1,
if $0.9> \rho > 0.7$ the processes is assigned correlation $0.8$ and so on.
The class width is typical of the spread of the results from the 
PDF sets, which are in generally very good, but not perfect agreement. 
The average is obtained using the most up-to-date PDF sets (CT10,
MSTW2008, NNPDF2.1) in the PDF4LHC 
recommendation, and it is  appropriate for use in conjunction 
with the cross-section results obtained in \Bref{Dittmaier:2011ti}, and with 
the background processes listed; the change of correlations due to 
updating the prescription is insignificant in comparison  to 
the class width.


%The minor update leads to only small changes 
%in the average correlation, and some calculations have now been updated 
%to the most recent PDF versions. 
We have also compared this PDF4LHC average to 
the average using all six PDF sets. In general there is rather little change. 
There are quite a few cases where the 
average moves into a neighbouring class but in many cases due to a very small 
change taking the average just over a boundary between two classes. There is 
only a move into the next-to-neighbouring class, \ie  a change of more than $0.2$, in a very small number of 
cases. For the VBF--$\PW\PGg$ correlation at $\MH=120,160\UGeV$
it reduces from 
$0.6$ to $0.2$, for the VBF--$\PW$ correlation at $\MH=500\UGeV$ it reduces from $0.4$
to $0$, for the $\PW\PGg$--$\PQt\PAQb$ correlation it reduces from $0.8$ to 
$0.4$, for $\PW\PZ$--$\PQt\PAQt\PH$ at $\MH=120\UGeV$ it increases from $-0.4$ to $0.0$, 
and for $\PW\PQb \PAQb$--$\PQt \PAQt \PH$ at $\MH=200\UGeV$ it increases from $-0.2$ 
to $0.2$. 


%%%%%%%%%%%%%%%%%%%
\begin{figure}
\centerline{  
\includegraphics[width=0.49\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig1.eps}
\includegraphics[width=0.49\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig2.eps}
}
\vspace*{-1em}
\caption{The correlations between $\PW$ production
and the other background processes considered. We show
the results for NNPDF2.1, CT10 and MSTW2008 in the left plot,
and HERAPDF, JR and ABKM in the right plot. In both cases we
show the up-to-date PDF4LHC average result.
\label{fig:corr-back-w}} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%
\begin{figure}
\centerline{  
\includegraphics[width=0.49\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig3.eps}
\includegraphics[width=0.49\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig4.eps}
}
\vspace*{-1em}
\caption{The same as \refF{fig:corr-back-w} for 
 $\PW\PW$ production. } 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%
\begin{figure}
\centerline{  
\includegraphics[width=0.49\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig5.eps}
\includegraphics[width=0.49\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig6.eps}
}
\vspace*{-1em}
\caption{The same as \refF{fig:corr-back-w} for 
 $\PW\PZ$ production.
\label{fig:corr-back-wz}} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%
\begin{figure}
\centerline{  
\includegraphics[width=0.49\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig7.eps}
\includegraphics[width=0.49\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig8.eps}
}
\vspace*{-1em}
\caption{The same as \refF{fig:corr-back-w} for 
 $\PW\PGg$ production.
\label{fig:corr-back-wg}} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%
\begin{figure}
\centerline{  
\includegraphics[width=0.49\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig9.eps}
\includegraphics[width=0.49\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig10.eps}
}
\vspace*{-1em}
\caption{The same as \refF{fig:corr-back-w} for 
 $\PW\PQb\PAQb$ production.
\label{fig:corr-back-wqq}} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%
\begin{figure}
\centerline{  
\includegraphics[width=0.49\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig11.eps}
\includegraphics[width=0.49\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig12.eps}
}
\vspace*{-1em}
\caption{The same as \refF{fig:corr-back-w} for 
 $\PQt\PAQt$ production.
\label{fig:corr-back-tt}} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%
\begin{figure}
\centerline{  
\includegraphics[width=0.49\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig13.eps}
\includegraphics[width=0.49\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig14.eps}
}
\vspace*{-1em}
\caption{The same as \refF{fig:corr-back-w} for 
 $\PQt\PAQb$ production.
\label{fig:corr-back-tb}} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%
\begin{figure}
\centerline{  
\includegraphics[width=0.49\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig15.eps}
\includegraphics[width=0.49\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig16.eps}
}
\vspace*{-1em}
\caption{The same as \refF{fig:corr-back-w} for 
 $t$-channel 
$\PQt(\to \PAQb)\PQq$ production.
\label{fig:corr-back-tbq}} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%
\begin{figure}
\centerline{  
\includegraphics[width=0.99\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig17.eps}
}
\vspace*{-.5em}
\caption{Correlation between the gluon-fusion $\Pg\Pg\to \PH$ process
and other signal and background processes as a function of $\MH$.
We show the results for the individual PDF sets as well
as the up-to-date PDF4LHC average.
\label{fig:corr-ggh1}} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%
\begin{figure}
\centerline{  
\includegraphics[width=0.99\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig18.eps}
}
\vspace*{-.5em}
\caption{Correlation between the gluon fusion $\Pg\Pg\to \PH$ process
and other signal and background processes as a function of $\MH$.
We show the results for the individual PDF sets as well
as the up-to-date PDF4LHC average.
\label{fig:corr-ggh2}} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%
\begin{figure}
\centerline{  
\includegraphics[width=0.99\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig19.eps}
}
\vspace*{-.5em}
\caption{The same as \refF{fig:corr-ggh1} for the vector
boson fusion process.
\label{fig:corr-vbf1}} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%
\begin{figure}
\centerline{  
\includegraphics[width=0.99\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig20.eps}
}
\vspace*{-.5em}
\caption{The same as \refF{fig:corr-ggh2} for the vector
boson fusion process.
\label{fig:corr-vbf2}} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%


A more complete list of processes, with results for each 
individual PDF set, may be found in the tables on the webpage at the
LHC Higgs Cross Section Working Group TWiki~\cite{Twiki}. Note, however, that there 
is a high degree of redundancy in the approximate correlations of 
many processes. For example $\PW$ production is very similar to $\PZ$ production, 
both depending on partons (quarks in this case) at very similar hard 
scales and $x$ values. 
Similarly for $\PW\PW$ and $\PZ\PZ$, and the subprocesses $\Pg\Pg\to \PW\PW(\PZ\PZ)$ and 
Higgs production via gluon--gluon fusion for $\MH=200\UGeV$.  

\begin{sloppypar}
More detailed results are presented for the MSTW2008, NNPDF2.1, CT10, 
HERAPDF1.5, GJR08, and ABKM09 PDFs in 
\refFs{fig:corr-back-w}--\ref{fig:corr-tth2}. 
The result using each individual PDF sets is compared to the 
(updated) PDF4LHC average. There is usually a fairly narrow clustering of the 
individual results about the average, with a small number of cases 
where there is one, or perhaps two outliers. The sets with the largest 
parametrisations 
for the PDFs generally tend to give smaller magnitude correlations or 
anticorrelations, 
but this is not always the case, \eg  NNPDF2.1 gives the largest 
anti-correlation for VBF--$\PQt\PAQt\PH$.
There are some unusual features, \eg  for HERAPDF1.5 and high values of $\MH$, 
the $\PQt \PAQt\PH$ 
correlations with quantities depending on the high-$x$ gluon, \eg  
$\Pg\Pg\PH$ and $\PQt\PAQt$ is opposite to the other sets and the correlations with 
quantities depending on high-$x$ quarks and antiquarks, \eg  VBF and $\PW\PW$, is
stronger. This is possibly related to the large high-$x$ 
antiquark distribution in 
HERAPDF which contributes to $\PQt \PAQt\PH$ but not $\Pg\Pg\PH$ or very much 
to $\PQt\PAQt$. GJR08 has a tendency to obtain more correlation between 
some gluon dominated processes, \eg  $\Pg\Pg\PH$ and $\Pt \PAQt$ and quark 
dominated processes, \eg  $\PW$  and $\PW\PZ$, perhaps because the dynamical 
generation of PDFs couples the gluon and quark more strongly.     
\end{sloppypar}

We can now also see the origin of 
the cases where the averages move two classes.
For VBF--$\PW\PGg$ at lower masses GJR08, ABKM09, and HERAPDF1.5 all lie lower than 
the (updated) PDF4LHC average, but not too different to CT10. For VBF--$\PW$ at 
$\MH=500\UGeV$, ABKM09, and GJR08 give a small anticorrelation, while others 
give a correlation, though it is only large in the case of MSTW2008. For 
$\PW\PGg$--$\PQt\PAQb$ the change is due to slightly lower 
correlations for GJR08. However, although the change is two 
classes, in practice it is barely more than $0.2$.  
For $\PW\PZ$--$\PQt\PAQt\PH$ at $\MH=120\UGeV$ both HERAPDF1.5 and GJR08 have a larger 
correlation. This increases with $\MH$ for HERAPDF1.5 as noted, whereas 
GJR08 heads closer to the average, but the move at $\MH=120\UGeV$ is only 
marginally two classes, and is only one class for other masses. For 
$\PW\PQb \PAQb$--$\PQt \PAQt \PH$ at $\MH=200\UGeV$ the situation is similar, and 
MSTW2008 gives easily the biggest pull in the direction of anticorrelation. 

%%%%%%%%%%%%%%%%%%%
\begin{figure}
\centerline{  
\includegraphics[width=0.99\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig21.eps}
}
\vspace*{-.5em}
\caption{The same as \refF{fig:corr-ggh1} for the $\PW\PH$ production process.
\label{fig:corr-wh1}} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%
\begin{figure}
\centerline{  
\includegraphics[width=0.99\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig22.eps}
}
\vspace*{-.5em}
\caption{The same as \refF{fig:corr-ggh2} for the $\PW\PH$ production process.
\label{fig:corr-wh2}} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%
\begin{figure}
\centerline{  
\includegraphics[width=0.99\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig23.eps}
}
\vspace*{-.5em}
\caption{The same as \refF{fig:corr-ggh1} for the $\PQt\PAQt\PH$ production process.
\label{fig:corr-tth1}} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%
\begin{figure}
\centerline{  
\includegraphics[width=0.99\textwidth]{YRHXS2_PDF/YRHXS2_PDF_fig24.eps}
}
\vspace*{-.5em}
\caption{The same as \refF{fig:corr-ggh2} for the $\PQt\PAQt\PH$ production process.
\label{fig:corr-tth2}} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%



\subsubsection{Additional correlation studies}

The inclusion of the $\alphas$ uncertainty on the correlations, compared to 
PDF only variation, was also 
studied for some PDF sets, \eg  MSTW2008 using the 
approximation that the PDF sets for 
the upper and lower $\alphas$ values~\cite{Martin:2009bu} simply form another pair of orthogonal
eigenvectors (show to be true in the quadratic approximation~\cite{Lai:2010nw}). 
This increases the correlation between some processes, \ie  $\PW$ 
production 
and Higgs via gluon fusion, because the former increases with $\alphas$ due 
to increased evolution of quarks while the latter increases due to direct 
dependence on $\alphas$. Similarly for \eg  Higgs production via gluon 
fusion and $\PQt\PAQt$ production since each depends directly on $\alphas$.
This may also contribute to some of the stronger correlations 
seen using GJR08 in some similar cases.   
In a handful of processes it reduces correlation, \eg  $\PW$ and $\PW\PQb\PAQb$ 
since the latter has a much stronger $\alphas$ dependence. In most cases the 
change compared to PDF-only correlation for a given PDF sets small, and it is not an obvious contributing factor to the cases where the 
(updated) PDF4LHC average is noticeably different to the average using six sets, 
except possibly to $\PW\PQb \PAQb$--$\PQt \PAQt \PH$ at $\MH=200\UGeV$, where 
$\alphas$ does increase correlation.   
  
A small number of correlations were also calculated at NNLO for MSTW2008 PDFs, 
\ie  $\PW$, $\PZ$ and $\Pg\Pg \to \PH$ for the same range of $\MH$. The correlations 
when taking into account PDF uncertainty alone were almost identical to those 
at NLO, variations being less than $0.05$. When $\alphas$ uncertainty 
was included the correlations changed a little more due to $\Pg\Pg \to \PH$
having more direct dependence on $\alphas$, but this is a relatively minor 
effect. Certainly the results in  
\refTs{tab:signal-correlations} 
and \ref{tab:back-correlations}, though calculated at NLO,  can be 
used with confidence at NNLO.  



