
\subsection{\POWHEGBOX{} and \MCatNLO{} comparison with \HqT{}
\footnote{K.~Hamilton, F.~Maltoni, P.~Nason and P.~Torrielli}}

\label{NLOPSsec:compHqT}

In general, there are several sources of  uncertainties possibly affecting the NLO+PS results:
\begin{itemize}
\item Factorisation and renormalisation scale uncertainties. Normally, the NLO
calculation underlying the NLO+PS generator has independent factorisation and
renormalisation scales, and results are affected at the NNLO level if these scales are varied.
\item Uncertainties related to the part of the real cross section that is treated with the shower algorithm.
\item Uncertainties related to how the shower algorithm is implemented.
\item Uncertainties related to the PDF's themselves and also  whether the PDF's used in the
shower algorithm are different from those used in the NLO calculation.
\item Further uncertainties common to all shower generators, i.e. hadronisation, underlying event, etc.
\end{itemize}
We focus here upon the first three items, that are by far the dominant ones.
As a relevant and phenomenologically important observable, we consider the transverse 
momentum distribution of the Higgs boson, in the process $\Pg\Pg\to \PH$. This observable is not  a fully NLO
one in the $\Pg\Pg\to \PH+X$ NLO cross section calculation.  For this reason it is very sensitive to both soft and hard effects and
the NLO+PS results displays more marked differences with respect to the pure
NLO calculation. The latter, in fact, is divergent for $\pT\to 0$, with
and infinite negative spike at $\pT=0$, representing the contribution
of the virtual corrections. The NLO+PS approaches, instead, yield a positive
cross section for all $\pT$, with the characteristic Sudakov peak at small $\pT$.
Furthermore, this distribution can be computed
(using the \HqT{} program \cite{hqt}) at a matched NNLL+NNLO accuracy, that can
serve as a benchmark to characterise the output of the generators.

We begin by showing in \refF{figNLOMC:ptMCatNLOMH} the
comparisons of \MCatNLO{}+\HERWIG{} and \POWHEG{}+\PYTHIA{} with \HqT{}, with
a choice of parameters that yields the best agreement,
in shape, of the $\pT$ distributions. This choice of parameters can be  therefore considered  the
main outcome of this study, i.e. it embodies our recommendation for the settings of the two generators.
The settings are as follows:
\begin{itemize}
\item
\MCatNLO{} should be run with the factorisation and renormalisation
scale equal to $\MH$.
\item
\POWHEG{} should be run with the $h$ parameter equal to $\MH/1.2$.
For $\MH=120\UGeV$, this setting is achieved introducing the line
{\tt hfact 100} in the {\tt powheg.input} file.
\end{itemize}
\begin{figure}[htb]
%\vspace{5pt}
\centering
\includegraphics[height=\figfact\linewidth]{YRHXS2_NLOMC/comb-11-MCNLO-1mmcnlo_UE_mH-log}\nolinebreak
\includegraphics[height=\figfact\linewidth]{YRHXS2_NLOMC/comb-11-MCNLO-1mmcnlo_UE_mH-lin}
\includegraphics[height=\figfact\linewidth]{YRHXS2_NLOMC/comb-11-PY-nfsc-log}\nolinebreak
\includegraphics[height=\figfact\linewidth]{YRHXS2_NLOMC/comb-11-PY-nfsc-lin}
%\vspace*{-15pt}
\caption{Uncertainty bands for the transverse momentum spectrum of the
  Higgs boson at LHC, $7\UTeV$, for a Higgs mass $\MH=120\UGeV$.  On the
  upper plots,
  the \MCatNLO{}+\HERWIG{} result obtained using the non-default value
  of the reference scale equal to $\MH$. On the lower plots, the
  \POWHEG{}+\PYTHIA{} output, using the non-default $\Rups +\Rupf $
  separation.  The uncertainty bands are obtained by changing $\muR$
  and $\muF$ by a factor of two above and below the central value,
  taken equal to $\MH$, with the restriction $0.5<\muR/\muF<2$.}
\label{figNLOMC:ptMCatNLOMH}
\end{figure}
In the figures, the uncertainty band of the \MCatNLO{}+\HERWIG{}
and of \POWHEG{}+\PYTHIA{},
both compared to the \HqT{} uncertainty band, are displayed.
In the lower insert in the figure,
the ratio of all results to the central value of \HqT{} is also displayed.
As stated above, \MCatNLO{}+\HERWIG{}
is run with the central value of the renormalisation scale fixed to $\MH$, and
\POWHEG{} is run with the input line {\tt hfact 100}.
The red, solid curves represent the uncertainty band of the NLO+PS, while
the dotted blue lines represent the band obtained with the \HqT{} program.
The reference scale is chosen equal to $\MH/2$ in \HqT{}, and the scale variations
are performed in the same way as in the NLO+PS generators:
once considers all variations of a factor of two above and below the central scale,
with the restriction $0.5<\muR/\muF<2$.
We have used the MSTW2008 NNLO central set for all curves. This is because \HqT{}
requires NNLO parton densities, and because we want to focus upon differences
that have to do with the calculation itself, rather than the PDF's. The
\HqT{} result has been obtained by running the program with full NNLL+NNLO accuracy,
using the ``switched'' result. The resummation scale $Q$ in \HqT{} has been set
to $\MH/2$.

We notice that both programs are compatible in shape with the \HqT{} prediction.
We also notice that the error band of the two NLO+PS generators is relatively small at
small $\pT$ and becomes larger at larger $\pT$. This should remind us that the
NLO+PS prediction for the high $\pT$ tail is in fact a tree level only prediction,
since the production of a Higgs plus a light parton starts at order $\alphas^3$,
its scale variation is of order $\alphas^4$, and its \emph{relative} scale variation
is of order $\alphas^4/\alphas^3$, i.e. of order $\alphas$.\footnote{Here we remind the reader that
$\muF^2\frac{d}{d\muF^2} \alphas^3(\muR)=-b_0 3\alphas^4(\muR)$.}
On the other hand the total integral of the curve, i.e. the total cross section
(and in fact also the Higgs rapidity distribution, that is obtained by
integrating over all transverse momenta)
are given by a term of order $\alphas^2$ plus
a term of order $\alphas^3$, and their scale variation is also of order $\alphas^4$. Thus, their
relative scale variation is of order $\alphas^4/\alphas^2$, i.e. $\alphas^2$.

It is instructive to analyze the difference between \MCatNLO{} and \POWHEG{} at their
default value of parameters. This is illustrated in \refF{figNLOMC:ptHdefault}.
\begin{figure}[htb]
%\vspace{5pt}
\centering
\includegraphics[height=\figfact\linewidth]{YRHXS2_NLOMC/comb-11-MCNLO-1mmcnlo_UE_mT-log}\nolinebreak
\includegraphics[height=\figfact\linewidth]{YRHXS2_NLOMC/comb-11-MCNLO-1mmcnlo_UE_mT-lin}
\includegraphics[height=\figfact\linewidth]{YRHXS2_NLOMC/comb-11-PY-f99std-log}\nolinebreak
\includegraphics[height=\figfact\linewidth]{YRHXS2_NLOMC/comb-11-PY-f99std-lin}
%\vspace*{-15pt}
\caption{The transverse momentum spectrum of the Higgs in \MCatNLO{}
(upper) and in \POWHEG{}+\PYTHIA{} (lower) compared to the \HqT{} result. In the lower
insert, the same results normalised to the \HqT{} central value are shown.}
\label{figNLOMC:ptHdefault}
\end{figure}
The two programs are in reasonable agreement at small transverse momentum, but
display a large difference (about a factor of 3) in the high transverse
momentum tail. This difference has two causes. One is the different scale
choice in \MCatNLO{}, where by default $\mu=\mT=\sqrt{\MH^2+\pT^2}$, where $\pT$ is the
transverse momentum of the Higgs. That accounts for a factor of $(\alphas(\mT)/\alphas(\MH))^3$,
which is about 1.6 for the last bin in the plots
(compare the upper plots of \refF{figNLOMC:ptMCatNLOMH} with those of \refF{figNLOMC:ptHdefault}). 
The remaining difference is due to the fact that in \POWHEG{}, used with default parameters, the NLO K-factor
multiplies the full transverse momentum distribution. The \POWHEG{} output is
thus similar to what is obtained with NLO+PS generator, as already observed
in the first volume of this report.

This point deserves a more detailed explanation, which can be given
along the lines of \Bref{Alioli:2008tz,Nason:2010ap}.
We write below the differential cross section for the hardest emission in 
NLO+PS implementations (see the first volume of this report for details)
\begin{equation}
\label{EqNLOMC:Powheg}
{\rm d}\sigma^{\rm NLO+PS} =
{\rm d}\Phi_B\bar B^{s}(\Phi_B)
\left[\Delta^s(\pperpmin) 
  +  {\rm d}\Phi_{R|B}
      \frac{\Rups (\Phi_R)}{B(\Phi_B)}
      \Delta^s(\pT(\Phi))\right] 
+ {\rm d}\Phi_R \Rupf (\Phi_R),
\end{equation}
where
\begin{equation}\label{eqNLOMC:nlocorr1}
\bar{B}^{s}=B(\Phi_B)+\left[V(\Phi_B)+
\int{\rm d}\Phi_{R|B} \Rups (\Phi_{R|B})\right]\,.
\end{equation}
The sum $\Rups +\Rupf $ yields the real cross section for $\Pg\Pg\to \PH\Pg$,
plus the analogous terms for quark-gluon. Quark-antiquark annihilation is finite
and therefore only contributes to $\Rupf $.

In \MCatNLO{}, the $\Rups $ term is the shower approximation to the real cross
section, and it depends upon the SMC that is being used in conjunction with it.
In \POWHEG{}, one has much freedom in choosing $\Rups $, with the only constraint
$\Rups \le R$, in order to avoid negative weights, and $\Rups \to R $
in the small transverse momentum limit  (in the sense that $\Rups-R$ should be integrable
in this region).

For the purpose of this review, we call $\mathbb{S}$ events (for shower)
those generated by the first term on the r.h.s. of~Eq.~(\ref{EqNLOMC:Powheg}), 
i.e.\ those generated using the shower algorithm, and $\mathbb{F}$ (for finite)
events those generated by the $\Rupf$ term.\footnote{In the \MCatNLO{} 
language, these are called $\mathbb{S}$ and $\mathbb{H}$ events, where 
$\mathbb{S}$ stands for standard, and $\mathbb{H}$ for hard.}
The scale dependence typically affects the $\bar{B}$
and the $\Rupf$ terms in a different way.
A scale variation in the square bracket on the r.h.s.
of Eq.~(\ref{EqNLOMC:Powheg}) is in practice never performed, since
in \MCatNLO{} this can only be achieved by changing
the scale in the Monte Carlo event generator that is being used,
and in \POWHEG{} the most straightforward way to perform it
(i.e. varying it naively by a constant factor) would spoil the NLL accuracy
of the Sudakov form factor. We thus assume from now on that the
scales in the square parenthesis are kept fixed. Scale variation
will thus affect $\bar{B}^{s}$ and $\Rupf $.

We observe now that the shape of the transverse momentum of the hardest 
radiation in $\mathbb{S}$ events is not affected by scale variations, given 
that the square bracket on the r.h.s.\ of Eq.~(\ref{EqNLOMC:Powheg}) is not 
affected by it, and that the factor $\bar{B}$
is $\pT$ independent. From this, it immediately
follows that the scale variation of the large transverse momentum
tail of the spectrum is of relative order $\alphas^2$, i.e. the same relative
order of the inclusive cross section, rather than of relative order $\alphas$,
since $\bar{B}$ is a quantity integrated in the transverse momentum.
Eq.~(\ref{EqNLOMC:Powheg}), in the large transverse momentum tail,
becomes
\begin{equation}
\label{EqNLOMC:PowhegLargePt}
{\rm d}\sigma^{\rm NLO+PS} \approx
{\rm d}\Phi_B\bar B^{s}(\Phi_B)
   {\rm d}\Phi_{R|B}
      \frac{\Rups (\Phi_R)}{B(\Phi_B)}
+ {\rm d}\Phi_R \Rupf (\Phi_R),
\end{equation}
From this equation we see that for large transverse momentum, the $\mathbb{S}$ 
event contribution to the cross section is enhanced by a factor $\bar{B}/B$, 
which is in essence the
K factor of the process. We wish to emphasize that this factor does not spoil
the NLO accuracy of the result, since it affects the distribution by terms of
higher order in $\alphas$. Now, in \POWHEG{}, in its default configuration,
$\Rupf $ is only given by the tiny contribution $\PQq\PAQq\to \PH \Pg$, which 
is non-singular, so that $\mathbb{S}$ events dominate the cross section. The 
whole transverse momentum distribution is thus affected by the K factor, 
yielding a result that is similar to what is obtained in ME+PS calculations, 
where the NLO K factor is applied to the LO distributions.  Notice also that 
changing the form of the central value of the scales again does not
change the transverse momentum distribution, that can only be affected by
touching the scales in the Sudakov form factor.

A simple approach to give a realistic assessment of the uncertainties
in \POWHEG{}, is to also exploit the freedom in the separation $R=\Rups+\Rupf$.
Besides the default value $\Rupf=0$, one can also perform the separation
\begin{equation}\label{eqNLOMC:SFsep}
\Rups=\frac{h^2}{h^2+\pT^2}R\,,\quad\quad \Rupf=\frac{\pT^2}{h^2+\pT^2}R\;.
\end{equation}
In this way, $\mathbb{S}$ and $\mathbb{F}$ events are generated, with the 
former dominating
the region $\pT<h$ and the latter the region $\pT>h$. Notice that by sending $h$
to infinity one recovers the default behaviour. It is interesting to ask what happens
if $h$ is made vanishingly small. It is easy to guess that in this limit the
\POWHEG{} results will end up coinciding with the pure NLO result.
The freedom in the choice of $h$, and also the freedom in changing the form
of the separation in Eq.~(\ref{eqNLOMC:SFsep}) can be exploited to explore further
uncertainties in \POWHEG{}. The Sudakov exponent changes by terms subleading
in $\pT^2$, and so we can explore in this way uncertainties related to the
shape of the Sudakov region.
Furthermore, by suppressing $\Rups$ at large $\pT$ the hard tail of the transverse
momentum distribution becomes more sensitive to the scale choice.
The lower plots of \refF{figNLOMC:ptMCatNLOMH} displays the \POWHEG{} result obtained using $h=\MH/1.2$. Notice that in this way the large transverse
momentum tail becomes very similar to the \MCatNLO{} result. The shape of the distribution
at smaller transverse momenta is also altered, and in better agreement
with \HqT{}. If $\mT$ rather than $\MH$ is chosen as reference value for the scale,
we obtain the result of \refF{figNLOMC:pt99POWHEG}, where we see also here
a fall of the cross section at large transverse momentum.
\begin{figure}[htb]
%\vspace{5pt}
\centering
\includegraphics[height=\figfact\linewidth]{YRHXS2_NLOMC/comb-11-PY-nrsc-log}\nolinebreak
\includegraphics[height=\figfact\linewidth]{YRHXS2_NLOMC/comb-11-PY-nrsc-lin}
%\vspace*{-15pt}
\caption{The transverse momentum spectrum of the Higgs in \POWHEG{}+\PYTHIA{},
using the separation of $\mathbb{S}$ and $\mathbb{F}$ events. The central 
scale is chosen equal to $\sqrt{\pT^2+\MH^2}$.}
\label{figNLOMC:pt99POWHEG}
\end{figure}

The shape of the $\pT$ distribution in \MCatNLO{}+\HERWIG{} is not much affected by
the change of scale for $\pT\le 100 \UGeV$. This is due to the fact that
this region is dominated by $\mathbb{S}$ events. It is interesting
to ask whether this region is affected if one changes
the underlying shower Monte Carlo generator.
In \MCatNLO{}, an interface to \PYTHIA{}, using the virtuality ordered shower,
is also available \cite{Torrielli:2010aw}. The results are
displayed in \refF{figNLOMC:mcatnlopy}.
\begin{figure}[htb]
%\vspace{5pt}
\centering
\includegraphics[height=\figfacto\linewidth]{YRHXS2_NLOMC/comb-11-mH-mcatnlo_py-log}\nolinebreak
\includegraphics[height=\figfacto\linewidth]{YRHXS2_NLOMC/comb-11-mH-mcatnlo_py-lin}
\includegraphics[height=\figfacto\linewidth]{YRHXS2_NLOMC/comb-11-mT-mcatnlo_py-log}\nolinebreak
\includegraphics[height=\figfacto\linewidth]{YRHXS2_NLOMC/comb-11-mT-mcatnlo_py-lin}
%\vspace*{-15pt}
\caption{The transverse momentum spectrum of the Higgs in \MCatNLO{}+\PYTHIA{}.
The central scale is chosen
equal to $\MH$ in the upper plot, and to $\sqrt{\pT^2+\MH^2}$ in the lower
plot. The bare \PYTHIA{} result, normalised to the \HqT{} central value total
cross section, is also shown.}
\label{figNLOMC:mcatnlopy}
\end{figure}
We observe the following. The large transverse momentum tails are consistent with
the \HERWIG{} version. We expect that since this region is dominated by $\mathbb{F}$ events.
The shape of the Sudakov region has changed, showing a behaviour that is more
consistent with the \HqT{} central value, down to scales of about $30\UGeV$. Below
this scale, we observe a $50\%{}$ increase of the cross section as smaller
$\pT$ values are reached. It is clear from the figure that this feature is inherited
from \PYTHIA{}. In fact, the shape from the transverse momentum spectrum in \MCatNLO{}
is inherited from the shower Monte Carlo.
It is likely that the transverse momentum ordered \PYTHIA{} may yield better agreement
with the \HqT{} result.

Summarizing, we find large uncertainties in both \MCatNLO{} and \POWHEG{}
NLO+PS generators for Higgs production in gluon fusion. We have explored here 
uncertainties having to do with scale variation and to the separation of 
$\mathbb{S}$ and $\mathbb{F}$ events. If a higher accuracy result (namely
\HqT{}) was not available, the whole envelope of the uncertainties within
each approach should be considered. These large uncertainties are all
a direct consequence of the large NLO K-factor of the $gg\to H$
process.  In spite of this fact, we have seen that there are choices
of scales and parameters that bring the NLO+PS results close in shape
to the higher accuracy calculation of the transverse momentum spectrum
provided by the \HqT{} program. It is thus advisable to adopt these choices.
