\providecommand{\MHpm}{M_{\PSHpm}}
\providecommand{\lsim}
{\;\raisebox{-.3em}{$\stackrel{\displaystyle <}{\sim}$}\;}
\providecommand{\gsim}
{\;\raisebox{-.3em}{$\stackrel{\displaystyle >}{\sim}$}\;}
\providecommand{\mhmaxx}{{\ensuremath{m_{\rm h}^{\rm max}}}~}
\providecommand{\eqn}[1]{Eq.\,(\ref{#1})}
\providecommand{\fig}[1]{Fig.\,\ref{#1}}

\section{Charged-Higgs-boson production and decay\footnote{%
    M.~Flechl, S.~Heinemeyer, M.~Kr\"amer, S.~Lehti (eds.);
    M. Hauru, M. Spira and M. Ubiali.}}

Many extensions of the Standard Model, in particular supersymmetric
theories, require two Higgs doublets leading to five physical scalar
Higgs bosons, including two (mass-degenerate) charged particles $\PSHpm$.
The discovery of a charged Higgs boson would provide
unambiguous evidence for an extended Higgs sector beyond the Standard
Model. Searches at LEP have set a limit $\MHpm > 79.3$\UGeV\
on the mass of a charged Higgs boson in a general two-Higgs-doublet
model~\cite{Heister:2002ev}. 
One usually distinguishes a ``light charged Higgs'', $\MHpm < \Mt$, and a
``heavy charged Higgs'', $\MHpm > \Mt$. 
Within the MSSM, the charged Higgs boson
mass is constrained by the pseudoscalar Higgs mass and the $\PW$-boson
mass through $\MHpm^2 = \MA^2 + \MW^2$ at
tree level, with only moderate higher-order
corrections~\cite{Gunion:1988pc,Brignole:1991wp,Diaz:1991ki,Frank:2006yh}. A
mass limit on the MSSM charged Higgs boson can thus 
be derived from the limit on the pseudoscalar Higgs boson,
$\MA > 93.4$\UGeV~\cite{Schael:2006cr}, resulting in
$\MHpm \gsim 120$\UGeV. At the Tevatron, searches for light charged
%$\MHpm \approx 120$\UGeV. At the Tevatron, searches for light charged
Higgs bosons in top-quark decays $\PQt \to \PQb \PSHpm$~\cite{Aaltonen:2009ke,:2009zh} 
have placed some constraints
on the MSSM parameter space, but do not provide any further generic
bounds on $\MHpm$.

%The main mechanism for light charged Higgs boson production at the 
%LHC is from decays of top quarks in $\ttbar$ events, 
%$\PQt \to \PSHpm \PQb$. 
There are two main mechanisms for charged Higgs 
boson production at
the LHC:
\begin{displaymath}
\begin{array}{lcl}
  \mbox{top-quark decay:} & \PQt \to \PQb \PSHpm{\rm + X} &\; {\rm if}\;\;
  \MHpm \lsim m_{\rm t}\,, \\
  \mbox{associated production:} & \Pp\Pp \to \PQt \PQb\PSHpm{\rm + X}
  &\; {\rm if}\;\; \MHpm \gsim \Mt\,.
\end{array}
\end{displaymath}
The first process is dominant for the light charged Higgs, while the second
process dominates for a heavy charged Higgs boson.
Alternative production mechanisms like quark--antiquark annihilation
$\Pq\bar \Pq'\to \PSHpm$ and $\PSHpm+\mathrm{jet}$ production~\cite{Dittmaier:2007uw},
associated $\PSHpm \PWmp$ production~\cite{BarrientosBendezu:1998gd,BarrientosBendezu:1999vd,BarrientosBendezu:2000tu,Brein:2000cv,Eriksson:2006yt}, or
Higgs pair production~\cite{Krause:1997rc,Alves:2005kr,Brein:1999sy} have
suppressed rates, and it is not yet clear whether a signal could be
established in any of those channels.
Some of the above production processes may,
however, be enhanced in models with non-minimal flavour violation.
As the cross section for light-charged-Higgs-boson production are significantly 
higher than for heavy charged Higgs bosons, early searches at the LHC and also 
this section focuses mainly on those. All 
results shown below are for the \mhmaxx\ scenario of the MSSM.
% unless we find time later to do more. First with mu=200, later mu=-200, +/-1000.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Light charged Higgs boson}

To estimate the cross section for events with charged Higgs bosons in 
top-quark pair production, the following ingredients are needed: The top-quark
pair production cross section, the  
branching ratio BR($\PQt \to \PQb \PSHp$) and the light-charged-Higgs-boson
decay branching ratios. 
Complete scans of the ($M_{\PSHpm},\tan\beta)$
plane for $\sqrt{s}=7 \UTeV$ are available in electronic format~\footnote{{\tt https://twiki.cern.ch/twiki/bin/view/LHCPhysics/MSSMCharged}}.


\subsubsection{Top-quark pair production cross section}

The $\ttbar$ production cross section at $\sqrt{s}=7\UTeV$ is predicted to be 
$165^{+4}_{-9}$(scale)$^{+7}_{-7}$(PDF)~pb by approximate NNLO
calculations~\cite{Moch:2008ai,Langenfeld:2009tc}  
recommended by the ATLAS Top Working Group~\cite{Aad:2011yb}. The scale
uncertainty is obtained as the ``envelope'' from a variation of the
renormalisation scale $\muR$ and the factorisation scale $\muF$ 
from $0.5$~to~$2$ times $\Mt$ (with $0.5 <   \muF/\muR <   2$). 
The PDF uncertainty, obtained using MSTW2008~\cite{Martin:2009iq}, is taken
at the $68\%$~C.L.; the two uncertainties should be added linearly.



\subsubsection{Top-quark decays via a charged Higgs boson}
\label{sec:chiggs_top_quark_decays}

The decay width calculation of the top quark to
a light charged Higgs boson is compared for two different programs, {\sc
FeynHiggs}, 
version\,2.8.5~\cite{Heinemeyer:1998yj,Heinemeyer:1998np,Degrassi:2002fi,Frank:2006yh}, and 
{\sc HDECAY}, version 4.43~\cite{Spira:1996if,hdecay2}. The
\mhmaxx\ benchmark scenario is used~\cite{Carena:2002qg},
which (in the on-shell scheme) is defined
in \eqn{YRHXS_MSSM_neutral_eq:mhmax}. 
We slightly deviate from the original definition and use $\MHpm$ instead of
$\MA$ as input parameter. Furthermore, also the $\mu$ parameter
is varied with values $\pm 1000, \pm 200$\UGeV~\cite{Carena:2005ek}.
The Standard Model parameters are taken as given in
\Bref{Dittmaier:2011ti}, Appendix~A.

\medskip
The {\sc FeynHiggs} calculation is based on the evaluation of
$\Gamma(\PQt \to \PWp \PQb)$ and $\Gamma(\PQt \to \PSHp \PQb)$. The former is
calculated at NLO according to \Bref{Campbell:2004ch}. The decay
to the charged Higgs boson and the bottom quark uses $\Mt(\Mt)$ and
$\Mb(\Mt)$ in the Yukawa coupling, where the
latter receives the additional correction factor $1/(1 +
\Delta_{\PQb})$. 
%The leading contribution to $\Delta_{\PQb}$ is given
%by~\cite{Carena:1999py}
%\begin{equation}
%\label{eq:qcd_tanb_enhanced}
%\Delta_{\PQb} = \frac{C_F}{2}\frac{\alphas}{\pi}
%m_{\tilde{\Pg}} \mu \tan\beta  \,
%I(m_{\tilde{\PQb}_1},m_{\tilde{\PQb}_2},m_{\tilde{\Pg}}) \, .
%\end{equation}
%%with $C_F = 4/3$ and the auxiliary function
%%\begin{equation}
%%\label{eq:I}
%%I(a,b,c) = \frac{1}{(a^2-b^2)(b^2-c^2)(a^2-c^2)} \left(
%%a^2 b^2 \ln \frac{a^2}{b^2} +
%%b^2 c^2 \ln \frac{b^2}{c^2} +
%%c^2 a^2 \ln \frac{c^2}{a^2}
%%\right) \, .
%%\end{equation}
%Here, $\tilde{\PQb}_{1,2}$ are the sbottom mass eigenstates, and
%$m_{\tilde{\Pg}}$ is the gluino mass. 
The numerical results presented
here are based on the evaluation of $\Delta_{\PQb}$ in
\Bref{Hofer:2009xb}.
Furthermore additional QCD corrections taken from
\Bref{Carena:1999py} are included, see also \Bref{Czarnecki:1992ig}.

%The {\sc HDECAY} calculation is based on the evaluation of
%$\Gamma(\PQt \to \PWp \PQb)$ and $\Gamma(\PQt \to \PSHp \PQb)$.
%The former decay was evaluated including the full NLO QCD corrections
%(including bottom mass effects)~\cite{Campbell:2004ch} (and references
%therein). The latter decay also includes the full NLO QCD corrections
%(including bottom mass effects). The top and (kinematical) bottom masses are
The {\sc HDECAY} calculation is based on the evaluation of
$\Gamma(\PQt \to \PWp \PQb)$ and $\Gamma(\PQt \to \PSHp \PQb)$.
The decays were evaluated including the full NLO QCD corrections (including
bottom mass effects)~\cite{Campbell:2004ch} (and references therein).
The top and (kinematical) bottom masses are
taken as the pole masses while the bottom mass of the Yukawa coupling is taken
as running $\overline{{\rm MS}}$ mass at the scale of the top mass.
SUSY QCD and electroweak corrections are approximated via $\Delta_{\PQb}$ based 
on
\Brefs{Hall:1993gn,Hempfling:1993kv,Carena:1994bv,Pierce:1996zz,Carena:1999py,Guasch:2003cv,Noth:2008tw,Noth:2010jy,Mihaila:2010mp}.
%(more details can be found in \cite{hdecayweb}.)
% Last line is dummy until Spira provides more accurate description.


%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
  \centering
  \includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig1a.eps} 
  \includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig1b.eps} \\[-1.5em]
  \includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig1c.eps} 
  \includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig1d.eps} 
\vspace{-.5cm}
\caption{The decay width $\Gamma(\PQt \to \PQb \PSHpm)$ calculated
  with {\sc FeynHiggs} and {\sc HDECAY} as a function of $\tan\beta$
  for different values of $\mu$ and $\MHpm$.}
\label{fig:GammaTToBHp}
\end{figure}
%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
  \centering
  \includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig2a.eps} 
  \includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig2b.eps} \\[-1.5em]
  \includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig2c.eps} 
  \includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig2d.eps} 
\vspace{-.6cm}
\caption{The branching fraction BR$(\PQt \to \PQb \PSHpm)$ calculated
  with {\sc FeynHiggs} and {\sc HDECAY} as a function of $\tan\beta$
  for different values of $\mu$ and $\MHpm$.}
\label{fig:BRTToBHp}
%\end{figure}
%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{.5em}
%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{figure}%
%\includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/dGammatHpb_01.eps}
%\includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/sigmatt_BRtHpb_03.eps}
\includegraphics[height=6.8cm]{YRHXS2_chiggs/dGammatHpb_01.eps}
\hfill
\includegraphics[height=6.8cm]{YRHXS2_chiggs/sigmatt_BRtHpb_03B.eps}
\vspace*{-0.3cm}
\caption{
Left: Relative uncertainty in $\Gamma(\PQt \to \PQb \PSHpm)$ 
assuming a $3\%$ residual uncertainty in $\Delta_{\PQb}$.
Right: $\sigma_{\Pt\Pt} \cdot \mathrm{BR}(\PQt \to \PQb \PSHpm) \cdot \mathrm{BR}(\PQt \to \PQb \PWpm) \cdot 2$ including
scale and PDF uncertainties, uncertainties for missing electroweak and QCD
corrections, and $\Delta_{\PQb}$-induced uncertainties.
}
\label{fig:chiggs_gamma_unc}
\end{figure}
%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
{\sc FeynHiggs} has been run with the selected set
of parameters. The {\sc FeynHiggs} output is then used to set the values
for the {\sc HDECAY} input parameters. The main result   
from the comparison is shown in \refFs{fig:GammaTToBHp}
and \ref{fig:BRTToBHp}. The decay width $\Gamma(\PQt \to \PWp \PQb)$ calculated
by both programs agrees very well,  
the difference being only of the order of $0.2\%$. The difference in 
$\Gamma(\PQt \to \PSHp \PQb)$ varies from negligible ($0.1\%$) to a maximum of
about $9\%$, the largest difference being at high values of $\tan\beta$.
The source of the differences is thought to come from differences in the
$\Delta_{\PQb}$ evaluation, largest at high values of $\tan\beta$, 
or from two-loop corrections to $\Delta_{\PQb}$.
%, where differences in the $\Delta_{\PQb}$ evaluation are largest.
A similarly good agreement is observed for the corresponding branching ratio. 

\refF{fig:chiggs_gamma_unc} (left) shows the relative uncertainty of the
total decay width $\Gamma(\PQt \to \PSHp \PQb)$, assuming a residual
uncertainty of the $\Delta_{\PQb}$ corrections of $3\%$ (which is reached after
the inclusion of the leading two-loop contributions to
$\Delta_{\PQb}$~\cite{Noth:2008tw,Noth:2010jy,Mihaila:2010mp}). 
Combining the calculations on $\ttbar$ production and decay, the 
cross section for the process $\Pp\Pp \to \ttbar \to \PW \PQb \PSHpm \PQb$ can
be predicted. The result for $\sigma_{\Pt\Pt} \cdot 
\mathrm{BR}(\PQt \to \PQb \PSHpm) \cdot \mathrm{BR}(\PQt \to \PQb \PWpm) \cdot 2$
at $\sqrt{s}=7 \UTeV$ is shown in
\refF{fig:chiggs_gamma_unc} (right), together with the dominating  
systematic uncertainties: PDF and scale uncertainties on the $\ttbar$ cross
section, $5\%$ for missing one-loop electroweak diagrams, $2\%$ for missing
two-loop QCD diagrams, and 
$\Delta_{\PQb}$-induced uncertainties. The theory uncertainties are added
linearly to the (quadratically) combined experimental uncertainties.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Light-charged-Higgs-boson decay}

In the \mhmaxx\ scenario of the MSSM, the BR$(\PSHpm \to \PGt\PGn) \approx 1$
for all parameter values is still allowed by the LEP
experiments~\cite{Heister:2002ev}. 
(Only for very large values of $\MHpm$ the off-shell decay to $\Pt\PQb$ can
reach a level of up to $10\%$.)
The uncertainty on this assumption is less than $1\%$ and thus negligible
compared to other uncertainties.

The charged-Higgs-boson decay widths calculated with {\sc FeynHiggs} and 
{\sc HDECAY} in \mhmaxx\ benchmark scenario are compared
in the same manner as described in \refS{sec:chiggs_top_quark_decays}.
The total decay width of the charged Higgs boson is shown in
\refF{fig:GammaHp}. 
The decay channels
$\PH^{\pm} \to \PGt\PGn_{\PGt}$, $\PH^{\pm} \to \PA\PW$, $\PH^{\pm} \to \PQc\PQs$, 
$\PH^{\pm} \to \PH\PW$, $\PH^{\pm} \to \PGm\PGn_{\PGm}$, and $\PH^{\pm} \to \Pt\PQb$
available in both programs are studied. 
For $\PH^{\pm} \to \PGt\PGn_{\PGt}$, {\sc FeynHiggs} includes the
Higgs propagator corrections up to the two-loop level.
Concerning the latter, in {\sc HDECAY} these corrections are included in the
approximation of vanishing external momentum.
On the other hand, it 
includes the full NLO QCD corrections to charged-Higgs decays
into quarks, which are incorporated in {\sc FeynHiggs} only in the
approximation of a heavy charged Higgs boson.
The experimentally most interesting decay channel
$\PH^{\pm} \to \PGt\PGn_{\PGt}$ showed a good agreement, with {\sc HDECAY}
consistently predicting a $3.5\%$ larger decay width than {\sc FeynHiggs}, due
to the differences described above.
The result is shown in \refF{fig:GammaHpToTauNu}. A good agreement is also
found in the $\PH^{\pm} \to \PGm\PGn_{\PGm}$ channel, again with {\sc HDECAY}
predicting consistently a $\sim 3.5\%$ larger decay width than {\sc
FeynHiggs}. In the $\PH^{\pm} \to \PQc\PQs$ channel 
a notable discrepancy of $7{-}19\%$ is found. The result of the comparison in the 
$\PH^{\pm} \to \PQc\PQs$ channel is shown in \refF{fig:GammaHpToCS}.
%, and the total decay width of the charged Higgs boson in Fig.\ref{fig:GammaHp}.
%
%speculation in emails..
The differences in $\PH^{\pm} \to \PQc\PQs$ may be attributed to 
the QCD corrections implemented in {\sc FeynHiggs}, which are valid only in
the limit of large charged-Higgs masses (in comparison to the quark masses),
whereas 
in {\sc HDECAY} they are more complete. 
%Conversely, the evaluation of
%$\Delta_{\PQs}$, which is crucial for the calculation of 
%$\Gamma(\PH^{\pm} \to \PQc\PQs)$, is more complete in {\sc FeynHiggs}. 
This channel can only play a significant role for very low values of
$\tan\beta$ and is numerically negligible within the \mhmaxx\ scenario.
%There is a difference of about 2.5\% in the
%running charm mass which induces a 5\% difference in the partial decay width for small $\tan\beta$. There is a 10\% difference in the strange
%mass which induces a 20\% difference in the partial decay width for large $\tan\beta$. By changing the strange mass by 5%
%(95 MeV -> 100 MeV) a shift of the partial decay width by roughly 10% was observed.
%This should originate from the
%different treatment of the QCD corrections.
%
\begin{figure}
  \centering
\includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig4a.eps} 
\includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig4b.eps} \\[-1.5em]
\includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig4c.eps} 
\includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig4d.eps}
\vspace{-.5cm}
\caption{The decay width of $\PSHpm$ calculated with {\sc FeynHiggs} and {\sc HDECAY} as a function
of $\tan\beta$ for different values of $\mu$ and $\MHpm$.}
\label{fig:GammaHp}
\end{figure}
%
\begin{figure}
  \centering
  \includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig5a.eps} 
  \includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig5b.eps} \\[-1.5em]
  \includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig5c.eps} 
  \includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig5d.eps} 
\vspace{-.5cm}
\caption{The decay width $\Gamma(\PH^{\pm} \to \PGt\PGn_{\PGt})$ calculated with {\sc FeynHiggs} and
{\sc HDECAY} as a function of $\tan\beta$ for different values of $\mu$ and $M_{\PH^{\pm}}$.}
\label{fig:GammaHpToTauNu}
\end{figure}
%
\begin{figure}
  \centering
  \includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig6a.eps} 
  \includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig6b.eps} \\[-1.5em]
  \includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig6c.eps} 
  \includegraphics[width=0.48\textwidth]{YRHXS2_chiggs/YRHXS2_chiggs_fig6d.eps} 
\vspace{-.5cm}
\caption{The decay width $\Gamma(\PH^{\pm} \to \PQc\PQs)$ calculated
  with {\sc FeynHiggs} and {\sc HDECAY} as a function of $\tan\beta$
  for different values of $\mu$ and $M_{\PH^{\pm}}$. A discrepancy
  of $7{-}19\%$ is observed.}
\label{fig:GammaHpToCS}
\end{figure}

\subsection{Heavy charged Higgs boson}
For heavy charged Higgs bosons,  $\MHpm \gsim \Mt$,
associated production $\Pp\Pp \to {\Pt}\PQb \PSHpm{\rm + X}$ is the
dominant production mode. 
Two different formalisms can be employed to calculate the cross
section for associated ${\Pt}\PQb\PSHpm$ production.  In the
four-flavour scheme (4FS) with no $\PQb$ quarks in the initial state,
the lowest-order QCD production processes are gluon--gluon fusion and
quark--antiquark annihilation, $\Pg\Pg \to {\Pt}\PQb\PSHpm$ and
$\Pq\bar \Pq \to {\Pt}\PQb\PSHpm$, respectively. Potentially
large logarithms $\propto \ln(\muF/\Mb)$, which arise from
the splitting of incoming gluons into nearly collinear $\PQb\bar \PQb$
pairs, can be summed to all orders in perturbation theory by
introducing bottom parton densities. This defines the five-flavour
scheme (5FS)~\cite{Barnett:1987jw}. The use of bottom distribution
functions is based on the approximation that the outgoing $\PQb$ quark
is at small transverse momentum and massless, and the virtual $\PQb$
quark is quasi on shell. In this scheme, the leading-order (LO)
process for the inclusive $\Pt\PQb\PSHpm$ cross section is
gluon--bottom fusion, $\Pg \PQb \to \Pt \PSHpm$.  The next-to-leading
order (NLO) cross section in the 5FS includes ${\cal
  O}(\alphas)$ corrections to $\Pg \PQb \to \Pt  \PSHpm$
and the tree-level processes $\Pg\Pg \to \Pt\PQb\PSHpm$ and $\Pq\bar
\Pq \to \Pt\PQb\PSHpm$. To all orders in perturbation theory the four-
and five-flavour schemes are identical, but the way of ordering the
perturbative expansion is different, and the results do not match
exactly at finite order. For the inclusive production of neutral Higgs 
bosons with bottom quarks, $\Pp\Pp \to \PQb\PAQb\PH{\rm + X}$, the
four- and five-flavour scheme calculations numerically agree within
their respective uncertainties, once higher-order QCD corrections are
taken into account~\cite{Dittmaier:2003ej, Campbell:2004pu,
  Dawson:2005vi, Buttar:2006zd}, see \refS{sec:MSSM} of this Report.
  
There has been considerable progress recently in improving the cross-section 
predictions for the associated production of charged Higgs
bosons with heavy quarks by calculating NLO SUSY QCD and electroweak
corrections in the four- and five-flavour schemes~\cite{Zhu:2001nt,
  Gao:2002is, Plehn:2002vy, Berger:2003sm, Kidonakis:2005hc,
  Peng:2006wv, Beccaria:2009my, Kidonakis:2010ux}, and the matching of
the NLO five-flavour scheme calculation with parton
showers~\cite{Weydert:2009vr}. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsubsection{Santander matching}

A simple and pragmatic formula for the combination of the 
four- and five-flavour scheme calculations of 
bottom-quark associated Higgs-boson production
has been suggested in~\Bref{santander}. The matching formula originated
from discussions among the authors of \Bref{santander} at the 
{\it Higgs Days at Santander 2009} 
and is therefore dubbed ``Santander matching''. The matching
scheme has been described in some detail in
\refS{Santander-neutral}. Here we shall very briefly summarise
the main features of the scheme and provide matched
predictions for the inclusive cross section 
$\Pp\Pp \to {\Pt}\PQb \PSHpm{\rm + X}$
at the LHC operating at a centre-of-mass energy of $7\UTeV$. 

The 4FS and 5FS calculations provide the unique description of the
cross section in the asymptotic limits $\MH/\Mb \to 1$ and
$\MH/\Mb \to \infty$, respectively (here and in the following $\MH$ denotes a
generic Higgs boson mass). The two approaches are combined in
such a way that they are given variable weight, depending on the value
of the Higgs-boson mass. The difference between the two approaches is formally
logarithmic. Therefore, the dependence of their relative importance
on the Higgs-boson mass should be controlled by a logarithmic term.
This leads to the following formula \cite{santander}:
\begin{equation}
\begin{split}
\sigma^\text{matched}= \frac{\sigma^\text{4FS} +
  w\,\sigma^\text{5FS}}{1+w}\,,
\end{split}
\end{equation}
with the weight $w$ defined as 
\begin{equation}
\begin{split}
w = \ln\frac{\MH}{\Mb}  - 2\,,
\label{eq::t}
\end{split}
\end{equation}
and $\sigma^\text{4FS}$ and $\sigma^\text{5FS}$ denote the total
inclusive cross section in the 4FS and the 5FS, respectively.
For $\Mb=4.75\UGeV$ the weight factor increases from $w \approx 1.75$
at $\MH = 200\UGeV$ to $w\approx 2.65$ at $\MH = 500\UGeV$. 

The theoretical uncertainties  in the 4FS and the 5FS calculations
should be added linearly, using the
weights $w$ defined in \eqn{eq::t}.  This ensures that the combined
error is always larger than the minimum of the two individual
errors, see the discussion in \Bref{santander}. As the uncertainties
can be asymmetric, the combination should be done separately for
the upper and the lower uncertainty limits:
\begin{equation}
\begin{split}
\Delta\sigma_\pm = \frac{\Delta\sigma_\pm^\text{4FS}
  + w\,\Delta\sigma_\pm^\text{5FS}}{1+w}\,,
\end{split}
\label{eq::error}
\end{equation}
where $\Delta\sigma_\pm^\text{4FS}$ and
$\Delta\sigma_\pm^\text{5FS}$ are the upper/lower uncertainty limits
of the 4FS and the 5FS, respectively.

We shall now discuss the numerical implications of the Santander matching
and provide matched
predictions for the inclusive cross section $\Pp\Pp \to {\Pt}\PQb \PSHpm{\rm + X}$
at the LHC operating at a centre-of-mass energy of $7\UTeV$. The individual
numerical results for the 4FS and 5FS calculations have been
obtained 
%in the context of the {\it LHC Higgs Cross Section Working Group} 
with input parameters as
described in \Bref{Dittmaier:2011ti}. Note that the cross-section
predictions presented below correspond to NLO QCD results
for the production of heavy charged Higgs bosons in a
two-Higgs-doublet model. SUSY
effects can be taken into account by rescaling the bottom Yukawa
coupling to the proper value~\cite{Dittmaier:2006cz,Dawson:2011pe}.

Let us briefly discuss the choice of renormalisation and factorisation
scales and the estimate of the theory uncertainty in the
4FS and 5FS NLO calculations as presented in
\Bref{Dittmaier:2011ti}. In the 4FS both scales have been set to 
$\mu = (\Mt + \Mb + M_{\PSHm})/3$ as the default choice. The NLO scale 
uncertainty has been estimated from the variation of the scales by a
factor of three about the central scale, see also the discussion in  
\Bref{Dittmaier:2009np}. The residual NLO scale uncertainty in the 4FS 
is then approximately $\pm 30$\%. In the 5FS calculation the central
scale has been set to $\mu=(\Mt+M_{\PSHm})/4$ (see \Bref{Berger:2003sm}). 
A small residual NLO scale uncertainty of less then about $10\%$
is found when varying the scales by a factor three about the central
scale. We note that the scale uncertainty of the NLO 5FS calculation
is surprisingly small, and that the scale dependence of the NLO cross
section is monotonically rising when the scale is
decreased. Furthermore, the choice of the factorisation scale in the
5FS calculations is intricate and may depend both on the
kinematics of the process and on the Higgs-boson mass, see \eg 
\Brefs{Rainwater:2002hm,Boos:2003yi,Maltoni:2007tc}. Thus, a proper 
estimate of the theory uncertainty of the 5FS calculation may require 
further investigation. 

For the results presented below, however, we simply adopt the 4FS and
5FS cross section and theory uncertainty predictions as presented in 
\Bref{Dittmaier:2011ti}. As no estimate of the PDF error of the
5FS calculation has been given, we only consider the renormalisation
and factorisation scale
uncertainty. 

\refF{fig::xs4f5fhks}\,(a) shows the central values for the 4FS and
the 5FS cross section, as well as the matched result, as a function
of the Higgs-boson mass. The corresponding theory error estimates are shown in
\refF{fig::xs4f5fhks}\,(b). Taking the scale uncertainty into account, the 4FS and
5FS cross sections at NLO are consistent, even though the predictions
in the 5FS at our choice of the central scales are larger than those of
the 4FS by approximately $30\%$, almost independently of the Higgs-boson
mass. Qualitatively similar results have been obtained from a
comparison of 4FS and 5FS NLO calculations for single-top production
at the LHC~\cite{Campbell:2009ss}. We note that the lower end of the 
error band of the matched calculation approximately coincides with the 
central prediction of the 4FS, while the upper end of the band is very 
close to the upper band of the 5FS scale error. 

%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\begin{figure}
  \centerline{
    \begin{tabular}{cc} 
%      \includegraphics[bb = 175 500 500 750pt, width=.45\textwidth]{YRHXS2_chiggs/totalxs_7TeV_santander_gnu.eps} &
%      \includegraphics[bb = 175 500 500 750pt, width=.45\textwidth]{YRHXS2_chiggs/totalxs_7TeV_santander_error_gnu.eps} \\[.2em]
      \includegraphics[height=.4\textwidth, width=.49\textwidth]{YRHXS2_chiggs/totalxs_7TeV_santander_gnu.eps} &
      \hspace*{-1em}
      \includegraphics[height=.4\textwidth, width=.49\textwidth]{YRHXS2_chiggs/totalxs_7TeV_santander_error_gnu.eps} \\[-1em]
      (a) & (b)
    \end{tabular}
  }
      \caption{\label{fig::xs4f5fhks}\sloppy (a) Central values for the total inclusive cross
        section in the 5FS (red, dashed), the 4FS (green, dashed),
        and for the matched cross section (blue, solid). Here and in the
        following we use the MSTW2008\, PDF
        set~\cite{Martin:2009iq}. (b) Theory uncertainty
        bands for the total inclusive cross section in the 5FS (red,
        dashed), the 4FS (green, dashed), and for the matched cross
        section (blue, solid).}
\end{figure}
%
%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Differential distributions}
Let us now turn to the transverse-momentum distributions
of the final-state particles. To illustrate the impact of the
higher-order QCD corrections on the shape of the distributions and to
analyse the difference between the 4FS and 5FS calculations, we
discuss results for the LHC operating at $14\UTeV$ energy, as presented 
in \Brefs{Dittmaier:2011ti,Weydert:2009vr}. In both calculations, the 
distributions have been evaluated for the
default scale choice $\mu = (m_{\rm t} + m_{\PQb} + M_{{\rm H}^-})/3$
and $\mu=(\Mt+M_{\PSHm})/4$, respectively. 

We first discuss the transverse-momentum distribution of the top and 
Higgs particles. As shown in \Brefs{Dittmaier:2011ti,Weydert:2009vr}, 
the impact of higher-order corrections, including the parton shower,
is small for the top and Higgs $\pT$. In
\refF{fig::pt4f5f}\,(a) and (b) we compare the NLO (4FS) and NLO +
parton-shower (5FS) results for the transverse-momentum distribution 
of the top and Higgs, respectively. 
%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\begin{figure}
  \centerline{
    \begin{tabular}{cc} 
%      \includegraphics[bb = 75 100 500 250pt, width=.45\textwidth]{YRHXS2_chiggs/ptH_45FS_gnu.eps} &
%      \includegraphics[bb = 75 100 500 250pt, width=.45\textwidth]{YRHXS2_chiggs/ptt_45FS_gnu.eps} \\[.2em]
      \includegraphics[height=.4\textwidth, width=.49\textwidth]{YRHXS2_chiggs/ptH_45FS_gnu.eps} &
      \hspace*{-1em}
      \includegraphics[height=.4\textwidth, width=.49\textwidth]{YRHXS2_chiggs/ptt_45FS_gnu.eps} \\[-1em]
      (a) & (b)
    \end{tabular}
    }
      \caption{\label{fig::pt4f5f} \sloppy Normalised transverse-momentum distributions
      of the Higgs boson (a) and the top quark (b) for $\Pp\Pp \to {\rm t}\PAQb{\rm
    H}^-+X$ at the LHC ($14\UTeV$). Shown are results in the 4FS (NLO)
  and 5FS (NLO plus parton shower).}
\end{figure}
%
%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Note that we have normalised both distribution to one. It is evident
from  the comparison shown in \refF{fig::pt4f5f} that the shapes of the
Higgs and top transverse-momentum distribution in the 4FS and 5FS
agree very well.

The bottom $\pT$ distribution is described with different accuracy in the
4FS and 5FS calculations. While in the 4FS the kinematics of the process is
treated exactly already at LO, the 5FS is based on the approximation
that the bottom quark is produced at small transverse momentum. In the
5FS a finite bottom $\pT$ is thus only generated at NLO through the parton
process $\Pg\Pg \to {\Pt}\PQb\PSHpm$, which is the LO process of the
4FS.   We thus focus on the 
transverse-momentum distribution of the bottom quark as described within the 4FS
and compare the predictions at LO and at
NLO in \refF{fig::chiggsptb}, see \Bref{Dittmaier:2009np}.
%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\begin{figure}
  \centerline{
%      \includegraphics[bb = 175 500 500 750pt, width=.45\textwidth]{YRHXS2_chiggs/ptb_lo_nlo_ps_gnu.eps} 
      \includegraphics[height=.4\textwidth, width=.49\textwidth]{YRHXS2_chiggs/ptb_lo_nlo_ps_gnu.eps} 
   } \vspace*{-1em}
      \caption{\label{fig::chiggsptb} \sloppy Transverse-momentum distributions
      of the bottom quark in the 4FS for $\Pp\Pp \to {\rm t}\PAQb{\rm
    H}^-+X$ at the LHC ($14\UTeV$). Shown are results at LO and NLO.}
\end{figure}
%
%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The bottom-quark $\pT$ distribution, which extends
to $p_{\mathrm{T,b}} \gg \Mb$, is significantly softened at NLO. The
large impact on the $p_{\mathrm{T,b}}$  
distribution is due to collinear gluon
radiation off bottom quarks that is enhanced by a factor $\alphas \ln(\Mb/p_{\mathrm{T,b}})$. The enhancement should
be significantly reduced if the bottom quarks are reconstructed from jets, since the application
of a jet algorithm treats the bottom--gluon system inclusively in the collinear cone, so that the
logarithmic enhancement cancels out.

\clearpage

