\newcommand{\orde}[1]{\mathcal{O}(#1)}
\newcommand{\total}{\mathrm{total}}
\newcommand{\df}{\mathrm{d}}
\newcommand{\jet}{\mathrm{jet}}
%\newcommand{\eq} [1]{Eq.~\eqref{eq:#1}}
\newcommand{\eqr}[1]{Eq.~\eqref{eq:#1}}
\newcommand{\eqs}[2]{Eqs.~\eqref{eq:#1} and \eqref{eq:#2}}
\newcommand{\pb}{\,\mathrm{pb}}
\newcommand{\GeV}{\,\mathrm{GeV}}
\newcommand{\TeV}{\,\mathrm{TeV}}
\newcommand{\abs}[1]{\lvert#1\rvert}
\def\ltap{\raisebox{-.6ex}{\rlap{$\,\sim\,$}}\raisebox{.4ex}{$\,<\,$}}
\def\gtap{\raisebox{-.4ex}{\rlap{$\,\sim\,$}} \raisebox{.4ex}{$\,>\,$}}

\newcommand{\nb}{\,\mathrm{nb}}
\newcommand{\incl}{\mathrm{incl}}
\newcommand{\excl}{\mathrm{excl}}
\newcommand{\Tcm}{{\mathcal{T}_\mathrm{cm}}}
\newcommand{\Tcmc}{\mathcal{T}_\mathrm{cm}^\mathrm{cut}}



\section{The gluon-fusion process\footnote{M.~Grazzini, F.~Petriello (eds.); E.A.~Bagnaschi, A.~Banfi, D.~de~Florian,  G.~Degrassi, G.~Ferrera, G.~P.~Salam, P.~Slavich, I.~W.~Stewart, F.~Stoeckli, F.~J.~Tackmann, D.~Tommasini, A.~Vicini, W.~J.~Waalewijn and G.~Zanderighi.}}

%%%%% Short introduction the the report

In the first volume of this Handbook, the status of the inclusive cross section for Higgs production in gluon fusion was summarised~\cite{Dittmaier:2011ti}.  Corrections arising from higher-order QCD, electroweak effects, as well as contributions beyond the commonly used effective-theory approximation  were analysed.  Uncertainties arising from missing terms in the perturbative expansion, as well as imprecise knowledge of parton distribution functions, were estimated to range from approximately $15\%$ to $20\%$, with the precise value depending on the Higgs-boson mass.

Our goal in this second volume is to extend the previous study of the gluon-fusion mechanism to include differential cross sections.  The motivation for such investigations is clear; experimental analyses must impose cuts on the final state in order to extract the signal from background.  A precise determination of the corresponding signal acceptance is needed.  Hopefully, it is computed to the same order in the perturbative expansion as the inclusive cross section.  Possible large logarithms that can occur when cuts are imposed should be identified and controlled.  In the case of Higgs production through gluon fusion, numerous tools exist to perform a precise theoretical calculation of the acceptance.  The fully differential Higgs cross section to next-to-next-to-leading order (NNLO) in QCD is available in two parton-level Monte Carlo simulation codes, {\sc FEHiP}~\cite{Anastasiou:2004xq,Anastasiou:2005qj} and {\sc HNNLO}~\cite{Catani:2007vq,Grazzini:2008tf}.  The Higgs transverse-momentum spectrum has been studied with next-to-leading order (NLO) accuracy \cite{deFlorian:1999zd,Ravindran:2002dc,Glosser:2002gm}, and supplemented with next-to-next-to-leading logarithmic (NNLL) resummation of small-$\pT$ logarithms \cite{Bozzi:2005wk,Cao:2009md,deFlorian:2011xf}.  Other calculations and programs for studying Higgs production in the gluon-fusion channel are available, as discussed throughout this section.  All of these results are actively used by the experimental community.

We aim in this section to discuss several issues that arise when studying differential results in the gluon-fusion channel which are relevant to LHC phenomenology.  A short summary of the contents of this section is presented below.
%
\begin{itemize}

\item The amplitude for Higgs production through gluon fusion begins at one loop.  An exact NNLO calculation of the cross section would therefore require a multi-scale, three-loop calculation.  Instead, an effective-field-theory approach is utilised, which is valid for relatively light Higgs masses.  The validity of this effective-theory approach has been extensively studied for the inclusive cross section, and was reviewed in the first volume of this Handbook~\cite{Dittmaier:2011ti}.  However, the accuracy of the effective-theory approach must also be established for differential distributions.  For example, finite-top-mass corrections of order $(\pT^{\mathrm{H}}/\Mt)^2$ can appear beyond the effective theory.  It is also clear that bottom-quark mass effects on Higgs differential distributions cannot be accurately modeled within the effective theory.  These issues are investigated in \refS{sec:finitemass} of this report.  
A distortion of up to $\orde{10\%}$ of the Higgs transverse-momentum spectrum 
is possible in the low-$\pT$ region,
due to the bottom effects in case of a light Higgs,
while in the high-$\pT$ region the finite top mass can induce 
an even  larger modification of the shape of the distribution~\cite{Bagnaschi:2011tu}.

\item The composition of the background to Higgs production in gluon fusion, followed by the decay $\PH \to \PW^+\PW^-$, differs dramatically depending on how many jets are observed together with the Higgs in the final state.  In the zero-jet bin, the dominant background comes from continuum production of $\PW^+\PW^-$.  In the 1- and 2-jet bins, top-quark pair production becomes large.  The optimisation of the experimental analysis therefore utilises a split into different jet-multiplicity bins.  Such a split induces large logarithms associated with the ratio of the Higgs mass over the defining $\pT$ of the jet.  The most natural method of evaluating the theoretical uncertainty in the zero-jet bin, that of performing naive scale variations, leads to a smaller estimated uncertainty than the error on the inclusive cross section \cite{Anastasiou:2007mz,Grazzini:2008tf,Anastasiou:2009bt,Stewart:2011cf}.  This indicates a possible cancellation between logarithms of the $\pT$ cut and the large corrections to the Higgs cross section, and a potential underestimate of the theoretical uncertainty.  An improved prescription for estimating the perturbative uncertainty in the exclusive jet bins is discussed in \refS{sec:jetveto}.  Evidence is given that the cancellation suggested above does indeed occur, and that the uncertainty in the zero-jet bin is in fact nearly twice as large as the estimated error of the inclusive cross section.  Further evidence for the accidental cancellation between large Sudakov logarithms and corrections to the total cross section is given in \refS{sec:vetosum}, where it is shown that different prescriptions for treating the uncontrolled $\orde{\alphas^3}$ corrections to the zero-jet event fraction lead to widely varying predictions at the LHC. 

\item A significant hindrance in our modeling of the cross section in exclusive jet bins is our inability to directly resum the large logarithms mentioned above.  Instead, we must obtain insight into the all-orders structure of the jet-vetoed cross section using related observables.  An example of such a quantity is the Higgs transverse-momentum spectrum.  Through $\orde{\alphas}$ it is identical to the jet-vetoed cross section, since at this order the Higgs can only recoil against a single parton whose $\pT$ matches that of the Higgs.  The large logarithms which arise when the Higgs transverse momentum becomes small can be resummed.  \refS{sec:hqt}
presents a resummed computation of the $\pT$ spectrum at full NNLL accuracy matched to the NLO result valid at large $\pT$, which is implemented
in an updated version of the code {\sc HqT} \cite{deFlorian:2011xf}.
Although the large logarithmic terms that are resummed are not the same
appearing in the jet-bin cross sections,
a study of the uncertainties of the {\em shape} of the resummed
$\pT$ spectrum may help to quantify those uncertainties.  Another variable used to gain insight into the effect of a jet veto is beam thrust, which is equivalent to a rapidity-weighted version of the standard variable $H_{\mathrm{T}}$.  The resummation of beam thrust~\cite{Stewart:2009yx,Berger:2010xi}, and the reweighting of the Monte Carlo program {\sc MC@NLO} to the NNLL+NNLO prediction for beam thrust in order to gain insight into how well the jet-vetoed cross section is predicted by currently used programs, is described in \refS{sec:jetvetosum}.

\end{itemize}
%
Unlike the first volume of this report, no explicit numbers are given for use in experimental studies.  The possible distributions of interest in the myriad studies performed at the LHC are too diverse to compile here.  Our goal is to identify and discuss the relevant issues that arise, and to give prescriptions for their solution.  We must also stress that such prescriptions are not set in stone, and are subject to change if theoretical advances occur.  For example, if an exact NNLO calculation of the Higgs cross section with the full dependence on the quark masses becomes available, or the jet-vetoed Higgs cross section is directly resummed, these new results must be incorporated into the relevant experimental investigations.

%%%%% Sections inputed at this point

\input ./YRHXS2_ggF/gghtb.tex
\input ./YRHXS2_ggF/jetveto.tex
\input ./YRHXS2_ggF/jetveto-2.tex
\input ./YRHXS2_ggF/gghqt.tex
\input ./YRHXS2_ggF/jetvetosum.tex

\clearpage
