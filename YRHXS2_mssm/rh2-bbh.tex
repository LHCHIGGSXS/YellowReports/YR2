%\section{Outline}

Apart from the total inclusive cross section for Higgs production at the
LHC, it is important to have predictions for (a) kinematical
distributions of the Higgs boson and associated jets, and (b) the
fraction of events with zero, one, or more jets.  
%For the gluon fusion
%process, which is the dominant Higgs production mechanism in the
%SM, these quantities have been studied in great detail over
%the last few
%years\cite{deFlorian:1999zd,Glosser:2002gm,Ravindran:2002dc,deFlorian:2000pr,
%  Catani:2001cr,Berger:2002ut,Bozzi:2005wk,Anastasiou:2004xq,Anastasiou:2005qj,
%  Campbell:2006xx,Catani:2007vq,Bozzi:2007pn,Berger:2010xi,Field:2002pb,
%  Field:2003yy,Langenegger:2006wu,Brein:2003df,Brein:2007da,Brein:2010xj}.
%
In supersymmetric theories
%however, there are processes that could be
%equally or even more important than gluon fusion.  For example, 
the associated production of a Higgs boson with bottom quarks is the
dominant process in a large parameter region of the MSSM (see, e.g.,
\Bref{Dittmaier:2011ti}). The proper theory description of this
process has been a subject of discussion for quite some time. The result
is a rather satisfactory reconciliation of the two possible approaches,
the so-called four- and five-flavor scheme (4FS and 5FS), which led to
the ``Santander-matching'' procedure, see above.%\cite{bbh-santander}.

Higher-order corrections in the 5FS are usually easier to calculate than
in the 4FS, because one deals with a $2\to 1$ rather than a $2\to 3$
process at LO. However, since the 5FS works in the collinear
approximation of the outgoing bottom quarks, effects from large
transverse momenta of the bottom quarks are taken into account only at
higher orders in this approach. In fact, NNLO plays a special role in
the 5FS, which becomes obvious by noticing that it is the first order
where the 5FS contains the LO Feynman diagram of the
4FS\cite{Harlander:2003ai}. Hence only then the 5FS includes two
outgoing bottom quarks at large transverse momentum.

With the total inclusive cross section under good theoretical control,
it is natural to study more differential quantities. Being a $2\to 1$
process, kinematical distributions in the 5FS are trivial
at LO, just like in gluon fusion: the $p_{\rm T}$ of the Higgs boson vanishes,
and the rapidity distribution is given by the boost of the partonic
relative to the hadronic system.

Non-trivial distributions require a jet in the final state.  The $p_{\rm
T}$
and $y$ distributions of the Higgs boson in the process $b\bar b\to
\phi+$jet were studied at NLO in \Bref{Harlander:2010cz} (here and
in what follows, $\phi\in\{\Ph,\PH,\PA\}$). Combining these results with the
NNLO inclusive total cross section\cite{Harlander:2003ai}, one may
obtain NNLO results with kinematical cuts.

As mentioned above, particularly interesting for experimental analyses
is the decomposition of the events into $\phi+n$-jet bins. The case
$n=0$ can be obtained at NNLO by calculating the case $n\geq 1$ at NLO
level, and subtracting it from the total inclusive cross
section\cite{Catani:2001cr,Harlander:2011fx}. For consistency, however,
both ingredients should be evaluated using NNLO PDFs and running of
$\alphas$. This is indicated by the superscript NLO$'$ in the following
equation:
\begin{equation}
\begin{split}
\sigma^\text{NNLO}_\text{jet-veto} \equiv \sigma^\text{NNLO}_{0\text{-jet}} = \sigma^\text{NNLO}_\text{tot} -
\sigma^{\text{NLO}'}_{\geq 1\text{-jet}}\,.
\label{eq::0jet}
\end{split}
\end{equation}
Note that this equation is understood without any flavor requirements
on the outgoing jet.

%- {{{ fig::012jet:

%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\begin{figure}
  \begin{center}
    \begin{tabular}{cc}
      \includegraphics[width=.45\textwidth]{%
        YRHXS2_mssm/mhH-plot_tb5.ps} &
      \includegraphics[width=.45\textwidth]{%
        YRHXS2_mssm/mA-plot_tb5.ps} \\
      (a) & (b) \\
      \includegraphics[width=.45\textwidth]{%
        YRHXS2_mssm/mhH-plot_tb30.ps} &
      \includegraphics[width=.45\textwidth]{%
        YRHXS2_mssm/mA-plot_tb30.ps} \\
      (c) & (d)
    \end{tabular}
    \caption[]{\label{fig::012jet}\sloppy
      Total inclusive (solid/red) and Higgs plus $n$-jet cross
      section for $n=0$ (dashed/blue) and $n\geq 1$ (dotted/black) in
      the \mhmaxx\ scenario for (a,b) $\tan\beta=5$ and (c,d)
      $\tan\beta=30$. Left and right column correspond to the CP-even
      and the CP-odd Higgs bosons, respectively. }
  \end{center}
\end{figure}
%
%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\begin{figure}
  \begin{center}
    \begin{tabular}{cc}
      \includegraphics[width=.45\textwidth]{%
        YRHXS2_mssm/err_0-jet.ps} &
      \includegraphics[width=.45\textwidth]{%
        YRHXS2_mssm/err_1+2-jet.ps} \\
      (a) & (b)
    \end{tabular}
    \caption[]{\label{fig::err012}\sloppy
      Relative perturbative and full error estimates for (a) the jet-vetoed
      cross section and (b) the inclusive $\phi+$jet cross section.
      The full error estimate is obtained by adding the perturbative and the
      PDF$+\alphas$-error quadratically.
    }
  \end{center}
\end{figure}
%
%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%% T A B L E %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}
\begin{center}
\caption[]{\label{tab::012jettb5}Numerical values for the total cross
  section $\sigma_\text{tot}^\text{NNLO}$, the $\phi+0$-jet rate
  $\sigma_\text{0-jet}^\text{NNLO}$, and the inclusive $\phi+$jet rate
  $\sigma_{\geq1\text{-jet}}^\text{NLO}$ for the
  \mhmaxx\ scenario for $\tan\beta=5$ ($\phi\in\{\Ph,\PH,\PA\}$).}
\renewcommand{\arraystretch}{1.2}
\vspace{.5em}
\begin{tabular}{c|ccc|ccc|ccc}
\hline\multicolumn{10}{c}{LHC @
  7\,TeV,\ \ $|y^\text{jet}|<4.8$,\ \ $p_{\rm T}^\text{jet} > 20 \UGeV$,\ \ 
$R = 0.4$;\ \ \mhmaxx$(\tan\beta=5)$} 
\\\hline
$\MA$&\multicolumn{3}{c|}{$\sigma_\text{tot}^\text{NNLO}$ [pb]}&\multicolumn{3}{c|}{$\sigma_{0\text{-jet}}^\text{NNLO}$ [pb]}&\multicolumn{3}{c}{$\sigma_{\geq1\text{-jet}}^\text{NLO}$ [pb]}\\ {}[GeV]&$\PA$&$\Ph$&$\PH$&$\PA$&$\Ph$&$\PH$&$\PA$&$\Ph$&$\PH$\\\hline
 $100$&$   8.42$&$   9.84$&$  0.392$&$   4.43$&$   5.38$&$  0.180$&$   4.03$&$   4.59$&$  0.214$\\
%\hline
$110$&$   6.12$&$   7.10$&$  0.574$&$   3.09$&$   3.74$&$  0.261$&$   3.05$&$   3.40$&$  0.315$\\
%\hline
$120$&$   4.54$&$   5.05$&$  0.788$&$   2.22$&$   2.59$&$  0.353$&$   2.35$&$   2.47$&$  0.436$\\
%\hline
$130$&$   3.43$&$   3.54$&$  0.974$&$   1.62$&$   1.78$&$  0.429$&$   1.83$&$   1.77$&$  0.545$\\
%\hline
$140$&$   2.63$&$   2.48$&$   1.07$&$   1.19$&$   1.23$&$  0.461$&$   1.44$&$   1.26$&$  0.606$\\
%\hline
$150$&$   2.04$&$   1.78$&$   1.06$&$  0.894$&$  0.878$&$  0.446$&$   1.14$&$  0.913$&$  0.611$\\
%\hline
$160$&$   1.60$&$   1.33$&$  0.974$&$  0.681$&$  0.652$&$  0.400$&$  0.924$&$  0.686$&$  0.573$\\
%\hline
$170$&$   1.27$&$   1.03$&$  0.861$&$  0.522$&$  0.505$&$  0.344$&$  0.749$&$  0.536$&$  0.516$\\
%\hline
$180$&$   1.02$&$  0.836$&$  0.744$&$  0.406$&$  0.407$&$  0.291$&$  0.613$&$  0.434$&$  0.453$\\
%\hline
$190$&$  0.825$&$  0.698$&$  0.635$&$  0.320$&$  0.339$&$  0.242$&$  0.503$&$  0.364$&$  0.392$\\
%\hline
$200$&$  0.673$&$  0.599$&$  0.539$&$  0.254$&$  0.290$&$  0.201$&$  0.417$&$  0.313$&$  0.337$\\
%\hline
$210$&$  0.552$&$  0.526$&$  0.457$&$  0.203$&$  0.254$&$  0.165$&$  0.348$&$  0.275$&$  0.290$\\
%\hline
$220$&$  0.457$&$  0.470$&$  0.387$&$  0.163$&$  0.227$&$  0.136$&$  0.292$&$  0.246$&$  0.249$\\
%\hline
$230$&$  0.380$&$  0.427$&$  0.328$&$  0.133$&$  0.206$&$  0.113$&$  0.246$&$  0.224$&$  0.213$\\
%\hline
$240$&$  0.318$&$  0.393$&$  0.278$&$  0.108$&$  0.190$&$ 0.0942$&$  0.208$&$  0.206$&$  0.184$\\
%\hline
$250$&$  0.267$&$  0.366$&$  0.237$&$ 0.0894$&$  0.176$&$ 0.0785$&$  0.178$&$  0.192$&$  0.158$\\
%\hline
$260$&$  0.226$&$  0.343$&$  0.203$&$ 0.0735$&$  0.165$&$ 0.0649$&$  0.152$&$  0.180$&$  0.136$\\
%\hline
$270$&$  0.192$&$  0.324$&$  0.173$&$ 0.0600$&$  0.156$&$ 0.0541$&$  0.130$&$  0.170$&$  0.118$\\
%\hline
$280$&$  0.163$&$  0.308$&$  0.149$&$ 0.0509$&$  0.148$&$ 0.0461$&$  0.112$&$  0.162$&$  0.102$\\
%\hline
$290$&$  0.140$&$  0.294$&$  0.129$&$ 0.0426$&$  0.142$&$ 0.0388$&$ 0.0967$&$  0.155$&$ 0.0891$\\
%\hline
$300$&$  0.120$&$  0.283$&$  0.111$&$ 0.0363$&$  0.136$&$ 0.0338$&$ 0.0836$&$  0.149$&$ 0.0774$\\
\hline
\end{tabular}
\end{center}
\end{table}
%%%%%%%%%%%%%%%%%% T A B L E %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%% T A B L E %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}
\begin{center}
\caption[]{\label{tab::012jettb30}Same as Table\,\ref{tab::012jettb5},
  but for $\tan\beta=30$.}
\renewcommand{\arraystretch}{1.2}
\vspace{.5em}
\begin{tabular}{c|ccc|ccc|ccc}
\hline\multicolumn{10}{c}{LHC @
  7\,TeV,\ \ $|y^\text{jet}|<4.8$,\ \ $p_{\rm T}^\text{jet} > 20 \UGeV$,\ \ $R =
  0.4$;\ \ \mhmaxx$(\tan\beta=30)$} 
\\\hline
$\MA$&\multicolumn{3}{c|}{$\sigma_\text{tot}^\text{NNLO}$ [pb]}&\multicolumn{3}{c|}{$\sigma_{0\text{-jet}}^\text{NNLO}$ [pb]}&\multicolumn{3}{c}{$\sigma_{\geq1\text{-jet}}^\text{NLO}$ [pb]}\\ {}[GeV]&$\PA$&$\Ph$&$\PH$&$\PA$&$\Ph$&$\PH$&$\PA$&$\Ph$&$\PH$\\\hline
 $100$&$259$&$265$&$  0.776$&$136$&$140$&$  0.365$&$124$&$127$&$  0.415$\\
%\hline
$110$&$188$&$195$&$   1.93$&$   94.9$&$   98.4$&$  0.906$&$   93.8$&$   97.0$&$   1.03$\\
%\hline
$120$&$140$&$141$&$   7.19$&$   68.2$&$   68.9$&$   3.38$&$   72.3$&$   72.7$&$   3.85$\\
%\hline
$130$&$105$&$   64.2$&$   46.0$&$   49.7$&$   30.6$&$   21.4$&$   56.3$&$   34.0$&$   24.8$\\
%\hline
$140$&$   80.8$&$   11.1$&$   71.2$&$   36.6$&$   5.25$&$   32.2$&$   44.3$&$   5.92$&$   39.2$\\
%\hline
$150$&$   62.7$&$   3.83$&$   59.8$&$   27.5$&$   1.81$&$   26.2$&$   35.2$&$   2.04$&$   33.6$\\
%\hline
$160$&$   49.3$&$   2.03$&$   48.0$&$   20.9$&$  0.955$&$   20.4$&$   28.4$&$   1.08$&$   27.7$\\
%\hline
$170$&$   39.1$&$   1.32$&$   38.4$&$   16.1$&$  0.621$&$   15.8$&$   23.0$&$  0.702$&$   22.6$\\
%\hline
$180$&$   31.4$&$  0.961$&$   30.9$&$   12.5$&$  0.453$&$   12.3$&$   18.9$&$  0.513$&$   18.6$\\
%\hline
$190$&$   25.4$&$  0.756$&$   25.1$&$   9.86$&$  0.356$&$   9.74$&$   15.5$&$  0.404$&$   15.3$\\
%\hline
$200$&$   20.7$&$  0.625$&$   20.5$&$   7.81$&$  0.295$&$   7.74$&$   12.8$&$  0.334$&$   12.7$\\
%\hline
$210$&$   17.0$&$  0.535$&$   16.9$&$   6.26$&$  0.252$&$   6.20$&$   10.7$&$  0.286$&$   10.6$\\
%\hline
$220$&$   14.0$&$  0.471$&$   13.9$&$   5.01$&$  0.222$&$   4.97$&$   8.98$&$  0.252$&$   8.92$\\
%\hline
$230$&$   11.7$&$  0.423$&$   11.6$&$   4.08$&$  0.200$&$   4.05$&$   7.56$&$  0.226$&$   7.51$\\
%\hline
$240$&$   9.77$&$  0.386$&$   9.71$&$   3.33$&$  0.182$&$   3.31$&$   6.41$&$  0.206$&$   6.37$\\
%\hline
$250$&$   8.22$&$  0.357$&$   8.17$&$   2.75$&$  0.168$&$   2.73$&$   5.46$&$  0.191$&$   5.43$\\
%\hline
$260$&$   6.95$&$  0.334$&$   6.91$&$   2.26$&$  0.157$&$   2.25$&$   4.66$&$  0.178$&$   4.64$\\
%\hline
$270$&$   5.90$&$  0.315$&$   5.87$&$   1.85$&$  0.148$&$   1.84$&$   3.99$&$  0.168$&$   3.97$\\
%\hline
$280$&$   5.03$&$  0.299$&$   5.00$&$   1.57$&$  0.141$&$   1.56$&$   3.44$&$  0.160$&$   3.43$\\
%\hline
$290$&$   4.30$&$  0.286$&$   4.28$&$   1.31$&$  0.135$&$   1.30$&$   2.97$&$  0.153$&$   2.96$\\
%\hline
$300$&$   3.70$&$  0.274$&$   3.68$&$   1.12$&$  0.129$&$   1.11$&$   2.57$&$  0.146$&$   2.56$\\
\hline
\end{tabular}
\end{center}
\end{table}
%%%%%%%%%%%%%%%%%% T A B L E %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%- }}}

As an exemplary case, we consider results for the jet parameters
(anti-$k_{\mathrm{T}}$\cite{Cacciari:2008gp})
\begin{equation}
\begin{split}
R = 0.4,\qquad
p_{\rm T}^\text{jet} > 20 \UGeV,\qquad
|\eta^\text{jet}| < 4.8.
\end{split}
\end{equation}
Fig.~\ref{fig::012jet} shows the contributions of the NNLO jet-vetoed
($\phi+0$-jet) and the NLO inclusive $\phi+$jet rate to the NNLO total cross
section in the \mhmaxx\ scenario for two different values of
$\tan\beta$.  The corresponding numbers are given in
Tables~\ref{tab::012jettb5} and \ref{tab::012jettb30}.  Note, however,
that the sum of the $\phi+n$-jet cross sections does not add up exactly
to the total rate, because they are evaluated at different perturbative
orders, and therefore with different sets of PDFs and $\alphas$
evolution. These numbers have been derived from the SM results of
\Bref{Harlander:2011fx}, reweighted by the MSSM bottom Yukawa
coupling with the help of {\tt FeynHiggs}.
%\cite{Heinemeyer:1998yj,Hahn:2006np}.

Fig.\,\ref{fig::err012} displays the relative perturbative error
estimates,
obtained by varying the renormalisation scale $\mu_{\rm R}$ by a factor
of two around $\mu_0=M_\phi/4$ while keeping the factorisation scale
$\mu_{\rm F}$ fixed at $\mu_0$, and then doing the same with $\mu_{\rm
F}$ and $\mu_{\rm R}$ interchanged. The PDF$+\alphas$ uncertainties are
those from MSTW2008\cite{Martin:2009iq}. The full error estimate
was obtained by
quadratically adding the perturbative to the PDF$+\alphas$ error
estimate.  Note
that these plots are the same as in the SM; the precise numerical values
can therefore be taken from \Bref{Harlander:2011fx}.

The 5FS has also been used in order to evaluate associated $\phi+\Pb$
production through NLO\cite{Campbell:2002zm}, which is contained in the
recent $\phi+$jet calculation \cite{Harlander:2011fx}. Updated numbers
can be obtained quite easily with the help of the program {\tt
  MCFM}\cite{MCFMweb}. Here, however, we use the program described in
\Bref{Harlander:2011fx}. Employing again the NNLO result for the
total inclusive cross section, one may evaluate the $\PQb$-vetoed cross
section through NNLO, along the lines of Eq.\,(\ref{eq::0jet}):
\begin{equation}
\begin{split}
\sigma^\text{NNLO}_{\Pb\text{-jet veto}} \equiv \sigma^\text{NNLO}_{0\Pb} = \sigma^\text{NNLO}_\text{tot}
- \sigma^{\text{NLO}'}_{\geq 1\Pb\text{-jet}}\,.
\end{split}
\end{equation}
It should be recalled, however, that this quantity does not take into
account the finite $\Pb$-jet efficiency $\ep_{\Pb}$. This distinguishes it from
the Higgs cross section with zero $\Pb$-tags $\sigma_{0\Pb\text{-tag}}$, which
can be obtained by combining $\sigma_{0\Pb}$ with the rate
for having one or two $\Pb$ quarks in the final state, i.e.,
$\sigma_{1\Pb}$ and $\sigma_{2\Pb}$\cite{Harlander:2011fx}:
\begin{equation}
\begin{split}
\sigma^\text{NNLO}_{0\Pb\text{-tag}} &= 
\sigma^\text{NNLO}_{0\Pb} +
(1-\ep_{\PQb})\sigma^\text{NLO}_{1\Pb} +
(1-\ep_{\PQb})^2\sigma^\text{LO}_{2\Pb}.
\label{eq::0btag}
\end{split}
\end{equation}
In this case, we set the jet parameters to
\begin{equation}
\begin{split}
R=0.4,\qquad
p_{\rm T}^b > 20\,\text{GeV},\qquad
|\eta^b| < 2.5.
\end{split}
\end{equation}
The $\phi+0\Pb$-, $1\Pb$-, and $2\Pb$-contributions are shown, together with the
total cross section, in Fig.\,\ref{fig::012b}, for the same parameters
as in Fig.\,\ref{fig::012jet}. The numbers were again produced with the
help of the results from Ref.\,\cite{Harlander:2011fx}, reweighted by
the corresponding MSSM Yukawa coupling.

%- {{{ fig::012jet:

%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\begin{figure}
  \begin{center}
    \begin{tabular}{cc}
      \includegraphics[width=.45\textwidth]{%
        YRHXS2_mssm/mhH-b-plot_tb5.ps} &
      \includegraphics[width=.45\textwidth]{%
        YRHXS2_mssm/mA-b-plot_tb5.ps} \\
      (a) & (b) \\
      \includegraphics[width=.45\textwidth]{%
        YRHXS2_mssm/mhH-b-plot_tb30.ps} &
      \includegraphics[width=.45\textwidth]{%
        YRHXS2_mssm/mA-b-plot_tb30.ps} \\
      (c) & (d)
    \end{tabular}
    \caption[]{\label{fig::012b}\sloppy Total inclusive (solid/red)
      and Higgs plus $n\Pb$-jet cross section for $n=0$ (dashed/blue),
      $n=1$ (dotted/black), and $n=2$ (dash-dotted/brown) in the
      \mhmaxx\  scenario for (a,b) $\tan\beta=5$ and (c,d)
      $\tan\beta=30$. Left and right column correspond to the CP-even
      and the CP-odd Higgs bosons, respectively. }
  \end{center}
\end{figure}
%
%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

