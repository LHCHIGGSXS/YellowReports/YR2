\subsection{Uncertainties in \protect{$\mathbf{\Pg\Pg\to\PH\to\PW\PW^{(*)}}$}
\footnote{M.~Grazzini, H.~Hoeth, F.~Krauss, F.~Petriello,  M.~Schonherr, F.~Siegert}}

\label{NLOPSsec:Sherpa}

\noindent
In this section a wide variety of different physics effects and uncertainties 
related to key observables in the process 
$\Pg\Pg\to \PH\to \PWp\PWm\to\PGmp\,\PGnGm\,\Pem\,\PAGne$ is presented; 
they include
\begin{itemize} 
\item an appraisal of NLO matching methods, including a comparison with 
  \HNNLO \cite{Grazzini:2008tf} for some observables on the parton level; 
\item perturbative uncertainties, like the impact of scale and PDF
  variations;
\item the impact of parton showering and non-perturbative effects on 
  parton-level results; and
\item non-perturbative uncertainties, and in particular the impact of 
  fragmentation variations and the effect of the underlying event and
  changes in its simulation.
\end{itemize}

\subsubsection{Setup}

In the following sections the \SHERPA\ event generator \cite{Gleisberg:2008ta}
has been used in two modes: Matched samples have been produced according 
to the \POWHEG~\cite{Nason:2004rx,Frixione:2007vw} and
\MCatNLO~\cite{Frixione:2002ik} methods, implemented as described in
\cite{Hoche:2010pf} and \cite{Hoeche:2011fd}, respectively.  In the following
the corresponding samples will be denoted as \SHERPA-\POWHEG\ and
\SHERPA-\MCatNLO, respectively.
Unless stated otherwise, $\MH = 160\,\UGeV$ and the following cuts have been 
applied to leptons ($\Pl = \Pe,\,\PGm$) and jets $j$:
\begin{itemize}
\item leptons: $\pT^{(\Pl)} \ge 15 \,\UGeV$ and $|\eta^{(\Pl)}| \le 2.5$
\item jets (defined by the anti-$k_t$ algorithm \cite{Cacciari:2008gp} 
  with $R=0.4$): $\pT^{(j)} \ge 25 \,\UGeV$, $|\eta^{(j)}| \le 4.5$.
\end{itemize}
By default, for purely perturbative studies, the central PDF from the MSTW2008 
NLO set~\cite{Martin:2009iq} has been used, while for those plots involving
non-perturbative effects such as hadronisation and the underlying event, the
central set of CT10 NLO~\cite{Lai:2010vv} has been employed, since \SHERPA\
has been tuned to jet data with this set.  In both cases, the PDF set also 
fixes the strong coupling constant and its running behaviour in both matrix
element and parton shower, and in the underlying event.

\subsubsection{Algorithmic dependence: \protect\POWHEG vs.\ \protect\MCatNLO}

Before embarking in this discussion, a number of points need to be stressed:
\begin{enumerate}
\item The findings presented here are very recent, and they are partially at
  odds with previous conclusions.  So they should be understood as
  contributions to an ongoing discussion and hopefully trigger further work;
\item clearly the issue of scales and, in particular, of resummation scales
  tends to be tricky.  By far and large, however, the community agrees that the
  correct choice of resummation scale $Q$ is of the order of the factorisation 
  scale, in the case at hand here, therefore $Q = {\cal O}(\MH)$\footnote{
    In \HqT\ this scale by default is chosen to be $Q = \MH/2$.
  }.  It is
  notoriously cumbersome to directly translate analytic resummation and scale 
  choices there to parton shower programs;
\item the version of the \MCatNLO algorithm presented here differs in some
  aspects from the version implemented in the original \MCatNLO program; there, 
  typically the \HERWIG{} parton shower is being used, guaranteeing an upper 
  limitation in the resummed phase space given by the factorisation scale, and
  the FKS method \cite{Frixione:1995ms} is used for the infrared subtraction 
  on the matrix element level, the difference is compensated by a suitable
  correction term.  In contrast, here, \SHERPA\ is used, where both the parton 
  shower and the infrared subtraction rely on Catani-Seymour subtraction 
  \cite{Catani:1996vz,Schumann:2007mg}, and the phase space limitation in
  the resummation bit is varied through a suitable parameter \cite{Nagy:2003tz}
  $\alpha\in [0,\,1]$, with $\alpha = 1$ relating to no phase space 
  restriction.  We will indicate the choice of $\alpha$ by a superscript.
  However, the results shown here should serve as an example only.
\end{enumerate}

Starting with a comparison at the matrix element level, consider 
\refF{fig:hww:uncert:nlomcs:hnnlo_me}, where results from \HNNLO are 
compared with an NLO calculation and the \POWHEG and \MCatNLO implementations 
in \SHERPA\, where the parton shower has been stopped after the first emission.
For the Higgs boson transverse momentum we find that the \HNNLO result is
significantly softer than both the \SHERPA-\POWHEG and the 
\SHERPA-\MCatNLO$^{(\alpha=1)}$ sample - both have a significant shape 
distortion w.r.t.\ \HNNLO over the full $\pT(\PH)$ range.  In contrast the 
NLO and the \SHERPA-\MCatNLO$^{(\alpha=0.03)}$ result have a similar shape as 
\HNNLO in the mid-$\pT$ region, before developing a harder tail.  The shape 
differences in the low-$\pT$ region, essentially below $\pT(\PH)$ of about 
$20\,\UGeV$, can be attributed to resummation effects.  The picture becomes 
even more interesting when considering jet rates at the matrix element level.  
Here, both the \SHERPA-\POWHEG and the \SHERPA-\MCatNLO$^{(\alpha=1)}$ sample 
have more 1 than 0-jet events, clearly at odds with the three other samples.  
Of course, switching on the parton shower will lead to a migration to higher 
jet bins, as discussed in the next paragraph.  With this in mind, one could 
argue that the 1-jet rates in both the \SHERPA-\POWHEG and the 
\SHERPA-\MCatNLO$^{(\alpha=1)}$ sample seem to be in fair agreement with the 
{\em inclusive} 1-jet rate of \HNNLO - this however does not resolve the 
difference in the 0-jet bin, and ultimately it nicely explains why these two 
samples produce a much harder radiation tail than \HNNLO. 
\begin{figure}[t]
  \centering
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/ME/H_pT.eps}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/ME/jet_multi_exclusive.eps}
  
  \caption{\label{fig:hww:uncert:nlomcs:hnnlo_me}
    The Higgs transverse momentum in all events (left) and the jet 
    multiplicities (right) in $\Pp\Pp\to \PH\to\Pem\,\PGmp\,\PGnGm\,\PAGne$ 
    production.  Here different approaches are compared with each other: two 
    fixed order parton-level calculations at NNLO (\protect\HNNLO, black solid)
    and NLO (black, dashed), and three matched results in the \protect\SHERPA\
    implementations (\protect{\SHERPA-\POWHEG}, blue and 
    \protect{\SHERPA-\MCatNLO} with $\alpha=1$, red dashed and with 
    $\alpha=0.03$, red solid) truncated after the first emission.  The yellow 
    uncertainty band is given by the scale uncertainties of \protect\HNNLO.}
\end{figure}

In \refF{fig:hww:uncert:nlomcs:hnnlo} now all plots are at the shower 
level.  We can summarise the findings of this figure as follows: The 
\SHERPA-\POWHEG and \SHERPA-\MCatNLO$^{(\alpha=1)}$ results exhibit fairly
identical trends for most observables, in particular, the Higgs boson 
transverse momentum in various samples tends to be harder than the pure NLO 
result, \HNNLO, or \SHERPA-\MCatNLO$^{(\alpha=0.03)}$.  Comparing \HNNLO with the 
NLO result and \SHERPA-\MCatNLO$^{(\alpha=0.03)}$ we find that in most cases the 
latter two agree fairly well with each other, while there are differences with 
respect to \HNNLO.  In the low-$\pT$ region of the Higgs boson, the 
difference seems to be well described by a global $K$-factor of about 1.3-1.5, 
while \HNNLO becomes softer in the high-$\pT$ tail, leading to a sizable 
shape difference.  One may suspect that this is due to a different description 
of configurations with two jets, where quantum interferences lead to 
non-trivial correlations of the outgoing particles in phase space, which, of 
course, are correctly accounted for in \HNNLO, while the other results either 
do not include such configurations (the parton-level NLO curve) or rely on the 
spin-averaged and therefore correlation-blind parton shower to describe them.
This is also in agreement with findings in the jet multiplicities, where
the \SHERPA-\MCatNLO$^{(\alpha=0.03)}$ and the NLO result agree with \HNNLO in 
the 0-jet bin, while the \SHERPA-\POWHEG and \SHERPA-\MCatNLO$^{(\alpha=1)}$ 
result undershoot by about $30\%$, reflecting their larger QCD activity.  For 
the 1- and 2-jet bins, however, their agreement with the \HNNLO result 
improves and in fact, as already anticipated from the ME-level results, 
multiplying the \SHERPA-\POWHEG result with a global $K$-factor of about 1.5 
would bring it to a very good agreement with \HNNLO for this observable.  In 
contrast the \SHERPA-\MCatNLO$^{(\alpha=0.03)}$ result undershoots \HNNLO in the 
1-jet bin by about $25\%$ and in the 2-jet bin by about a factor of 4.  Clearly 
here support from higher order tree-level matrix elements like in ME+PS or 
MENLOPS-type \cite{Hamilton:2010wh,Hoche:2010kg} approaches would be helpful. 

\begin{figure}[t]
  \centering
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/PS/H_pT.eps}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/PS/H_pT_0.eps}\\
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/PS/H_pT_1.eps}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/PS/H_pT_2.eps}\\
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/PS/lept_mass.eps}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/PS/jet_multi_exclusive.eps}
  \caption{\label{fig:hww:uncert:nlomcs:hnnlo}
    The Higgs transverse momentum in all events, in events with 0, 1, and
    2 and more jets, the invariant lepton mass, and the jet 
    multiplicities in $\Pp\Pp\to \PH\to\Pem\,\PGmp\,\PGnGm\,\PAGne$ production.
    Here different approaches are compared with each other: two fixed
    order parton-level calculations at NNLO (\protect\HNNLO, black solid) and
    NLO (black, dashed), and three matched results in the \protect\SHERPA\
    implementations (\protect{\SHERPA-\POWHEG}, blue and 
    \protect{\SHERPA-\MCatNLO} with $\alpha=1$, red dashed and with 
    $\alpha=0.03$, red solid).  The yellow uncertainty band is given by the 
    scale uncertainties of \protect\HNNLO.
  }
\end{figure}

Where not stated otherwise, in the following sections, all curves relate
to an NLO matching according to the \SHERPA-\MCatNLO prescription with 
$\alpha = 0.03$.

\subsubsection{Perturbative uncertainties}
The impact of scale variations on the Higgs transverse momentum in all events 
and in events with 0, 1 and 2 or more jets is exhibited in 
\refF{fig:hww:uncert:scale}, where a typical variation by factors of 2 
and 1/2 has been performed around the default scales $\muR = \muF = \MH$.  
For the comparison with the fixed scale we find that distributions that 
essentially are of leading order accuracy, such as $\pT$ distributions of 
the Higgs boson in the 1-jet region or the 1-jet cross section, the scale 
uncertainty is of the order of $40\%$, while for results in the resummation 
region of the parton shower, \ie\ the 0-jet cross section or the Higgs boson 
transverse momentum in the 0-jet bin, the uncertainties are smaller, at about 
$20\%$.  

In contrast differences between the functional form of the scales choice are
smaller, also exemplified in refF{fig:hww:uncert:scale}, where the 
central value $\muF = \muR = \MH/2$ has been compared with an alternative 
scale $\muF = \muR = m_T^{\PH}/2 = \sqrt{\MH^2+p_{T,\PH}^2}/2$.  It should be 
noted though that there the two powers of $\alphas$ related to the effective 
$ggH$ vertex squared have been evaluated at a scale $\tfrac{1}{2}\,\MH$.
Anyway, with this in mind it is not surprising that differences only start
to become sizable in the large $\pT$ regions of additional radiation,
where they reach up to about $40\%$.  

\begin{figure}[t]
  \centering
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/scale/H_pT.eps}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/scale/H_pT_0.eps}\\
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/scale/H_pT_1.eps}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/scale/jet_multi_exclusive.eps}
  \caption{\label{fig:hww:uncert:scale}
    The impact of scale variation by a factor of $2$ and $1/2$ (yellow band) around 
    the central scale $\muR=\muF=\tfrac{1}{2}\,\MH$ (black solid line) and 
    of a variation of the functional form of the scale to 
    $\muR=\muF=\tfrac{1}{2}\,m_T^{\PH}$ (red dashed line) on the transverse 
    momentum of the Higgs boson in all events, events with no and with one 
    jet, and on jet multiplicities.  All curves are obtained by an NLO 
    matching according to the \protect{\SHERPA-\MCatNLO} prescription with 
    $\alpha = 0.03$.
  }
\end{figure}

Rather than performing a full PDF variation according to the recipe in
\Bref{Alekhin:2011sk}, in \refF{fig:hww:uncert:PDF} results for MSTW2008 
NLO, have been compared to those obtained with the central NLO set of CT10.
By far and large, there is no sizable difference in any relevant shape.
However, a difference of about $10\%$ can be observed in the total normalisation,
which can be traced back to the combined effect of minor differences in both 
the gluon PDF and the value of $\alphas$.  
\begin{figure}[t]
  \centering
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/pdfvar/H_pT.eps}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/pdfvar/H_pT_0.eps}\\
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/pdfvar/H_pT_1.eps}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/pdfvar/jet_multi_exclusive.eps}
  \caption{\label{fig:hww:uncert:PDF}
    The impact of different choices of PDFs - MSTW2008 NLO (black) and 
    CT10 NLO (red) - for a $\MH = 160\UGeV$ on the transverse momentum of
    the Higgs boson in all events, events with no and with one jet, and on
    jet multiplicities.  All curves are obtained by an NLO matching according 
    to the \protect{\SHERPA-\MCatNLO} prescription with $\alpha = 0.03$.
  }
\end{figure}


\subsubsection{Parton to hadron level}

In \refF{fig:hww:uncert:levels} the impact of adding parton showering,
fragmentation and the underlying event is exemplified for the same 
observables as in the figures before.  In addition, the invariant lepton mass 
and the jet multiplicities are exhibited at the same stages of event 
generation.  All curves are obtained by an NLO matching according to the 
\SHERPA-\MCatNLO prescription with $\alpha = 0.03$.  In the following, the 
CT10 PDF was used to be able to use the corresponding tuning of the 
non-perturbative \SHERPA\ parameters.

In general, the effect of parton showering is notable, resulting in a shift
of the transverse momentum of the Higgs boson away from very low transverse 
momenta to values of about $20{-}80 \UGeV$ with deviations up to $10\%$.  This 
effect is greatly amplified in the exclusive jet multiplicities, where the 
{\em exclusive} 1-jet bin on the parton level feeds down to higher jet 
multiplicities emerging in showering, leading to a reduction of about $50\%$ in 
that bin (which are of course compensated by higher bins such that the net 
effect on the {\em inclusive} 1-jet bin is much smaller).  A similar effect 
can be seen in the Higgs transverse momentum in the exclusive 0-jet bin, where 
additional radiation allows for larger transverse kicks'' of the Higgs boson 
without actually resulting in jets.  In all cases, however, the additional 
impact of the underlying event is much smaller, with a maximal effect of 
about $15{-}20\%$ in the 2-jet multiplicity and in some regions of the Higgs 
transverse momentum.
\begin{figure}[t]
  \centering
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/levels/H_pT.eps}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/levels/H_pT_0.eps}\\
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/levels/H_pT_1.eps}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/levels/H_pT_2.eps}\\
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/levels/lept_mass.eps}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/levels/jet_multi_exclusive.eps}
  \caption{\label{fig:hww:uncert:levels}
    The impact of going from the parton level (orange) over the parton shower
    level (red) to the hadron level without (blue) and with (black) the 
    underlying event.  The error band relates to the statistical fluctuations 
    of the reference result - the full simulation.  All curves are obtained by 
    an NLO matching according to the \protect{\SHERPA-\MCatNLO} prescription 
    with $\alpha = 0.03$.
  }
\end{figure}


\subsubsection{Non-perturbative uncertainties}

In the following, uncertainties due to non-perturbative effects have been 
estimated.  Broadly speaking, two different physics reasons have been 
investigated, namely the impact of fragmentation, which has been assessed
by switching from \SHERPA's default cluster fragmentation~\cite{Winter:2003tt}
to the Lund string fragmentation~\cite{Andersson:1983ia} encoded in \PYTHIA 6.4 
\cite{Sjostrand:2006za} and suitably interfaced\footnote{
  Both fragmentation schemes in the \SHERPA\ framework have been tuned 
  to the same LEP data, yielding a fairly similar quality in the description 
  of data.},
and the impact of variations in the modelling of the underlying event.
There \SHERPA's model, which is based on \cite{Sjostrand:1987su}, has been
modified such that the transverse activity (the plateau region of $N_{\rm ch}$ 
in the transverse region) is increased/decreased by $10\%$.  

We find that in all relevant observables the variation of the fragmentation
model leads to differences which are consistent with statistical fluctuations
in the different Monte Carlo samples.  This is illustrated by the Higgs boson
transverse momentum and the jet multiplicities, displayed in 
\refF{fig:hww:uncert:frag}.
\begin{figure}[t]
  \centering
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/fragvar/H_pT.eps}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/fragvar/jet_multi_exclusive.eps}
  \caption{\label{fig:hww:uncert:frag}
    The impact of different fragmentation models - cluster (black) and 
    string (red) - for a $\MH = 160\UGeV$ on the transverse momentum of the 
    Higgs boson in all events, and on jet multiplicities.   All curves are 
    obtained by an NLO matching according to the \protect{\SHERPA-\MCatNLO} 
    prescription with $\alpha = 0.03$.  The yellow band indicates a 
    combination of statistical differences and the differences of the two 
    fragmentation schemes.
  }
\end{figure}

Similarly, differences due to the underlying event on the Higgs boson transverse
momentum and various jet-related observables are fairly moderate and typically
below $10\%$.  This is especially true for jet multiplicities, where the $10\%$
variation of the underlying event activity translates into differences of the
order of $2{-}3\%$ only.  However, it should be stressed here that the variation 
performed did not necessarily affect the hardness of the parton multiple 
scatterings, \ie the amount of jet production in secondary parton scatters,
but rather increased the number of comparably soft scatters, leading to an
increased soft activity.  In order to obtain a more meaningful handle on jet 
production due to multiple parton scattering dedicated analyses are mandatory
in order to validate and improve the relatively naive model employed here.
\begin{figure}[t]
  \centering
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/uevar/H_pT.eps}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/uevar/H_pT_0.eps}\\
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/uevar/H_pT_1.eps}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/uevar/jet_multi_exclusive.eps}
  \caption{\label{fig:hww:uncert:ue}
    Differences in the Higgs boson transverse momentum in all events, in events
    with 0, 1, and in jet multiplicities due to variations in the modelling of 
    the underlying event - central (black) vs.\ increased (blue) and 
    suppressed (purple) activity - for a $\MH = 160\UGeV$.  All curves are 
    obtained by an NLO matching according to the \protect{\SHERPA-\MCatNLO} 
    prescription with $\alpha = 0.03$.  The yellow band indicates the
    uncertainties related to the modelling of the underlying event.
  }
\end{figure}
