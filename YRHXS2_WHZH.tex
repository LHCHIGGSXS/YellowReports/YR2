%% \documentclass[12pt]{article}
%% \usepackage{epsf,amsmath,amssymb,graphicx,axodraw,longtable}
%% \usepackage{lhchiggs,heppennames2,cernunits}
%% \begin{document}

\section{$\PW\PH$/$\PZ\PH$ production mode\footnote{%
    S.~Dittmaier, R.V.~Harlander, J.~Olsen, G.~Piacquadio (eds.);
    A.~Denner, G.~Ferrera, M.~Grazzini, S.~Kallweit, A.~M\"uck and F.~Tramontano.}}

\subsection{Theoretical developments}
\label{sec:YRHX2_WHZH_th}

In the previous report~\cite{Dittmaier:2011ti} state-of-the-art predictions
and error estimates for the total cross sections for $\Pp\Pp\to\PW\PH/\PZ\PH$ 
have been compiled, based on (approximate) next-to-next-to-leading-order (NNLO)
QCD and next-to-leading-order (NLO) electroweak (EW) corrections.
In more detail, the QCD corrections ($\sim30\%$) comprise
Drell--Yan-like contributions~\cite{Brein:2003wg}, which respect factorisation
according to $\Pp\Pp\to \PV^*\to \PV\PH$ and represent the dominant
parts, and a smaller remainder, which contributes beyond NLO.
The NLO EW corrections to the total cross sections were evaluated as in
\Bref{Ciccolini:2003jy} and turn out to be about $-(5{-}10)\%$.
In the report~\cite{Dittmaier:2011ti} the Drell--Yan-like NNLO QCD
predictions are dressed with the NLO EW corrections in fully factorised form
as suggested in \Bref{Brein:2004ue},
i.e.\ the EW corrections simply enter as multiplicative correction factor,
which is rather insensitive to QCD scale and parton distribution function (PDF)
uncertainties. For ZH production the one-loop-induced subprocess $\Pg\Pg\to\PZ\PH$,
which is part of the non-Drell--Yan-like NNLO QCD corrections, was taken into account as well.
For the LHC with a centre-of-mass (CM) energy of $7(14)\UTeV$
the QCD scale uncertainties
were assessed to be about $1\%$ and $1{-2}(3{-}4)\%$ for $\PW\PH$
and $\PZ\PH$ production, respectively, while
uncertainties of the PDFs turn out to be about $3{-}4\%$.

After the completion of the report~\cite{Dittmaier:2011ti} theoretical progress
has been made in various directions: 
\begin{itemize}
\item 
On the QCD side, the Drell--Yan-like NNLO corrections to $\PW\PH$ production 
are available now~\cite{Ferrera:2011bk} including the full kinematical information
of the process and leptonic W decays.
\item
For total cross sections the non-Drell--Yan-like remainder at NNLO QCD (apart from
the previously known $\Pg\Pg\to\PZ\PH$ subprocess) has
been calculated recently~\cite{Brein:2011vx}; in particular, this
includes contributions with a top-quark induced gluon--Higgs coupling. As
previously assumed, these effects are at the per-cent level. They
typically increase towards larger Higgs masses and scattering energies,
reaching about $2.5\%(3\%)$ for ZH production at $7\UTeV(14\UTeV)$. 
Since these Yukawa-induced terms arise for the first time at
${\cal O}(\alphas^2)$, they also increase the perturbative uncertainty
which, however, still remains below the error from the PDFs.
\item
On the EW side, the NLO corrections have been generalised to the more complex processes
$\Pp\Pp\to\PW\PH\to\PGn_{\Pl}\Pl\PH$ and
$\Pp\Pp\to\PZ\PH\to\Pl^-\Pl^+\PH/\PGn_{\Pl}\PAGn_{\Pl}\PH$ including the
W/Z decays, also fully supporting differential observables~\cite{whzhhawk}; 
these results are
available as part of the {\HAWK} Monte Carlo 
program~\cite{HAWK}, which was originally designed for the description of Higgs
production via vector-boson fusion including NLO QCD and EW
corrections~\cite{Ciccolini:2007jr}.
\end{itemize}

The following numerical results on differential quantities are, thus, obtained as follows:
\begin{itemize}
\item WH production: 
The fully differential (Drell--Yan-like) NNLO QCD prediction of \Bref{Ferrera:2011bk}
is reweighted with the relative EW correction factor calculated with {\HAWK},
analogously to the previously used procedure for the total cross section.
The reweighting is done bin by bin for each distribution.
\item ZH production: 
Here the complete prediction is obtained with {\HAWK} including NLO QCD and EW
corrections, employing the factorisation of relative EW corrections as well.
\end{itemize}

\subsection{Numerical results}

For the numerical results in this section, we have used the following
setup. The renormalisation and factorisation scales have been identified
and set to
\begin{equation}
\muR=\muF=\MH+\MV,
\end{equation}
where $\MV$ is the \PW/\PZ-boson mass for WH/ZH production. We
have employed the MSTW2008 PDF sets 
at NNLO for WH production and the PDF4LHC prescription to 
calculate the cross section for ZH production at NLO. 
The relative EW corrections have been calculated using the 
central NLO MSTW2008 PDF, but hardly depend
on the PDF and scale choice. We use the $\GF$ scheme to fix the
electromagnetic coupling $\alpha$ and use the values
of $\alphas$ associated to the given PDF set. For the QCD predictions to  
$\PW\PH$ production, we employ the full CKM matrix which
enters via global factors multiplying the different partonic channels. 
For all the \HAWK{} predictions, we neglect mixing of the first two generations
with the third generation. In the EW loop corrections, the CKM matrix
is set to unity, since quark mixing is negligible there.
For $\PZ\PH$ production, bottom quarks in the initial state are only
included in LO within \HAWK{} because of their small impact on the cross sections.

Both the NNLO QCD prediction as well as the \HAWK{} predictions are obtained
for off-shell vector bosons. The vector-boson width can be viewed as a free
parameter of the calculation, and we have chosen 
$\Gamma_{\PW} = 2.08872\UGeV$ and
$\Gamma_{\PZ}$ from the default input. The NNLO QCD calculation~\cite{Ferrera:2011bk} predicts the
integrated \PW-boson production cross section in the presence of the cuts defined
below, so that it had to be multiplied by the branching ratio 
$\mathrm{BR}_{\PW \to \Pl \PGn_{\Pl}}$ for a specific leptonic
final state. Because the EW radiative corrections to the BR are included in
the EW corrections from \HAWK{}, the partial \PW~width has to be used at Born level
in the QCD-improved cross section,
i.e.\ $\mathrm{BR}_{\PW \to \Pl \PGn_{\Pl}}= 
\Gamma^{\mathrm{Born}}_{\PW \to \Pl \PGn_{\Pl}}/\Gamma_{\PW}$, where
$\Gamma^{\mathrm{Born}}_{\PW \to \Pl \PGn_{\Pl}}=\GF \MW^3/(6 \sqrt{2} \pi)$. 
Using any different input value for $\Gamma^{\mathrm{new}}_{\PW}$, all results 
are thus, up to negligible corrections, changed by the ratio 
$\Gamma_{\PW}/\Gamma^{\mathrm{new}}_{\PW}$. In the \HAWK{} prediction, the 
branching ratios for the different leptonic channels are implicitly included by
calculating the full matrix elements. However, up to negligible corrections
the same scaling holds if another numerical value for the input width was used.
In particular, the relative EW corrections hardly depend on 
$\Gamma_{\PW}$.

All results are given for a specific 
leptonic decay mode, e.g.\ for $\PH \Pe^+\Pe^-$ or $\PH \PGmp\PGmm$ production, 
and are not summed over lepton generations. While for charged leptons
the results depend on the prescription for lepton--photon recombination 
(see below), the results for invisible \PZ\ decays, 
of course, do not depend on the neutrino flavour and can be trivially obtained 
by multiplying the $\PH \PGn_{\Pl} \PAGn_{\Pl}$ results by three.

In the calculation of EW corrections, we alternatively apply two versions of
handling photons that become collinear to outgoing charged leptons. The first
option is to assume perfect isolation between charged leptons and photons, an
assumption that is at least approximately fulfilled for (bare) muons. The second
option performs a recombination of photons and nearly collinear charged leptons
and, thus, mimics the inclusive treatment of electrons within electromagnetic
showers in the detector. Specifically, a photon $\PGg$ and a lepton \Pl\ are
recombined for $R_{\Pl\PGg}<0.1$, where
$R_{\Pl\PGg}=\sqrt{(y_{\Pl}-y_{\PGg})^2+\phi^2_{\Pl\PGg}}$
is the usual separation variable in the $y{-}\phi$-plane with $y$ denoting the
rapidity and $\phi_{\Pl\PGg}$ the angle between \Pl\ and $\PGg$ in the
plane perpendicular to the beams. If \Pl\ and $\PGg$ are recombined, we simply
add their four-momenta and treat the resulting object as quasi-lepton. If more
than one charged lepton is present in the final state, the possible
recombination is performed with the lepton delivering the smaller value of
$R_{\Pl\PGg}$. The corresponding EW corrections are labeled
$\delta^\mathrm{bare}$ and $\delta^\mathrm{rec}$, respectively.

After employing the recombination procedure we apply the following cuts on the
charged leptons,
\begin{equation}
p_{\mathrm{T},\Pl} > 20\UGeV, \quad
|y_{\Pl}|< 2.5,
\end{equation}
where $p_{\mathrm{T},\Pl}$ is the transverse momentum of the lepton \Pl. For
channels with at least one neutrino in the final state we require a missing
transverse momentum
\begin{equation}
p_{\mathrm{T,miss}} > 25\UGeV,
\end{equation}
which is defined as the total transverse momentum of the neutrinos in the event.
In addition, we apply the cuts
\begin{equation}
p_{\mathrm{T},\PH} > 200\UGeV, \quad
p_{\mathrm{T},\PW/\PZ} > 190\UGeV
\label{eq:VHpTcuts}
\end{equation}
on the transverse momentum of the Higgs and the weak gauge bosons, respectively.
The corresponding selection of events with boosted Higgs bosons is improving the
signal-to-background ratio in the context of employing the measurement of the
jet substructure in $\PH \to \Pb \bar\Pb$ decays leading to a single fat jet.
The need for background suppression calls for (almost) identical cuts on the
transverse momentum of the vector bosons and the Higgs boson. However, symmetric
cuts induce large radiative corrections in fixed-order calculations
in the corresponding $\pT$ distributions near the cut. Since the
Higgs boson and the vector boson are back-to-back at LO, any
initial-state radiation will either decrease $p_{\mathrm{T},\PH}$ or
$p_{\mathrm{T},\PW/\PZ}$ and the event may not pass the cut anymore. Hence,
the differential cross section near the cut is sensitive to almost collinear
and/or rather soft initial-state radiation. By choosing the above (slightly
asymmetric) cuts this large sensitivity to higher-order corrections can be
removed for the important $p_{\mathrm{T},\PH}$-distribution. Of course, since
the LO distribution for $p_{\mathrm{T},\PW/\PZ}$ is vanishing
for $p_{\mathrm{T},\PW/\PZ}< 200\UGeV$ due to the $p_{\mathrm{T},\PH}$ cut,
the higher-order corrections to the $p_{\mathrm{T},\PW/\PZ}$ distributions
are still large in this region.

In the following plots, we show several relative corrections 
and the absolute cross-section predictions based on 
factorisation for QCD and EW corrections,
\begin{equation}
\sigma=\sigma^\mathrm{QCD}\times
\left( 1 + \delta_{\mathrm{EW}}^\mathrm{rec} \right)
+ \sigma_\gamma\, ,
\end{equation}
where $\sigma^\mathrm{QCD}$ is the best QCD prediction at hand,
$\delta_{\mathrm{EW}}^\mathrm{rec}$ is the relative EW correction with
recombination and $\sigma_\gamma$ is the cross section due to photon-induced
processes which are at the level of $1\%$ and estimated employing the
MRSTQED2004 PDF set for the photon.
In detail, we discuss the distributions in $p_{\mathrm{T},\PH}$,
$p_{\mathrm{T},\PV}$, $p_{\mathrm{T},\Pl}$, and $y_{\PH}$.
More detailed results can be found in \Bref{whzhhawk}.

\begin{figure}
\includegraphics[width=7.5cm]{YRHXS2_WHZH/pth_plot_boosted3_7T_abs_W_YR.eps}
\hfill
%\includegraphics[width=7.5cm]{YRHXS2_WHZH/pth_plot_boosted3_7T_abs_Z_YR.eps}
\includegraphics[width=7.5cm]{YRHXS2_WHZH/pth_plot_PDF4LHC_7T_abs_Z_YR.eps}
\\
\includegraphics[width=7.5cm]{YRHXS2_WHZH/ptV_plot_boosted3_7T_abs_W_YR.eps}
\hfill
%\includegraphics[width=7.5cm]{YRHXS2_WHZH/ptV_plot_boosted3_7T_abs_Z_YR.eps}
\includegraphics[width=7.5cm]{YRHXS2_WHZH/ptV_plot_PDF4LHC_7T_abs_Z_YR.eps}
\\
\includegraphics[width=7.5cm]{YRHXS2_WHZH/ptlpm_plot_boosted3_7T_abs_W_YR.eps}
\hfill
%\includegraphics[width=7.5cm]{YRHXS2_WHZH/ptlp_plot_boosted3_7T_abs_Z_YR.eps}
\includegraphics[width=7.5cm]{YRHXS2_WHZH/ptlp_plot_PDF4LHC_7T_abs_Z_YR.eps}
\\
\includegraphics[width=7.5cm]{YRHXS2_WHZH/yh_plot_boosted3_7T_abs_W_YR.eps}
\hfill
%\includegraphics[width=7.5cm]{YRHXS2_WHZH/yh_plot_boosted3_7T_abs_Z_YR.eps}
\includegraphics[width=7.5cm]{YRHXS2_WHZH/yh_plot_PDF4LHC_7T_abs_Z_YR.eps}
\caption{\label{fi:abs_pTcut}
Predictions for the
$p_{\mathrm{T},\PH}$, $p_{\mathrm{T},\PV}$,
$p_{\mathrm{T},\Pl}$, and $y_{\PH}$ distributions (top to bottom)
for Higgs strahlung off \PW\ bosons (left) and \PZ\ bosons (right)
for boosted Higgs bosons at
the $7\UTeV$ LHC for $\MH=120\UGeV$.
}
\end{figure}

\begin{figure}
\includegraphics[width=7.5cm]{YRHXS2_WHZH/pth_plot_boosted3_7T_rel_W_YR.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS2_WHZH/pth_plot_boosted3_7T_rel_Z_YR.eps}
\\
\includegraphics[width=7.5cm]{YRHXS2_WHZH/ptV_plot_boosted3_7T_rel_W_YR.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS2_WHZH/ptV_plot_boosted3_7T_rel_Z_YR.eps}
\\
\includegraphics[width=7.5cm]{YRHXS2_WHZH/ptlpm_plot_boosted3_7T_rel_W_YR.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS2_WHZH/ptlp_plot_boosted3_7T_rel_Z_YR.eps}
\\
\includegraphics[width=7.5cm]{YRHXS2_WHZH/yh_plot_boosted3_7T_rel_W_YR.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS2_WHZH/yh_plot_boosted3_7T_rel_Z_YR.eps}
\caption{\label{fi:rel_pTcut}
Relative EW corrections for the
$p_{\mathrm{T},\PH}$, $p_{\mathrm{T},\PV}$,
$p_{\mathrm{T},\Pl}$, and $y_{\PH}$ distributions (top to bottom)
for Higgs strahlung off \PW\ bosons (left) and \PZ\ bosons (right)
for boosted Higgs bosons at
the $7\UTeV$ LHC for $\MH=120\UGeV$.
}
\end{figure}

\begin{figure}
\includegraphics[width=7.5cm]{YRHXS2_WHZH/pth_plot_incl3_7T_rel_W_YR.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS2_WHZH/pth_plot_incl3_7T_rel_Z_YR.eps}
\\
\includegraphics[width=7.5cm]{YRHXS2_WHZH/ptV_plot_incl3_7T_rel_W_YR.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS2_WHZH/ptV_plot_incl3_7T_rel_Z_YR.eps}
\\
\includegraphics[width=7.5cm]{YRHXS2_WHZH/ptlpm_plot_incl3_7T_rel_W_YR.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS2_WHZH/ptlp_plot_incl3_7T_rel_Z_YR.eps}
\\
\includegraphics[width=7.5cm]{YRHXS2_WHZH/yh_plot_incl3_7T_rel_W_YR.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS2_WHZH/yh_plot_incl3_7T_rel_Z_YR.eps}
\caption{\label{fi:rel_basiccuts}
Relative EW corrections for the
$p_{\mathrm{T},\PH}$, $p_{\mathrm{T},\PV}$,
$p_{\mathrm{T},\Pl}$, and $y_{\PH}$ distributions (top to bottom)
for Higgs strahlung off \PW\ bosons (left) and \PZ\ bosons (right)
for the basic cuts at the $7\UTeV$ LHC for $\MH=120\UGeV$.
}
\end{figure}

Figure~\ref{fi:abs_pTcut} shows the 
distributions for the two WH production channels $\PH\Pl^+\PGn$ and
$\PH\Pl^-\PAGn$ and for the ZH production channels $\PH\Pl^+\Pl^-$ and
$\PH\PGn\PAGn$. 
The respective EW corrections are depicted in \refF{fi:rel_pTcut} for
the two different treatments of radiated photons, but the difference
between the two versions, which amounts to $1{-}3\%$, is small. 
The bulk of the EW corrections, which are typically in the range
of $-(10{-}15)\%$, is thus of pure weak origin.
In all $\pT$ distributions the EW corrections show
a tendency to grow more and more negative for larger $\pT$,
signalling the onset of the typical logarithmic high-energy behaviour
(weak Sudakov logarithms). The rapidity distributions receive rather flat
EW corrections, which resemble the ones to the respective integrated
cross sections. Note that the latter are significantly larger in size
than the ones quoted in \Bref{Dittmaier:2011ti} for the total cross sections,
mainly due to the influence of the $\pT$ cuts on the Higgs
and gauge bosons, which enforce the dominance of larger scales in
the process.
This can be clearly seen upon comparing the results with the ones shown
in \refF{fi:rel_basiccuts}, where only the 
basic cuts are applied, but not Eq.~(\ref{eq:VHpTcuts}).
For the basic cuts, the EW corrections are globally smaller in size
by about $5\%$, but otherwise show the same qualitative features.

The relative EW corrections shown here could be taken into account in any
QCD-based prediction for the respective distributions 
(based on the quoted cuts) via reweighting. For this purpose the
data files of the histograms are available at the TWiki page
of the WH/ZH working group%
\footnote{https://twiki.cern.ch/twiki/bin/view/LHCPhysics/WHZH}.
The small photon-induced contributions, which are included in our best prediction
and at the level of $1\%$ for WH production and negligible for ZH production,
are also available and could be simply added.

For definiteness, in \refT{ta:MH120results}, we show the integrated results 
corresponding to the cuts in the boosted setup.

Finally, we estimate the uncertainties resulting from
the remaining spurious QCD scale dependences, missing higher-order contributions, and
uncertainties in the PDFs:
\begin{itemize}
\item
We estimate the scale uncertainties upon varying the renormalisation 
and factorisation scales independently by a factor of two around our default scale choice.
At NNLO for WH production, the integrated cross section for the boosted Higgs analysis varies by
$\Delta_{\mathrm{scale}}=2\%$. 
In the considered distributions, the variation of the scales 
only affects the overall normalisation.
Only in the $p_{\mathrm{T},\PW/\PZ}$ distribution 
near $200\UGeV$, also the scale variation indicates that higher-order corrections are large,
as discussed above. Here, scale variation leads to an error estimate of a few $10\%$ at NLO. 
For ZH production at NLO, the scale variation even leads to an error estimate slightly
below $2\%$. 
\item
Both for WH and ZH production, starting at NNLO new types of higher-order QCD contributions
arise that are not reflected by scale variations at (N)NLO.
Specifically, this comprises the gluon-induced contribution to ZH production
(not taken into account in our NLO prediction here), which
is known to be sizable, and the top-loop-induced NNLO
contributions to WH and ZH production, which have been computed recently at the inclusive  
level~\cite{Brein:2011vx}.
The corresponding uncertainty $\Delta_{\mathrm{HO}}= 7 (1)\%$ for 
ZH (WH) production, which we estimate from the known size of those effects on the total cross sections, 
is also shown in \refT{ta:MH120results}.
The relatively large uncertainty for ZH production will be reduced once the NNLO
QCD corrections are known at the differential level as well.
\item
Concerning PDF uncertainties, as stated above, all
central values for WH correspond to the central MSTW2008 prediction at NNLO. At $68\%$
confidence level (C.L.), the MSTW error sets indicate a PDF error slightly below $2\%$.
In distributions again only the overall normalisation is affected, and the distributions are 
not distorted (not shown in the plots). 
According to the PDF4LHC prescription, we rescale the NNLO uncertainty from MSTW by the additional spread
observed at NLO (which is a factor of $\sim 2.5$) when including the CT10 (rescaled to $68\%$ C.L.) 
and NNPDF~2.1 at $68\%$ C.L.\ in the error estimate. 
While for $\PW^+\PH$ production the error 
band is symmetric, the actual error for $\PW^-\PH$ production covers the region from $-8\%$ to
$+2\%$. For $\PZ\PH$ production, we follow the NLO prescription and use the midpoint of the 
above PDF sets as the central value. The resulting PDF error also amounts to about $\pm 5\%$.
For the $\pT$ distributions, individual PDF sets again only lead to differences in the overall 
normalisation. Only in the rapidity distribution of the Higgs boson, the shape of the distribution is
distorted at the level of a few per cent.
\end{itemize}

\providecommand{\phz}{\phantom{(0)}}
\begin{table}
$$ \begin{array}{ccccc}
\hline
\mathrm{channel} & \;\PH\Pl^+ \PGn_{\Pl} +X \;& \;\PH\Pl^- \PAGn_{\Pl} +X \;& \;\PH\Pl^+ \Pl^- +X \;& \;\PH\PGn_{\Pl} \PAGn_{\Pl} +X \; \\ 
\hline
\sigma/\Ufb                                                  \; & \; 1.384(4)       \; & \; 0.617(2)      \; & \; 0.3467(1)      \; & \; 0.7482(1)           \\ 
% NNLO results for WH from Massimilianos number:
%   NNLO QCD         LO BR       EW corr    gamma induced
%(14.45 +/- 0.04)*0.108862*(1 - 0.1333161)+ 0.0203
%( 6.42 +/- 0.02)*0.108862*(1 - 0.130539) + 0.0098
% This is central value MSTW2008 NNLO
% NLO results for ZH:
% central value for PDF4LHC prescription is 1.698% low
% so multiply central MSTW2008 NLO values by 0.9830 
% 0.3527 * 0.9830 = 0.3467
% 0.7611 * 0.9830 = 0.7482
%--------------------------------------------------                                                                     
\hline                                                                                                                 
\delta_{\ELWK}^{\mathrm{bare}} / \%  \; & \; -14.3\phz	   \; & \; -14.0\phz	   \; & \; -11.0\phz	   \; & \; -6.9\phz		\\ 
\delta_{\ELWK}^{\mathrm{rec}} / \%   \; & \; -13.3\phz	   \; & \; -13.1\phz	   \; & \; -9.0\phz	   \; & \; -6.9\phz		\\ 
\sigma_{\ga}/\Ufb                  \; & \; 0.020	   \; & \; 0.010	   \; & \; 0.0002	   \; & \; 0.0000	 \\ 
\hline                             							     
\Delta_{\mathrm{PDF}} / \%                  \; & \; \pm5\phz	   \; & \; \pm5\phz	   \; & \; \pm5\phz	   \; & \; \pm5\phz		\\ 
\Delta_{\mathrm{scale}} / \%                \; & \; \pm2\phz	   \; & \; \pm2\phz	   \; & \; \pm2\phz	   \; & \; \pm2\phz		\\ 
\Delta_{\mathrm{HO}} / \%                   \; & \; \pm1\phz	   \; & \; \pm1\phz	   \; & \; \pm7\phz	   \; & \; \pm7\phz		\\ 
%                                             Figure 6a) in 1111.0761
\hline
\end{array} $$
\caption{\label{ta:MH120results} Integrated cross sections, EW corrections, and estimates 
$\Delta_{\mathrm{PDF/scale}}$ for the PDF and scale uncertainties for the
different Higgs-strahlung channels in the boosted setup for the LHC at $7\UTeV$ for $\MH=120\UGeV$.}
\end{table}

\clearpage

