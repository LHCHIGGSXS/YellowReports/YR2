
\subsection{Higgs production via gluon fusion: finite mass effects}
\label{NLOPSsec:ggHfullmt}

Current implementations in NLO+PS codes of  Higgs production process via gluon fusion
~\cite{Frixione:2005gz,Alioli:2008tz,Hamilton:2009za,Hoche:2010pf}
are based on matrix elements evaluated in the Higgs Effective Theory (ET), i.e. in
a theory where the heavy-quark loop(s) are shrunk to effective vertices.  
In several cases  the user is given  the possibility of rescaling the total cross section by a normalisation
factor, defined as the ratio between the exact Born contribution where
the full dependence on the top and bottom masses is kept into account
and the Born contribution evaluated in the ET. While the ET is a very good approximation
for light SM Higgs, fixed-order computations have shown that it fails for heavier Higgs masses,
at large Higgs transverse momentum and for a BSM Higgs with enhanced couplings to $b$ quarks.

Very recently, progress to include finite mass effects has been achieved on two different fronts:
matched predictions for Higgs production via heavy-quark loops in the SM and beyond have been obtained~\cite{Alwall:2011cy}
and the NLO full-mass dependent calculation has been implemented in POWHEG~\cite{Bagnaschi:2011tu}.

\subsubsection{Higgs production via gluon fusion with loops via LO+PS merging\footnote{J. Alwall, Q. Li, F. Maltoni}}

In \Bref{Alwall:2011cy} was presented the first fully exclusive simulation
of gluon fusion inclusive Higgs production based on the (leading order) exact one-loop
matrix elements for $\PH +0,\,1,\,2$ partons, matched to \pythia\
parton showers using multiple merging schemes implemented in MadGraph 5~\cite{Alwall:2011uj}.

In order to take into account the full kinematic dependence of the
heavy quark loop in Higgs production, the full one-loop amplitudes for
all possible subprocesses contributing to $\PH +0,\,1,\,2$ partons have been calculated. 
To gain in speed and efficiency (the evaluation of multi-parton loop amplitudes is, in general, computationally quite expensive)
a method has been devised to map the integrand in a quick (though approximate) way and to limit
the evaluation of loops  to a small number of points.
Parton level events for  $\PH +0 ,\,1,\,2$ partons are generated via MadGraph/MadEvent \ in the ET, 
with scale choices optimised  for the subsequent merging procedure. Before passing them to
the PS program, events are reweighted by the ratio of full
one-loop amplitudes over the ET ones, $r= |{\cal M}_{\rm LOOP}|^2/|{\cal M}_{\rm ET}|^2$.
The  reweighted parton-level events are unweighted, 
passed through \pythia\ and matched using the $k_T$-MLM or the shower-$k_T$
scheme. Event samples are finally normalised to reference NNLO cross sections.
The reweighting method does not make any approximation and with large enough statistics is exactly equivalent
to the integration of the phase space of the full one-loop amplitudes.

Representative results for SM Higgs and a $b$-philic Higgs at the LHC at 7 TeV 
are shown in \refFs{hpt1,ptj} and in \refF{hpt2}. Jets are defined via the $k_T$ algorithm with resolution parameter set to $D = 1$. 
Jets are required to satisfy $|\eta_{j}|< 4.5$ and $\pT^{j}>30\,\UGeV$.
For sake of simplicity, we adopt Yukawa couplings corresponding to the
pole masses, \ie  for the top quark  $\Mt=173\UGeV$ and for the bottom-quark mass $\Mb=4.6\UGeV$. Other quark masses are neglected. 
Throughout our calculation, we adopt the CTEQ6L1 parton distribution functions
(PDFs)~\cite{Pumplin:2002vw} with the core process renormalisation and
factorisation scales $\muR=\muF=M_T^H\equiv\sqrt{(\pT^H)^2+\MH^2}$.
For the merging performed in MadGraph/MadEvent, the  $k_T$-MLM scheme is chosen, with $Q^{\rm ME}_{\rm min}=30\UGeV$ and $Q^{\rm jet}_{\rm min}=50\UGeV$.

In \refF{hpt1} we show the Higgs $\pT$ distribution for Standard Model Higgs gluon-gluon fusion production at
the LHC with $\MH=140\UGeV$ in a range of $\pT$ relevant for experimental analysis.  
We compare matched results in the ET theory and in the full theory
(LOOP) with \pythia\ with $2\to 2$ matrix element corrections.  We also include the predictions from the analytic computation
at NNLO+NNLL as obtained by {\sc HqT}~\cite{Bozzi:2005wk,deFlorian:2011xf}. The curves are all normalised
to the NNLO+NNLL predictions.  The three Monte-Carlo based predictions agree
very well  in all the shown range of $\pT$, suggesting that for this observable,
higher multiplicity matrix element corrections (starting from $2\to3$) and
loop effects are not important. This is the case also for jet $\pT$ distributions (not shown) in
the same kinematical range. 
 
% --------------------------------------------------------------------
\begin{figure}
\includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/fm/pth0}
\includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/fm/pth0-low}
\caption{SM Higgs  $\pT$ distributions for
$\MH=140\UGeV$ and  $\MH=500\UGeV$ in gluon fusion production at $7\UTeV$ LHC.  In
the upper plot results in the ET and with full loop dependence
(LOOP) are compared over a large range of $\pT$ values to the default
\pythia\ implementation, which accounts for $2\to2$ matrix element corrections. In the lower plot the low-$\pT$ range is compared to the NNLO+NNLL results as obtained by HqT~\cite{Bozzi:2005wk,deFlorian:2011xf}. Curves normalised to the NNLO total cross sections see Ref.~\cite{Alwall:2011cy}.}
\label{hpt1}
\end{figure}
% --------------------------------------------------------------------
\begin{figure}
\includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/fm/ptj1}
\includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/fm/ptj2}
\caption{Jet $\pT$ distributions for associated jets in gluon fusion
  production of 
$\MH=140\UGeV$ and  $\MH=500\UGeV$ Higgs bosons at 7 TeV LHC.Plots from Ref.~\cite{Alwall:2011cy}.} 
\label{ptj}
\end{figure}
% --------------------------------------------------------------------
% --------------------------------------------------------------------
\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{YRHXS2_NLOMC/figures/fm/mt0pth140}
\end{center}
\caption{$b$-philic Higgs  $\pT$ distribution a the Tevatron and the LHC with $\MH=140\UGeV$.   Results in the ET approximation (red curve) 
and with full loop dependence (green) are shown.  Spectrum of  Higgs produced via $\PQb\PAQb$ fusion in the five flavor scheme is also shown.  All samples are matrix-element matched with up to two partons in the final state.  Curves normalised to the corresponding NNLO total cross sections see Ref.~\cite{Alwall:2011cy}.}
\label{hpt2}
\end{figure}

In Figs.~\ref{hpt1}-\ref{ptj}, the Higgs and jet $\pT$
distributions are shown for standard model Higgs gluon-gluon fusion production at
the 7 TeV LHC with $\MH=140$ and $500\UGeV$. 
Monte-Carlo based results agree well with each other. As expected, loop effects
show a softening of the Higgs $\pT$, but only at quite high $\pT$. 
We also see that the heavier the Higgs, the more important are the loop
effects.  This is expected, since the heavy Higgs boson can probe the internal structure of 
the top-quark loop already at small $\pT$. The jet $\pT$ distributions do confirm the overall
picture and again indicate loop effects to become relevant only for rather high values
of the $\pT$. The agreement with the NNLO+NNLL predictions at small $\pT$ for
both Higgs masses it is quite remarkable. Key distributions, such
as the $\pT$ of the Higgs, do agree remarkably well with the best available
predictions, for example NNLO+NNLL at small Higgs $\pT$, and offer
improved and easy-to-use predictions for other key observables such as the jet 
rates and distributions. In addition, for heavy Higgs masses and/or large $\pT$, 
loop effects, even though marginal  for phenomenology,  can also be taken 
into account in the same approach, if needed. 


In \refF{hpt2},  the $\pT^H$ distributions for gluon-gluon fusion
production at the $7\UTeV$ LHC of a $b$-philic Higgs with $\MH=140\UGeV$ are shown.
For the sake of illustration, we define a  simplified scenario where the Higgs coupling to the top quark is set to zero and study the Higgs and jet distributions relative to a ``large $\tan\beta$" scenario with  bottom-quark loops dominating. Note that for simplicity we keep the same normalisation 
as in the standard model, i.e., $y_b/\sqrt{2} = \Mb/v$ with $\Mb=4.6\UGeV$, as the corresponding cross sections
in enhanced scenarios can be easily obtained by rescaling.  In the $b$-philic Higgs production, the particle running in the loop is nearly massless, and there is no region in $\MH$ or $\pT$ where an effective description is valid. This
also means that a parton-shower generator  alone has no possibility of correctly describing the effects of jet radiation, and genuine loop matrix-elements
plus a matched description are needed for achieving reliable simulations. For a $b$-philic Higgs the largest production cross section does not come  from loop induced gluon fusion, but from tree-level $\PQb\PAQb$ fusion. We have therefore included also this production mechanism in \refF{hpt2}. The corresponding histogram is obtained by  merging tree-level matrix elements 
for  $\PH +0,1,2$ partons  (with a $hb\bar b$ vertex)  in the five flavor scheme to the parton shower.  This provides  a complete  and consistent event 
simulation of inclusive Higgs production in a $b$-philic (or large $\tan \beta$) scenario. 
 
\subsubsection{Finite quark mass effects in the gluon fusion process in POWHEG\footnote{E. Bagnaschi, G. Degrassi, P. Slavich and A. Vicini}}

Ref.~\cite{Bagnaschi:2011tu} presented an upgraded version of
the code that allows to compute the gluon fusion cross section in the SM
and in the MSSM.  
The SM simulation is based on matrix elements evaluated retaining the exact top
and bottom mass dependence at NLO-QCD and NLO-EW.
The MSSM simulation is based on matrix elements in which the exact dependence on
the masses of quarks, squarks and Higgs boson has been
retained in the LO amplitude, in the one-loop diagrams with
real-parton emission and in the two-loop diagrams with quarks and
gluons, whereas the approximation of vanishing Higgs mass has been
employed in the two-loop diagrams involving superpartners.
The leading NLO-EW effects have also been included in the evaluation of the
matrix elements.
%
Results obtained with this new version of {\sc POWHEG} are presented
in the SM and in the MSSM  sections of this Report.

The code provides a complete description of on-shell Higgs production,
including NLO-QCD corrections matched with a QCD parton shower and
NLO-EW corrections.  In the examples discussed in
\Bref{Bagnaschi:2011tu}, the combination {\sc POWHEG + PYTHIA}
has been considered.  In the MSSM case, the relevant parameters of the
MSSM Lagrangian can be passed to the code via a Susy Les Houches
Accord spectrum file.

The code is available from the authors upon request. A release of the
code that includes all the Higgs decay channels is in preparation.

